/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import AsyncStorage from '@react-native-community/async-storage';
import React, { Component } from "react";
import { BackHandler, Image, StatusBar, StyleSheet, ToastAndroid, View, Text } from "react-native";
import SplashScreen from 'react-native-splash-screen';
import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { createBottomTabNavigator } from 'react-navigation-tabs';
//Importing Third Parties
import { Provider } from "react-redux";
import { PersistGate } from 'redux-persist/integration/react';
import Authentication from "./src/components/Authentication";
import BookingList from "./src/components/BookingList";
import BookingSuccess from "./src/components/BookingSuccess";
import Calender from "./src/components/Calender";
import EditProfile from "./src/components/EditProfile";
//import ReviewBooking from "./src/components/Hotel/ReviewBooking/ReviewBooking";
import BookingDetails from './src/components/Flight/BookingDetails';
import FlightDetails from "./src/components/Flight/FlightDetails/FlightDetails";
import FlightList from "./src/components/Flight/FlightList/FlightList";
import FlightSearch from "./src/components/Flight/FlightSearch/FlightSearch";
import PassengerDetails from "./src/components/Flight/PassengerDetails/PassengerDetails";
import SearchAirport from "./src/components/Flight/SearchAirport";
import ForgotPassword from "./src/components/ForgotPassword";
//Importing Classes
import HomeScreen from "./src/components/Home";
import GuestDetails from "./src/components/Hotel/GuestDetails/GuestDetails";
import HotelBookingDetails from "./src/components/Hotel/HotelBookingDetail/HotelBookingDetails";
import HotelList from "./src/components/Hotel/HotelList/HotelList";
import HotelOverview from "./src/components/Hotel/HotelOverview/HotelOverview";
import HotelReview from "./src/components/Hotel/HotelReview/HotelReview";
import HotelSearch from "./src/components/Hotel/HotelSearch/HotelSearch";
import RoomsList from "./src/components/Hotel/RoomList/RoomsList";
import Message from "./src/components/Message";
import PaymentScreen from "./src/components/Payment/PaymentScreen";
import PaypalWebView from "./src/components/Payment/PaypalWebView";
import Profile from "./src/components/Profile";
import Tutorials from "./src/components/Tutorials";
import BookingsHelp from "./src/components/HelpScreens/BookingsHelp";
import { persistor, store } from './src/Redux/Store/Store';
import { colors } from "./src/Utils/Colors";
import Images from "./src/Utils/Images";
import NavigationServices from './src/Utils/NavigationServices';
import MyLoader from './src/components/custom/MyLoader';
import ViewTicket from './src/components/ViewTicket/ViewTicket';
import ChangePassword from "./src/components/ChangePassword";
import Help from './src/components/Help'
import TopDestinationsAndFlights from './src/components/TopDestinationsAndFlights'



// const store = configureStore();


class TabIcon extends Component {
  render() {
    // console.log("props are:=" + JSON.stringify(this.props));
    return (
      <View
        style={[
          styles.iconStyle,
          {
            borderTopWidth: this.props.focused ? 1.5 : 0,
            borderTopColor: this.props.tintColor
          }
        ]}
      >
        <Image source={this.props.imageIcon} style={{ width: 18, height: 18, resizeMode: "contain" }}></Image>
      </View>
    );
  }
}
const ProfileStack = createStackNavigator({
  Profile: {
    screen: Profile
  },
  Authentication: {
    screen: Authentication
  },
  ForgotPassword: {
    screen: ForgotPassword
  },
  ChangePassword: {
    screen: ChangePassword
  }
}, {
  headerMode: 'none',
  navigationOptions: {
    headerVisible: false,
  }
});



const TabNavigator = createBottomTabNavigator({
  Home: {
    screen: HomeScreen,
    // screen: FlightSearch,
    navigationOptions: () => ({
      tabBarIcon: ({ focused, tintColor }) => (
        <TabIcon focused={focused}
          tintColor={tintColor}
          imageIcon={focused ? Images.ic_home_active : Images.ic_home_deactive}>
        </TabIcon>
      )
    })
  },
  "My Trips": {
    screen: BookingList,
    navigationOptions: () => ({
      tabBarIcon: ({ focused, tintColor }) => (
        <TabIcon focused={focused}
          tintColor={tintColor}
          imageIcon={focused ? Images.ic_strip_active : Images.ic_strip_deactive}>
        </TabIcon>
      ),
      // tabBarLabel: <View />
    })
  },
  Accounts: {
    screen: ProfileStack,
    navigationOptions: () => ({
      tabBarIcon: ({ focused, tintColor }) => (
        <View style={{
          position: "relative",
          height: '100%',
          width: '100%',
          justifyContent: "center",
          alignItems: "center"
        }}>
          <View style={{
            position: "relative",
            zIndex: 99,
            top: -15,
            shadowColor: colors.colorShadow,
            shadowOpacity: .5,
            shadowRadius: 5,
            shadowOffset: { height: 4, width: 1 },
            elevation: 5,
            zIndex: 0,
            borderRadius: 27,
          }}>
            <Image source={Images.ic_image_account} style={{
              width: 45,
              height: 45,
              resizeMode: "contain",
            }}></Image>
          </View>
        </View>
      ),
    })
  },
  Wallet: {
    screen: Message,
    navigationOptions: () => ({
      tabBarIcon: ({ focused, tintColor }) => (
        <TabIcon focused={focused}
          tintColor={tintColor}
          imageIcon={focused ? Images.ic_wallet_active : Images.ic_wallet_deactive}>
        </TabIcon>
      ),
    })
  },
  Help: {
    screen: Help,
    navigationOptions: () => ({
      tabBarIcon: ({ focused, tintColor }) => (
        <TabIcon focused={focused}
          tintColor={tintColor}
          imageIcon={focused ? Images.ic_help_active : Images.ic_help_deactive}>
        </TabIcon>
      ),
    })
  }
}, {
//  initialRouteName: "Wallet",
  tabBarOptions: {
    keyboardHidesTabBar: true,
    showLabel: true,
    inactiveTintColor: colors.colorBlack,
    activeTintColor: colors.colorBlue
  },
  lazy: true,
});

const DashboardStack = createStackNavigator({
  HomeTabStack: {
    screen: TabNavigator
  },
  FlightSearch: {
    screen: FlightSearch
  },
  FlightList: {
    // screen: FlightListPrevious1,
    screen: FlightList,
  },
  PassengerDetails: {
    screen: PassengerDetails
  },
  SearchAirport: {
    screen: SearchAirport
  },
  HotelSearch: {
    screen: HotelSearch
  },
  HotelList: {
    screen: HotelList
  },
  HotelOverview: {
    screen: HotelOverview
  },
  RoomsList: {
    screen: RoomsList
  },
  EditProfile: {
    screen: EditProfile
  },
  Calender: {
    screen: Calender
  },
  FlightDetails: {
    screen: FlightDetails
  },
  GuestDetails: {
    screen: GuestDetails
  },
  HotelBookingDetails: {
    screen: HotelBookingDetails
  },
  PaymentScreen: {
    screen: PaymentScreen
  },
  PaypalWebView: {
    screen: PaypalWebView
  },
  HotelReview: {
    screen: HotelReview
  },
  BookingDetails: {
    screen: BookingDetails
  },
  ViewTicket: {
    screen: ViewTicket
  },
  // FlightList: {
  //   screen: FlightList
  // },
  // HotelBookingDetails: {
  //   screen: HotelBookingDetails
  // },
  TopDestinationsAndFlights:{
    screen:TopDestinationsAndFlights
  },

  BookingsHelp: {
    screen: BookingsHelp
  }

}, {
  headerMode: 'none',
  navigationOptions: {
    headerVisible: false,
  }
});

const TutorialsStack = createStackNavigator({
  Tutorials: {
    screen: Tutorials
  },
}, {
  headerMode: 'none',
  navigationOptions: {
    headerVisible: false,
  }
});





class App extends Component {
  constructor(props) {
    super(props)
    this.lastBackButtonPress = null;
    this.state = {
      isTutorial: null
    }
  }


  componentDidMount() {
    AsyncStorage.getItem("isTutorial", (error, res) => {
      if (error) {
        isTutorial = false
      } else if (res) {
        isTutorial = JSON.parse(res)
      }
      else isTutorial = false
      this.setState({
        isTutorial: isTutorial
      }, () => {
        setTimeout(() => {
          // console.log("HIDING SPLASH SCREEN")
          SplashScreen.hide()
        }, 1000);

      })
    })
    this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {

      if (this.lastBackButtonPress + 2000 >= new Date().getTime()) {
        BackHandler.exitApp();
        return true;
      }

      else {
        ToastAndroid.show("Press back again to exit", ToastAndroid.SHORT);
        this.lastBackButtonPress = new Date().getTime();
        return true;
      }


    });
  }
  componentWillUnmount() {
    this.backHandler.remove()
  }

  returnStack() {

  }




  render() {
    if (this.state.isTutorial === null) { return null }
    const SwitchNavigator = createAppContainer(
      createSwitchNavigator({
        DashboardStack: DashboardStack,
        TutorialsStack: TutorialsStack,
        BookingSuccess: BookingSuccess
      }, {
        initialRouteName: this.state.isTutorial ? "DashboardStack" : "TutorialsStack"
      })
    );

    console.disableYellowBox = true;
    return (
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <StatusBar barStyle="light-content" backgroundColor={colors.colorBlue} />
          <SwitchNavigator ref={navigatorRef => {
            NavigationServices.setTopLevelNavigator(navigatorRef);
          }} />
          <MyLoader />
        </PersistGate>
      </Provider>
    );
  }
}

const styles = StyleSheet.create({
  tabBar: {
    borderTopWidth: 0.5,
    borderColor: "gray",
    backgroundColor: "#f5f5f5",
    opacity: 1
  },
  navTitle: {
    color: "white" // changing navbar title color
  },
  iconStyle: {
    // alignItems: "center",
    // alignSelf: "center",
    // justifyContent: "center",
    // height: "90%",
    // width: "50%",
    // marginBottom: 5
    borderWidth: 2, height: '100%',
    width: '100%', borderColor: 'white',
    justifyContent: 'center', alignItems: 'center'
  }
});

export default App