import ActionType from '../Actions/actionType'


const initialState = {
  flightSearchParams: null,
  flightItem: null
}

export const FlightsReducer = (state = initialState, action) => {
  switch (action.type) {
    case ActionType.SAVE_FLIGHT_SEARCH_PARAMS:
      return {
        ...state,
        flightSearchParams: action.payload
      }
    case ActionType.SAVE_FLIGHT_ITEM:
      return {
        ...state,
        flightItem: action.payload
      }
    default:
      return state;
  }
}