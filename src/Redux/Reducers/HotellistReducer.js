import ActionType from '../Actions/actionType'
const initialState = {

  selectedHotelItem: null,
  hotelSearchParam: null,
  selectedRoomData: null
};
export const HotelListreducer = (state = initialState, action) => {
  switch (action.type) {

    // case SAVE_HOTELLIST_DATA:
    //   return {
    //     ...state,
    //     HotellistData: action.payload
    //   };

    case ActionType.SELECTED_HOTEL:
      return {
        ...state,
        selectedHotelItem: action.payload
      }

    case ActionType.Add_HOTEL_SEARCH_PARAMS:
      return {
        ...state,
        hotelSearchParam: action.payload
      }

    case ActionType.SAVE_SELECTED_ROOM_DATA:
      return {
        ...state,
        selectedRoomData: action.payload
      }

    //HotelScreen2
    // case SAVE_SELECTED_HOTEL_PARAMS:  
    // return{
    //        ...state,
    //  }

    //   case DECREMENT:
    //     return {
    //       ...state,
    //       counter: Number(state.counter) - Number(action.payload)
    //     };

    default:
      return state;
  }
};