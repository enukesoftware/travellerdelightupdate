import ActionTypes from '../Actions/actionType'

export const nationalityDataReducer = (state = null, action) => {
  switch (action.type) {
      case ActionTypes.NATIONALITY_DATA:
          return action.payload
      default:
          return state
  }
}