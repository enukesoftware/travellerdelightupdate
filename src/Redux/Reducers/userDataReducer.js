import ActionTypes from '../Actions/actionType'

export const isLoginReducer = (state = false, action) => {
    switch (action.type) {
        case ActionTypes.IS_LOGIN:
            return action.payload
        default:
            return state
    }
}

export const isFacebookLoginReducer = (state = false, action) => {
    switch (action.type) {
        case ActionTypes.IS_FACEBOOK_LOGIN:
            return action.payload
        default:
            return state
    }
}

export const isGoogleLoginReducer = (state = false, action) => {
    switch (action.type) {
        case ActionTypes.IS_GOOGLE_LOGIN:
            return action.payload
        default:
            return state
    }
}

export const userDataReducer = (state = null, action) => {
    switch (action.type) {
        case ActionTypes.USER_DATA:
            return action.payload
        default:
            return state
    }
}