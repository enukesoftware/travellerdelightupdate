import { createStore, combineReducers, applyMiddleware } from "redux";
import { HotelListreducer } from "../Reducers/HotellistReducer";
import { FlightsReducer } from "../Reducers/FlightsReducer";
import { isLoadingReducer,loadingMsgReducer } from "../Reducers/isLoadingReducer"
import createSagaMiddleware from 'redux-saga';
import { rootSaga } from '../Saga/index';
import { userDataReducer, isLoginReducer, isFacebookLoginReducer, isGoogleLoginReducer } from "../Reducers/userDataReducer"
import { agencyIdReducer } from "../Reducers/agencyIdReducer"
import { airportListDataReducer } from "../Reducers/airportListDataReducer"
import { dailyTokenReducer } from "../Reducers/dailyTokenReducer"
import internetReducer from "../Reducers/internetReducer"
import { userTokenReducer } from "../Reducers/userTokenReducer"
import { nationalityDataReducer } from "../Reducers/nationalityDataReducer"
import { persistStore, persistReducer } from 'redux-persist';
import AsyncStorage from '@react-native-community/async-storage';


const sagaMiddleware = createSagaMiddleware();

const persistConfig = {
  // Root
  key: 'root',
  // Storage Method (React Native)
  storage: AsyncStorage,
  // Whitelist (Save Specific Reducers)
  whitelist: [
    "userDataReducer",
    "userTokenReducer",
    "isLoginReducer",
    "FlightsReducer",
    "isFacebookLoginReducer",
    "isGoogleLoginReducer",
    "airportListDataReducer"
  ],
  blacklist: []
};




const rootReducer = combineReducers({
  HotelListreducer: HotelListreducer,
  FlightsReducer: FlightsReducer,
  isLoadingReducer: isLoadingReducer,
  loadingMsgReducer:loadingMsgReducer,
  isLoginReducer: isLoginReducer,
  agencyIdReducer: agencyIdReducer,
  airportListDataReducer: airportListDataReducer,
  dailyTokenReducer: dailyTokenReducer,
  internetReducer: internetReducer,
  userTokenReducer: userTokenReducer,
  userDataReducer: userDataReducer,
  isFacebookLoginReducer: isFacebookLoginReducer,
  isGoogleLoginReducer: isGoogleLoginReducer,
  nationalityDataReducer: nationalityDataReducer,
})


const persistedReducer = persistReducer(persistConfig, rootReducer);

let store = createStore(
  persistedReducer,/* preloadedState, */
  applyMiddleware(sagaMiddleware)
)

// Middleware: Redux Persist Persister
let persistor = persistStore(store);

sagaMiddleware.run(rootSaga);

export { store, persistor }

// export const configureStore = () => {
//     return createStore(rootReducer,/* preloadedState, */

//         window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
//     )
// }