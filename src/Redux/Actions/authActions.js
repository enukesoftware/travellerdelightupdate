import ActionType from './actionType';


export const dailyTokenAction = (dailyToken) =>{
  return{
      type:ActionType.DAILY_TOKEN,
      payload:dailyToken
  };
}

export const agencyIdAction = (agencyId) =>{
  return{
      type:ActionType.AGENCY_ID,
      payload:agencyId
  };
}

export const doLoginAction = (data) => {
  // console.log("setting user token" + JSON.stringify(token));
  return {
    type: ActionType.DO_LOGIN_SAGA,
    payload: data,
  };
}

export const doLogoutAction = () => {
  return {
    type: ActionType.DO_LOGOUT_SAGA,
  };
}

export const doRegisterAction = (data) => {
  return {
    type: ActionType.DO_REGISTER_SAGA,
    payload: data,
  };
}

export const forgotPasswordAction = (data) => {
  return {
    type: ActionType.FORGOT_PASSWORD_SAGA,
    payload: data,
  };
}

export const updateUserProfileAction = (data, props) => {
  return {
    type: ActionType.UPDATE_USER_PROFILE_SAGA,
    payload: data,
    props: props,
  };
}

export const facebookLoginAction = (data) => {
  return {
    type: ActionType.FACEBOOK_LOGIN_SAGA,
    payload: data,
  };
}

export const googleLoginAction = (data) => {
  return {
    type: ActionType.GOOGLE_LOGIN_SAGA,
    payload: data,
  };
}

export const userDataAction = (userData) =>{
  return{
      type:ActionType.USER_DATA,
      payload:userData
  };
}

export const changePasswordAction = (data) =>{
  return{
      type:ActionType.CHANGE_PASSWORD_SAGA,
      payload:data
  };
}

export const isLoginAction = (isLogin) =>{
return{
    type:ActionType.IS_LOGIN,
    payload:isLogin
};
}

export const isFacebookLoginAction = (isLogin) =>{
  return{
      type:ActionType.IS_FACEBOOK_LOGIN,
      payload:isLogin
  };
}

export const isGoogleLoginAction = (isLogin) =>{
  return{
      type:ActionType.IS_GOOGLE_LOGIN,
      payload:isLogin
  };
}

export const userTokenAction = (userToken) =>{
  return{
      type:ActionType.USER_TOKEN,
      payload:userToken
  };
}