import ActionType from './actionType';


export const paypalTokenAction = (payload) => {
  return {
    type: ActionType.PAYPAL_TOKEN_SAGA,
    payload: payload
  };
}

export const paypalUrlAction = (data) => {
  return {
    type: ActionType.PAYPAL_URL_SAGA,
    payload: data

  };
}

export const capturePaymentAction = (data) => {
  return {
    type: ActionType.CAPTURE_PAYMENT_SAGA,
    payload: data

  };
}