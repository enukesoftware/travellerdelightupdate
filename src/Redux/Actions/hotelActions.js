import ActionType from './actionType';


export const searchCityAction = (data) => {
  return {
  type:ActionType.SEARCH_CITY_SAGA,
      payload:data
};
}


export const getHotelTicketListAction = (props) => {
  return {
    type: ActionType.HOTEL_TICKET_LIST,
    payload: props,
  };
}


export const getHotelListAction = (data) => {
  return {
    type: ActionType.HOTEL_LIST,
    payload: data
  };
}

export const getHotelDetailsAction = (data) => {
  return {
    type: ActionType.HOTEL_DETAILS_SAGA,
    payload: data
  };
}

export const getHotelBookingDetailsAction = (data) => {
  return {
    type: ActionType.HOTEL_BOOKING_DETAILS_SAGA,
    payload: data
  };
}

export const bookHotelAction = (data, nav) => {
  return {
    type: ActionType.BOOK_HOTEL_SAGA,
    payload: data,
    nav: nav
  };
}

export const checkRoomBlock = (data) => {
  return {
    type: ActionType.ROOM_BLOCK,
    payload: data,
  };
}

export const selectedHotel = hotelData => {
  return {
    type: ActionType.SELECTED_HOTEL,
    payload: hotelData
  }
}

export const saveHotelSearchParams = params => {
  return {
    type: ActionType.Add_HOTEL_SEARCH_PARAMS,
    payload: params
  }
}

export const saveSelectedRoomData = roomData => {
  return {
    type: ActionType.SAVE_SELECTED_ROOM_DATA,
    payload: roomData
  }
}

export const cancelHotelBookingAction = (data) => {
  return {
    type: ActionType.CANCEL_HOTEL_BOOKING_SAGA,
    payload: data,
  };
}

export const cancelHotelBookingStatusAction = (data) => {
  return {
    type: ActionType.CANCEL_HOTEL_BOOKING_STATUS_SAGA,
    payload: data,
  };
}
