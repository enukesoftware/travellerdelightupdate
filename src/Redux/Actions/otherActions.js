import ActionType from './actionType';
import { store } from '../Store/Store';

export const setLoadingAction = item => {
  if(item==false){
    store.dispatch(setLoadingMsg("Loading"))
  }
  return{
      type:ActionType.IS_LOADING,
      payload:item
  }
}

export const setLoadingMsg = item =>{
  return {
    type:ActionType.LOADING_MSG,
    payload:item
  }
}


export const getPageSettingAction = (data) => {
  return {
    type: ActionType.PAGE_SETTING_SAGA,
  };
}

export const getNationalityAction = (data) => {
  return {
    type: ActionType.NATIONALITY_SAGA,
  };
}

export const nationalityDataAction = (data) => {
  return {
    type: ActionType.NATIONALITY_DATA,
    payload:data
  };
}

export const internetConnected = () =>{
  console.log('ACTION_CONNECTED')
  return{
      type:ActionType.INTERNET_CONNECTED
  };
}

export const internetDisconnected = () =>{
  console.log('ACTION_DISCONNECTED')
  return{
      type:ActionType.INTERNET_DISCONNECTED
  };
}