import { DeviceEventEmitter } from 'react-native';
import { call, put, takeLatest } from 'redux-saga/effects';
import { capturePaymentApi } from '../../Utils/APIManager/ApiProvider';
import { getApprovalUrlApi, getPaypalTokenApi } from "../../Utils/APIManager/PaypalApiProvider";
import NavigationServices from '../../Utils/NavigationServices';
import StringConstants from '../../Utils/StringConstants';
import ActionType from '../Actions/actionType';
import { setLoadingAction, setLoadingMsg } from '../Actions';

function* getPaypalToken({ type, payload }) {

  yield put(setLoadingAction(true));
  yield put(setLoadingMsg("We are connecting you to the Merchant.\nPlease wait..."));
  try {
    let response = yield call(getPaypalTokenApi);
    console.log('TOKEN_RES:' + JSON.stringify(response));

    if (response && response.access_token) {
      yield put(setLoadingAction(false));

      let accessToken = response.access_token;
      DeviceEventEmitter.emit(StringConstants.PAYPAL_TOKEN, accessToken)

    } else {
      console.log("Error getting Token" + JSON.stringify(response))
      yield put(setLoadingAction(false));
      NavigationServices.goBack()
      setTimeout(() => { alert("Error getting Token") }, 1000)
    }

  }
  catch (error) {
    NavigationServices.goBack()
    console.log(error);
    // Dispatch Action To Redux Store
    yield put(setLoadingAction(false));
    if (error.message) {
      setTimeout(() => {
        alert(error.message);
      }, 100);
    } else if (error.code && error.code.description) {
      setTimeout(() => {
        alert(error.code.description);
      }, 100);
    } else if (error) {
      setTimeout(() => {
        alert(JSON.stringify(error));
      }, 100);
    }
  }
};

function* getPaypalUrl({ type, payload }) {
  console.log("responseGetApprovalUrlApi:" + JSON.stringify(payload))
  try {
    let response = yield call(getApprovalUrlApi, payload);
    yield put(setLoadingAction(true));

    //console.log("responseGetApprovalUrlApi:" + JSON.stringify(response))

    if (response && response.id && response.links) {

      console.log("URlS" + JSON.stringify(response))
      yield put(setLoadingAction(false));

      const { id, links } = response
      const approvalUrl = links.find(response => response.rel == "approval_url")
      console.log("URL:" + JSON.stringify(approvalUrl))
      DeviceEventEmitter.emit(StringConstants.PAYPAL_URL, {
        paymentId: id,
        approvalUrl: approvalUrl.href,
        accessToken: payload.accessToken,
      })
    } else {
      NavigationServices.goBack()
      yield put(setLoadingAction(false));
      setTimeout(() => { alert("Error getting Approval") }, 1000)
    }

  }
  catch (error) {
    console.log(error);
    NavigationServices.goBack()
    // Dispatch Action To Redux Store
    yield put(setLoadingAction(false));
    if (error.message) {
      setTimeout(() => {
        alert(error.message);
      }, 100);
    } else if (error.code && error.code.description) {
      setTimeout(() => {
        alert(error.code.description);
      }, 100);
    } else if (error) {
      setTimeout(() => {
        alert(JSON.stringify(error));
      }, 100);
    }
  }
};

function* capturePayment({ type, payload }) {
  console.log("capturePaymentPayload:" + JSON.stringify(payload))
  try {
    let response = yield call(capturePaymentApi, payload);
    yield put(setLoadingAction(true));

    console.log("captureResponse" + JSON.stringify(response))
    if (response ) {
      if (response.success) {
        yield put(setLoadingAction(false));
        DeviceEventEmitter.emit(StringConstants.CAPTURE_PAYMENT_EVENT, true)
      } else {
        NavigationServices.goBack();
        if (response.message) {
          setTimeout(() => { alert(response.data.message) }, 1000)
        }
        else {
          setTimeout(() => { alert("Error Capturing Payment") }, 1000)
        }

      }
    } else {
      NavigationServices.goBack();
      yield put(setLoadingAction(false));
      setTimeout(() => { alert("Error Capturing Payment") }, 1000)
    }

  }
  catch (error) {
    console.log(error);
    NavigationServices.goBack()
    // Dispatch Action To Redux Store
    yield put(setLoadingAction(false));
    if (error.message) {
      setTimeout(() => {
        alert(error.message);
      }, 100);
    } else if (error.code && error.code.description) {
      setTimeout(() => {
        alert(error.code.description);
      }, 100);
    } else if (error) {
      setTimeout(() => {
        alert(JSON.stringify(error));
      }, 100);
    }
  }
};



// Watcher: watch auth request
export default function* watchPaypal() {
  // Take Last Action Only
  yield takeLatest(ActionType.PAYPAL_TOKEN_SAGA, getPaypalToken);
  yield takeLatest(ActionType.PAYPAL_URL_SAGA, getPaypalUrl);
  yield takeLatest(ActionType.CAPTURE_PAYMENT_SAGA, capturePayment)
};