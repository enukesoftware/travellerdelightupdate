import { Alert, DeviceEventEmitter } from 'react-native';
import { call, put, takeLatest } from 'redux-saga/effects';
import { bookFlightApi, confirmQuoteApi, getAirPortList, getFlightBookingDetailApi, getFlightList, getFlightRuleApi, getFlightsBookedListApi } from "../../Utils/APIManager/ApiProvider";
import NavigationServices from '../../Utils/NavigationServices';
import StringConstants from '../../Utils/StringConstants';
import ActionType from '../Actions/actionType';
import { setLoadingAction, setLoadingMsg } from '../Actions';
function* getAirportList() {
  console.log("AIRPORT_REQ:")
  // yield put(setLoadingAction(true));
  try {
    let response = yield call(getAirPortList);
    console.log("AIRPORT_RES:" + JSON.stringify(response))

    if (response && response.code && response.code.code == 200) {
      let data = response.data;
      yield put({ type: ActionType.AIRPORT_LIST_DATA, payload: data });
      // yield put(setLoadingAction(false));

      //this event is only for SearchAirport page
      DeviceEventEmitter.emit(StringConstants.SEARCH_AIRPORT_EVENT, data)

    } else {
      // yield put(setLoadingAction(false));
      if (response && response.message) {
        setTimeout(() => {
          alert(response.message);
        }, 100);
      }
    }
  }
  catch (error) {
    console.log(error);
    // Dispatch Action To Redux Store
    // yield put(setLoadingAction(false));
    if (error.message) {
      setTimeout(() => {
        alert(error.message);
      }, 100);
    } else if (error.code && error.code.description) {
      setTimeout(() => {
        alert(error.code.description);
      }, 100);
    }
  }
}

function* getFlightRule({ type, payload }) {
  yield put(setLoadingAction(true));
  yield put(setLoadingMsg("Fetching Cancellation Policy"));
  const param = { type: payload.type, flight: payload.flight, traceId: payload.traceId, }
  try {
    let response = yield call(getFlightRuleApi, param);
    console.log("AIRPORT_RES:" + JSON.stringify(response))

    if (response && response.success && response.data) {
      yield put(setLoadingAction(false));
      const data = { response: response.data, sectionIndex: payload.sectionIndex, index: payload.index }
      DeviceEventEmitter.emit(StringConstants.FLIGHT_RULE_EVENT, data)
    } else {
      DeviceEventEmitter.emit(StringConstants.FLIGHT_RULE_EVENT, null)
      yield put(setLoadingAction(false));
      if (response && response.message) {
        setTimeout(() => {
          alert(response.message);
        }, 100);
      }
    }
  }
  catch (error) {
    console.log(error);
    // Dispatch Action To Redux Store
    DeviceEventEmitter.emit(StringConstants.FLIGHT_RULE_EVENT, null)
    yield put(setLoadingAction(false));
    if (error.message) {
      setTimeout(() => {
        alert(error.message);
      }, 100);
    } else if (error.code && error.code.description) {
      setTimeout(() => {
        alert(error.code.description);
      }, 100);
    }
  }
}

function* bookFlight({ type, payload }) {
  yield put(setLoadingAction(true));
  try {
    // console.clear();
    console.log("data is:=" + JSON.stringify(payload));
    let response = yield call(bookFlightApi, payload);

    if (response && response.success && response.data && response.data.booking_id) {
      console.log("BOOK_FLIGHT_RES:" + JSON.stringify(response))

      yield put(setLoadingAction(false));
      let params = {
        data: {
          bookingId: response.data.booking_id,
          transactionId: payload.orderID
        },
        isHotel: false
      }
      setTimeout(() => {
        NavigationServices.navigate('BookingSuccess', params)
      }, 100);
    } else {
      console.log("failed data is:=" + JSON.stringify(response));
      yield put(setLoadingAction(false));
      if (response && response.message) {
        setTimeout(() => {
          Alert.alert(
            "Failed",
            response.message,
            [{
              text: "Ok", onPress: () => {
                NavigationServices.goBack();
                // DeviceEventEmitter.emit(StringConstants.SESSION_EXPIRE_EVENT) 
              }
            }],
            { cancelable: false }
          )
          // alert(response.message);
        }, 100);
      }
    }
  }
  catch (error) {
    console.log(error);
    // Dispatch Action To Redux Store
    yield put(setLoadingAction(false));
    if (error.message) {
      setTimeout(() => {
        alert(error.message);
      }, 100);
    } else if (error.code && error.code.description) {
      setTimeout(() => {
        alert(error.code.description);
      }, 100);
    }
  }
}

function* confirmQuote({ type, payload }) {

  console.log("Payload:" + JSON.stringify(payload))
  yield put(setLoadingMsg("Confirming Fare.\nPlease wait…"));
  yield put(setLoadingAction(true));
  try {
    let response = yield call(confirmQuoteApi, payload);
    if (response && response.success && response.data) {
      DeviceEventEmitter.emit(StringConstants.CONFIRM_QUOTE_EVENT, { data: response.data, success: true })
      // console.log("QUOTE_RES:" + JSON.stringify(response))
      yield put(setLoadingAction(false));
    } else if (response.message) {
      yield put(setLoadingAction(false));
      DeviceEventEmitter.emit(StringConstants.CONFIRM_QUOTE_EVENT, { success: false, message: response.message })
    }
    else {
      DeviceEventEmitter.emit(StringConstants.CONFIRM_QUOTE_EVENT, { success: false, message: null })
    }
  }
  catch (error) {
    console.log("error:" + JSON.stringify(error));
    DeviceEventEmitter.emit(StringConstants.CONFIRM_QUOTE_EVENT, { success: false, message: null })
    // Dispatch Action To Redux Store
    yield put(setLoadingAction(false));
    // if (error.message) {
    //   setTimeout(() => {
    //     alert(error.message);
    //   }, 100);
    // } else if (error.code && error.code.description) {
    //   setTimeout(() => {
    //     alert(error.code.description);
    //   }, 100);
    // }
  }
}

function* searchFlight({ type, payload }) {

  console.log("FLIGHT_LIST_payload" + JSON.stringify(payload))
 
  yield put(setLoadingAction(true));
  yield put(setLoadingMsg(payload.reload?"Updating flight prices.\nPlease wait":"We are searching for flights.\nPlease wait…"))
  try {
    let response = yield call(getFlightList, payload);
    console.log("FLIGHT_LIST_RESP:" + JSON.stringify(response))

    yield put(setLoadingAction(false));
    if (response && response.success && response.success == true) {
      if (response.data && response.data.others && response.data.others.traceId) {
        DeviceEventEmitter.emit(StringConstants.FLIGH_LIST_EVENT, response.data)
      }
    }
    else if (response && response.success == false && response.message) {
      console.log("FLIGHT_LIST_RESP4:", JSON.stringify(response))
      setTimeout(() => {
        alert(response.message)
      }, 100);
      NavigationServices.goBack()
    }
  }
  catch (error) {
    NavigationServices.goBack()
    console.log("Catch Error:", JSON.stringify(error));
    // Dispatch Action To Redux Store
    yield put(setLoadingAction(false));
    if (error.message) {
      setTimeout(() => {
        alert(error.message);
      }, 100);
    } else if (error.code && error.code.description) {
      setTimeout(() => {
        alert(error.code.description);
      }, 100);
    }
  }
};

function* getFlightBookedList() {
  yield put(setLoadingAction(true));
  try {
    let response = yield call(getFlightsBookedListApi);
    if (response && response.success && response.data) {
      // yield put(setLoadingAction(false));
      DeviceEventEmitter.emit(StringConstants.FLIGHT_BOOKED_LIST_EVENT, response.data)
      DeviceEventEmitter.emit(StringConstants.FLIGHT_BOOKED_LIST_EVENT_FLAG)
    } else if (response && response.code && response.code.code && response.code.code == 500) {
      // yield put(setLoadingAction(false));
      DeviceEventEmitter.emit(StringConstants.FLIGHT_BOOKED_LIST_EVENT_FLAG)
      setTimeout(() => {
        Alert.alert(
          "Alert",
          "Please Login Again",
          [{ text: "Ok", onPress: () => { DeviceEventEmitter.emit(StringConstants.SESSION_EXPIRE_EVENT) } }],
          { cancelable: false }
        )
      }, 100)
    } else if (response && response.message) {
      // yield put(setLoadingAction(false));
      DeviceEventEmitter.emit(StringConstants.FLIGHT_BOOKED_LIST_EVENT_FLAG)
      setTimeout(() => {
        alert(response.message)
      }, 100);

    }
  }
  catch (error) {
    console.log(error);
    // Dispatch Action To Redux Store
    DeviceEventEmitter.emit(StringConstants.FLIGHT_BOOKED_LIST_EVENT_FLAG)
    // yield put(setLoadingAction(false));
    if (error.message) {
      setTimeout(() => {
        alert(error.message);
      }, 100);
    } else if (error.code && error.code.description) {
      setTimeout(() => {
        alert(error.code.description);
      }, 100);
    }
  }
}

function* getFlightBookingDetails({ type, payload }) {
  yield put(setLoadingAction(true));
  try {
    let response = yield call(getFlightBookingDetailApi, payload);
    // console.log("FLIGHT_DETAIL_RESP",JSON.stringify(response))
    yield put(setLoadingAction(false));
    if (response && response.success && response.data && response.data.res_json) {
      DeviceEventEmitter.emit(StringConstants.FLIGHT_BOOKING_DETAILS_EVENT, response.data)
    }
    else if (response && response.message) {
      setTimeout(() => {
        alert(error.message);
      }, 100);
    }
  }
  catch (error) {
    console.log(error);
    // Dispatch Action To Redux Store
    yield put(setLoadingAction(false));
    if (error.message) {
      setTimeout(() => {
        alert(error.message);
      }, 100);
    } else if (error.code && error.code.description) {
      setTimeout(() => {
        alert(error.code.description);
      }, 100);
    }
  }
}

// Watcher: watch flight request
export default function* watchFlight() {
  // Take Last Action Only
  yield takeLatest(ActionType.AIRPORT_LIST_SAGA, getAirportList);
  yield takeLatest(ActionType.FLIGHT_RULE, getFlightRule);
  yield takeLatest(ActionType.BOOK_FLIGHT_SAGA, bookFlight);
  yield takeLatest(ActionType.CONFIRM_QUOTE_SAGA, confirmQuote);
  yield takeLatest(ActionType.SEARCH_FLIGHT_SAGA, searchFlight);
  yield takeLatest(ActionType.FLIGHT_BOOKED_LIST_SAGA, getFlightBookedList);
  yield takeLatest(ActionType.FLIGHT_BOOKING_DETAILS_SAGA, getFlightBookingDetails);

};