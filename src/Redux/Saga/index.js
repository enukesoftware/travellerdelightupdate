import { all, fork, spawn } from 'redux-saga/effects';
import watchAuth from "./authSaga";
import watchFlight from "./flightSaga";
import watchHotel from "./hotelSaga";
import watchCity from "./citySaga";
import watchPaypal from './paypalSaga'
import watchNationality from './nationalitySaga'

export function* rootSaga() {
  yield all([
    fork(watchAuth),
    fork(watchFlight),
    fork(watchHotel),
    fork(watchCity),
    fork(watchPaypal),
    fork(watchNationality),
  ]);
}