import { Alert, DeviceEventEmitter } from 'react-native';
import { call, put, select, takeLatest } from 'redux-saga/effects';
import { facebookLoginApi, forgotPasswordApi, googleLoginApi, loginUser, logoutUser, registerUserApi, updateUserProfileApi, changePasswordApi } from "../../Utils/APIManager/ApiProvider";
import { fbLogout, googleLogout } from '../../Utils/APIManager/SocialAuthentication';
import NavigationServices from '../../Utils/NavigationServices';
import StringConstants from '../../Utils/StringConstants';
import ActionType from '../Actions/actionType';
import { setLoadingAction, setLoadingMsg, changePasswordAction } from '../Actions';
import { MyAlert, toUpperEveryWord } from '../../Utils/Utility';

function* doLogin({ type, payload, }) {
  console.log("email is:-" + payload.email);
  console.log("password is:-" + payload.password);
  let { email, password, loginFrom } = payload;
  yield put(setLoadingAction(true));
  yield put(setLoadingMsg("Validating Your Credentials.\nPlease wait…"));
  try {
    let response = yield call(loginUser, { email, password });
    console.log("Login Response:", JSON.stringify(response))

    if (response && response.success && response != null && response.data != null && response.data.token != null && response.data.user != null) {
      console.log("Login Success")
      yield put({ type: ActionType.IS_LOGIN, payload: true });
      yield put({ type: ActionType.USER_TOKEN, payload: response.data.token });
      yield put({ type: ActionType.USER_DATA, payload: response.data.user });
      yield put(setLoadingAction(false));
      console.log("dataUser:", JSON.stringify(response.data))
      DeviceEventEmitter.emit(StringConstants.IS_LOGINEVENT, true)
      if (loginFrom === "Auth")
        setTimeout(() => {
          NavigationServices.replace("Profile")
          NavigationServices.navigate("Home")
        }, 100);

    }
    else if (response && !response.success) {

      let title = "Error"
      if (response.error) {
        title = toUpperEveryWord(JSON.parse(response.error))
      }
      if (response.msg) {
        setTimeout(() => { MyAlert(title, response.msg) }, 200);
        yield put(setLoadingAction(false));
      } else
        yield put(setLoadingAction(false));

    }

    else {
      console.log("Data not found" + JSON.stringify(response))
      yield put(setLoadingAction(false));
      setTimeout(() => { alert("User data not found") }, 1000)
    }
    // console.log("login failed" + JSON.stringify(response));
    //setTimeout(() => { alert("Login failed " + response.code.description) }, 1000);
  }

  catch (error) {
    console.log("Catch Error", error);
    // Dispatch Action To Redux Store
    yield put(setLoadingAction(false));
    if (error.message) {
      setTimeout(() => {
        alert(error.message);
      }, 100);
    } else if (error.code && error.code.description) {
      setTimeout(() => {
        alert(error.code.description);
      }, 100);
    }
  }
};

function* doLogout() {
  yield put(setLoadingAction(true));
  yield put(setLoadingMsg("Validating your credentials"));
  let isGoogleLogin = false, isFbLogin = false;
  yield select(({ isGoogleLoginReducer, isFacebookLoginReducer }) => {
    isGoogleLogin = isGoogleLoginReducer;
    isFbLogin = isFacebookLoginReducer;
  });
  if (isFbLogin) {
    yield call(fbLogout)
  }
  else
    if (isGoogleLogin) {
      if (yield call(googleLogout) === false) {
        yield put({ type: ActionType.IS_GOOGLE_LOGIN, payload: false });
      }
    }
  try {
    //let response = yield call(logoutUser);
    console.log("LOGOUT RESP:" + JSON.stringify(response));
    let response = true

    if (response) {
      yield put({ type: ActionType.IS_LOGIN, payload: false });
      yield put({ type: ActionType.IS_FACEBOOK_LOGIN, payload: false });
      yield put({ type: ActionType.USER_TOKEN, payload: null });
      yield put({ type: ActionType.USER_DATA, payload: null });
      yield put(setLoadingAction(false));
      NavigationServices.replace("Authentication")
      // DeviceEventEmitter.emit(StringConstants.IS_LOGOUTEVENT, true)
    } else if (response && !response.success) {
      console.log("login failed" + JSON.stringify(response));
      yield put(setLoadingAction(false));
      // DeviceEventEmitter.emit(StringConstants.IS_LOGOUTEVENT, false)
      let title = "Error"
      if (response.error) {
        title = JSON.parse(response.error)
      }
      if (response.msg) {
        setTimeout(() => { MyAlert(title, response.msg) }, 1000);
      }
    }
  }
  catch (error) {
    console.log(error);
    // Dispatch Action To Redux Store
    //  DeviceEventEmitter.emit(StringConstants.IS_LOGOUTEVENT, false)
    yield put(setLoadingAction(false));
    if (error.message) {
      setTimeout(() => {
        alert(error.message);
      }, 100);
    } else if (error.code && error.code.description) {
      setTimeout(() => {
        alert(error.code.description);
      }, 100);
    }
  }
}

function* doRegisterUser({ type, payload }) {
  console.log("REGISTER_REQ:" + JSON.stringify(payload))
  yield put(setLoadingAction(true));
  yield put(setLoadingMsg("Creating Your Account.\nPlease wait…"));

  try {
    let response = yield call(registerUserApi, payload);
    console.log("REGISTER_RES:" + JSON.stringify(response))

    // getErrorFromJson(error,isCatchSection)
    yield put(setLoadingAction(false));

    var msg = 'Verification link has been sent to your mail please verify email to continue login'
    if (response && response.data && response.data.message && response.data.message == 'VERIFICATION_MAIL_SENT') {
      setTimeout(() => {
        alert(msg);
      }, 100);
      DeviceEventEmitter.emit(StringConstants.REGISTER_EVENT, response.data)
    }

    else if (response && !response.success) {
      let title = "Error"
      let error = null
      if (response.msg) {
        title = response.msg
      }
      if (response.error) {
        let errorObj = JSON.parse(response.error)
        if (errorObj.email && errorObj.email.length > 0) {
          error = errorObj.email[0]
        }
        if (errorObj.password && errorObj.password.length > 0) {
          error = errorObj.password[0]
        }
      }
      if (error)
        setTimeout(() => {
          MyAlert(title, error)
        }, 200);
    }
  }
  catch (error) {
    console.log(error);
    // Dispatch Action To Redux Store
    yield put(setLoadingAction(false));
    if (error.message) {
      setTimeout(() => {
        alert(error.message);
      }, 100);
    } else if (error.code && error.code.description) {
      setTimeout(() => {
        alert(error.code.description);
      }, 100);
    }

  }
}

function* forgotPassword({ type, payload }) {
  console.log("FORGOT_PASSWORD_REQ:" + JSON.stringify(payload))
  yield put(setLoadingAction(true));
  try {
    let response = yield call(forgotPasswordApi, payload);

    // getErrorFromJson(error,isCatchSection)
    yield put({ type: ActionType.IS_LOADING, payload: false });
    if (response && response.data && response.data === "RESET_LINK_SENT") {
      let msg = "Reset password link has been sent to your email"
      setTimeout(() => {
        Alert.alert('Alert', msg, [
          { text: 'OK', onPress: () => NavigationServices.replace('Authentication') }
        ])
      }, 100)

    }
    else if (response && !response.success) {
      let title = "Error"
      if (response.msg) {
        setTimeout(() => {
          MyAlert(title, response.msg)
        }, 200);
      }
    }
  }
  catch (error) {
    console.log(error);
    // Dispatch Action To Redux Store
    yield put(setLoadingAction(false));
    if (error.message) {
      setTimeout(() => {
        alert(error.message);
      }, 100);
    } else if (error.code && error.code.description) {
      setTimeout(() => {
        alert(error.code.description);
      }, 100);
    }

  }
}

function* updateUserProfile({ type, payload }) {
  yield put(setLoadingAction(true));
  try {
    let response = yield call(updateUserProfileApi, payload);
    console.log('Update_RES  ' + JSON.stringify(response))
    // getErrorFromJson(error,isCatchSection)
    yield put(setLoadingAction(false));
    if (response && response.data) {

      yield put({ type: ActionType.USER_DATA, payload: response.data });

      console.log('Update API Response  ' + JSON.stringify(response))
      setTimeout(() => {
        Alert.alert('Alert', 'Profile updated successfully.', [{
          text: 'Ok', onPress: () => {
            //props.updateSuccess()
            NavigationServices.goBack()
          }
        }])
      }, 100);
    }
    else if (response && !response.success) {

      let title = "Error"
      // if (response.error) {
      //   title = JSON.parse(response.error)
      // }
      if (response.msg) {
        setTimeout(() => { MyAlert(title, response.msg) }, 200);
        yield put(setLoadingAction(false));
      } else
        yield put(setLoadingAction(false));

    }
  }
  catch (error) {
    console.log(error);
    // Dispatch Action To Redux Store
    yield put(setLoadingAction(false));
    if (error.message) {
      setTimeout(() => {
        Alert.alert('Alert', error.message, [{
          text: 'Ok', onPress: () => {
            NavigationServices.goBack();
          }
        }])
      }, 100);
    } else if (error.code && error.code.description) {
      setTimeout(() => {
        Alert.alert('Alert', error.code.description, [{
          text: 'Ok', onPress: () => {
            NavigationServices.goBack();
          }
        }])
      }, 100);
    }

  }
}

function* googleLogin({ type, payload }) {
  yield put(setLoadingAction(true));
  yield put(setLoadingMsg("Validating Your Credentials.\nPlease wait…"));

  try {
    let response = yield call(googleLoginApi, payload);
    if (response && response != null && response.data != null && response.data.token != null && response.data.user != null) {
      console.log("Login Success")
      yield put({ type: ActionType.IS_LOGIN, payload: true });
      yield put({ type: ActionType.USER_TOKEN, payload: response.data.token });
      yield put({ type: ActionType.USER_DATA, payload: response.data.user });
      yield put(setLoadingAction(false));
      yield put({ type: ActionType.IS_GOOGLE_LOGIN, payload: true });
      console.log("dataUser:", JSON.stringify(response.data))
      DeviceEventEmitter.emit(StringConstants.IS_LOGINEVENT, true)

      NavigationServices.replace("Profile")
    }
    else if (response && !response.success) {

      let title = "Error"
      // if (response.error) {
      //   title = JSON.parse(response.error)
      // }
      if (response.msg) {
        setTimeout(() => { MyAlert(title, response.msg) }, 200);
        yield put(setLoadingAction(false));
      } else
        yield put(setLoadingAction(false));

    }

    else {
      console.log("Data not found" + JSON.stringify(response))
      yield put(setLoadingAction(false));
      setTimeout(() => { alert("User data not found") }, 1000)
    }

  }
  catch (error) {
    console.log(error);
    // Dispatch Action To Redux Store
    yield put(setLoadingAction(false));
    if (error.message) {
      setTimeout(() => {
        alert(error.message);
      }, 100);
    } else if (error.code && error.code.description) {
      setTimeout(() => {
        alert(error.code.description);
      }, 100);
    }
  }
};

function* changePassword({ type, payload }) {
  yield put(setLoadingAction(true));
  yield put(setLoadingMsg("Validating Your Credentials.\nPlease wait…"));
  try {
    let response = yield call(changePasswordApi, payload);
    console.log("CHANGE_PASSWORD_RESP:", JSON.stringify(response))
    if (response && response.success) {
      yield put(setLoadingAction(false));
      setTimeout(() => {
        MyAlert("Success", "Password Changed Successfully")
      }, 200);
      NavigationServices.goBack()
    } else if (response.msg) {
      yield put(setLoadingAction(false));
      setTimeout(() => {
        MyAlert("ERROR", response.msg)
      }, 200);
    }
    else {
      yield put(setLoadingAction(false));
      setTimeout(() => {

        MyAlert("ERROR", "Unknown Error Occurred")
      }, 200);
    }
  }
  catch (e) {
    console.log(error);
    // Dispatch Action To Redux Store
    yield put(setLoadingAction(false));
    if (error.message) {
      setTimeout(() => {
        alert(error.message);
      }, 100);
    } else if (error.code && error.code.description) {
      setTimeout(() => {
        alert(error.code.description);
      }, 100);
    }
  }

}

function* facebookLogin({ type, payload }) {
  yield put(setLoadingAction(true));
  try {
    let response = yield call(facebookLoginApi, payload);
    if (response.code.code == 200) {
      if (response != null && response.data != null && response.data.token != null && response.data.user != null) {
        console.log("Login Success")
        yield put({ type: ActionType.IS_LOGIN, payload: true });
        yield put({ type: ActionType.USER_TOKEN, payload: response.data.token });
        yield put({ type: ActionType.USER_DATA, payload: response.data.user });
        yield put(setLoadingAction(false));
        yield put({ type: ActionType.IS_FACEBOOK_LOGIN, payload: true });
        console.log("dataUser:", JSON.stringify(response.data))
        DeviceEventEmitter.emit(StringConstants.IS_LOGINEVENT, true)

        NavigationServices.replace("Profile")
      } else {
        console.log("Data not found" + JSON.stringify(response))
        yield put(setLoadingAction(false));
        setTimeout(() => { alert("User data not found") }, 1000)
      }
    } else {
      console.log("login failed" + JSON.stringify(response));
      setTimeout(() => { alert("Login failed " + response.code.description) }, 1000);
    }
  }
  catch (error) {
    console.log(error);
    // Dispatch Action To Redux Store
    yield put(setLoadingAction(false));
    if (error.message) {
      setTimeout(() => {
        alert(error.message);
      }, 100);
    } else if (error.code && error.code.description) {
      setTimeout(() => {
        alert(error.code.description);
      }, 100);
    }
  }
};

// Watcher: watch auth request
export default function* watchAuth() {
  // Take Last Action Only
  yield takeLatest(ActionType.DO_LOGIN_SAGA, doLogin);
  yield takeLatest(ActionType.DO_LOGOUT_SAGA, doLogout);
  yield takeLatest(ActionType.DO_REGISTER_SAGA, doRegisterUser);
  yield takeLatest(ActionType.FORGOT_PASSWORD_SAGA, forgotPassword);
  yield takeLatest(ActionType.UPDATE_USER_PROFILE_SAGA, updateUserProfile);
  yield takeLatest(ActionType.FACEBOOK_LOGIN_SAGA, facebookLogin);
  yield takeLatest(ActionType.GOOGLE_LOGIN_SAGA, googleLogin);
  yield takeLatest(ActionType.CHANGE_PASSWORD_SAGA, changePassword);
};