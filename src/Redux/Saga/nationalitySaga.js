import { DeviceEventEmitter } from 'react-native';
import { call, put, takeLatest } from 'redux-saga/effects';
import { getNationalityApi, getPageSettingApi } from "../../Utils/APIManager/ApiProvider";
import StringConstants from '../../Utils/StringConstants';
import ActionType from '../Actions/actionType';
import { setLoadingAction } from '../Actions';

function* getNationality() {
  // yield put(setLoadingAction(true));
  try {
    let response = yield call(getNationalityApi);
    if (response) {
      // yield put(setLoadingAction(false));
      if (response.result) {
        yield put({ type: ActionType.NATIONALITY_DATA, payload: response.result });
        DeviceEventEmitter.emit(StringConstants.NATIONALITY_DATA_EVENT, response.result)
      }
    } else {
      // yield put(setLoadingAction(false));
      if (response && response.message) {
        setTimeout(() => {
          // alert(response.message);
        }, 100);
      }
    }
  }
  catch (error) {
    console.log(error);
    // Dispatch Action To Redux Store
    // yield put(setLoadingAction(false));
  }
};
function* getPageSetting() {
  // yield put(setLoadingAction(true));
  try {
    let response = yield call(getPageSettingApi);
    if (response) {
      // yield put(setLoadingAction(false));
      if (response.data) {
        //yield put({ type: ActionType.NATIONALITY_DATA, payload: response.result });
        DeviceEventEmitter.emit(StringConstants.PAGE_SETTING_EVENT, response.data)
      }
    } else {
      // yield put(setLoadingAction(false));
      if (response && response.message) {
        setTimeout(() => {
          // alert(response.message);
        }, 100);
      }
    }
  }
  catch (error) {
    console.log(error);
    // Dispatch Action To Redux Store
    // yield put(setLoadingAction(false));
  }
};


// Watcher: watch nationality request
export default function* watchNationality() {
  // Take Last Action Only
  yield takeLatest(ActionType.NATIONALITY_SAGA, getNationality);
  yield takeLatest(ActionType.PAGE_SETTING_SAGA, getPageSetting);
};