import { Alert, DeviceEventEmitter } from 'react-native';
import { call, put, takeLatest } from 'redux-saga/effects';
import { blockRoomApi, bookHotelApi, cancelHotelBookingApi, getHotelBookedListApi, getHotelBookingDetailApi, getHotelDetailsApi, getHotelListApi } from "../../Utils/APIManager/ApiProvider";
import NavigationServices from '../../Utils/NavigationServices';
import StringConstants from '../../Utils/StringConstants';
import ActionType from '../Actions/actionType';
import { setLoadingAction, setLoadingMsg } from '../Actions';

function* getHotelBookedList(props) {
  console.log("HOTEL_BOOK_LIST_PROPS:" + JSON.stringify(props))
  yield put(setLoadingAction(props.payload));
  if(props.payload)  yield put(setLoadingMsg("Fetching Bookings"));
  try {
    let response = yield call(getHotelBookedListApi);
    console.log("HOTEL_BOOK_LIST_RES:" + JSON.stringify(response))
    // getErrorFromJson(error,isCatchSection)

    if (response && response.success) {

      // yield put(setLoadingAction(false));
      DeviceEventEmitter.emit(StringConstants.HOTEL_BOOKING_LIST_EVENT, response.data)
      DeviceEventEmitter.emit(StringConstants.HOTEL_BOOKED_LIST_EVENT_FLAG)

    }
    else if (response && response.message) {
      DeviceEventEmitter.emit(StringConstants.HOTEL_BOOKED_LIST_EVENT_FLAG)
      // yield put(setLoadingAction(false));
      setTimeout(() => {
        alert(response.message)
      }, 100);

    } else if (response && response.code && response.code.code && response.code.code == 500) {
      DeviceEventEmitter.emit(StringConstants.HOTEL_BOOKED_LIST_EVENT_FLAG)
      // yield put(setLoadingAction(false));
      setTimeout(() => {
        Alert.alert(
          "Alert",
          "Please Login Again",
          [{ text: "Ok", onPress: () => { DeviceEventEmitter.emit(StringConstants.SESSION_EXPIRE_EVENT) } }],
          { cancelable: false }
        )
      }, 100)
    }
  }
  catch (error) {
    console.log(error);
    // Dispatch Action To Redux Store
    DeviceEventEmitter.emit(StringConstants.HOTEL_BOOKED_LIST_EVENT_FLAG)
    // yield put(setLoadingAction(false));
    if (error.message) {
      setTimeout(() => {
        alert(error.message);
      }, 100);
    } else if (error.code && error.code.description) {
      setTimeout(() => {
        alert(error.code.description);
      }, 100);
    }
  }
}

function* getHotelList({ type, payload }) {
  console.log("HOTEL_LIST_REQ:" + JSON.stringify(payload))
  yield put({ type: ActionType.LOADING_MSG, payload: "Loading Hotel List" });
  yield put(setLoadingAction(true));
  yield put(setLoadingMsg("We are searching for Hotels.\nPlease wait..."));

  try {
    let response = yield call(getHotelListApi, payload);
    console.log("HOTEL_LIST_RES:" + JSON.stringify(response))
    yield put(setLoadingAction(false));
    if (response && response.success && response.data && response.data.results) {
      DeviceEventEmitter.emit(StringConstants.HOTEL_LIST_EVENT, response.data)
    } else if (response && response.message) {
      setTimeout(() => {
        alert(response.message);
      }, 100);
      NavigationServices.goBack();
    }
  }
  catch (error) {
    console.log(error);
    // Dispatch Action To Redux Store
    yield put(setLoadingAction(false));
    if (error.message) {
      setTimeout(() => {
        alert(error.message);
      }, 100);
    } else if (error.code && error.code.description) {
      setTimeout(() => {
        alert(error.code.description);
      }, 100);
    }
  }
}


function* getHotelBookingDetails({ type, payload }) {
  yield put(setLoadingAction(true));
  try {
    let response = yield call(getHotelBookingDetailApi, payload);
    yield put(setLoadingAction(false));
    if (response && response.success) {
      DeviceEventEmitter.emit(StringConstants.HOTEL_BOOKING_DETAILS_EVENT, response.data)
    }
    else if (response && response.message) {
      setTimeout(() => {
        alert(response.message);
      }, 100);
      NavigationServices.goBack();
    }
    else if (response && response.code && response.code.code && response.code.code == 500) {
      setTimeout(() => {
        alert(JSON.stringify(StringConstants.session_expired))
      }, 100)
      NavigationServices.goBack()
    }
    else {
      alert("An error occurred\nTry Again");
      NavigationServices.goBack()
    }
  }
  catch (error) {
    console.log(error);
    // Dispatch Action To Redux Store
    yield put(setLoadingAction(false));
    if (error.message) {
      setTimeout(() => {
        alert(error.message);
      }, 100);
    } else if (error.code && error.code.description) {
      setTimeout(() => {
        alert(error.code.description);
      }, 100);
    }
  }
}

function* getHotelDetails({ type, payload }) {
  yield put(setLoadingAction(true));
  yield put(setLoadingMsg("Fetching Hotel Details.\n Please Wait...."));
  try {
    let response = yield call(getHotelDetailsApi, payload);
    yield put(setLoadingAction(false));
    if (response && response.success && response.data) {
      console.log(JSON.stringify(response));
      DeviceEventEmitter.emit(StringConstants.HOTEL_DETAILS_EVENT, response.data)
    }
    else if (response && response.message) {
      setTimeout(() => {
        alert(response.message);
      }, 100);
      NavigationServices.goBack();
    }
  }
  catch (error) {
    console.log(error);
    // Dispatch Action To Redux Store
    yield put(setLoadingAction(false));
    if (error.message) {
      setTimeout(() => {
        alert(error.message);
      }, 100);
    } else if (error.code && error.code.description) {
      setTimeout(() => {
        alert(error.code.description);
      }, 100);
    }
  }
}
function* checkRoomBlock({ type, payload }) {
  yield put(setLoadingAction(true));
  yield put(setLoadingMsg("Processing...."));
  try {
    let response = yield call(blockRoomApi, payload.data);
    console.log(JSON.stringify(response));
    yield put(setLoadingAction(false));
    if (response && response.success && response.data && response.data.body && response.data.body.BlockRoomResult && response.data.body.BlockRoomResult) {
      if (response.data.body.BlockRoomResult.IsPriceChanged === false) {
        NavigationServices.navigate("PaymentScreen", payload)
      } else {
        NavigationServices.navigate("HomeTabStack")
        setTimeout(() => {
          alert("Hotel Price Changed...\n Please proceed again")
        }, 100);
      }
    }
    else {
      if (response && response.message) {
        setTimeout(() => {
          alert(response.message);
        }, 100);
      }
      //NavigationServices.goBack();
    }
  }
  catch (error) {
    console.log(error);
    // Dispatch Action To Redux Store
    yield put(setLoadingAction(false));
    if (error.message) {
      setTimeout(() => {
        alert(error.message);
      }, 100);
    } else if (error.code && error.code.description) {
      setTimeout(() => {
        alert(error.code.description);
      }, 100);
    }
    setTimeout(() => {
      NavigationServices.navigate("HomeTabStack")
    }, 300);
  }
}

function* bookHotel({ type, payload, nav }) {
  yield put(setLoadingAction(true));
  try {
    let response = yield call(bookHotelApi, payload);

    console.log("BOOK_HOTEL_RES:" + JSON.stringify(response))
    if (response && response.success && response.data.status && response.data.status == "confirmed") {
      yield put(setLoadingAction(false));
      setTimeout(() => {
        let routeName = "BookingSuccess"
        let params = {
          "data": {
            bookingId: response.data.booking_id,
            checkInDate: payload.check_in,
            checkOutDate: payload.check_out,
            transactionId: payload.orderID
          },
          isHotel: true
        }
        NavigationServices.navigate(routeName, params)
      }, 100);
    } else {
      yield put(setLoadingAction(false));
      if (response && response.message) {
        setTimeout(() => {
          alert(response.message);
        }, 100);
      }
      NavigationServices.goBack();
    }
  }
  catch (error) {
    NavigationServices.goBack();
    console.log(JSON.stringify(error));
    // Dispatch Action To Redux Store
    yield put(setLoadingAction(false));
    if (error.message) {
      setTimeout(() => {
        alert(error.message);
      }, 100);
    } else if (error.code && error.code.description) {
      setTimeout(() => {
        alert(error.code.description);
      }, 100);
    }
  }
}

function* cancelHotelBooking({ type, payload }) {
  yield put(setLoadingAction(true));
  yield put(setLoadingMsg("Canceling Booking"));
  try {
    let response = yield call(cancelHotelBookingApi, payload);
    console.log("CANCEL_HOTEL_BOOKING_RESP:", JSON.stringify(response))
    if (response && response.success) {
      DeviceEventEmitter.emit(StringConstants.CANCEL_HOTEL_BOOKING_EVENT, response.data)
      yield put(setLoadingAction(false));

    } else {
      yield put(setLoadingAction(false));
      if (response && response.message) {
        setTimeout(() => {
          alert(response.message);
        }, 100);
      }
    }
  }
  catch (error) {
    console.log(JSON.stringify(error));
    yield put(setLoadingAction(false));
    if (error.message) {
      setTimeout(() => {
        alert(error.message);
      }, 100);
    } else if (error.code && error.code.description) {
      setTimeout(() => {
        alert(error.code.description);
      }, 100);
    }
  }
}

function* checkCancelStatus({ type, payload }) {
  yield put(setLoadingAction(true));
  try {
    let response = yield call(cancelHotelBookingApi, payload);
    console.log("CHECK_CANCEL_STATUS_HOTEL:", JSON.stringify(response))
    if (response && response.status && response.status == "confirmed") {
      DeviceEventEmitter.emit(StringConstants.CHECK_CANCEL_STATUS_HOTEL, response)
      yield put(setLoadingAction(false));

    } else {
      yield put(setLoadingAction(false));
      if (response && response.message) {
        setTimeout(() => {
          alert(response.message);
        }, 100);
      }
    }
  }
  catch (error) {
    console.log(JSON.stringify(error));
    yield put(setLoadingAction(false));
    if (error.message) {
      setTimeout(() => {
        alert(error.message);
      }, 100);
    } else if (error.code && error.code.description) {
      setTimeout(() => {
        alert(error.code.description);
      }, 100);
    }
  }
}

// Watcher: watch hotel request
export default function* watchHotel() {
  // Take Last Action Only
  yield takeLatest(ActionType.HOTEL_TICKET_LIST, getHotelBookedList);
  yield takeLatest(ActionType.HOTEL_LIST, getHotelList);
  yield takeLatest(ActionType.HOTEL_BOOKING_DETAILS_SAGA, getHotelBookingDetails);
  yield takeLatest(ActionType.HOTEL_DETAILS_SAGA, getHotelDetails);
  yield takeLatest(ActionType.BOOK_HOTEL_SAGA, bookHotel);
  yield takeLatest(ActionType.ROOM_BLOCK, checkRoomBlock);
  yield takeLatest(ActionType.CANCEL_HOTEL_BOOKING_SAGA, cancelHotelBooking);
  yield takeLatest(ActionType.CANCEL_HOTEL_BOOKING_STATUS_SAGA, checkCancelStatus);
};