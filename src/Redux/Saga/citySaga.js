import { DeviceEventEmitter } from 'react-native';
import { call, put, takeLatest } from 'redux-saga/effects';
import { getCitiesApi } from "../../Utils/APIManager/ApiProvider";
import StringConstants from '../../Utils/StringConstants';
import ActionType from '../Actions/actionType';
import { setLoadingAction } from '../Actions';
function* searchCity({ type, payload }) {
  //   yield put(setLoadingAction(true));
  try {
    let response = yield call(getCitiesApi, payload);
    if (response) {
      yield put(setLoadingAction(false));
      if (response.data) {
        DeviceEventEmitter.emit(StringConstants.SEARCH_CITY_EVENT, response.data)
      }
    } else {
      yield put(setLoadingAction(false));
      if (response && response.message) {
        setTimeout(() => {
          alert(response.message);
        }, 100);
      }
    }
  }
  catch (error) {
    console.log(error);
    // Dispatch Action To Redux Store
    yield put(setLoadingAction(false));
  }
};


// Watcher: watch city request
export default function* watchCity() {
  // Take Last Action Only
  yield takeLatest(ActionType.SEARCH_CITY_SAGA, searchCity);
};