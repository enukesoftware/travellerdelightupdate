import React, { Component } from 'react'
import { DeviceEventEmitter, Dimensions, Image, SafeAreaView, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import CardView from 'react-native-cardview'
import { ScrollView, TextInput } from 'react-native-gesture-handler'
// import { Dropdown } from 'react-native-material-dropdown'
import { connect } from 'react-redux'
// import countries from '../../json/countries'
// import countriesCodes from '../../json/countriesCodes'
import { doLoginAction, doRegisterAction, facebookLoginAction, googleLoginAction } from '../../Redux/Actions'
import * as SocialAuth from '../../Utils/APIManager/SocialAuthentication'
import { colors } from '../../Utils/Colors'
import Fonts from '../../Utils/Fonts'
import Images from '../../Utils/Images'
import NavigationServices from '../../Utils/NavigationServices'
import StringConstants from '../../Utils/StringConstants'
import { isEmailValid } from '../../Utils/Utility'
import SubmitButton from '../custom/SubmitButton'
import {Environment} from '../../Utils/APIManager/APIConstants'

class Authentication extends Component {
  constructor(props) {
    super(props)
    if (props.isLogin) {
      NavigationServices.replace("Profile")
    }
    this.state = {
      isLoginActive: true,
      isPersonalLogin: true,
      email: "",
      password: "",
      firstName: '',
      lastName: '',
      confirmPassword: '',
      employeeCode: '',
      phone: '',
      country: { "id": "", "value": "" },
      countryCode: { "id": "", "value": "" },
    }
    this.updateRegisterDataFromApi = this.updateRegisterDataFromApi.bind(this);
  }

  componentDidMount() {
    // this.setState({
    //   email: Environment==='Production'?'vipin.sharma1979@gmail.com':Environment === 'PlayStore'? '':'akash.kulshreshtha@enukesoftware.com',
    //   password: Environment==='Production'?'vipin55':Environment === 'PlayStore'? '':'123456',
    // })
    DeviceEventEmitter.addListener(StringConstants.REGISTER_EVENT, this.updateRegisterDataFromApi)
  }

  updateRegisterDataFromApi(data) {
    this.changeLoginModule(true)
  }

  loginUser() {
    const { email, password } = this.state;

    if (!email) {
      alert("Please Enter Email")
      return;
    }

    if (!isEmailValid(email)) {
      alert("Please Enter Valid Email")
      return;
    }

    if (!password) {
      alert("Please Enter Password")
      return;
    }
    if (password.length < 6) {
      alert("Please Enter Valid 6 Digit Password")
      return;
    }

    let param = {
      email: email,
      password: password,
      loginFrom: "Auth"
    }

    this.props.doLoginAction(param)

  }

  googleSignIn() {
    console.log("GOOGLE_LOGIN_REQ")
    SocialAuth.googleLogin().then((res) => {
      console.log("GOOGLE:" + JSON.stringify(res))
      if (res && res.accessToken) {
        let param = {
          token: res.accessToken,
        }

        this.props.googleLoginAction(param)
      }
    }).catch((err) => {
      console.log("GOOGLE:" + JSON.stringify(err))
    })
  }

  fbLogin() {
    console.log("FACEBOOK_LOGIN_REQ")
    SocialAuth.fbLogin().then((res) => {
      console.log("FACEBOOK:" + JSON.stringify(res))
      if (res && res.accessToken) {
        let param = {
          token: res.accessToken,
        }
        //this.props.facebookLoginAction(param)
      }
    }).catch((err) => {
      console.log("FACEBOOK:" + JSON.stringify(err))
    })
    // try {
    // 	const data = await SocialHelper.fbLogin();
    // 	if (data) {
    // 		this.setState({ loading: true });
    // 		ApiManager.fbLogin({
    // 			user_id: data.userID,
    // 			access_token: data.accessToken
    // 		}).then((res) => {
    // 			setTimeout(() => {
    // 				this.setState({ loading: false });
    // 			}, 100);
    // 			setTimeout(() => {
    // 				if (res && res.success && res.code == 200) {
    // 					console.log("login succes" + JSON.stringify(res));
    // 					constants.user = res.data.user;
    // 					NavigationService.navigate(constants.previousStateForLogin, { logined: true });
    // 				} else {
    // 					Toast.showWithGravity(res.singleStringMessage, Toast.SHORT, Toast.CENTER);
    // 				}
    // 			}, 500)
    // 		}).catch((err) => {
    // 			this.setState({ loading: false }, () => {
    // 				console.log(JSON.stringify(err));
    // 			})
    // 		})
    // 	}
    // } catch (error) {
    // 	this.setState({ loading: false }, () => {
    // 		console.log('error in fb sign in' + JSON.stringify(error));
    // 	});
    // }
  }

  changeLoginModule(isLoginActive) {
    this.setState({
      isLoginActive: isLoginActive,
      email: '',
      password: '',
      employeeCode: '',
      firstName: '',
      lastName: '',
      confirmPassword: '',
      phone: '',
      country: { "id": "", "value": "" },
      countryCode: { "id": "", "value": "" },
    })
  }

  renderLoginButtons() {
    return (
      <View style={{ padding: 5 }}>
        <View style={{ width: '100%', flexDirection: 'row', justifyContent: 'center' }}>
          <View style={{ width: '50%', alignItems: 'center', justifyContent: 'center' }}>
            <TouchableOpacity onPress={() => this.changeLoginModule(true)}>
              <Text style={{ color: this.state.isLoginActive ? colors.colorBlue : colors.lightBlue, fontSize: 14, fontFamily: Fonts.bold }}>LOGIN</Text>
            </TouchableOpacity>
          </View>
          <View style={{ width: '50%', alignItems: 'center', justifyContent: 'center' }}>
            <TouchableOpacity onPress={() => this.changeLoginModule(false)} >
              <Text style={{ color: !this.state.isLoginActive ? colors.colorBlue : colors.lightBlue, fontSize: 14, fontFamily: Fonts.bold }}>SIGNUP</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    )
  }

  renderLoginScreen() {
    let { countries } = this.state;
    return (
      <View style={{ width: '100%', padding: 10 }}>

        {!this.state.isPersonalLogin ?
          <TextInput
            style={{
              height: 34,
              margin: 4,
              fontSize: 12,
              padding: 4,
              fontFamily: Fonts.regular,
              borderBottomColor: 'grey',
              borderBottomWidth: 1
            }}
            value={this.state.employeeCode}
            onChangeText={text => this.setState({ employeeCode: text })}
            placeholder={'Employee Code'}
            autoCapitalize={"none"}
            autoCorrect={false}
            placeholderTextColor={'grey'}
          /> : null}

        <TextInput
          style={{
            height: 34,
            margin: 4,
            fontSize: 12,
            padding: 4,
            fontFamily: Fonts.regular,
            borderBottomColor: 'grey',
            borderBottomWidth: 1
          }}
          value={this.state.email}
          onChangeText={text => this.setState({ email: text })}
          placeholder={'Email'}
          autoCapitalize={"none"}
          keyboardType={'email-address'}
          autoCorrect={false}
          placeholderTextColor={'grey'}
        />

        <TextInput
          style={{
            height: 34,
            fontSize: 12,
            margin: 4,
            padding: 4,
            fontFamily: Fonts.regular,
            borderBottomColor: 'grey',
            borderBottomWidth: 1
          }}
          value={this.state.password}
          onChangeText={text => this.setState({ password: text })}
          placeholder={"Password"}
          autoCapitalize={"none"}
          autoCorrect={false}
          secureTextEntry
          placeholderTextColor={'grey'}
        />
        <View style={{ margin: 4, alignItems: 'flex-end' }}>
          <TouchableOpacity onPress={() => this.onForgetPasswordPressed()}>
            <Text style={{ color: 'grey', fontFamily: Fonts.semiBold, fontSize: 12 }}>Forgot Password?</Text></TouchableOpacity>
        </View>

      </View>
    )
  }

  renderSignUpScreen() {

    return (
      <View style={{ width: '100%', padding: 10 }}>
        <TextInput
          style={{
            height: 34,
            margin: 4,
            fontSize: 12,
            padding: 4,
            fontFamily: Fonts.regular,
            borderBottomColor: 'grey',
            borderBottomWidth: 1
          }}
          value={this.state.firstName}
          onChangeText={text => this.setState({ firstName: text })}
          placeholder={'First Name'}
          autoCapitalize={"none"}
          autoCorrect={false}
          placeholderTextColor={'grey'}
        />

        <TextInput
          style={{
            height: 34,
            margin: 4,
            fontSize: 12,
            padding: 4,
            fontFamily: Fonts.regular,
            borderBottomColor: 'grey',
            borderBottomWidth: 1
          }}
          value={this.state.lastName}
          onChangeText={text => this.setState({ lastName: text })}
          placeholder={'Last Name'}
          autoCapitalize={"none"}
          autoCorrect={false}
          placeholderTextColor={'grey'}
        />

        <TextInput
          style={{
            height: 34,
            margin: 4,
            fontSize: 12,
            padding: 4,
            fontFamily: Fonts.regular,
            borderBottomColor: 'grey',
            borderBottomWidth: 1
          }}
          value={this.state.email}
          onChangeText={text => this.setState({ email: text })}
          placeholder={'Email'}
          autoCapitalize={"none"}
          keyboardType={'email-address'}
          autoCorrect={false}
          placeholderTextColor={'grey'}
        />
        <TextInput
          style={{
            height: 34,
            margin: 4,
            fontSize: 12,
            padding: 4,
            fontFamily: Fonts.regular,
            borderBottomColor: 'grey',
            borderBottomWidth: 1
          }}
          value={this.state.phone}
          onChangeText={text => this.setState({ phone: text })}
          placeholder={'Phone'}
          autoCapitalize={"none"}
          keyboardType={'phone-pad'}
          autoCorrect={false}
          placeholderTextColor={'grey'}
          maxLength={10}
        />

        <TextInput
          style={{
            height: 34,
            fontSize: 12,
            margin: 4,
            padding: 4,
            fontFamily: Fonts.regular,
            borderBottomColor: 'grey',
            borderBottomWidth: 1
          }}
          value={this.state.password}
          onChangeText={text => this.setState({ password: text })}
          placeholder={"Password"}
          autoCapitalize={"none"}
          autoCorrect={false}
          secureTextEntry
          placeholderTextColor={'grey'}
        />
        <TextInput
          style={{
            height: 34,
            fontSize: 12,
            margin: 4,
            padding: 4,
            fontFamily: Fonts.regular,
            borderBottomColor: 'grey',
            borderBottomWidth: 1
          }}
          value={this.state.confirmPassword}
          onChangeText={text => this.setState({ confirmPassword: text })}
          placeholder={"Confirm Password"}
          autoCapitalize={"none"}
          autoCorrect={false}
          secureTextEntry
          placeholderTextColor={'grey'}
        />

        {/* <View style={{ width: '100%', flexDirection: 'row' }}>
          <View style={{ width: '60%' }}>
            <Dropdown placeholder={"Country"}
              placeholderTextColor={"grey"}
              data={countries}
              fontSize={12}
              fontFamily={Fonts.regular}
              value={this.state.country.value}
              containerStyle={{
                paddingRight: 10,
                paddingLeft: 10,
                width: "100%",
              }}
              inputContainerStyle={{
                borderBottomColor: 'transparent',
              }}
              pickerStyle={{
                width: "90%",
                marginLeft: 10,
                marginTop: 40,
              }}
              itemTextStyle={{ color: "grey", fontSize: 12, height: 34, fontFamily: Fonts.regular }}
              itemColor={"grey"}
              dropdownOffset={{ top: 10, left: 0 }}
              onChangeText={(value, index) => {
                this.setState({ country: countries[index], countryCode: countriesCodes[index] });
              }}
            />
          </View>
          <View style={{ width: '40%' }}>
            <Dropdown placeholder={"Country Code"}
              placeholderTextColor={"grey"}
              data={countriesCodes}
              fontSize={12}
              fontFamily={Fonts.regular}
              value={this.state.countryCode.value}
              containerStyle={{
                paddingRight: 10,
                paddingLeft: 10,
                width: "100%",
              }}
              inputContainerStyle={{
                borderBottomColor: 'transparent',
              }}
              pickerStyle={{
                width: "90%",
                marginLeft: 10,
                marginTop: 40,
              }}
              itemTextStyle={{ color: "grey", fontSize: 12, height: 34, fontFamily: Fonts.regular }}
              itemColor={"grey"}
              dropdownOffset={{ top: 10, left: 0 }}
              onChangeText={(value, index) => {
                this.setState({ country: countries[index], countryCode: countriesCodes[index] });
              }}
            />
          </View>
        </View> */}

      </View>
    )
  }

  changeLoginType(isPersonalLogin) {
    if (isPersonalLogin != this.state.isPersonalLogin) {
      this.setState({
        isPersonalLogin: isPersonalLogin
      })
    }


  }
  onForgetPasswordPressed() {
    NavigationServices.navigate("ForgotPassword")

  }


  Submit() {
    if (this.state.isLoginActive) {
      this.loginUser()
    }
    else {
      this.validateFieldsAndRegister()
    }
  }


  validateFieldsAndRegister() {
    this.setState(p => {
      return { firstName: p.firstName.trim(), lastName: p.lastName.trim(), email: p.email.trim(), phone: p.phone.replace(/[^0-9]/g, '') }
    }, () => {



      const { firstName, lastName, email, phone, password, confirmPassword } = this.state;

      if (firstName.trim().length == 0) {
        alert('Please Enter your first name')
        return
      }
      if (!firstName.match(/^[a-zA-Z ]+$/)) {
        alert('Only alphabets are allowed in first name');
        return false;
      }
      if (lastName.trim().length == 0) {
        alert('Please Enter your last name')
        return
      }
      if (!lastName.match(/^[a-zA-Z ]+$/)) {
        alert('Only alphabets are allowed in last name');
        return false;
      }
      if (email.trim().length == 0) {
        alert('Please Enter your Email')
        return
      }
      if (!isEmailValid(email)) {
        alert('Please Enter valid Email')
        return
      }
      if (phone.trim().length == 0) {
        alert('Please Enter your Phone Number')
        return
      }
      if (phone.trim().length < 10) {
        alert('Please Enter valid Phone Number')
        return
      }
      if (password.trim().length == 0) {
        alert('Please Enter your Password')
        return
      }
      if (password.trim().length < 6) {
        alert('Password must be at least 6 characters')
        return
      }
      if (confirmPassword.trim().length == 0) {
        alert('Please Enter your Password to confirm')
        return
      }
      if (password != confirmPassword) {
        console.log(
          'password: ' +
          password +
          ' confirm password: ' +
          confirmPassword
        )
        alert('Password not matched')
        return
      }
      // if (!this.state.checked) {
      //   console.log('uncheck')
      //   alert('Please tick the terms and conditions')
      //   return
      // }
      console.log('register')
      let param = {
        first_name: firstName,
        last_name: lastName,
        email: email,
        phone: phone,
        password: password,
        password_confirmation: confirmPassword
      }
      var msg =
        'Verification link has been sent to your mail please verify email to continue login'

      this.props.doRegisterAction(param)
    })
  }


  renderSocialLoginButtons() {
    return (
      <View style={{ paddingHorizontal: 20, width: '100%', flexDirection: 'row', }}>
        <View style={{ width: '50%', paddingHorizontal: 5 }}>
          <TouchableOpacity activeOpacity={0.8} onPress={() => this.fbLogin()}>
            <CardView
              style={{
                overflow: 'hidden',
                backgroundColor: '#3a549f',
                flexDirection: 'row',
              }}
              cardElevation={3}
              cardMaxElevation={3}
              cornerRadius={6}>
              <View style={{ width: 35, height: 35, backgroundColor: '#1e3e95', alignItems: 'center', justifyContent: 'center' }}>
                <Image style={{ width: 20, height: 20, resizeMode: 'contain' }} source={Images.ic_image_facebook}></Image>
              </View>
              <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                <Text style={{ color: 'white', fontFamily: Fonts.semiBold, fontSize: 13 }}>Facebook</Text>
              </View>
            </CardView>
          </TouchableOpacity>
        </View>
        <View style={{ width: '50%', paddingHorizontal: 5 }}>
          <TouchableOpacity activeOpacity={0.8} onPress={() => this.googleSignIn()}>
            <CardView
              style={{
                overflow: 'hidden',
                backgroundColor: '#d6373b',
                flexDirection: 'row',
              }}
              cardElevation={3}
              cardMaxElevation={3}
              cornerRadius={6}>
              <View style={{ width: 35, height: 35, backgroundColor: '#b81317', alignItems: 'center', justifyContent: 'center' }}>
                <Image style={{ width: 20, height: 20, resizeMode: 'contain' }} source={Images.ic_image_google}></Image>
              </View>
              <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                <Text style={{ color: 'white', fontFamily: Fonts.semiBold, fontSize: 13 }}>Google</Text>
              </View>
            </CardView>
          </TouchableOpacity>
        </View>
      </View>
    )
  }

  render() {

    return (
      <View style={{ flex: 1 }}>
        <View style={{ position: 'absolute', left: 0, right: 0, top: 0, }}>
          <Image resizeMode={'stretch'}
            resizeMethod="resize"
            style={styles.backgroundImage}
            source={Images.img_home_bg} >
          </Image>
        </View>
        <ScrollView contentContainerStyle={{ flexGrow: 1, paddingBottom: 10 }}>
          <View style={{ flex: 1, }}>
            <View style={{ marginTop: 40, padding: 30 }} >
              <Text style={{ fontSize: 18, fontWeight: 'bold', color: 'white', margin: 5, fontFamily: Fonts.bold }}> {"Welcome to Traveller Delight"}</Text>
              <Text style={{ fontSize: 14, fontWeight: '600', color: 'white', textAlign: 'justify', fontFamily: Fonts.regular, lineHeight: 20, margin: 5, marginTop: 7 }}> {(this.state.isLoginActive ? "Login" : "Signup") + " to Continue"}</Text>
              <CardView
                style={{
                  paddingBottom: 27,
                  marginTop: 35,
                  padding: 10,
                  backgroundColor: 'white'
                }}
                cardElevation={3}
                cardMaxElevation={3}
                cornerRadius={10}>
                {this.renderLoginButtons()}

                {this.state.isLoginActive ? this.renderLoginScreen() : this.renderSignUpScreen()}

              </CardView>
              <SubmitButton onPress={() => { this.Submit() }} marginTop={-25} />


              {this.state.isPersonalLogin || !this.state.isLoginActive ? <View style={{ width: '100%', alignItems: 'center', marginVertical: 35 }}>
                <TouchableOpacity onPress={() => { NavigationServices.navigate("Home") }}>
                  <Text style={{ fontFamily: Fonts.bold, fontSize: 12, color: 'grey' }}>
                    Continue As A Guest User >>
                </Text>
                </TouchableOpacity>
              </View> : null}

              {/* {this.state.isPersonalLogin || !this.state.isLoginActive ? this.renderSocialLoginButtons() : null} */}


            </View>
          </View>
        </ScrollView>

        {this.state.isLoginActive ?
          <View style={{ padding: 10, left: 0, right: 0, backgroundColor: this.state.isPersonalLogin ? colors.colorBlue : colors.colorRed, width: '100%', alignItems: 'center', justifyContent: 'center', flexDirection: 'row', height: 62, paddingBottom: 22 }} >
            <TouchableOpacity onPress={() => { this.changeLoginType(true) }}>
              <Text style={{ fontSize: 13, fontFamily: Fonts.semiBold, color: 'white' }}>
                Personal Login
                </Text>
            </TouchableOpacity>
            <View style={{ paddingHorizontal: 10 }}>
              <TouchableOpacity onPress={() => { this.changeLoginType(!this.state.isPersonalLogin) }}>
                <Image source={!this.state.isPersonalLogin ? Images.ic_switch_one : Images.ic_switch_two} style={{ resizeMode: 'contain', width: 28, height: 25 }} />
              </TouchableOpacity>
            </View>
            <TouchableOpacity onPress={() => { this.changeLoginType(false) }}>
              <Text style={{ fontSize: 13, fontFamily: Fonts.semiBold, color: 'white' }}>
                Corporate Login
              </Text>
            </TouchableOpacity>

          </View> : null}

        <SafeAreaView />
      </View>
    )
  }
}
const mapStateToProps = (state) => {
  // console.log("LOGIN_STATE:" + JSON.stringify(state))
  return {
    isLogin:state.isLoginReducer
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    doLoginAction: (data) => dispatch(doLoginAction(data)),
    doRegisterAction: (data) => dispatch(doRegisterAction(data)),
    googleLoginAction: (data) => dispatch(googleLoginAction(data)),
    facebookLoginAction: (data) => dispatch(facebookLoginAction(data)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Authentication)

const { width: viewportWidth, height: viewportHeight } = Dimensions.get('window');

const styles = StyleSheet.create({

  backgroundImage: {
    width: '100%',
  },
})

