import React, { Component } from 'react';
import { Image, SafeAreaView, ScrollView, StatusBar, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { TextInput } from 'react-native-gesture-handler';
import { connect } from "react-redux";
import { forgotPasswordAction } from '../Redux/Actions';
import { colors } from '../Utils/Colors';
import Fonts from '../Utils/Fonts';
import Images from '../Utils/Images';
import CommonHeader from './custom/CommonHeader.js';


class ForgotPassword extends Component {
  constructor(props) {
    super(props)
    this.state = {
      email: ''
    }
  }

  recoverPasswordBtn() {
    return (
      <TouchableOpacity
        activeOpacity={0.8}
        style={styles.Buttons}
        underlayColor={'transparent'}
        onPress={() => {
          this.callApi()
        }}
      >
        <Text style={{ color: 'white', fontWeight: '600', fontSize: 15 }}>
          Recover Your Password
          </Text>
      </TouchableOpacity>
    )
  }

  isEmailValid(text) {
    console.log(text)
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/
    if (reg.test(text) === false) {
      console.log('Email is Not Correct')
      return false
    } else {
      console.log('Email is Correct')
      return true
    }
  }


  callApi() {
    if (this.state.email.trim().length == 0) {
      alert("Please Enter Your Email")
      return;
    }
    if (!this.isEmailValid(this.state.email)) {
      alert("Please Enter Valid Email")
      return;
    }

    let param = {
      email: this.state.email
    }

    this.props.forgotPasswordAction(param)
    //  APIManager.forgotPasswordApi(param,(success,response)=>{
    //    if(success){
    //      console.log('success',response.data)
    //      if(response.data === "RESET_LINK_SENT"){
    //        setTimeout(()=>{
    //          Alert.alert('Alert',msg,[
    //           { text: 'OK', onPress: () => Actions.Login() }
    //          ])
    //        },100)
    //      }
    //    }else{
    //      setTimeout(()=>{
    //        alert('error occured')
    //      },100)
    //      console.log('failed'+response)
    //    }
    //  })  
  }


  render() {
    return (
      <View style={styles.container}>
        <SafeAreaView backgroundColor={colors.colorBlue}></SafeAreaView>
        <StatusBar barStyle='light-content' />
        <CommonHeader title={"Forgot Password"} />
        <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
          <Image
            source={Images.ic_forgot_password}
            style={styles.forgotImage}
          />
          <Text style={{ margin: 20, alignSelf: 'center' }}>
            FORGOT YOUR PASSWORD
                </Text>
          <View style={styles.ContentContainer}>
            <View style={{ alignItems: 'center', marginBottom: 20 }}>
              <Text>
                Enter your E-mail below to receive your
                 </Text>
              <Text>
                password reset instruction
                 </Text>
            </View>

            <TextInput
              style={{
                height: 34,
                width: '80%',
                margin: 4,
                fontSize: 12,
                padding: 0,
                fontFamily: Fonts.regular,
                borderBottomColor: colors.colorBlue,
                borderBottomWidth: 1
              }}
              value={this.state.firstName}
              onChangeText={text => this.setState({ email: text })}
              placeholder="Type your email"
              autoCapitalize={"none"}
              autoCorrect={false}
              placeholderTextColor={colors.colorBlack}
              contentType='emailAddress'
              tintColor={colors.colorBlue}
            />
            {this.recoverPasswordBtn()}

          </View>
        </ScrollView>
        <SafeAreaView />

      </View>
    )
  }
}


const mapDispatchToProps = (dispatch) => {
  return {
    forgotPasswordAction: (data) => dispatch(forgotPasswordAction(data))
  };
};

export default connect(null, mapDispatchToProps)(ForgotPassword)

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // backgroundColor: 'rgba(244,248,254,1)'
  },
  forgotImage: {
    marginTop: 50,
    width: 250,
    height: 250,
    alignSelf: 'center',
    shadowColor: "black",
    shadowOpacity: 0.3,
    shadowOffset: { width: 0, height: 3 },

  },
  Buttons: {
    height: 50,
    width: "80%",
    backgroundColor: colors.colorBlue,
    marginTop: 30,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 7
  },
  ContentContainer: {
    margin: 20,

    backgroundColor: 'white',
    alignItems: 'center',
    // height: 80,
    justifyContent: 'center',
    borderRadius: 10,
    shadowColor: "black",
    shadowOpacity: 0.3,
    shadowOffset: { width: 1, height: 2 },
    shadowRadius: 4,
    elevation: 6,
    padding: 20
  },
})