
import moment from 'moment';
import React, { Component } from "react";
import { DeviceEventEmitter, Dimensions, Image, ScrollView, Text, TouchableOpacity, View } from "react-native";
import CardView from 'react-native-cardview';
import { SafeAreaView } from "react-navigation";
//import HotelBookingDetails from "./Hotel/HotelBookingDetail/HotelBookingDetails";
import { connect } from "react-redux";
import { saveFlightSearchParams } from "../../../Redux/Actions";
import { colors } from "../../../Utils/Colors";
import Fonts from '../../../Utils/Fonts';
import Images from "../../../Utils/Images";
import NavigationServices from "../../../Utils/NavigationServices";
import StringConstants from "../../../Utils/StringConstants";
import SubmitButton from "../../custom/SubmitButton";
import SelectFlightClass from "../SelectFlightClass";
import SelectTravellerModal from "../SelectTraveller/SelectTravellerModal";
import styles from "./Styles";

const screenWidth = Dimensions.get('screen').width;
const screenHeight = Dimensions.get('screen').height;

class FlightSearch extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cities: [{
        id: 1
      }],
      tripType: 1,
      selectedStoppage: 1,
      openedCalendar: false,
      openedTravellerModal: false,
      openedFlightClass: false,
      oneWayActive: true,
      // departDate: new Date(),
      isForHotel: false,
      forReturnDate: false,
      //old keys
      // departDate: (new Date()).setDate((new Date).getDate() + 10),
      departDate: new Date(),
      returnDate: null,
      oneWayActive: true,
      // sourceDict: {
      //   airport_name: "Delhi Indira Gandhi Intl",
      //   country_code: "IN",
      //   country_name: "INDIA",
      //   city_name: "Delhi",
      //   airport_code: "DEL"
      // },
      // destinationDict: {
      //   airport_name: "Chhatrapati Shivaji",
      //   country_code: "IN",
      //   country_name: "INDIA",
      //   city_name: "Mumbai",
      //   airport_code: "BOM"
      // },
      sourceDict: null,
      destinationDict: null,
      selectTravellersModel: false,
      selectClassModel: false,
      numberOfAdults: 1,
      numberOfChilds: 0,
      numberOfInfant: 0,
      selectedClassKey: 2,
      selectedClassCode:"Economy",
      // selectedClassName: 'All',
      selectedClassName: "Economy Class",
      disabled: false,
      isNonStop: true,
      selectedNationality: null
    }
  }
  //-----------------Components Life cycle Methods ---------------

  componentDidMount() {

    const { navigation } = this.props;
    const data = navigation.getParam('data', null);
    if (data) {
      this.setState({ sourceDict: data.sourceDict, destinationDict: data.destinationDict });
    }

    DeviceEventEmitter.addListener(StringConstants.AIRPORT_SELECT, data => {
      if (data.isSource) {
        this.setState({ sourceDict: data },()=>{console.log(JSON.stringify(data))});
      } else {
        this.setState({ destinationDict: data });
      }
    });

    DeviceEventEmitter.addListener(StringConstants.DATE_SELECT, dict => {
      // if (dict.returnDate && !this.state.oneWayActive) {
      //   this.setState({
      //     departDate: dict.departDate,
      //     returnDate: dict.returnDate,
      //     oneWayActive: false
      //   });
      // } else {
      //   this.setState({
      //     departDate: dict.departDate,
      //     returnDate: null,
      //     oneWayActive: true
      //   });
      // }


      this.setState({
        departDate: dict.departDate,
        returnDate: dict.returnDate,
        oneWayActive: dict.isOneWayActive,
        tripType: dict.isOneWayActive ? 1 : 2

        // oneWayActive: this.setState.
      });

    });

    DeviceEventEmitter.addListener(StringConstants.TRAVELLER_SELECT, dict => {
      if (dict) {
        this.setState({
          numberOfAdults: dict.adultsNum,
          numberOfChilds: dict.childNum,
          numberOfInfant: dict.infantNum
        })
      }
    });

    DeviceEventEmitter.addListener(StringConstants.TRAVELLER_CLASS, dict => {
      // alert(JSON.stringify(dict))
      if (dict) {
        this.setState({
          selectedClassKey: dict.key,
          selectedClassName: dict.value,
          selectedClassCode:dict.code
        })
      }
    });

    let airportsData = this.props.airPortListData;
    if (airportsData && airportsData.length != 0) {
      let srcArr = airportsData.filter(data => {
        return data.airport_code == "DEL";
      });
      let destArr = airportsData.filter(data => {
        return data.airport_code == "BOM";
      });
      this.setState({
        sourceDict: srcArr[0],
        destinationDict: destArr[0]
      });
    }
  }

  closeFlightClass = () => {
    this.setState({ openedFlightClass: false });
  }

  closeAirportModal = () => {
    this.setState({ openedAirport: false });
  }

  closeCalendarModal = () => {
    this.setState({ openedCalendar: false });
  }

  closeTravellerModal = () => {
    this.setState({ openedTravellerModal: false });
  }

  getDate(date) {
    //return dateFormat(date, "ddd, mmm dS");
    return moment(date).format("DD MMM YYYY");
  }

  returnNumbersOfTravellersString() {
    let numAdults = this.state.numberOfAdults
    let numChild = this.state.numberOfChilds
    let numInfant = this.state.numberOfInfant
    let str = numAdults + ' Adult'

    if (numChild > 0) {
      str = str + ',' + numChild + ' Child'
    }

    if (numInfant > 0) {
      str = str + ',' + numInfant + ' Infant'
    }

    return str
  }

  tripTypeSelectionFunction(status) {
    if (status == false) {
      let depart_date = new Date(this.state.departDate);
      let return_Date = new Date(this.state.departDate);
      if (this.state.returnDate)
        return_Date = this.state.returnDate
      else
        return_Date.setDate(depart_date.getDate() + 1)
      this.setState({ oneWayActive: status, returnDate: return_Date })
    }
    else {
      this.setState({ oneWayActive: status, })
    }
  }

  switchOrigins = () => {
    let { sourceDict, destinationDict } = this.state;
    let a = Object.assign({}, sourceDict);
    let b = Object.assign({}, destinationDict);
    let tempDict = Object.assign({}, a);
    a = Object.assign({}, b);
    b = Object.assign({}, tempDict);
    this.setState({
      sourceDict: a,
      destinationDict: b
    })
  }
  validDetails() {
    const { selectedClassName, sourceDict, destinationDict, selectedNationality } = this.state;
    if (!selectedClassName) {
      alert("Please Select Class");
      return false
    }
    if (!sourceDict) {
      alert("Please Select Origin");
      return false
    }
    if (!destinationDict) {
      alert("Please Select Destination");
      return false
    }
    // if (!selectedNationality) {
    //   alert("Please Select Nationality");
    //   return false
    // }
    return true
  }

  renderSingleCityCardView = () => {
    let { sourceDict, destinationDict, tripType, selectedClassName, returnDate, isNonStop, selectedClassKey,selectedClassCode, selectedNationality,
      departDate, oneWayActive, numberOfAdults, numberOfChilds, numberOfInfant } = this.state;
    return (
      <View style={{ marginBottom: 25 }}>
        <CardView
          cardElevation={2}
          cardMaxElevation={3}
          cornerRadius={5}
          style={styles.card}>
          <View style={{ borderRadius: 5, overflow: "hidden", width: '100%' }}>
            <View style={styles.cardRow}>
              <TouchableOpacity style={[styles.cardBtn, { alignItems: 'flex-start' }]} onPress={() => {
                // this.setState({ openedAirport: true })
                NavigationServices.navigate('SearchAirport', { isSource: true, forFlightSearch: true });
              }}>
                <Text style={styles.cardText}>From</Text>
                {sourceDict ?
                  <Text style={styles.stnCode}>{sourceDict.airport_code}</Text> :
                  <Text style={styles.selection}>Origin</Text>}
              </TouchableOpacity>
              <TouchableOpacity disabled={!(sourceDict && destinationDict)} style={styles.iconButton} onPress={() => {
                this.switchOrigins();
              }}>
                <Image source={Images.ic_switch_flight}
                  style={styles.icon}></Image>
              </TouchableOpacity>
              <TouchableOpacity style={[styles.cardBtn, { alignItems: 'flex-end' }]} onPress={() => {
                NavigationServices.navigate('SearchAirport', { isSource: false, forFlightSearch: true });
              }}>
                <View style={{ minWidth: 90 }}>
                  <Text style={styles.cardText}>To</Text>
                  {destinationDict ?
                    <Text style={styles.stnCode}>{destinationDict.airport_code}</Text> :
                    <Text style={styles.selection}>Destination</Text>}
                </View>
              </TouchableOpacity>
            </View>
            <View style={styles.border}></View>

            <View style={styles.cardRow}>
              <TouchableOpacity style={styles.cardBtn} onPress={() => {
                requestAnimationFrame(() => {
                  NavigationServices.navigate('Calender', {
                    isForHotel: false,
                    isOneWayActive: oneWayActive,
                    departDate,
                    returnDate,
                    forReturnDate: false
                  })
                })
              }}>
                <Text style={styles.cardText}>Departure</Text>
                <Text style={[styles.stnCode, {
                  fontFamily: Fonts.semiBold, fontSize: 14
                }]}>{this.getDate(departDate)}</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.iconButton} disabled={true}>
                <Image source={Images.ic_image_calender}
                  style={styles.icon}></Image>
              </TouchableOpacity>
              <TouchableOpacity style={[styles.cardBtn, { alignItems: 'flex-end' }]} onPress={() => {

                requestAnimationFrame(() => {
                  NavigationServices.navigate('Calender', {
                    isForHotel: false,
                    departDate: this.state.departDate,
                    isOneWayActive: false,
                    forReturnDate: true,
                    returnDate: this.state.returnDate
                  })
                })
              }}><View style={{ minWidth: 90 }}>
                  <Text style={styles.cardText}>Return</Text>
                  {
                    !oneWayActive ?
                      <Text style={[styles.stnCode, {
                        fontFamily: Fonts.semiBold, fontSize: 14
                      }]}>{this.getDate(returnDate)}</Text> :
                      <Text style={styles.selection}>Select Date</Text>
                  }
                </View>
              </TouchableOpacity>
            </View>
            <View style={styles.border}></View>
            <View style={styles.cardRow}>
              <TouchableOpacity style={styles.cardBtn} onPress={() => this.setState({ openedTravellerModal: true })}>
                <Text style={styles.cardText}>Passengers</Text>
                <Text style={[styles.stnCode, {
                  fontFamily: Fonts.medium
                }]}>{numberOfAdults + numberOfChilds + numberOfInfant}</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.iconButton}>
                <Image source={Images.ic_image_user}
                  style={styles.icon}></Image>
              </TouchableOpacity>
              <TouchableOpacity style={[styles.cardBtn, { alignItems: 'flex-end' }]} onPress={() => this.setState({ openedFlightClass: true })}>
                <View style={{ minWidth: 90 }}>
                  <Text style={styles.cardText}>Class</Text>
                  <Text style={styles.selection}>{!selectedClassName ? "Select Class" : selectedClassName}</Text>
                </View>
              </TouchableOpacity>
            </View>
            <View style={styles.border}></View>


            {/* 
            <View style={styles.cardRow}>
              <TouchableOpacity style={styles.cardBtn} onPress={() => {
                // this.setState({ openedAirport: true })
                NavigationServices.navigate('SearchAirport', {
                  isSource: true, forNationalitySearch: true, onNationalitySelect: (data) => {
                    this.setState({ selectedNationality: data })
                  }
                });
              }}>
                <Text style={styles.cardText}>Nationality</Text>
                {selectedNationality ?
                  <Text style={styles.stnCode}>{selectedNationality.code}</Text> :
                  <Text style={styles.selection}>Select Nationality</Text>}
              </TouchableOpacity>
              
            </View>
            <View style={styles.border}></View> */}



            <View style={[styles.classContainer, { marginTop: 10, marginBottom: 25 }]}>
              <View style={{ width: '50%', alignItems: 'center' }}>
                <TouchableOpacity style={[styles.classButton, {
                  backgroundColor: isNonStop ? colors.colorBlue : colors.lightgrey,
                }]} onPress={() => this.setState({ isNonStop: true })}>
                  <Text style={[styles.classBtnText, {
                    color: isNonStop ? colors.colorWhite : colors.colorBlack62
                  }]}>NONSTOP</Text>
                </TouchableOpacity>
              </View>
              <View style={{ width: '50%', alignItems: 'center' }}>
                <TouchableOpacity style={[styles.classButton, {
                  marginHorizontal: 13,
                  backgroundColor: !isNonStop ? colors.colorBlue : colors.lightgrey,
                }]} onPress={() => this.setState({ isNonStop: false })}>
                  <Text style={[styles.classBtnText, {
                    color: !isNonStop ? colors.colorWhite : colors.colorBlack62
                  }]}>MULTISTOP</Text>
                </TouchableOpacity>
              </View>

            </View>

          </View>
        </CardView>
        <SubmitButton marginTop={-25} onPress={() => {

          if (this.validDetails())
            requestAnimationFrame(() => {
              let dict = {
                origin: sourceDict.airport_code,
                originCountryCode: sourceDict.country_code,
                destination: destinationDict.airport_code,
                destinationCountryCode: destinationDict.country_code,
                departure_date: moment(this.state.departDate).format('YYYY-MM-DD'),
                return_date: tripType == 1 ? null : ((returnDate == null) ? null : moment(returnDate).format('YYYY-MM-DD')),
                travel_class: selectedClassCode,
                ADT: numberOfAdults,
                CHD: numberOfChilds,
                INF: numberOfInfant,
                non_stop: isNonStop,
                tripType
              };
              this.props.saveFlightSearchParams(dict);
              sourceDict.airport_code == destinationDict.airport_code ? alert("Origin and Destination cannot be the same") : NavigationServices.navigate('FlightList', { tripType });
            });
        }} />
      </View>
    )
  }


  render() {
    let { tripType, selectedClassKey, openedTravellerModal, isSource, forFlightSearch,
      departDate, openedCalendar, isForHotel, forReturnDate, openedFlightClass } = this.state;
    return (
      <View style={styles.container}>
        <SelectFlightClass modalVisible={openedFlightClass}
          closeModal={this.closeFlightClass}
          selectedClassKey={selectedClassKey} />

        <SelectTravellerModal modalVisible={openedTravellerModal}
          closeModal={this.closeTravellerModal}
          numberOfAdults={this.state.numberOfAdults}
          numberOfChilds={this.state.numberOfChilds}
          numberOfInfant={this.state.numberOfInfant} />
        <Image source={Images.img_home_bg}
          style={{
            height: screenHeight / 2.5,
            width: screenWidth,
            resizeMode: "stretch"
          }}></Image>
        <View style={styles.mainContainer}>
          <SafeAreaView />
          <View style={styles.header}>
            <View style={{ width: '20%' }}>
              <TouchableOpacity style={styles.headerBackButton} onPress={() => NavigationServices.goBack()}>
                <Image source={Images.ic_image_back} style={{ width: 15, height: 15, resizeMode: "contain" }}></Image>
                <Text style={{
                  color: colors.colorWhite,
                  fontFamily: Fonts.semiBold
                }}> Back</Text>
              </TouchableOpacity>
            </View>
            <View style={{ width: '60%', alignItems: 'center' }}>
              <Text style={styles.headerTitle}>Search Flight</Text>
            </View><View style={{ width: '20%' }}>
              {/* <TouchableOpacity style={styles.headerButton}>
                <Image source={Images.ic_image_notifications} style={{ width: 20, height: 20, resizeMode: "contain" }}></Image>
              </TouchableOpacity> */}
            </View></View>
          <View style={{ width: '100%', padding: 20 }}>
            <View style={styles.tabs}>
              <TouchableOpacity style={[styles.tabButton, {
                backgroundColor: tripType == 1 ? colors.colorGold : colors.colorWhite,
              }]} onPress={() => {
                this.setState({ tripType: 1, oneWayActive: true });
                this.tripTypeSelectionFunction(true)
              }}>
                <Text style={[styles.tabText, {
                  color: tripType == 1 ? colors.colorWhite : colors.colorBlack
                }]}>One Way</Text>
              </TouchableOpacity>
              <TouchableOpacity style={[styles.tabButton, {
                backgroundColor: tripType == 2 ? colors.colorGold : colors.colorWhite,

              }]} onPress={() => {
                this.setState({ tripType: 2, oneWayActive: false });
                this.tripTypeSelectionFunction(false)
              }}>
                <Text style={[styles.tabText, {
                  color: tripType == 2 ? colors.colorWhite : colors.colorBlack
                }]}>Round Trip</Text>
              </TouchableOpacity>
              {/* <TouchableOpacity style={[styles.tabButton, {
              backgroundColor: tripType == 3 ? colors.colorGold : colors.colorWhite,
            }]} onPress={() => this.setState({ tripType: 3, oneWayActive: false })}>
              <Text style={[styles.tabText, {
                color: tripType == 3 ? colors.colorWhite : colors.colorBlack
              }]}>Multi City</Text>
            </TouchableOpacity> */}
            </View>

          </View>

          <View style={{ width: '100%' }}>
            <ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={{ width: '100%' }} >
              {
                tripType == 3 ?
                  this.renderMultiCityCardView() :
                  this.renderSingleCityCardView()
              }
            </ScrollView>
          </View>
        </View>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    userData: state.userDataReducer,
    isLogin: state.isLoginReducer,
    userToken: state.userTokenReducer
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    saveFlightSearchParams: (data) => dispatch(saveFlightSearchParams(data))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(FlightSearch)
