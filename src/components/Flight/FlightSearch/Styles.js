import { Dimensions, StyleSheet } from "react-native";
import { colors } from "../../../Utils/Colors";

const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;

export default Styles = StyleSheet.create({
  addCityText: {
    color: colors.colorGold,
    fontFamily: Fonts.bold,
    fontSize: 14
  },
  addCityBtn: {
    height: 30, width: screenWidth,
    marginTop: -10,
    justifyContent: "center",
    alignItems: "center"
  },
  classBtnText: {
    fontFamily: Fonts.medium,
    fontSize: 14, letterSpacing: .5,
  },
  submitBtn: {
    width: 60,
    backgroundColor: "red",
    height: "100%",
    justifyContent: "center",
    alignItems: "center",
    elevation: 5,
    shadowColor: colors.colorBlack,
    shadowOpacity: 0.3,
    shadowOffset: { width: 2, height: 2 },
    shadowRadius: 5,
  },
  submitContainer: {
    width: 60,
    height: 60,
    alignItems: "center",
    justifyContent: "center",
    // marginTop: 20,
    position: "absolute",
    bottom: -20,
    zIndex: 99
  },
  classButton: {
    backgroundColor: colors.colorBlue,
    padding: 10,
    paddingLeft: 15,
    paddingRight: 15,
    borderRadius: 5
  },
  classContainer: {
    width: "100%",
    height: 60,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-around"
  },
  border: {
    width: "100%",
    height: 1,
    backgroundColor: "lightgrey",
  },
  selection: {
    fontFamily: Fonts.regular,
    fontSize: 12,
    color: colors.colorBlack,
    lineHeight: 25
  },
  miltiCitycardbtn: {
    height: "100%",
    minWidth: 50,
  },
  cardBtn: {
    width:'40%'
    // backgroundColor: "red"
  },
  icon: {
    width: 30, height: 30,
    resizeMode: "contain",
    marginTop: 7
  },
  iconButton: {
    width: '20%',
    alignItems: "center",
    justifyContent:'center'
  },
  stnCode: {
    fontFamily: Fonts.bold,
    fontSize: 20,
    color: colors.colorBlue,
    lineHeight: 25
  },
  cardText: {
    fontFamily: Fonts.medium,
    fontSize: 14,
    color: colors.colorBlack,
    lineHeight: 25
  },
  cardRow: {
    width: "100%",
    padding:15,
    flexDirection: "row",
    //alignItems: "center"
    //backgroundColor:'yellow'
  },
  multiCardView: {
    width: screenWidth - 24,
    // height: 430,
    backgroundColor: colors.colorWhite,
    borderRadius: 10,
    // overflow: "hidden",
    marginTop: 25,
    padding: 15,
    paddingBottom: 35,
    borderColor: "lightgrey",
    borderWidth: 1,
    alignItems: "center",
    elevation: 5,
    shadowColor: colors.colorBlack,
    shadowOpacity: 0.3,
    shadowOffset: { width: 2, height: 2 },
    shadowRadius: 5,
  },
  card: {
    backgroundColor: colors.colorWhite,
    marginHorizontal: 15,

  },
  tabs: {
    height: 40,
    width: '100%',
    borderRadius: 10,
    flexDirection: "row",
    overflow: "hidden",
    borderWidth: .5,
    borderColor: "lightgrey",
    marginTop: 20,
    elevation: 5,
    shadowColor: colors.colorBlack,
    shadowOpacity: 0.3,
    shadowOffset: { width: 2, height: 2 },
    shadowRadius: 5,
  },
  tabButton: {
    height: "100%",
    width: '50%',
    alignItems: "center",
    justifyContent: "center",
    elevation:5
  },
  tabText: {
    color: colors.colorBlack,
    fontSize: 14,
    fontFamily: Fonts.medium
  },
  headerBackButton: {
    width: 70,
    // backgroundColor: "red",
    height: "100%",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center"
  },
  header: {
    height: 45,
    flexDirection: "row",
    alignItems: "center",
    width:'100%'
  },
  mainContainer: {
    position: "absolute",
    top: 0 ,
    width: screenWidth,
    flex: 1,
    left: 0,
    alignItems: "center",
     //backgroundColor: "yellow"
  },
  headerButton: {
    height: "100%",
    width: 50,
    // backgroundColor: "blue",
    justifyContent: "center",
    alignItems: "center"
  },
  headerTitle: {
    color: colors.colorWhite,
    fontSize: 16,
    // fontWeight: "600"
    fontFamily: Fonts.bold
  },
  container: {
    flex: 1,
    //backgroundColor: "rgba(244,248,254,1)"
    backgroundColor: "#FDFDFD"
  },
});