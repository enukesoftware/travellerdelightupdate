import { default as Moment, default as moment } from 'moment';
import React, { Component } from "react";
import { Alert, DeviceEventEmitter, Dimensions, Image, Text, TextInput, TouchableOpacity, View } from "react-native";
import CardView from 'react-native-cardview';
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { Dropdown } from 'react-native-material-dropdown';
import DateTimePicker from "react-native-modal-datetime-picker";
import { SafeAreaView } from "react-navigation";
import { connect } from "react-redux";
import Titles from "../../../json/Titles";
import { confirmQuoteAction } from "../../../Redux/Actions";
import { colors } from "../../../Utils/Colors";
import Fonts from "../../../Utils/Fonts";
import Images from "../../../Utils/Images";
import StringConstants from "../../../Utils/StringConstants";
import { getAge, roundOffTwo, MyAlert, toUpperEveryWord } from "../../../Utils/Utility";
import BookingBelt from "../../custom/BookingBelt";
import BottomStrip from "../../custom/BottomStrip";
import Counter from "../../custom/Counter";
import FlightListHeader from "../../custom/FlightListHeader";
import NavigationServices from './../../../Utils/NavigationServices';
import { styles } from "./Styles";

const screenSize = Dimensions.get("window");
let QuoteCounter = 0

const passengerType = {
  Adult: "Adult",
  child: "Child",
  Infant: "Infant"
};

class PassengerDetails extends Component {
  constructor(props) {
    super(props);
    this.ActionSheet = null;
    this.state = {
      passengersArray: [],
      adultDict: {
        passenger_type: passengerType.Adult,
        title: "",
        fName: "",
        mName: "",
        lName: "",
        dob: "",
        nationality: "",
        // gender: "",
        dep_beg: "",
        dep_meal: ""
      },
      childDict: {
        passenger_type: passengerType.child,
        title: "",
        fName: "",
        mName: "",
        lName: "",
        dob: "",
        nationality: "",
        // gender: "",
        dep_beg: "",
        dep_meal: ""
      },
      infantDict: {
        passenger_type: passengerType.Infant,
        title: "",
        fName: "",
        mName: "",
        lName: "",
        dob: "",
        nationality: "",
        //   gender: ""
      },

      phoneNumber: "",
      email: "",
      titlesArr: ["Mr.", "Mrs.", "Ms.", "Cancel"],
      loading: false,
      isDateTimePickerVisible: false,
      isDateTimePickerExpiry: false,
      selectedIndexForDob: -1,
      genderIndex: -1,
      fareQuotes: [],
      baggageOrMealModal: false,
      isForBaggage: true,
      baggageDynamic: [],
      mealDynamic: [],
      isForDept: true,
      flightType: null,
      origin: null,
      destination: null,
      isRoundTrip: null,
      isInterNational: null,
      fareDetails: null,
      traceId: null,
      passengers: null,
      flightData: null,
      flights: null,
      isPassengerDetailHidden: false,
      isBaggageHidden: false,
      isBaggageHidden2: false,
      isMealHidden: false,
      isMealHidden2: false,
      isFareHidden: false,
      mealPrice: 0,
      baggagePrice: 0,
      totalFare: 0,
      tax_charges: 0,
      tax: 0,
      discount: 0,
      otherCharges: 0,
      baseFare: 0,
      serviceFee: 0,
      fuelCharge: 0,
      currency: '',
      agencyFare: 0,
      subBaseFare: null,
      selectedMealArray: [
        [],
        []
      ],
      selectedBaggageArray: [
        [],
        []
      ],
      chooseMaximum: 0,
      passportRequired: null,
      userId: null,
      currentDate: null,
    };

    this._renderListItem = this._renderListItem.bind(this);
    // this._renderAddressDetailsView = this._renderAddressDetailsView.bind(this);
    this.validateEntryFields = this.validateEntryFields.bind(this);
    this.updateFareQuoteFromApi = this.updateFareQuoteFromApi.bind(this);

    // this.APIFlightBooking = this.APIFlightBooking.bind(this);
  }

  //----------------- Component Life cycle Methods ----------

  componentDidMount() {

    let { navigation } = this.props;
    // let flightData = Flightadata;
    // let passengers = {
    //   adult: 2,
    //   child: 2,
    //   infant: 1
    // };
    let flightData = navigation.getParam('flightdata', null);
    let passengers = navigation.getParam('passengers', null);
    let traceId = navigation.getParam('traceid', null);
    let flightType = navigation.getParam('flightType', null);
    let origin = navigation.getParam('origin', null);
    let destination = navigation.getParam('destination', null);
    let isRoundTrip = navigation.getParam('isRoundTrip', null);
    let isInterNational = navigation.getParam('isInterNational', null);
    let phone = navigation.getParam('phone', '');
    let email = navigation.getParam('email', '');
    let userId = navigation.getParam('userId', null);

    let flights = [];

    if (flightData.length == 2) {
      flightData.forEach(obj => {
        let dict = {};
        dict["index"] = obj.index;
        dict["isLCC"] = obj.isLCC;
        flights.push(dict);
      });
    } else {
      let obj = flightData[0];
      let dict = {};
      dict["index"] = obj.index;
      dict["isLCC"] = obj.isLCC;
      flights.push(dict);
    }

    console.log("DDDeTa", JSON.stringify(flightData))

    this.calculateFareDetails(flightData, isRoundTrip)
    //let baggag = [[{ "AirlineCode": "G8", "FlightNumber": "2501", "WayType": 2, "Code": "XC05", "Description": 2, "Weight": 5, "Currency": "INR", "Price": 1900, "Origin": "DEL", "Destination": "BOM" }, { "AirlineCode": "G8", "FlightNumber": "2501", "WayType": 2, "Code": "XC10", "Description": 2, "Weight": 10, "Currency": "INR", "Price": 3800, "Origin": "DEL", "Destination": "BOM" }, { "AirlineCode": "G8", "FlightNumber": "2501", "WayType": 2, "Code": "XC15", "Description": 2, "Weight": 15, "Currency": "INR", "Price": 5700, "Origin": "DEL", "Destination": "BOM" }, { "AirlineCode": "G8", "FlightNumber": "2501", "WayType": 2, "Code": "XC30", "Description": 2, "Weight": 30, "Currency": "INR", "Price": 11400, "Origin": "DEL", "Destination": "BOM" }], [{ "AirlineCode": "G8", "FlightNumber": "341", "WayType": 2, "Code": "XC05", "Description": 2, "Weight": 5, "Currency": "INR", "Price": 1900, "Origin": "BOM", "Destination": "DEL" }, { "AirlineCode": "G8", "FlightNumber": "341", "WayType": 2, "Code": "XC10", "Description": 2, "Weight": 10, "Currency": "INR", "Price": 3800, "Origin": "BOM", "Destination": "DEL" }, { "AirlineCode": "G8", "FlightNumber": "341", "WayType": 2, "Code": "XC15", "Description": 2, "Weight": 15, "Currency": "INR", "Price": 5700, "Origin": "BOM", "Destination": "DEL" }, { "AirlineCode": "G8", "FlightNumber": "341", "WayType": 2, "Code": "XC30", "Description": 2, "Weight": 30, "Currency": "INR", "Price": 11400, "Origin": "BOM", "Destination": "DEL" }]]
    this.setState({
      traceId: traceId,
      loading: !this.state.loading,
      passengers: passengers,
      flightData: flightData,
      flightType: flightType,
      origin: origin,
      destination: destination,
      isRoundTrip: isRoundTrip,
      isInterNational: isInterNational,
      phoneNumber: phone,
      email: email,
      flights: flights,
      userId: userId
      // baggageDynamic: baggag
    }, () => {
      DeviceEventEmitter.addListener(StringConstants.CONFIRM_QUOTE_EVENT, this.updateFareQuoteFromApi)
      this.calculatePassengers(passengers.adult, passengers.child, passengers.infant);
      this.APIConfirmQuote();
    });
  }

  componentWillUnmount() {
    DeviceEventEmitter.removeListener(StringConstants.CONFIRM_QUOTE_EVENT, this.updateFareQuoteFromApi)
  }

  //-------------- Date Picker Methods ----------

  APIConfirmQuote() {
    let param = {
      type: "TBO",
      flight: this.state.flights,
      traceId: this.state.traceId
    };
    console.log("param for Quotes" + JSON.stringify(param));
    this.props.confirmQuoteAction(param);
  }

  updateFareQuoteFromApi(dat) {
    //     MyAlert("Error", "Flight price has been changed. You must search again", () => { DeviceEventEmitter.emit(StringConstants.RELOAD_FLIGHT_LIST, true); NavigationServices.navigate("FlightList") })
    // return
    const success = dat.success
    const response = dat.data
    const message = dat.message
    console.log("RESP::", JSON.stringify(response))
    if (success && response) {
      isPriceChanged = false
      if (response.length > 0) {
        isPriceChanged = response[0].OB.farequote.IsPriceChanged
        if (!isPriceChanged && response.length == 2) {
          isPriceChanged = response[1].IB.farequote.IsPriceChanged
        }
      }
      if (isPriceChanged) {
        NavigationServices.navigate("FlightSearch")
        setTimeout(() => {
          MyAlert("Error", "Flight price has been changed. You must search again", () => { DeviceEventEmitter.emit(StringConstants.RELOAD_FLIGHT_LIST, true); NavigationServices.navigate("FlightList") })
        }, 500);
      }



      const baggageDynamic = this.returnBaggageItem(response)
      const mealDynamic = this.returnMealItem(response)
      this.setState({
        fareQuotes: response,
        baggageDynamic: baggageDynamic,
        mealDynamic: mealDynamic
      }, () => {
        QuoteCounter = 0
        console.log("mealDynamic:", JSON.stringify(mealDynamic));
        console.log("baggageDynamic:", JSON.stringify(baggageDynamic));
      })
    }
    else {
      if (QuoteCounter > 0) {
        QuoteCounter = 0
        setTimeout(() => {
          Alert.alert("Session Expired", "" + message ? message + "\n" : "" + "Please go back and search again", [{ text: "Dismiss", onPress: () => { NavigationServices.navigate("FlightSearch") } }], { cancelable: false })
        }, 100);
      }
      else {
        setTimeout(() => {
          Alert.alert("Error", "" + message ? message + "\n" : "Failed to fetch data", [{ text: "Try Again", onPress: () => { this.APIConfirmQuote(); QuoteCounter++; } }], { cancelable: false })
        }, 100);
      }
    }

  }

  returnBaggageItem = (response) => {
    try {
      let baggageDynamic = []
      let baggageIndexes = []
      let finalArray = []
      if (response.length > 0 && response[0].OB.ssr && response[0].OB.ssr.Baggage && response[0].OB.ssr.Baggage.length > 0) {

        response[0].OB.ssr.Baggage.forEach((item) => {
          let a = []; let indexes = []; let previousOrigin = null;
          item.forEach((item2) => {
            if (item2.Weight > 0) {
              if (previousOrigin == null || previousOrigin != item2.Origin) {
                indexes.push(a.length)
                previousOrigin = item2.Origin
              }
              item2.count = 0
              a.push(item2)
            }
          })
          if (a.length > 0) {
            baggageDynamic.push(a)
            baggageIndexes.push(indexes)
          }
        })
      }
      if (response.length > 1 && response[1].IB.ssr && response[1].IB.ssr.Baggage && response[1].IB.ssr.Baggage.length > 0 && response[1].IB.ssr.Baggage[0].length > 0) {
        let a = []; let indexes = []; let previousOrigin = null;
        response[1].IB.ssr.Baggage[0].forEach((item) => {
          if (item.Weight > 0) {
            item.count = 0
            a.push(item)
          }
        })
        if (previousOrigin == null || previousOrigin != item2.Origin) {
          indexes.push(a.length)
          previousOrigin = item2.Origin
        }
        if (a.length > 0) {
          baggageDynamic.push(a)
          baggageIndexes.push(indexes)
        }
      }
      baggageDynamic.forEach((item, index) => {
        let innerArray = []
        const l = baggageIndexes[index].length
        for (let i = 0; i < l; i++) {
          innerArray.push[[]]
        }
        baggageIndexes[index].forEach((item2, index2) => {
          innerArray[index2] = JSON.parse(JSON.stringify(item)).splice(item2, index2 + 1 == l ? item.length : baggageIndexes[index][index2 + 1])
        })
        //console.log("innerArray",JSON.stringify(innerArray))
        finalArray.push(innerArray)
      })
      return finalArray

    }
    catch (error) {
      console.log("ERROR:", JSON.stringify(error))
      const a = []
      return a
    }
  }

  returnMealItem = (response) => {
    try {
      let mealDynamic = []
      let mealIndexes = []
      let finalArray = []
      if (response && response.length > 0 && response[0].OB.ssr && response[0].OB.ssr.MealDynamic && response[0].OB.ssr.MealDynamic.length > 0
      ) {
        response[0].OB.ssr.MealDynamic.forEach((item) => {
          let a = []; let indexes = []; let previousOrigin = null;
          item.forEach((item2) => {
            if (item2.Quantity > 0) {
              if (previousOrigin == null || previousOrigin != item2.Origin) {
                indexes.push(a.length)
                previousOrigin = item2.Origin
              }
              item2.count = 0
              a.push(item2)
            }
          })
          if (a.length > 0) {
            mealDynamic.push(a)
            mealIndexes.push(indexes)
          }
        })
      }
      if (response.length > 1 && response[1].IB.ssr && response[1].IB.ssr.MealDynamic && response[1].IB.ssr.MealDynamic.length > 0 && response[1].IB.ssr.MealDynamic[0].length > 0) {
        let a = []; let indexes = []; let previousOrigin = null;
        response[1].IB.ssr.MealDynamic[0].forEach((item) => {
          if (item.Quantity > 0) {
            if (previousOrigin == null || previousOrigin != item2.Origin) {
              indexes.push(a.length)
              previousOrigin = item2.Origin
            }
            item.count = 0
            a.push(item)
          }
        })
        if (a.length > 0) {
          mealDynamic.push(a)
          mealIndexes.push(indexes)
        }
      }

      mealDynamic.forEach((item, index) => {
        let innerArray = []
        const l = mealIndexes[index].length
        for (let i = 0; i < l; i++) {
          innerArray.push[[]]
        }
        mealIndexes[index].forEach((item2, index2) => {
          innerArray[index2] = JSON.parse(JSON.stringify(item)).splice(item2, index2 + 1 == l ? item.length : mealIndexes[index][index2 + 1])
        })
        //console.log("innerArray",JSON.stringify(innerArray))
        finalArray.push(innerArray)
      })
      return finalArray
    }
    catch (error) {
      console.log("ERROR:", JSON.stringify(error))
      const a = []
      return a
    }
  }

  _hideDateTimePicker = () => this.setState({ isDateTimePickerVisible: false, isDateTimePickerExpiry: false });

  _handleDatePicked = date => {
    console.log("A date has been picked: ", date);
    let arr = this.state.passengersArray;
    let dict = arr[this.state.selectedIndexForDob];
    //dict.dob = dateFormat(date, "yyyy-mm-dd") + 'T00:00:00';
    dict.dob = moment(date).format("YYYY-MM-DD") + 'T00:00:00';
    arr[this.state.selectedIndexForDob] = dict;
    this.setState({
      passengersArray: arr,
      loading: !this.state.loading
    })
    this._hideDateTimePicker();
  };
  _handleDatePickedExpiry = date => {
    console.log("A date has been picked: ", date);
    let arr = this.state.passengersArray;
    let dict = arr[this.state.selectedIndexForDob];
    //dict.dob = dateFormat(date, "yyyy-mm-dd") + 'T00:00:00';
    dict.passport_expiry = moment(date).format("YYYY-MM-DD") + 'T00:00:00';
    arr[this.state.selectedIndexForDob] = dict;
    this.setState({
      passengersArray: arr,
      loading: !this.state.loading
    })
    this._hideDateTimePicker();
  };

  //------------ Custom Methods -----------

  calculatePassengers(numOfAdult, numOfChild, numOfInfant) {
    let passportRequired;
    if (this.props.flightSearchParams.originCountryCode == this.props.flightSearchParams.destinationCountryCode)
      passportRequired = false; else passportRequired = true;
    let arr = [];
    let adultCount = 0;
    let childCount = 0;
    let infantCount = 0;
    if (numOfAdult > 0) {
      for (i = 0; i < numOfAdult; i++) {
        let dict = {
          passenger_type: passengerType.Adult,
          index: adultCount + 1,
          title: "",
          fName: "",
          mName: "",
          lName: "",
          dob: "",
          nationality: "",
          passport_expiry: '',
          passport_no: '',
          passportrqd: passportRequired,
          // gender: "",
          dep_beg: "",
          dep_meal: ""
        };
        if (this.state.flightType == 1) {
          dict["ret_beg"] = "";
          dict["ret_meal"] = "";
        }
        arr.push(dict);
        adultCount++
      }
    }
    if (numOfChild > 0) {
      for (i = 0; i < numOfChild; i++) {
        let dict = {
          passenger_type: passengerType.child,
          index: childCount + 1,
          title: "",
          fName: "",
          mName: "",
          lName: "",
          dob: "",
          nationality: "",
          dep_beg: "",
          dep_meal: "",
          passport_expiry: '',
          passport_no: '',
          passportrqd: passportRequired
        };
        if (this.state.flightType == 1) {
          dict["ret_beg"] = "";
          dict["ret_meal"] = "";
        }
        arr.push(dict);
        childCount++
      }
    }
    if (numOfInfant > 0) {
      for (i = 0; i < numOfInfant; i++) {
        arr.push({
          ...this.state.infantDict, ...{
            passport_expiry: '',
            passport_no: '',
            passportrqd: passportRequired, index: infantCount + 1
          }
        })
        infantCount++;
      }
    }
    // console.log("Passenger Details  ", JSON.stringify(arr));
    this.setState({
      passengersArray: arr,
      loading: !this.state.loading,
      passportRequired: passportRequired
    });
  }

  onChangeText(index, text, key) {
    console.log(text)
    let dict = this.state.passengersArray[index];
    dict[key] = text;
    let arr = this.state.passengersArray;
    arr[index] = dict;
    this.setState({
      passengersArray: arr,
      loading: !this.state.loading
    });
  }

  isEmailValid(text) {
    // console.log(text);
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (reg.test(text) === false) {
      // console.log("Email is Not Correct");
      return false;
    } else {
      // console.log("Email is Correct");
      return true;
    }
  }

  validateEntryFields() {
    let arr = this.state.passengersArray;
    let isDetailsValid = true;
    console.log("Validation", JSON.stringify(arr))

    if (arr[0].title.length == 0) {
      MyAlert("Add Passenger", "Please add traveller detail");
      return;
    }

    let passportArray = []

    arr.some((item, index) => {
      if (item.title.trim().length == 0) {
        alert("Please Select Title.");
        isDetailsValid = false;
        this.setState({ isPassengerDetailHidden: false })
        return true
      }
      if (String(item.fName).trim().length == 0) {
        alert("Please Enter First Name.");
        isDetailsValid = false;
        this.setState({ isPassengerDetailHidden: false })
        return true
      }
      if (String(item.lName).trim().length == 0) {
        alert("Please Enter Last Name.");
        isDetailsValid = false;
        this.setState({ isPassengerDetailHidden: false })
        return true
      }
      if (String(item.dob).trim().length == 0) {
        alert("Please Select DOB.");
        isDetailsValid = false;
        this.setState({ isPassengerDetailHidden: false })
        return true
      }

      if (item.passenger_type === passengerType.Adult) {
        // let age = moment(new Date()).diff(item.dob, 'years');
        const age = getAge(item.dob)
        if (age <= 12) {
          alert("Adult's age must be greater than 12 years");
          console.warn(age);
          this.setState({ isPassengerDetailHidden: false })
          isDetailsValid = false;
          return true
        }
      }
      if (item.passenger_type === passengerType.child) {
        // let age = moment(new Date()).diff(item.dob, 'years');
        const age = getAge(item.dob)
        if (age > 12 || age < 2) {
          alert("Childs age must be between 2 to 12 years");
          console.warn(age);
          this.setState({ isPassengerDetailHidden: false })
          isDetailsValid = false;
          return true
        }
      }
      if (item.passenger_type === passengerType.Infant) {
        // let age = moment(new Date()).diff(item.dob, 'years');
        const age = getAge(item.dob)
        if (age > 2) {
          alert("Infant's age must be Below 2 years");
          console.warn(age);
          this.setState({ isPassengerDetailHidden: false })
          isDetailsValid = false;
          return true
        }
      }
      if (this.state.passportRequired == true) {
        if (String(item.passport_no).trim().length == 0) {
          alert("Please Enter Passport No.");
          isDetailsValid = false;
          this.setState({ isPassengerDetailHidden: false })
          return true
        }


        if (String(item.passport_expiry).trim().length == 0) {
          alert("Please Enter Passport Expiry");
          isDetailsValid = false;
          this.setState({ isPassengerDetailHidden: false })
          return true
        }
      }

      if (String(item.passport_no).trim().length > 0) {
        let filter = passportArray.filter(i => {
          return i == item.passport_no
        })
        // console.log("filter",JSON.stringify(filter))
        // console.log("passportArray",JSON.stringify(passportArray))
        if (filter.length == 0) {
          passportArray.push(item.passport_no)
          // console.log("passportArray AFTER PUSHED",JSON.stringify(passportArray))

        }
        else {
          alert("Passport numbers can't be same for different passengers");
          isDetailsValid = false;
          this.setState({ isPassengerDetailHidden: false })
          return true
        }
      }
    })

    if (!isDetailsValid) {
      this.setState({ isPassengerDetailHidden: false })
      return;
    }

    // if (this.state.email.trim().length == 0) {
    //   alert("Please enter an email.");
    //   return;
    // }

    // if (!this.isEmailValid(this.state.email)) {
    //   alert("Please enter an valid email address.");
    //   return;
    // }

    // if (this.state.phoneNumber.trim().length == 0) {
    //   alert("Please enter phone number.");
    //   return;
    // }
    // if (this.state.phoneNumber.trim().length < 10) {
    //   alert("Please enter valid phone number");
    //   return;
    // }

    this.bookTicket()


  }

  calculateFareDetails = (flightData, isRoundTrip) => {
    if (!flightData) {
      return
    }
    let fare = flightData[0].fare;
    if (fare == null) {
      return
    }
    const differenceInBaseFare = fare.baseFare - fare.agencyBaseFare
    let fareDetail = flightData[0].fareDetails;
    let fareDetail2 = null;
    let currency = fare.currency;
    let baseFare;
    let agencyBaseFare;
    let tax_charges;
    let discount;
    let totalFare = null
    let otherCharges;
    let tax;
    let serviceFee;
    let fuelCharge;
    let payableCharges;
    let subBaseFare = {
      adult: null,
      child: null,
      infant: null,
    }

    let baseFarePercentage = 100;
    if (differenceInBaseFare > 0) {
      baseFarePercentage = (differenceInBaseFare * 100) / fare.baseFare;
    }
    if (isRoundTrip) {
      console.log("Type:", "This is domestic round trip")
      fareDetail2 = flightData[1].fareDetails;
      let fare1 = flightData[0].fare;
      let fare2 = flightData[1].fare;
      const differenceInBaseFare2 = fare2.baseFare - fare2.agencyBaseFare
      agencyBaseFare = (roundOffTwo(fare1.agencyBaseFare + fare2.agencyBaseFare))
      otherCharges = (roundOffTwo(fare1.otherCharges + fare2.otherCharges))
      let discount1 = (fare1.discount == "NaN" || fare1.discount == null)
        ? "0"
        : fare1.discount;
      let discount2 = (fare2.discount == "NaN" || fare2.discount == null)
        ? "0"
        : fare2.discount;
      discount = roundOffTwo(discount1 + discount2)
      tax = (roundOffTwo((fare1.tax - (fare1.serviceFee + fare1.fuelCharge)) + (fare2.tax - (fare2.serviceFee + fare2.fuelCharge))))
      serviceFee = (roundOffTwo(fare1.serviceFee + fare2.serviceFee))
      fuelCharge = (roundOffTwo(fare1.fuelCharge + fare2.fuelCharge))
      tax_charges = roundOffTwo(fare1.tax + fare2.tax);
      totalFare = (roundOffTwo(fare1.agencyFare + fare2.agencyFare))
      payableCharges = Math.ceil(fare1.agencyFare + fare2.agencyFare)

      let baseFarePercentage2 = 100;
      if (differenceInBaseFare2 > 0) {
        baseFarePercentage2 = (differenceInBaseFare2 * 100) / fare2.baseFare;
      }
      console.log("baseFarePercentage2:", baseFarePercentage2)

      fareDetail.forEach((item, index) => {
        console.log("itemBaseFare:", item.baseFare)
        console.log("itemAgencyFare:", ((100 - baseFarePercentage) * item.baseFare) / 100)
        const data = {
          passengerCount: item.passengerCount,
          agencyBaseFare: ((100 - baseFarePercentage) * item.baseFare) / 100,
          tax: item.tax
        }
        if (item.passengerType == 1) {
          subBaseFare.adult = data
        }
        if (item.passengerType == 2) {
          subBaseFare.child = data
        }
        if (item.passengerType == 3) {
          subBaseFare.infant = data
        }
      })

      fareDetail2.forEach((item, index) => {
        const agencyBaseFare = ((100 - baseFarePercentage2) * item.baseFare) / 100
        if (item.passengerType == 1) {
          subBaseFare.adult = {
            passengerCount: item.passengerCount,
            agencyBaseFare: roundOffTwo(subBaseFare.adult.agencyBaseFare + agencyBaseFare),
            tax: subBaseFare.adult.tax + item.tax
          }
        }
        if (item.passengerType == 2) {
          subBaseFare.child = {
            passengerCount: item.passengerCount,
            agencyBaseFare: roundOffTwo(subBaseFare.child.agencyBaseFare + agencyBaseFare),
            tax: subBaseFare.child.tax + item.tax
          }
        }
        if (item.passengerType == 3) {
          subBaseFare.infant = {
            passengerCount: item.passengerCount,
            agencyBaseFare: roundOffTwo(subBaseFare.infant.agencyBaseFare + agencyBaseFare),
            tax: subBaseFare.child.tax + item.tax
          }
        }
      })

    } else {
      console.log("Type:", "This is one way domestic or International")
      let fare = flightData[0].fare;
      agencyBaseFare = roundOffTwo(fare.agencyBaseFare);
      otherCharges = roundOffTwo(fare.otherCharges);
      discount = (fare.discount == "NaN" || fare.discount == null)
        ? "0"
        : fare.discount;
      discount = roundOffTwo(discount);
      tax_charges = roundOffTwo(fare.tax);
      tax = roundOffTwo((fare.tax - (fare.serviceFee + fare.fuelCharge)))
      serviceFee = roundOffTwo(fare.serviceFee);
      fuelCharge = roundOffTwo(fare.fuelCharge);
      totalFare = roundOffTwo(fare.agencyFare);
      payableCharges = Math.ceil(fare.agencyFare)

      console.log("fareDetail", JSON.stringify(fareDetail))
      fareDetail.forEach((item, index) => {
        const data = {
          passengerCount: item.passengerCount,
          agencyBaseFare: roundOffTwo(((100 - baseFarePercentage) * item.baseFare) / 100),
          tax: item.tax
        }
        if (item.passengerType == 1) {
          subBaseFare.adult = data
        }
        if (item.passengerType == 2) {
          subBaseFare.child = data
        }
        if (item.passengerType == 3) {
          subBaseFare.infant = data
        }
      })
      console.log("fareDetail", JSON.stringify(fareDetail))


    }
    const chooseMaximum = ((subBaseFare.adult && subBaseFare.adult.passengerCount) ? parseInt(subBaseFare.adult.passengerCount) : 0) + ((subBaseFare.child && subBaseFare.child.passengerCount) ? parseInt(subBaseFare.child.passengerCount) : 0)
    this.setState({
      totalFare,
      tax_charges,
      tax,
      discount,
      otherCharges,
      agencyBaseFare,
      serviceFee,
      fuelCharge,
      currency,
      subBaseFare,
      chooseMaximum
    })
  }



  _renderListItem = (item, index) => {
    return (
      <View style={{ width: '100%' }}>
        <View style={{ width: '100%', backgroundColor: '#f6f6f6', padding: 10 }}>
          <Text style={{ fontFamily: Fonts.medium, fontSize: 13, color: colors.colorBlack62 }}>{item.passenger_type + " " + item.index}</Text>
        </View>
        <View style={{ paddingHorizontal: 12 }}>
          <View style={{ width: '100%', flexDirection: 'row' }}>
            <View style={{ width: '50%', padding: 5 }}>
              <Dropdown placeholder={"Title"}
                placeholderTextColor={"grey"}
                data={item.passenger_type == passengerType.Adult ? Titles.adult : Titles.child}
                fontSize={13}
                fontFamily={Fonts.regular}
                value={item.title}
                containerStyle={{
                  width: "100%",
                }}
                inputContainerStyle={{
                  borderBottomColor: 'grey',
                  borderBottomWidth: 1,
                  padding: 4,
                  height: 34,
                  margin: 4,
                }}
                pickerStyle={{
                  width: "50%",
                  marginLeft: 10,
                  marginTop: 40,
                }}
                itemTextStyle={{ color: "grey", fontSize: 13, height: 34, fontFamily: Fonts.regular }}
                itemColor={"grey"}
                dropdownOffset={{ top: 10, left: 0 }}
                onChangeText={text => this.onChangeText(index, text, "title")}
              />
            </View>
            <View style={{ width: '50%', padding: 5 }}>
              <TextInput
                style={{
                  height: 34,
                  margin: 4,
                  fontSize: 13,
                  padding: 4,
                  fontFamily: Fonts.regular,
                  borderBottomColor: 'grey',
                  borderBottomWidth: 1
                }}
                value={item.fName}
                onChangeText={text => this.onChangeText(index, text, "fName")}
                placeholder={'First Name'}
                autoCapitalize={"none"}
                autoCorrect={false}
                placeholderTextColor={'grey'}
              />
            </View>
          </View>
          <View style={{ width: '100%', flexDirection: 'row' }}>
            <View style={{ width: '50%', padding: 5 }}>
              <TextInput
                style={{
                  height: 34,
                  margin: 4,
                  fontSize: 13,
                  padding: 4,
                  fontFamily: Fonts.regular,
                  borderBottomColor: 'grey',
                  borderBottomWidth: 1
                }}
                value={item.lName}
                onChangeText={text => this.onChangeText(index, text, "lName")}
                placeholder={'Last Name'}
                autoCapitalize={"none"}
                autoCorrect={false}
                placeholderTextColor={'grey'}
              />
            </View>
            <View style={{ width: '50%', padding: 5 }}>
              <TouchableOpacity style={{
                height: 34,
                margin: 4,
                padding: 4,
                fontFamily: Fonts.regular,
                borderBottomColor: 'grey',
                flexDirection: 'row',
                borderBottomWidth: 1
              }} onPress={() => {
                this.setState({
                  selectedIndexForDob: index,
                  isDateTimePickerVisible: true,
                  currentDate: (item.dob && item.dob != '') ? moment(item.dob).toDate() : null
                });
              }}>
                <Text style={{
                  height: 34,
                  fontSize: 13,
                  padding: 4,
                  // backgroundColor: 'red',
                  fontFamily: Fonts.regular,
                  color: !item.dob ? 'grey' : null,
                }}>{item.dob ? Moment(item.dob).format("DD-MM-YYYY") : "Date of Birth"}</Text>
                <View style={{
                  flex: 1, height: 30, paddingBottom: 4,
                  //backgroundColor: 'blue',
                  alignItems: 'flex-end', justifyContent: 'center'
                }}>
                  <Image tintColor={colors.colorBlack21} style={{ width: 15, height: 15, resizeMode: 'contain' }} source={Images.ic_calander} />
                </View>
              </TouchableOpacity>
            </View>
          </View>
          <View style={{ width: '100%', flexDirection: 'row' }}>
            <View style={{ width: '100%', padding: 5 }}>
              <TextInput
                style={{
                  height: 34,
                  margin: 4,
                  fontSize: 13,
                  padding: 4,
                  fontFamily: Fonts.regular,
                  borderBottomColor: 'grey',
                  borderBottomWidth: 1
                }}
                value={item.passport_no}
                onChangeText={text => this.onChangeText(index, text.replace(/[^A-Za-z0-9]/g, ''), "passport_no")}
                placeholder={'Passport No (' + (item.passportrqd ? 'Required' : 'Optional') + ')'}
                autoCapitalize={'characters'}
                autoCorrect={false}
                placeholderTextColor={'grey'}
              />
            </View>
          </View>
          <View style={{ width: '100%', flexDirection: 'row' }}>
            <View style={{ width: '100%', padding: 5 }}>
              <TouchableOpacity style={{
                height: 34,
                margin: 4,
                padding: 4,
                fontFamily: Fonts.regular,
                borderBottomColor: 'grey',
                flexDirection: 'row',
                borderBottomWidth: 1
              }} onPress={() => {
                this.setState({
                  selectedIndexForDob: index,
                  isDateTimePickerExpiry: true,
                  currentDate: (item.passport_expiry && item.passport_expiry != '') ? moment(item.passport_expiry).toDate() : null
                });

              }}>
                <Text style={{
                  height: 34,
                  fontSize: 13,
                  padding: 4,
                  // backgroundColor: 'red',
                  fontFamily: Fonts.regular,
                  color: !item.passport_expiry ? 'grey' : null,
                }}>{item.passport_expiry ? Moment(item.passport_expiry).format("DD-MM-YYYY") : "Passport Expiry"}</Text>
                <View style={{
                  flex: 1, height: 30, paddingBottom: 4,
                  //backgroundColor: 'blue',
                  alignItems: 'flex-end', justifyContent: 'center'
                }}>
                  <Image tintColor={colors.colorBlack21} style={{ width: 15, height: 15, resizeMode: 'contain' }} source={Images.ic_calander} />
                </View>
              </TouchableOpacity>
            </View>
          </View>


        </View>
      </View>
    )
  }

  renderPassengerDetails = () => {
    const { isPassengerDetailHidden, passengersArray } = this.state
    return (
      <CardView style={{
        backgroundColor: colors.colorWhite,
        margin: 10,
      }}
        cardElevation={2}
        cardMaxElevation={3}
        cornerRadius={5}>
        <View style={{ borderRadius: 5, overflow: "hidden" }}>

          <TouchableOpacity onPress={() => this.setState({ isPassengerDetailHidden: !isPassengerDetailHidden })} style={{ padding: 10, alignItems: 'center', flexDirection: 'row' }}>
            <Text style={{ fontFamily: Fonts.medium, fontSize: 16, color: colors.colorBlack21 }}>Passenger Detail</Text>
            <View style={{ justifyContent: 'center', flex: 1, alignItems: 'flex-end', paddingEnd: 10 }}>
              <Image style={{ width: 15, height: 15, resizeMode: 'contain' }} source={isPassengerDetailHidden ? Images.ic_closed : Images.ic_opened} />
            </View>
          </TouchableOpacity>
          {!isPassengerDetailHidden && passengersArray.map(
            (item, index) => this._renderListItem(item, index)
          )}
        </View>
      </CardView>
    )
  }

  renderContactDetails = () => {
    const { phoneNumber, email } = this.state

    return (
      <CardView style={{
        backgroundColor: colors.colorWhite,
        margin: 10,
      }}
        cardElevation={2}
        cardMaxElevation={3}
        cornerRadius={5}>
        <View style={{ borderRadius: 5, overflow: "hidden" }}>

          <View style={{ paddingHorizontal: 10, paddingTop: 10, paddingBottom: 0, alignItems: 'center', flexDirection: 'row' }}>
            <Text style={{ fontFamily: Fonts.medium, fontSize: 16, color: colors.colorBlack21 }}>Contact Detail</Text>
          </View>

          <View style={{ paddingHorizontal: 15, paddingBottom: 10, paddingTop: 5 }}>
            <View style={{ width: '100%', flexDirection: 'row', paddingVertical: 5, }}>

              <Text style={{
                fontSize: 13,
                paddingLeft: 10,
                //backgroundColor: 'red',
                fontFamily: Fonts.bold,
                alignSelf: 'flex-start',
                color: colors.colorBlack62,
              }}>{"Email :"}</Text>

              <Text style={{
                fontSize: 13,
                paddingLeft: 10,
                flex: 1,
                alignSelf: 'baseline',
                // backgroundColor: 'blue',
                fontFamily: Fonts.regular,
                color: colors.colorBlack21,
              }}>{email}</Text>

            </View>
            <View style={{ width: '100%', flexDirection: 'row', paddingVertical: 5, }}>
              <Text style={{
                fontSize: 13,
                paddingLeft: 10,
                //backgroundColor: 'red',
                fontFamily: Fonts.bold,
                alignSelf: 'flex-start',
                color: colors.colorBlack62,
              }}>{"Phone :"}</Text>

              <Text style={{
                fontSize: 13,
                paddingLeft: 10,
                flex: 1,
                alignSelf: 'baseline',
                // backgroundColor: 'blue',
                fontFamily: Fonts.regular,
                color: colors.colorBlack21,
              }}>{phoneNumber}</Text>
            </View>
          </View>
        </View>
      </CardView>
    )
  }

  renderDatePickerModal = () => {

    return (
      <DateTimePicker
        isVisible={this.state.isDateTimePickerVisible}
        onConfirm={this._handleDatePicked}
        maximumDate={new Date()}
        date={this.state.currentDate || new Date()}
        onCancel={this._hideDateTimePicker}
      />
    )
  }

  renderPassportExpiryModal = () => {
    return (
      <DateTimePicker
        isVisible={this.state.isDateTimePickerExpiry}
        onConfirm={this._handleDatePickedExpiry}
        minimumDate={new Date()}
        date={this.state.currentDate || new Date()}
        onCancel={this._hideDateTimePicker}
      />
    )
  }

  renderBaggage = (i) => {
    const { baggageDynamic, chooseMaximum, selectedBaggageArray } = this.state
    if (baggageDynamic && baggageDynamic.length > i && baggageDynamic[i] && baggageDynamic[i].length > 0) {
      return baggageDynamic[i].map((myItem, myIndex) => {
        const origin = myItem[0].Origin
        let count = 0;
        count = selectedBaggageArray[i].filter(i => JSON.stringify(i.Origin) == JSON.stringify(origin)).length;
        const upto = chooseMaximum - count
        if (myItem && myItem.length > i) {
          return (
            <CardView style={{
              backgroundColor: colors.colorWhite,
              margin: 10,
            }}
              cardElevation={2}
              cardMaxElevation={3}
              cornerRadius={5}>
              <View style={{ borderRadius: 5, overflow: "hidden" }}>
                <TouchableOpacity onPress={() => {
                  this.setState({ [(i + myIndex) + "b"]: !this.state[(i + myIndex) + "b"] })
                }} style={{ padding: 10, alignItems: 'center', flexDirection: 'row' }}>
                  <Text style={{ fontFamily: Fonts.medium, fontSize: 16, color: colors.colorBlack21 }}>Baggage</Text>
                  <Text style={{ fontFamily: Fonts.medium, fontSize: 14, color: colors.colorBlack62 }}>{" (" + myItem[0].Origin + "-" + myItem[0].Destination + ") "}</Text>
                  <Text style={{ fontFamily: Fonts.medium, fontSize: 11, color: colors.colorBlack62, alignSelf: 'center' }}>{"  Choose upto " + upto}</Text>
                  <View style={{ justifyContent: 'center', flex: 1, alignItems: 'flex-end', paddingEnd: 10 }}>
                    <View >
                      <Image style={{ width: 15, height: 15, resizeMode: 'contain' }} source={this.state[(i + myIndex) + "b"] ? Images.ic_closed : Images.ic_opened} />
                    </View>
                  </View>
                </TouchableOpacity>
                {(!this.state[(i + myIndex) + "b"]) ? myItem.map(
                  (item, index) => this._renderBaggageItem(item, index, i, myIndex, count)
                ) : null}
              </View>
            </CardView>
          )
        }
      })
    }
  }

  _renderBaggageItem = (item, index, i, myIndex, count) => {
    return (
      <View style={{ paddingHorizontal: 15, paddingVertical: 10, width: '100%', flexDirection: 'row' }}>
        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-start', flex: 1, paddingRight: 5 }}>
          <Image style={{ width: 20, height: 20, resizeMode: 'contain' }} source={Images.ic_check_in} />
          <Text style={{ fontFamily: Fonts.medium, color: colors.colorBlack21, fontSize: 14, paddingLeft: 8, flexWrap: "wrap", flex: 1 }}>{item.Weight + " KG"}</Text>
        </View>
        <View style={{ flexDirection: 'row', flex: 1, alignItems: 'center', justifyContent: 'flex-end' }}>
          <Text style={{ fontFamily: Fonts.medium, color: colors.colorBlack21, fontSize: 12, paddingRight: 8 }}>{item.Currency + " " + item.Price}</Text>
          <Counter onPressAdd={() => this.onItemAdd(1, index, item.Price, i, myIndex, count)} onPressRemove={() => this.onItemRemove(1, index, item.Price, i, myIndex)} count={item.count} />
        </View>
      </View>
    )
  }

  renderMeal = (i) => {
    const { mealDynamic, chooseMaximum, selectedMealArray } = this.state
    // console.log("d1", mealDynamic)
    if (mealDynamic && mealDynamic.length > i && mealDynamic[i] && mealDynamic[i].length > 0) {
      // console.log("d2")

      return mealDynamic[i].map((myItem, myIndex) => {
        // console.log("d3")
        const origin = myItem[0].Origin
        let count = 0;
        //   for (let j = 0; i < selectedMealArray[i].length; ++i) {
        //    console.log("SelectedMealArray"+ JSON.stringify(selectedMealArray[i][j]))
        //     if (selectedMealArray[i][j] ==origin){
        //       count++;
        //   }
        // }
        count = selectedMealArray[i].filter(i => JSON.stringify(i.Origin) == JSON.stringify(origin)).length;
        const upto = chooseMaximum - count
        if (myItem && myItem.length > i) {
          // console.log("d4", JSON.stringify("origin:" + origin + " " + count))
          return (
            <CardView style={{
              backgroundColor: colors.colorWhite,
              margin: 10,
            }}
              cardElevation={2}
              cardMaxElevation={3}
              cornerRadius={5}>
              <View style={{ borderRadius: 5, overflow: "hidden" }}>
                <TouchableOpacity onPress={() => {
                  this.setState({ [i + myIndex]: !this.state[i + myIndex] })
                }} style={{ padding: 10, alignItems: 'center', flexDirection: 'row' }}>
                  <Text style={{ fontFamily: Fonts.medium, fontSize: 16, color: colors.colorBlack21 }}>Meal</Text>
                  <Text style={{ fontFamily: Fonts.medium, fontSize: 14, color: colors.colorBlack62 }}>{" (" + myItem[0].Origin + "-" + myItem[0].Destination + ") "}</Text>
                  <Text style={{ fontFamily: Fonts.medium, fontSize: 11, color: colors.colorBlack62, alignSelf: 'center' }}>{"  Choose upto " + upto}</Text>
                  <View style={{ justifyContent: 'center', flex: 1, alignItems: 'flex-end', paddingEnd: 10 }}>
                    <View >
                      <Image style={{ width: 15, height: 15, resizeMode: 'contain' }} source={this.state[i + myIndex] ? Images.ic_closed : Images.ic_opened} />
                    </View>
                  </View>
                </TouchableOpacity>
                {(!this.state[i + myIndex]) ? myItem.map(
                  (item, index) => this._renderMealItem(item, index, i, myIndex, count)
                ) : null}
              </View>
            </CardView>
          )
        }

      })
    }
  }


  _renderMealItem = (item, index, i, myIndex, count) => {
    return (
      <View style={{ paddingHorizontal: 15, paddingVertical: 10, width: '100%', flexDirection: 'row' }}>
        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-start', flex: 1, paddingRight: 5 }}>
          <Image style={{ width: 20, height: 20, resizeMode: 'contain' }} source={Images.ic_meal} />
          <Text style={{ fontFamily: Fonts.medium, color: colors.colorBlack21, fontSize: 11, paddingHorizontal: 8, flexWrap: "wrap", flex: 1 }}>{item.AirlineDescription}</Text>
        </View>
        <View style={{ flexDirection: 'row', flex: 1, alignItems: 'center', justifyContent: 'flex-end' }}>
          <Text style={{ fontFamily: Fonts.medium, color: colors.colorBlack21, fontSize: 12, paddingRight: 8 }}>{item.Currency + " " + item.Price}</Text>
          <Counter onPressAdd={() => this.onItemAdd(2, index, item.Price, i, myIndex, count)} onPressRemove={() => this.onItemRemove(2, index, item.Price, i, myIndex)} count={item.count} />
        </View>

      </View>
    )
  }

  onItemAdd = (type, index, price, i, myIndex, count) => {
    let { mealPrice, baggagePrice, chooseMaximum, baggageDynamic, mealDynamic, selectedBaggageArray, selectedMealArray, totalFare } = this.state

    if (type == 1 && count < chooseMaximum) {
      baggageDynamic[i][myIndex][index].count = (baggageDynamic[i][myIndex][index].count) + 1
      selectedBaggageArray[i].push(JSON.parse(JSON.stringify(baggageDynamic[i][myIndex][index])))
      this.setState({ baggageDynamic: baggageDynamic, baggagePrice: baggagePrice + price, totalFare: totalFare + price, selectedBaggageArray })
    }
    if (type == 2 && count < chooseMaximum) {

      mealDynamic[i][myIndex][index].count = (mealDynamic[i][myIndex][index].count) + 1
      selectedMealArray[i].push(JSON.parse(JSON.stringify(mealDynamic[i][myIndex][index])))
      this.setState({ mealDynamic: mealDynamic, mealPrice: mealPrice + price, totalFare: totalFare + price, selectedMealArray })

    }
  }

  onItemRemove = (type, index, price, i, myIndex) => {
    let { mealPrice, baggagePrice, baggageDynamic, mealDynamic, selectedBaggageArray, selectedMealArray, totalFare } = this.state
    if (type == 1 && (baggageDynamic[i][myIndex][index].count > 0)) {
      selectedBaggageArray[i] = selectedBaggageArray[i].filter((item) => {
        return !(JSON.stringify(item) == JSON.stringify(baggageDynamic[i][myIndex][index]));
      })
      baggageDynamic[i][myIndex][index].count = (baggageDynamic[i][myIndex][index].count) - 1
      this.setState({ baggageDynamic: baggageDynamic, baggagePrice: baggagePrice - price, totalFare: totalFare - price, selectedBaggageArray })
    }
    if (type == 2 && (mealDynamic[i][myIndex][index].count > 0)) {
      selectedMealArray[i] = selectedMealArray[i].filter((item) => {
        return JSON.stringify(item) != JSON.stringify(mealDynamic[i][myIndex][index]);
      })
      mealDynamic[i][myIndex][index].count = (mealDynamic[i][myIndex][index].count) - 1
      this.setState({ mealDynamic: mealDynamic, mealPrice: mealPrice - price, totalFare: totalFare - price, selectedMealArray })
    }
  }

  renderFareDetails = () => {
    const { isFareHidden, agencyBaseFare, currency, subBaseFare, tax, tax_charges, fuelCharge, serviceFee, otherCharges, mealDynamic, baggageDynamic, totalFare, mealPrice, baggagePrice } = this.state
    let addOnPrice = (mealPrice + baggagePrice)
    if (addOnPrice < 100) addOnPrice = addOnPrice.toFixed(2)
    return (
      <CardView style={{
        backgroundColor: colors.colorWhite,
        margin: 10,
      }}
        cardElevation={2}
        cardMaxElevation={3}
        cornerRadius={5}>
        <View style={{ borderRadius: 5, overflow: "hidden" }}>
          <TouchableOpacity onPress={() => this.setState({ isFareHidden: !isFareHidden })} style={{ padding: 10, alignItems: 'center', flexDirection: 'row' }}>
            <Text style={{ fontFamily: Fonts.medium, fontSize: 16, color: colors.colorBlack21 }}>Fare Details</Text>
            <View style={{ justifyContent: 'center', flex: 1, alignItems: 'flex-end', paddingEnd: 10 }}>
              <Image style={{ width: 15, height: 15, resizeMode: 'contain' }} source={isFareHidden ? Images.ic_closed : Images.ic_opened} />
            </View>
          </TouchableOpacity>
          {!isFareHidden &&
            <>
              <View style={{ height: 1, backgroundColor: colors.lightgrey, width: '100%' }} />
              <View style={{ paddingHorizontal: 10 }}>
                <View style={{ paddingVertical: 10 }}>
                  <View style={styles.fullView}>
                    <View style={{ justifyContent: 'center', alignItems: 'flex-start', width: '60%' }}>
                      <Text style={{ fontFamily: Fonts.medium, color: colors.colorBlack21, fontSize: 15, paddingHorizontal: 2 }}>Base Fare</Text>
                    </View>
                    <View style={{ justifyContent: 'center', alignItems: 'flex-end', width: '40%' }}>
                      <Text style={{ fontFamily: Fonts.medium, color: colors.colorBlack21, fontSize: 15, paddingHorizontal: 2 }}>{currency + " " + agencyBaseFare}</Text>
                    </View>
                  </View>
                  {subBaseFare &&
                    <>
                      {subBaseFare.adult && <View style={[styles.fullView, { paddingTop: 8, paddingLeft: 10 }]}>
                        <View style={{ justifyContent: 'center', alignItems: 'flex-start', width: '60%' }}>
                          <Text style={{ fontFamily: Fonts.medium, color: 'grey', fontSize: 14, paddingHorizontal: 2, }}>{subBaseFare.adult.passengerCount + (subBaseFare.adult.passengerCount > 1 ? " Adults" : " Adult")}</Text>
                        </View>

                        <View style={{ justifyContent: 'center', alignItems: 'flex-end', width: '40%' }}>
                          <Text style={{ fontFamily: Fonts.medium, color: 'grey', fontSize: 14, paddingHorizontal: 2 }}>{currency + " " + subBaseFare.adult.agencyBaseFare}</Text>
                        </View>
                      </View>}

                      {subBaseFare.child && <View style={[styles.fullView, { paddingTop: 8, paddingLeft: 10 }]}>
                        <View style={{ justifyContent: 'center', alignItems: 'flex-start', width: '60%' }}>
                          <Text style={{ fontFamily: Fonts.medium, color: 'grey', fontSize: 14, paddingHorizontal: 2, }}>{subBaseFare.child.passengerCount + (subBaseFare.child.passengerCount > 1 ? " Children" : " Child")}</Text>
                        </View>

                        <View style={{ justifyContent: 'center', alignItems: 'flex-end', width: '40%' }}>
                          <Text style={{ fontFamily: Fonts.medium, color: 'grey', fontSize: 14, paddingHorizontal: 2 }}>{currency + " " + subBaseFare.child.agencyBaseFare}</Text>
                        </View>
                      </View>}

                      {subBaseFare.infant && <View style={[styles.fullView, { paddingTop: 8, paddingLeft: 10 }]}>
                        <View style={{ justifyContent: 'center', alignItems: 'flex-start', width: '60%' }}>
                          <Text style={{ fontFamily: Fonts.medium, color: 'grey', fontSize: 14, paddingHorizontal: 2, }}>{subBaseFare.infant.passengerCount + (subBaseFare.infant.passengerCount > 1 ? " Infants" : " Infant")}</Text>
                        </View>

                        <View style={{ justifyContent: 'center', alignItems: 'flex-end', width: '40%' }}>
                          <Text style={{ fontFamily: Fonts.medium, color: 'grey', fontSize: 14, paddingHorizontal: 2 }}>{currency + " " + subBaseFare.infant.agencyBaseFare}</Text>
                        </View>
                      </View>}
                    </>}


                </View>
                <View style={{ height: 1, backgroundColor: colors.lightgrey, width: '100%' }} />
                <View style={{ paddingVertical: 10 }}>
                  <View style={styles.fullView}>
                    <View style={{ justifyContent: 'center', alignItems: 'flex-start', width: '60%' }}>
                      <Text style={{ fontFamily: Fonts.medium, color: colors.colorBlack21, fontSize: 15, paddingHorizontal: 2 }}>Taxes & Surcharges</Text>
                    </View>
                    <View style={{ justifyContent: 'center', alignItems: 'flex-end', width: '40%' }}>
                      <Text style={{ fontFamily: Fonts.medium, color: colors.colorBlack21, fontSize: 15, paddingHorizontal: 2 }}>{currency + " " + tax_charges}</Text>
                    </View>
                  </View>
                  <View style={[styles.fullView, { paddingTop: 8, paddingLeft: 10 }]}>
                    <View style={{ justifyContent: 'center', alignItems: 'flex-start', width: '60%' }}>
                      <Text style={{ fontFamily: Fonts.medium, color: 'grey', fontSize: 14, paddingHorizontal: 2, }}>{"Tax"}</Text>
                    </View>
                    <View style={{ justifyContent: 'center', alignItems: 'flex-end', width: '40%' }}>
                      <Text style={{ fontFamily: Fonts.medium, color: 'grey', fontSize: 14, paddingHorizontal: 2 }}>{currency + " " + (tax < 100 ? tax.toFixed(2) : tax)}</Text>
                    </View>
                  </View>
                  <View style={[styles.fullView, { paddingTop: 8, paddingLeft: 10 }]}>
                    <View style={{ justifyContent: 'center', alignItems: 'flex-start', width: '60%' }}>
                      <Text style={{ fontFamily: Fonts.medium, color: 'grey', fontSize: 14, paddingHorizontal: 2, }}>{"Passenger Service Fee"}</Text>
                    </View>

                    <View style={{ justifyContent: 'center', alignItems: 'flex-end', width: '40%' }}>
                      <Text style={{ fontFamily: Fonts.medium, color: 'grey', fontSize: 14, paddingHorizontal: 2 }}>{currency + " " + (serviceFee < 100 ? serviceFee.toFixed(2) : serviceFee)}</Text>
                    </View>
                  </View>
                  <View style={[styles.fullView, { paddingTop: 8, paddingLeft: 10 }]}>
                    <View style={{ justifyContent: 'center', alignItems: 'flex-start', width: '60%' }}>
                      <Text style={{ fontFamily: Fonts.medium, color: 'grey', fontSize: 14, paddingHorizontal: 2, }}>{"Airline Fuel Surcharge"}</Text>
                    </View>

                    <View style={{ justifyContent: 'center', alignItems: 'flex-end', width: '40%' }}>
                      <Text style={{ fontFamily: Fonts.medium, color: 'grey', fontSize: 14, paddingHorizontal: 2 }}>{currency + " " + (fuelCharge < 100 ? fuelCharge.toFixed(2) : fuelCharge)}</Text>
                    </View>
                  </View>
                </View>

                {mealDynamic.length > 0 || baggageDynamic.length > 0 ?
                  <>
                    <View style={{ height: 1, backgroundColor: colors.lightgrey, width: '100%' }} />
                    <View style={{ paddingVertical: 10 }}>
                      <View style={styles.fullView}>
                        <View style={{ justifyContent: 'center', alignItems: 'flex-start', width: '60%' }}>
                          <Text style={{ fontFamily: Fonts.medium, color: colors.colorBlack21, fontSize: 15, paddingHorizontal: 2 }}>Add On Charges</Text>
                        </View>
                        <View style={{ justifyContent: 'center', alignItems: 'flex-end', width: '40%' }}>
                          <Text style={{ fontFamily: Fonts.medium, color: colors.colorBlack21, fontSize: 15, paddingHorizontal: 2 }}>{currency + " " + addOnPrice}</Text>
                        </View>
                      </View>
                      <View style={[styles.fullView, { paddingTop: 8, paddingLeft: 10 }]}>
                        <View style={{ justifyContent: 'center', alignItems: 'flex-start', width: '60%' }}>
                          <Text style={{ fontFamily: Fonts.medium, color: 'grey', fontSize: 14, paddingHorizontal: 2, }}>{"Meal"}</Text>
                        </View>
                        <View style={{ justifyContent: 'center', alignItems: 'flex-end', width: '40%' }}>
                          <Text style={{ fontFamily: Fonts.medium, color: 'grey', fontSize: 14, paddingHorizontal: 2 }}>{currency + " " + (mealPrice < 100 ? mealPrice.toFixed(2) : mealPrice)}</Text>
                        </View>
                      </View>
                      <View style={[styles.fullView, { paddingTop: 8, paddingLeft: 10 }]}>
                        <View style={{ justifyContent: 'center', alignItems: 'flex-start', width: '60%' }}>
                          <Text style={{ fontFamily: Fonts.medium, color: 'grey', fontSize: 14, paddingHorizontal: 2, }}>{"Baggage"}</Text>
                        </View>

                        <View style={{ justifyContent: 'center', alignItems: 'flex-end', width: '40%' }}>
                          <Text style={{ fontFamily: Fonts.medium, color: 'grey', fontSize: 14, paddingHorizontal: 2 }}>{currency + " " + (baggagePrice < 100 ? baggagePrice.toFixed(2) : baggagePrice)}</Text>
                        </View>
                      </View>
                    </View>
                  </> : null}
                <View style={{ height: 1, backgroundColor: colors.lightgrey, width: '100%' }} />
                <View style={{ paddingVertical: 10 }}>
                  <View style={styles.fullView}>
                    <View style={{ justifyContent: 'center', alignItems: 'flex-start', width: '60%' }}>
                      <Text style={{ fontFamily: Fonts.medium, color: colors.colorBlack21, fontSize: 15, paddingHorizontal: 2 }}>Other Charges</Text>
                    </View>
                    <View style={{ justifyContent: 'center', alignItems: 'flex-end', width: '40%' }}>
                      <Text style={{ fontFamily: Fonts.medium, color: colors.colorBlack21, fontSize: 15, paddingHorizontal: 2 }}>{currency + " " + (otherCharges < 100 ? otherCharges.toFixed(2) : otherCharges)}</Text>
                    </View>
                  </View>
                </View>

                <View style={{ height: 1, backgroundColor: colors.lightgrey, width: '100%' }} />
                <View style={{ paddingVertical: 10 }}>
                  <View style={styles.fullView}>
                    <View style={{ justifyContent: 'center', alignItems: 'flex-start', width: '60%' }}>
                      <Text style={{ fontFamily: Fonts.medium, color: colors.colorBlack21, fontSize: 15, paddingHorizontal: 2 }}>Total Fare</Text>
                    </View>
                    <View style={{ justifyContent: 'center', alignItems: 'flex-end', width: '40%' }}>
                      <Text style={{ fontFamily: Fonts.medium, color: colors.colorBlack21, fontSize: 15, paddingHorizontal: 2 }}>{currency + " " + (totalFare < 100 ? totalFare.toFixed(2) : roundOffTwo(totalFare))}</Text>
                    </View>
                  </View>
                </View>

                <View style={{ height: 1, backgroundColor: colors.lightgrey, width: '100%' }} />
                <View style={{ paddingVertical: 10 }}>
                  <View style={styles.fullView}>
                    <View style={{ justifyContent: 'center', alignItems: 'flex-start', width: '60%' }}>
                      <Text style={{ fontFamily: Fonts.medium, color: colors.colorBlack21, fontSize: 15, paddingHorizontal: 2 }}>Payable Fare</Text>
                    </View>
                    <View style={{ justifyContent: 'center', alignItems: 'flex-end', width: '40%' }}>
                      <Text style={{ fontFamily: Fonts.medium, color: colors.colorBlack21, fontSize: 15, paddingHorizontal: 2 }}>{currency + " " + (Math.ceil(totalFare))}</Text>
                    </View>
                  </View>
                </View>

              </View>
            </>}
        </View>
      </CardView>
    )

  }

  bookTicket = () => {
    let flightData = this.state.flightData;
    console.log(JSON.stringify(flightData))
    let flights = [];
    if (flightData.length == 2) {
      flightData.forEach(obj => {
        flights.push(obj.index);
      });
    } else {
      let obj = flightData[0];
      flights.push(obj.index);
    }
    this.APIFlightBooking(flights);
  }

  APIFlightBooking(indexes) {
    console.log("fareQuotes:", JSON.stringify(this.state.fareQuotes))
    const price = Math.ceil(this.state.totalFare);
    const flightData = this.state.flightData;

    let finalDict = {};
    let selectedBaggageArray = JSON.parse(JSON.stringify(this.state.selectedBaggageArray))
    let selectedMealArray = JSON.parse(JSON.stringify(this.state.selectedMealArray))

    selectedBaggageArray.forEach((item) => {
      item.forEach((i2) => {
        delete i2['count'];
      })
    })
    selectedMealArray.forEach((item) => {
      item.forEach((i2) => {
        delete i2['count'];
      })
    })

    console.log(JSON.stringify(selectedBaggageArray))
    console.log(JSON.stringify(selectedMealArray))
    if (this.state.fareQuotes.length == 1) {
      let fareQuote = this.state.fareQuotes[0].OB.farequote;
      if (flightData[0].flightSegment.length > 1) {
        finalDict["OB"] = {
          Status: fareQuote.IsLCC,
          Baggage: selectedBaggageArray,
          MealDynamic: selectedMealArray,
          farequote: fareQuote
        };
      } else {
        finalDict["OB"] = {
          Status: fareQuote.IsLCC,
          Baggage: selectedBaggageArray[0],
          MealDynamic: selectedMealArray[0],
          farequote: fareQuote
        };
      }
      finalDict["IB"] = {
        Status: false,
        Baggage: "",
        MealDynamic: "",
        farequote: ""
      };
    } else {
      let OBfareQuote = this.state.fareQuotes[0].OB.farequote;
      let IBfareQuote = this.state.fareQuotes[1].IB.farequote;

      finalDict["OB"] = {
        Status: OBfareQuote.IsLCC,
        Baggage: selectedBaggageArray[0],
        MealDynamic: selectedMealArray[0],
        farequote: OBfareQuote
      };
      finalDict["IB"] = {
        Status: IBfareQuote.IsLCC,
        Baggage: selectedBaggageArray[1],
        MealDynamic: selectedMealArray[1],
        farequote: IBfareQuote
      };
    }

    let adlt = [];
    let child = [];
    let infant = [];
    this.state.passengersArray.forEach((obj) => {
      let passengerDict = {
        title: "",
        fname: "",
        mname: "",
        lname: "",
        dob: "",
        dep_baggage: "0",
        ret_baggage: "0",
        dep_meal: "0",
        ret_meal: "0",
        passport_no: "",
        passport_expiry: "",
        passportrqd: false

      }
      passengerDict.fname = obj.fName;
      passengerDict.mname = obj.mName;
      passengerDict.lname = obj.lName;
      passengerDict.dob = obj.dob;
      passengerDict.passport_no = obj.passport_no;
      passengerDict.passport_expiry = obj.passport_expiry;
      passengerDict.passportrqd = obj.passportrqd;
      passengerDict.title = String(obj.title)
      if (obj.passenger_type === passengerType.Adult) {
        adlt.push(passengerDict);
      } else if (obj.passenger_type === passengerType.child) {
        child.push(passengerDict);
      } else {
        infant.push(passengerDict);
      }
    })
    let param = {
      id: this.state.userId,
      email: this.state.email,
      mobile: this.state.phoneNumber,
      ADT: adlt,
      CHD: child,
      INF: infant,
      attributes: {
        traceId: this.state.traceId,
        index: indexes
      },
      type: "TBO",
      fQuoteSSR: finalDict,
      final_booking_amount: price,
      orderID: "",
      captureID: "",
      isFlight: true,
      price: {
        amount: price,
        currency: this.state.currency
      }
    };
    // console.clear();
    console.log("BOOK_FLIGHT_REQ:")
    console.log(JSON.stringify(param));
    NavigationServices.navigate("PaymentScreen", { data: param, isFlight: true });
    // this.props.bookFlightAction(param);
  }



  render() {
    return (
      <View style={styles.container}>
        <SafeAreaView backgroundColor={colors.colorBlue} />
        <FlightListHeader
          data={this.props.flightSearchParams} />
        <View style={styles.container}>
          <KeyboardAwareScrollView
            //contentContainerStyle={styles.container}
            contentInsetAdjustmentBehavior={"automatic"}>
            <BookingBelt type={2} style={{ paddingBottom: 5 }} />
            <View>
              {/* <View style={{ height: 600 }} /> */}

              {this.renderPassengerDetails()}
              {this.renderContactDetails()}

              {this.renderBaggage(0)}
              {this.renderBaggage(1)}
              {this.renderMeal(0)}
              {this.renderMeal(1)}

              {this.renderFareDetails()}

              {this.renderDatePickerModal()}
              {this.renderPassportExpiryModal()}

            </View>
          </KeyboardAwareScrollView>
          <BottomStrip fireEvent={() => {
            this.validateEntryFields()
            //this.bookTicket()
          }
          } price={this.state.currency + " " + Math.ceil(this.state.totalFare)} bottomStripStyle={{ position: 'relative' }} />

          <SafeAreaView backgroundColor={colors.colorBlue} />

        </View>
      </View>

    );
  }
}

const mapStateToProps = (state) => {
  // console.log("ReduxStore:",JSON.stringify(state))
  return {
    flightSearchParams: state.FlightsReducer.flightSearchParams,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    confirmQuoteAction: (data) => dispatch(confirmQuoteAction(data))
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(PassengerDetails)