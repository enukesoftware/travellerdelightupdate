import React, { Component } from "react";
import { Dimensions, FlatList, Image, Platform, ScrollView, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import { SafeAreaView } from "react-navigation";
import { colors } from "../../Utils/Colors";
import Fonts from "../../Utils/Fonts";
import Images from "../../Utils/Images";

const screenWidth = Dimensions.get("window").width;
const screenHeight = Dimensions.get('window').height;

export default class FlightFilters extends Component {
  constructor(props) {
    super(props);
    this.state = {
      originCity: this.props.originCity,
      destCity: this.props.destCity,
      modelVisible: false,
      timeFiltersArr: [
        {
          time: "00:00 - 06:00",
          image1: Images.ic_time1_active,
          image2: Images.ic_time1_deactive
        },
        {
          time: "06:00 - 12:00",
          image1: Images.ic_time2_active,
          image2: Images.ic_time2_deactive
        },
        {
          time: "12:00 - 18:00",
          image1: Images.ic_time3_active,
          image2: Images.ic_time3_deactive
        },
        {
          time: "18:00 - 24:00",
          image1: Images.ic_time4_active,
          image2: Images.ic_time4_deactive
        }

      ],
      loading: false,
      arrNonStopFilters: [
        {
          stop: 0,
          type: "Nonstop"
        },
        {
          stop: 1,
          type: "1 Stop"
        },
        {
          stop: 2,
          type: "1+ Stops"
        }
      ],
      arrFlightList: null,
      selectedFilters: {
        timeFilter: [],
        stopageFilter: [],
        refundable: false,
        freeMeal: false,
        airlines: [],
        timeFilter2: []
      }
    };
    console.log("Flight filterconstructor called")
    this._renderAirlineListItem = this._renderAirlineListItem.bind(this);
    this._renderAirlinesFilterView = this._renderAirlinesFilterView.bind(this);
    this._renderNonStopFilterItem = this._renderNonStopFilterItem.bind(this);
    this._renderNonStopFilterView = this._renderNonStopFilterView.bind(this);
    this._renderOtherFilters = this._renderOtherFilters.bind(this);
    this._renderTimeFilter = this._renderTimeFilter.bind(this);
    this._renderTimeFilterListItem = this._renderTimeFilterListItem.bind(this);
    this._renderNavBar = this._renderNavBar.bind(this);
    this._renderTimeFilterListItemInternational = this._renderTimeFilterListItemInternational.bind(this);
  }

  componentDidMount() {
    console.log("flight filters:--" + JSON.stringify(this.props));

    this.setState({
      arrFlightList: this.props.airlines
    })
    if (this.props) {
      const filter = this.props.slectedFilters;
      const dict = this.state.selectedFilters;
      if (filter.timeFilter.length > 0) {
        dict.timeFilter = filter.timeFilter;
      }
      if (filter.stopageFilter.length > 0) {
        dict.stopageFilter = filter.stopageFilter;
      }
      dict.refundable = filter.refundable;
      dict.freeMeal = filter.freeMeal;
      if (filter.timeFilter2.length > 0) {
        dict.timeFilter2 = filter.timeFilter2;
      }
      if (filter.airlines.length > 0) {
        dict.airlines = filter.airlines;
      }
      this.setState({ selectedFilters: dict })
    }

  }
  //----------- Render Navigation Bar -------------
  _renderNavBar() {
    return (
      <View style={styles1.header}>
        <TouchableOpacity style={styles1.headerBtn} onPress={() => this.props.onModelHide()}>
          <Image source={Images.ic_image_close} style={{ width: 20, height: 20, resizeMode: "contain" }}></Image>
        </TouchableOpacity>
        <View style={styles1.headerTitle}>
          <Text style={styles1.titleText}>Filters</Text>
        </View>
        <TouchableOpacity style={styles1.headerBtn} onPress={() => {
          let dict = this.state.selectedFilters
          dict.airlines = []
          dict.freeMeal = false
          dict.refundable = false
          dict.stopageFilter = []
          dict.timeFilter = []
          dict.timeFilter2 = []
          this.setState({
            selectedFilters: dict,
            loading: !this.state.loading
          })
        }}>
          <Text style={{
            fontSize: 14,
            fontFamily: Fonts.medium,
            color: colors.colorBlue
          }}>Reset</Text>
        </TouchableOpacity>
      </View>
    );
  }

  _renderTimeFilter() {
    return (
      <View>
        <View style={styles1.grayStrip}>
          <Text style={styles1.dividerText}>Preferred Departure Time From {this.state.originCity}</Text>
        </View>


        <View style={styles1.contentView}>
          <FlatList
            numColumns={4}
            pagingEnabled={true}
            scrollEnabled={false}
            showsHorizontalScrollIndicator={false}
            scrollEnabled={true}
            keyExtractor={(item, index) => index.toString()}
            data={this.state.timeFiltersArr}
            extraData={this.state.loading}
            renderItem={this._renderTimeFilterListItem}
          />
        </View>
      </View>
    );
  }

  _renderTimeFilterListItem({ item, index }) {
    let { selectedFilters } = this.state;
    return (
      <TouchableOpacity
        style={styles1.timeBtn}
        underlayColor="transparent"
        onPress={() => {
          let dict = this.state.selectedFilters;
          let filters = dict.timeFilter;
          if (filters.includes(item.time)) {
            filters = filters.filter(data => {
              return data != item.time;
            });
          } else {
            filters.push(item.time);
          }
          dict.timeFilter = filters;
          this.setState({
            selectedFilters: dict,
            loading: !this.state.loading
          });
        }}
      >
        <Image source={selectedFilters.timeFilter.includes(item.time) ? item.image1 : item.image2}
          style={{ width: "100%", height: "100%", resizeMode: "contain" }}></Image>
      </TouchableOpacity>
    );
  }

  _renderInternationalTimeFilter2 = () => {

    return (
      <View>
        <View style={styles1.grayStrip}>
          <Text style={styles1.dividerText}>Preferred Departure Time From {this.state.destCity}</Text>
        </View>

        <View style={styles1.contentView}>
          <FlatList
            numColumns={4}
            pagingEnabled={true}
            scrollEnabled={false}
            showsHorizontalScrollIndicator={false}
            scrollEnabled={true}
            keyExtractor={(item, index) => index.toString()}
            data={this.state.timeFiltersArr}
            extraData={this.state.loading}
            renderItem={this._renderTimeFilterListItemInternational}
          />
        </View>
      </View>
    );
  }

  _renderTimeFilterListItemInternational({ item, index }) {
    let { selectedFilters } = this.state;

    return (
      <TouchableOpacity
        style={styles1.timeBtn}
        underlayColor="transparent"
        onPress={() => {
          let dict = selectedFilters;
          let filters = dict.timeFilter2;
          if (filters.includes(item.time)) {
            filters = filters.filter(data => {
              return data != item.time;
            });
          } else {
            filters.push(item.time);
          }
          dict.timeFilter2 = filters;
          this.setState({
            selectedFilters: dict,
            loading: !this.state.loading
          });
        }}
      >
        <Image source={selectedFilters.timeFilter2.includes(item.time) ? item.image1 : item.image2}
          style={{ width: "100%", height: "100%", resizeMode: "contain" }}></Image>
      </TouchableOpacity>
    );
  }
  //-------------- Render Non Stop Filter -----------

  _renderNonStopFilterView() {
    return (
      <View >
        <View style={styles1.grayStrip}>
          <Text style={styles.dividerText}>Stops</Text>
        </View>
        <View style={styles1.contentView}>
          <FlatList
            // numColumns={4}
            // pagingEnabled={true}
            horizontal={true}
            //scrollEnabled={true}
            showsHorizontalScrollIndicator={false}
            showsVerticalScrollIndicator={false}
            keyExtractor={(item, index) => index.toString()}
            data={this.state.arrNonStopFilters}
            extraData={this.state.loading}
            renderItem={this._renderNonStopFilterItem}
          />
        </View>
      </View>
    );
  }

  _renderNonStopFilterItem({ item, index }) {
    let { selectedFilters } = this.state;
    return (
      <TouchableOpacity style={[styles1.btnStyle, {
        backgroundColor: selectedFilters.stopageFilter.includes(item.stop) ? colors.colorBlue : colors.lightBlue,
      }]} onPress={() => {
        let dict = this.state.selectedFilters;
        let filters = dict.stopageFilter;
        if (filters.includes(item.stop)) {
          filters = filters.filter(data => {
            return data != item.stop;
          });
        } else {
          filters.push(item.stop);
        }

        console.log("STOP_FILTER", JSON.stringify(filters))
        dict.stopageFilter = filters;
        this.setState({
          selectedFilters: dict,
        });
      }}>
        <Text style={[styles1.textStyle, {
          color: selectedFilters.stopageFilter.includes(item.stop) ? colors.colorWhite : colors.colorBlack,
        }]}>{item.type}</Text>
      </TouchableOpacity>
    );
  }

  _renderOtherFilters() {
    let { selectedFilters } = this.state;
    return (
      <View>
        <View style={styles1.grayStrip}>
          <Text style={styles1.dividerText}>Amenities</Text>
        </View>
        <View style={[styles1.contentView, { padding: 12, flexDirection: "column" }]}>
          <TouchableOpacity activeOpacity={0.8}
            style={styles1.checkView} onPress={() => {
              let selectedFilters1 = selectedFilters;
              selectedFilters1.freeMeal = !selectedFilters.freeMeal;
              this.setState({ selectedFilters });
            }}>
            <View style={styles1.checkViewContent}>
              <Image source={require("../../Assets/images/meal.png")} style={styles1.airlineImg}></Image>
              <Text style={styles1.textStyle}>Free Meal</Text>
            </View>
            <View style={styles1.checkBox}>
              <Image source={selectedFilters.freeMeal ? Images.ic_image_checked : Images.ic_image_unchecked} style={styles1.checkImg}></Image>
            </View>
          </TouchableOpacity>
          {/* <TouchableOpacity key={index} activeOpacity={0.8}
                      style={styles1.checkView} onPress={() => {
                        let selectedFilters1=selectedFilters;
                        slectedFilters1.freeMeal=!selectedFilters.freeMeal;
                        this.setState({selectedFilters});
                      }}>
                      <View style={styles1.checkViewContent}>
                        <Image source={require("../../Assets/images/meal.png")} style={styles.airlineImg}></Image>
                        <Text style={styles1.textStyle}>Refundable</Text>
                      </View>
                      <View style={styles1.checkBox}>
                        <Image source={selectedFilters.freeMeal ? Images.ic_image_checked : Images.ic_image_unchecked} style={styles.checkImg}></Image>
                      </View>
                    </TouchableOpacity> */}
        </View>
      </View>
    );
  }

  //--------------- Render Airlines Filter -------------
  _renderAirlinesFilterView() {
    return (
      <View style={{ marginTop: 20, marginBottom: 10 }}>
        <View style={styles1.grayStrip}>
          <Text style={styles.dividerText}>Prefer Airlines</Text>
        </View>

        <View style={[styles.contentView, { padding: 12, flexDirection: "column" }]}>
          <FlatList
            data={this.state.arrFlightList}
            extraData-={this.state.loading}
            keyExtractor={(item, index) => index.toString()}
            renderItem={this._renderAirlineListItem}
          />
        </View>
      </View>
    );
  }

  _renderAirlineListItem({ item, index }) {
    let { selectedFilters } = this.state;
    return (
      <TouchableOpacity activeOpacity={0.8}
        style={styles1.checkView} onPress={() => {
          let dict = this.state.selectedFilters;
          let filters = dict.airlines;
          if (filters.includes(index)) {
            filters = filters.filter(data => {
              return data != index;
            });
          } else {
            filters.push(index);
          }
          dict.airlines = filters;
          this.setState({
            selectedFilters: dict,
            loading: !this.state.loading
          });
        }}>
        <View style={styles1.checkViewContent}>
          <Image source={{ uri: "http://travel.yiipro.com/" + item.airlineImage }} style={styles1.airlineImg}></Image>
          <Text style={styles1.textStyle}>{item.airlineName}</Text>
        </View>
        <View style={styles1.checkBox}>
          <Image source={selectedFilters.airlines.includes(index) ? Images.ic_image_checked : Images.ic_image_unchecked}
            style={styles1.checkImg}></Image>
        </View>
      </TouchableOpacity>
      //   <View style={{ height: 60, width: "100%", flexDirection: "row" }}>
      //     <View
      //       style={{
      //         width: "15%",
      //         height: "100%",
      //         justifyContent: "center",
      //         alignItems: "center"
      //       }}
      //     >
      //       <Image
      //         style={{ height: 30, width: 30 }}
      //         // source={Images.imgFlightDefault}
      //         source={{ uri: "http://travel.yiipro.com/" + item.airlineImage }}
      //         resizeMode="contain"
      //       />
      //     </View>

      //     <View
      //       style={{
      //         width: "80%",
      //         height: "100%",
      //         flexDirection: "row",
      //         marginLeft: 10,
      //         marginRight: 10,
      //         justifyContent: "space-between",
      //         alignItems: "center"
      //       }}
      //     >
      //       <Text style={[commonstyle.blackText, { fontSize: 16 }]}>{item.airlineName}</Text>

      //       <TouchableHighlight
      //         style={styles.styleCheckBoxBtn}
      //         underlayColor="transparent"
      //         onPress={() => {
      //           let dict = this.state.selectedFilters;
      //           let filters = dict.airlines;
      //           if (filters.includes(index)) {
      //             filters = filters.filter(data => {
      //               return data != index;
      //             });
      //           } else {
      //             filters.push(index);
      //           }
      //           dict.airlines = filters;
      //           this.setState({
      //             selectedFilters: dict,
      //             loading: !this.state.loading
      //           });
      //         }}
      //       >
      //         <Image
      //           style={{ height: 20, width: 20 }}
      //           source={
      //             this.state.selectedFilters.airlines.includes(index)
      //               ? Images.ic_image_checked
      //               : Images.ic_image_unchecked
      //           }
      //           resizeMode="contain"
      //         />
      //       </TouchableHighlight>
      //     </View>



      //   </View>
    );
  }

  separatorView = () => {
    return (
      <View
        style={{
          width: "100%",
          height: 1,
          alignSelf: "center",
          backgroundColor: "rgb(211,211,211)"
        }}
      />
    )
  }

  render() {

    return (
      // <Modal
      //   transparent={false}
      //   animationType={"slide"}
      //   visible={this.props.modelVisible}>
      <SafeAreaView style={{ flex: 1, backgroundColor: colors.colorBlue }}>
        <View style={styles1.mainContainer}>
          {this._renderNavBar()}
          <ScrollView style={{ flex: 1, backgroundColor: colors.colorWhite }}>
            <View>
              {this._renderTimeFilter()}
                {this.props.isInterNational ? this._renderInternationalTimeFilter2() : null}
              {this._renderNonStopFilterView()}
              {this._renderAirlinesFilterView()}
              {this._renderOtherFilters()}
            </View>
          </ScrollView>
          <TouchableOpacity style={styles1.bottomBotton} onPress={() => {
            this.props.onModelHide();
            this.props.onFilterSelection(this.state.selectedFilters)
          }}>
            <Text style={styles1.bottomBottonText}>APPLY</Text>
          </TouchableOpacity>
        </View>
      </SafeAreaView>
      // </Modal>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: colors.colorWhite,

  },
  styleTimeFilterTitleView: {
    height: 40,
    justifyContent: "center",
  },
  styleTimeFileListView: {
    height: 80,
    margin: 1,
    backgroundColor: "white",
    borderRadius: 10,
    padding: 8,
    elevation: 2
  },
  styleTimeFilterListItem: {

    borderRightColor: "gray",
    height: 64,
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  styleOtherFilterView: {
    flexDirection: "row",
    justifyContent: "space-between",
    height: "50%",
    alignItems: "center"
  },
  styleCheckBoxBtn: {
    height: 30,
    width: 30,
    justifyContent: "center",
    alignItems: "center"
  },
  styleApplyFilterBtn: {
    marginBottom: 20,
    height: 50,
    width: "90%",
    alignSelf: "center",
    backgroundColor: colors.colorBlue,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 8
  }
});

const NavigationBarStyle = StyleSheet.create({
  styleNavBar: {
    height: ((Platform.OS == 'ios') ? 44 : 52),
    // height: ((Platform.OS == 'ios')? getStatusBarHeight(true):0) + ((Platform.OS == 'ios')? 44:52), 
    width: screenWidth,
    backgroundColor: "rgba(244,248,254,1)"
  },
  styleNavBarSubView: {
    height: ((Platform.OS == 'ios') ? 44 : 52),
    width: "100%",
    // marginTop: ((Platform.OS == 'ios')? getStatusBarHeight(true):0),
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: 'center',
    // backgroundColor:'green'
  },
});

const styles1 = StyleSheet.create({
  bottomBottonText: {
    color: colors.colorWhite,
    fontSize: 20, letterSpacing: .7,
    fontFamily: Fonts.bold
  },
  bottomBotton: {
    backgroundColor: colors.colorBlue,
    width: "100%",
    height: 50,
    justifyContent: "center",
    alignItems: "center"
  },
  checkImg: { width: 20, height: 20, resizeMode: "contain" },
  checkBox: {
    width: "15%",
    height: "100%",
    alignItems: "center",
    justifyContent: "center"
  },
  airlineImg: {
    width: 35, height: 35,
    resizeMode: "contain", marginRight: 10
  },
  checkViewContent: {
    width: "85%",
    height: "100%",
    flexDirection: "row",
    alignItems: "center"
  },
  checkView: {
    width: "100%",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    height: 40,
  },
  timeBtn: {
    // backgroundColor: "red",
    height: 58, width: 50,
    margin: 6
  },
  textStyle: {
    fontFamily: Fonts.medium,
    fontSize: 14, letterSpacing: .7
  },
  btnStyle: {
    width: 110, margin: 6,
    paddingLeft: 15,
    paddingRight: 15,
    padding: 10,
    borderRadius: 10,
    alignItems: "center"
  },
  grayStrip: {
    width: "100%",
    backgroundColor: colors.gray,
    height: 40,
    justifyContent: "center",
    paddingLeft: 12
  },
  dividerText: {
    color: colors.colorBlack,
    fontSize: 14, fontFamily: Fonts.semiBold
  },
  contentView: {
    width: "100%",
    flexDirection: "row",
    padding: 6,
    alignItems: "center"
  },
  header: {
    height: 50,
    width: "100%",
    backgroundColor: "rgb(246,245,246)",
    flexDirection: "row",
    alignItems: "center"
  },
  headerTitle: {
    width: screenWidth - 110,
    height: 50,
    justifyContent: "center",
    alignItems: "center"
  },
  titleText: {
    fontSize: 14,
    fontFamily: Fonts.medium,
    color: colors.colorBlack
  },
  mainContainer: {
    flex: 1,
    backgroundColor: colors.colorWhite,
   // marginTop: (Platform.OS === 'ios') ? 44 : 56 
  },
  headerBtn: {
    width: 50,
    height: "100%",
    alignItems: "center",
    justifyContent: "center",
    // backgroundColor: "red"
  }
})
