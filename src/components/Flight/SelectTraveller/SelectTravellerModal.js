import React, { Component } from "react";
import { DeviceEventEmitter, Dimensions, Image, Modal, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import { colors } from "../../../Utils/Colors";
import AppConstants from "../../../Utils/Constants";
import Fonts from "../../../Utils/Fonts";
import Images from "../../../Utils/Images";
import StringConstants from "../../../Utils/StringConstants";

const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;

export default class SelectTravellerModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      numberOfAdults: this.props.numberOfAdults,
      numberOfChilds: this.props.numberOfChilds,
      numberOfInfant: this.props.numberOfInfant,
    }
  }

  renderClassView = () => {
    return (
      <View>
        <View style={{
          alignItems: "center",
          padding: 15
        }}>
          <Text style={styles.text}>Stop</Text>
        </View>
        <View style={styles.classContainer}>
          <TouchableOpacity style={[styles.classButton, {
            backgroundColor: stopType == 1 ? colors.colorBlue : colors.lightBlue,
          }]} onPress={() => this.setState({ stopType: 1 })}>
            <Text style={[styles.btnText, {
              color: stopType == 1 ? colors.colorWhite : colors.colorBlack
            }]}>NONSTOP</Text>
          </TouchableOpacity>
          <TouchableOpacity style={[styles.classButton, {
            marginHorizontal: 13,
            backgroundColor: stopType == 2 ? colors.colorBlue : colors.lightBlue,
          }]} onPress={() => this.setState({ stopType: 2 })}>
            <Text style={[styles.btnText, {
              color: stopType == 2 ? colors.colorWhite : colors.colorBlack
            }]}>1 STOP</Text>
          </TouchableOpacity>
          <TouchableOpacity style={[styles.classButton, {
            backgroundColor: stopType == 3 ? colors.colorBlue : colors.lightBlue,
          }]} onPress={() => this.setState({ stopType: 3 })}>
            <Text style={[styles.btnText, {
              color: stopType == 3 ? colors.colorWhite : colors.colorBlack
            }]}>2+ STOP</Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }

  validateAdults(adultNum, childNum, infantNum) {
    if (adultNum == 0) {
      alert("Please add atleast 1 adult.");
      return;
    }

    let dict = {
      adultsNum: adultNum,
      childNum: childNum,
      infantNum: infantNum
    };

    DeviceEventEmitter.emit(StringConstants.TRAVELLER_SELECT, dict);
  }

  addTraveller(a) {
    let { child, infant, adult, numberOfAdults, numberOfChilds, numberOfInfant } = this.state;
    let totalPassenger = numberOfAdults + numberOfChilds + numberOfInfant

    if (totalPassenger == 9) {
      alert("Maximum 9 Travellers Allowed")
      return
    }
    switch (a) {
      case 1:
        this.setState({
          numberOfAdults: numberOfAdults + 1
        });
        break;
      case 2:
        this.setState({
          numberOfChilds: numberOfChilds + 1
        });
        break;
      case 3:
        if (numberOfInfant == numberOfAdults) {
          alert('Number of Infants cannot be greater than number of adults')
        }
        if (totalPassenger < AppConstants.maxNumForFlightSearch && numberOfInfant < numberOfAdults) {
          this.setState({
            numberOfInfant: numberOfInfant + 1
          });
        }
        break;
    }
  }

  removeTraveller(a) {
    let { child, infant, adult, numberOfAdults, numberOfChilds, numberOfInfant } = this.state;
    let totalPassenger = numberOfAdults + numberOfChilds + numberOfInfant

    if (numberOfAdults == numberOfInfant && a == 1) {
      alert("Number of Adults cannot be less than number of Infants.")
      return
    }
    switch (a) {
      case 1:
        if (numberOfAdults > 1) {
          this.setState({
            numberOfAdults: numberOfAdults - 1
          });
        }
        break;
      case 2:
        if (numberOfChilds > 0)
          this.setState({
            numberOfChilds: numberOfChilds - 1
          });
        break;
      case 3:
        if (numberOfInfant > 0)
          this.setState({
            numberOfInfant: numberOfInfant - 1
          });
        break;
    }
  }

  render() {
    let { child, infant, adult, numberOfAdults, numberOfChilds, numberOfInfant } = this.state;
    let totalPassenger = numberOfAdults + numberOfChilds + numberOfInfant
    let { closeModal } = this.props;
    return (
      <Modal
        animationType="slide"
        transparent={true}
        visible={this.props.modalVisible}
        onRequestClose={() => {
          Alert.alert('Modal has been closed.');
        }}
      >
        <View style={styles.mainContainer}>
          <View style={styles.header}>
            <TouchableOpacity style={styles.headerBtn} onPress={() =>{
              this.setState({
                numberOfAdults: this.props.numberOfAdults,
                numberOfChilds: this.props.numberOfChilds,
                numberOfInfant: this.props.numberOfInfant,
              },()=>closeModal())
            }}>
              <Image source={Images.ic_image_close} style={{ width: 20, height: 20, resizeMode: "contain" }}></Image>
            </TouchableOpacity>
            <View style={styles.headerTitle}>
              <Text style={styles.titleText}>Select Travellers & Stop</Text>
            </View>
            <TouchableOpacity style={styles.headerBtn} onPress={() => {
              console.log("numberOfAdults:-" + numberOfAdults)
              if (numberOfAdults == 0) {
                alert("Please add atleast 1 adult.");
                return;
              }

              let dict = {
                adultsNum: numberOfAdults,
                childNum: numberOfChilds,
                infantNum: numberOfInfant
              };

              closeModal();
              DeviceEventEmitter.emit(StringConstants.TRAVELLER_SELECT, dict);
            }}>
              <Text style={{
                fontSize: 14,
                fontFamily: Fonts.medium,
                color: colors.colorBlue
              }}>Done</Text>
            </TouchableOpacity>
          </View>
          <View style={styles.mainView}>
            <View style={styles.row}>
              <Text style={styles.text}>Adults
                <Text style={{
                  fontFamily: Fonts.regular,
                  fontSize: 12
                }}> (Above 12 Yrs)</Text>
              </Text>
              <View style={styles.buttonView}>
                <TouchableOpacity onPress={() => this.addTraveller(1)}>
                  <Image source={Images.ic_image_minus} style={styles.btnImg} ></Image>
                </TouchableOpacity>
                <View style={styles.inputStyle}>
                  <Text style={{ textAlign: "center" }}>{numberOfAdults}</Text>
                </View>
                <TouchableOpacity onPress={() => this.removeTraveller(1)}>
                  <Image source={Images.ic_image_minus} style={styles.btnImg} ></Image>
                </TouchableOpacity>
              </View>
            </View>
            <View style={styles.row}>
              <Text style={styles.text}>Children
                <Text style={{
                  fontFamily: Fonts.regular,
                  fontSize: 12
                }}> (2-12 Yrs)</Text>
              </Text>
              <View style={styles.buttonView}>
                <TouchableOpacity onPress={() => this.addTraveller(2)}>
                  <Image source={Images.ic_image_minus} style={styles.btnImg} ></Image>
                </TouchableOpacity>
                <View style={styles.inputStyle}>
                  <Text style={{ textAlign: "center" }}>{numberOfChilds}</Text>
                </View>
                <TouchableOpacity onPress={() => this.removeTraveller(2)}>
                  <Image source={Images.ic_image_minus} style={styles.btnImg} ></Image>
                </TouchableOpacity>
              </View>
            </View>
            <View style={styles.row}>
              <Text style={styles.text}>Infants
                <Text style={{
                  fontFamily: Fonts.regular,
                  fontSize: 12
                }}> (Under 2 Yrs)</Text>
              </Text>
              <View style={styles.buttonView}>
                <TouchableOpacity onPress={() => this.addTraveller(3)}>
                  <Image source={Images.ic_image_minus} style={styles.btnImg} ></Image>
                </TouchableOpacity>
                <View style={styles.inputStyle}>
                  <Text style={{ textAlign: "center" }}>{numberOfInfant}</Text>
                </View>
                <TouchableOpacity onPress={() => this.removeTraveller(3)}>
                  <Image source={Images.ic_image_minus} style={styles.btnImg} ></Image>
                </TouchableOpacity>
              </View>
            </View>
          </View>

        </View>
      </Modal>
    )
  }
}

const styles = StyleSheet.create({
  btnText: {
    fontFamily: Fonts.medium,
    fontSize: 14, letterSpacing: .5,
  },
  inputStyle: {
    width: 50, height: 30, borderBottomWidth: 1,
    borderTopWidth: 1,
    borderColor: "lightgrey",
    justifyContent: "center",
    alignItems: "center"
  },
  header: {
    height: 50,
    width: "100%",
    backgroundColor: "rgb(246,245,246)",
    flexDirection: "row",
    alignItems: "center"
  },
  mainContainer: {
    marginTop: 50,
    flex: 1,
    backgroundColor: colors.colorWhite
  },
  headerBtn: {
    width: 50,
    height: "100%",
    alignItems: "center",
    justifyContent: "center",
    // backgroundColor: "red"
  },
  headerTitle: {
    width: screenWidth - 110,
    height: 50,
    justifyContent: "center",
    alignItems: "center"
  },
  titleText: {
    fontSize: 14,
    fontFamily: Fonts.medium,
    color: colors.colorBlack
  },
  mainView: {
    width: screenWidth,
    height: 180,
    borderBottomColor: "lightgrey",
    borderBottomWidth: 1,
    padding: 12
  },
  row: {
    width: "100%",
    height: 50,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  buttonView: {
    width: 110,
    height: 30,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    overflow: "hidden",
  },
  btnImg: { width: 30, height: 30, resizeMode: "contain" },
  text: {
    fontSize: 14,
    color: colors.colorBlack,
    fontFamily: Fonts.bold
  },
  classButton: {
    backgroundColor: colors.colorBlue,
    padding: 10,
    paddingLeft: 15,
    paddingRight: 15,
    borderRadius: 5
  },
  classContainer: {
    width: "100%",
    height: 40,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center"
  },
})