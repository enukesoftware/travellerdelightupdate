import moment from "moment";
import React, { Component } from "react";
import { DeviceEventEmitter, FlatList, Image, Modal, Platform, Text, TouchableHighlight, TouchableOpacity, View } from "react-native";
import CardView from "react-native-cardview";
import Dash from "react-native-dash";
import Spinner from "react-native-loading-spinner-overlay";
import { SafeAreaView } from "react-navigation";
import { connect } from "react-redux";
import { saveFlightItem, searchFlightAction } from '../../../Redux/Actions';
import { imageBaseUrl } from "../../../Utils/APIManager/APIConstants";
import { colors } from "../../../Utils/Colors";
import Images from "../../../Utils/Images";
import NavigationServices from '../../../Utils/NavigationServices';
import StringConstants from '../../../Utils/StringConstants';
import FlightListHeader from "../../custom/FlightListHeader";
import FlightFilters from "../FlightFilters";
import styles from "./Styles";


class FlightList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      data: [],
      flightListData: [],
      filteresFlightData: [],
      completeResponseData: [],
      loading: false,
      selectedIndex: 0,
      expendingIndex: -1,
      spinner: false,
      traceId: "",
      modelVisible: false,
      isFilterActive: false,

      //for selecting two way depart or arrival
      selectedOriginIndex: 0,
      selectedOriginFlightIndex: 0,
      selectedDestinationFlightIndex: 0,
      selectedFilterIndex: 0,

      //for soting of flights
      selectedSort: 3,
      sortArrIncr: false,
      sortDepIncr: false,
      sortPriceIncr: true,
      sortDurIncr: false,
      sortAirlineIncr: false,

      twoWayTotalPrice: null,
      destinationFlightItem: null,
      originFlightItem: null,
      uniqueAirlines: [],
      filterForReturn: false,
      filterForDep: false,
      filterDataForTwoWayDep: null,
      filterDataForTwoWayRet: null,
      isRoundTrip: false,
      // sortDep

      isInterNational: false,

      selectedFiltersDepart: {
        timeFilter: [],
        stopageFilter: [],
        refundable: false,
        freeMeal: false,
        airlines: [],
        timeFilter2: []
      },

      selectedFiltersArr: {
        timeFilter: [],
        stopageFilter: [],
        refundable: false,
        freeMeal: false,
        airlines: [],
        timeFilter2: []
      }

    };

    this.depAirlines = [];
    this.arrvialAirlines = [];
    this.internationalAirlines = [];
    this.flatListref = null;
    this._renderListdate = this._renderListdate.bind(this);
    // this._renderFlightListItem = this._renderFlightListItem.bind(this);
    this.getFlightsList = this.getFlightsList.bind(this);
    this.onFilterSelection = this.onFilterSelection.bind(this);
    this._renderFlightSingleWayListItem = this._renderFlightSingleWayListItem.bind(
      this
    );
    this._renderSagmentView = this._renderSagmentView.bind(this);
    this._renderCommonListItem = this._renderCommonListItem.bind(this);
    this._renderInterNationalFlightListItem = this._renderInterNationalFlightListItem.bind(this);
    this.getBackgroundColorForItem = this.getBackgroundColorForItem.bind(this);
    this._renderBookNowBtn = this._renderBookNowBtn.bind(this);
    this.updateFlightListFromApi = this.updateFlightListFromApi.bind(this)
  }


  componentDidMount() {
    this.getFlightsList();
    DeviceEventEmitter.addListener(StringConstants.FLIGH_LIST_EVENT, this.updateFlightListFromApi)
    DeviceEventEmitter.addListener(StringConstants.RELOAD_FLIGHT_LIST, this.getFlightsList)
    // this.updateFlightListFromApi(internationalRoundMultiStop)

  }

  updateFlightListFromApi(response) {
    console.log("getFlightListSaga", JSON.stringify(response));
    if (!response) { alert("No Result Found"); return; }
    this.setState({ traceId: response.others.traceId });
    if (response.results.length == 2) {
      this.setState({
        isRoundTrip: true,
        completeResponseData: response.results,
        flightListData: response.results[0],
        loading: !this.state.loading,
        originFlightItem: response.results[0][0],
        destinationFlightItem: response.results[1][0],
        twoWayTotalPrice: response.results[0][0].fare.offeredFare + response.results[1][0].fare.offeredFare
      }, () => { this.setUniqueAirlines() });
    } else {
      if (response.results[0][0].flightSegment.length === 2) {
        this.setState({
          isInterNational: true
        })
      }
      this.setState({
        completeResponseData: response.results,
        flightListData: response.results[0],
        loading: !this.state.loading,
      }, () => { this.setUniqueAirlines() });
    }
  }

  componentWillUnmount() {
    DeviceEventEmitter.removeListener(StringConstants.RELOAD_FLIGHT_LIST, this.getFlightsList)
    DeviceEventEmitter.removeListener(StringConstants.FLIGH_LIST_EVENT, this.updateFlightListFromApi)
  }

  //------------ On Filter Selection -------------

  calulateTwoWayPrice() {
    let item1 = this.state.originFlightItem;
    let item2 = this.state.destinationFlightItem;
    let fare = item1.fare.offeredFare + item2.fare.offeredFare;
    fare = Math.ceil(fare);
    this.setState({ twoWayTotalPrice: fare });
  }

  scrollToitemindex = () => {
    if (this.state.selectedOriginIndex == 1) {
      if (this.state.selectedDestinationFlightIndex !== -1) {
        this.flatListref.scrollToIndex({ animated: true, index: this.state.selectedDestinationFlightIndex });
      }
    } else {
      if (this.state.selectedOriginFlightIndex !== -1) {
        this.flatListref.scrollToIndex({ animated: true, index: this.state.selectedOriginFlightIndex })
      }
    }

    //-------------- API Calling Methods --------------------
  }

  onFilterSelection(dict) {
    // console.log("on filter seletion");
    if (this.state.isRoundTrip) {
      console.log("Round Trip");

      if (this.state.selectedOriginIndex == 0) {
        this.setState({
          modelVisible: false,
          selectedFiltersDepart: dict
        });
        let timeFilters = dict.timeFilter;
        let stopageFilter = dict.stopageFilter;
        let isRefundable = dict.refundable;
        let isFreeMeal = dict.freeMeal;
        let airLines = dict.airlines;
        let filterDate = this.state.completeResponseData[0][0].flightSegment[0][0].origin.depTime;
        if (filterDate) {
          filterDate = filterDate.split("T")[0];
        }
        console.log(dict);
        let filterData = this.state.completeResponseData[0];
        // console.log("filterDate", JSON.stringify(filterDate))

        if (timeFilters.length > 0 && timeFilters.length < 4) {
          timeFilters.forEach((item, index) => {
            let timeArr = String(item).split(" - ");
            // console.log("timeArr", JSON.stringify(timeArr))

            //let finaltime1 = "2019-04-04T" + timeArr[0] + ":00";
            let finaltime1 = filterDate + "T" + timeArr[0] + ":00";
            let finaltime2 = filterDate;
            // console.log("finaltime1", JSON.stringify(finaltime1))
            // console.log("finaltime2", JSON.stringify(finaltime2))
            if (timeArr[1] == "24:00") {
              finaltime2 = finaltime2 + "T" + "23:59" + ":59";
            } else {
              finaltime2 = finaltime2 + "T" + timeArr[1] + ":00";
            }
            console.log("time is  " + finaltime1);
            let deptTime1 = finaltime1;
            let depTime2 = finaltime2;
            // let depttimeStr2 = dateFormat(finaltime2, "HH:MM");
            let arr = this.state.completeResponseData[0].filter(data => {
              let flightDeptTime = data.flightSegment[0][0].origin.depTime
              if (flightDeptTime >= deptTime1 && flightDeptTime <= depTime2) {
                console.log("true")
                return true;
              } else {
                return false;
              }
            });
            console.log(arr);
            if (index == 0) {
              filterData = arr;
            } else {
              console.log("arr length in second time " + arr.length);
              filterData = filterData.concat(arr);
            }
            console.log("filter data after time filter " + filterData.length);
          });
        }
        if (stopageFilter.length > 0 && stopageFilter.length < 3) {
          stopageFilter.sort((a, b) => { return a - b })
          // console.log("DTTstopageFilter:", JSON.stringify(stopageFilter))
          // console.log("DTTflightSegment:", JSON.stringify(data.flightSegment))
          let filterArr = []
          stopageFilter.forEach((item, index) => {
            let arr = filterData.filter(data => {
              if (item == 0) {
                return data.flightSegment[0].length == 1;
              } else if (item == 1) {
                return data.flightSegment[0].length == 2;
              } else {
                return data.flightSegment[0].length > 2;
              }
            });
            if (index == 0) {
              filterArr = arr;
            } else {
              console.log(arr)
              filterArr = filterArr.concat(arr);
            }
          });
          filterData = filterArr;
          console.log("filter data after stopage filter 1 " + filterData.length);
        }
        if (isRefundable) {
          filterData = filterData.filter(data => {
            return data.isRefundable == true;
          });
          console.log("filter data after refundable filter " + filterData.length);
        }
        if (isFreeMeal) {
          filterData = filterData.filter(data => {
            return !data.isLCC;
          });
          console.log("filter data after free meal filter " + filterData.length);
        }
        if (airLines.length > 0) {
          let arr2 = [];
          airLines.forEach((airlineIndex, index) => {
            let arr = filterData.filter((flightItem) => {
              if (flightItem.flightSegment[0][0].airline.airlineCode === this.depAirlines[airlineIndex].airlineCode) {
                return true;
              } else {
                return false;
              }
            })
            if (index == 0) {
              arr2 = arr;
            } else {
              console.log("arr length in second time " + arr.length);
              arr2 = arr2.concat(arr);
            }
          })
          filterData = arr2;
        }
        console.log("total data in list " + this.state.flightListData.length);
        console.log(timeFilters.length);
        console.log(filterData);
        console.log(filterData.length);
        if (timeFilters.length == 0 && stopageFilter.length == 0 && !isRefundable && !isFreeMeal && airLines.length == 0) {
          if (!this.state.filterForReturn) {
            this.setState({
              isFilterActive: false,
              filterForDep: false,
              loading: !this.state.loading
            });
          } else {
            this.setState({
              filterForDep: false,
              loading: !this.state.loading
            });
          }
        } else {
          this.setState({
            filterDataForTwoWayDep: filterData,
            filteresFlightData: filterData,
            selectedOriginFlightIndex: filterData.length > 0 ? 0 : -1,
            isFilterActive: true,
            filterForDep: true,
            loading: !this.state.loading
          });
        }
      }
      else {
        this.setState({
          modelVisible: false,
          selectedFiltersArr: dict
        });
        let timeFilters1 = dict.timeFilter;
        let stopageFilter = dict.stopageFilter;
        let isRefundable = dict.refundable;
        let isFreeMeal = dict.freeMeal;
        let airLines = dict.airlines;
        const lastItem = this.state.completeResponseData[1][0].flightSegment[0].length - 1;
        let filterDate = this.state.completeResponseData[1][0].flightSegment[0][0].origin.depTime;
        if (filterDate) {
          filterDate = filterDate.split("T")[0];
        }
        console.log(dict);
        let filterData = this.state.completeResponseData[1];
        if (timeFilters1.length > 0 && timeFilters1.length < 4) {
          timeFilters1.forEach((item, index) => {
            let timeArr = String(item).split(" - ");
            //let finaltime1 = "2019-04-04T" + timeArr[0] + ":00";
            let finaltime1 = filterDate + "T" + timeArr[0] + ":00";
            let finaltime2 = filterDate;
            if (timeArr[1] == "24:00") {
              finaltime2 = finaltime2 + "T" + "23:59" + ":59";
            } else {
              finaltime2 = finaltime2 + "T" + timeArr[1] + ":00";
            }
            console.log("time is  " + finaltime1);
            let deptTime1 = finaltime1;
            let depTime2 = finaltime2;
            // let depttimeStr2 = dateFormat(finaltime2, "HH:MM");
            let arr = this.state.completeResponseData[1].filter(data => {
              let lastItem = data.flightSegment[0].length - 1;
              let flightDeptTime = data.flightSegment[0][lastItem].origin.depTime;
              if (flightDeptTime >= deptTime1 && flightDeptTime <= depTime2) {
                console.log("true")
                return true;
              } else {
                return false;
              }
            });
            console.log(arr);
            if (index == 0) {
              filterData = arr;
            } else {
              console.log("arr length in second time " + arr.length);
              filterData = filterData.concat(arr);
            }
            console.log("filter data after time filter " + filterData.length);
          });
        }
        if (stopageFilter.length > 0 && stopageFilter.length < 3) {
          stopageFilter.sort((a, b) => { return a - b })
          // console.log("DTTstopageFilter:", JSON.stringify(stopageFilter))
          // console.log("DTTflightSegment:", JSON.stringify(data.flightSegment))
          let filterArr = []
          stopageFilter.forEach((item, index) => {
            let arr = filterData.filter(data => {
              if (item == 0) {
                return data.flightSegment[0].length == 1;
              } else if (item == 1) {
                return data.flightSegment[0].length == 2;
              } else {
                return data.flightSegment[0].length > 2;
              }
            });
            if (index == 0) {
              filterArr = arr;
            } else {
              console.log(arr)
              filterArr = filterArr.concat(arr);
            }
          });
          filterData = filterArr;
          console.log("filter data after stopage filter 2 " + filterData.length);
        }
        if (isRefundable) {
          filterData = filterData.filter(data => {
            return data.isRefundable == true;
          });
          console.log("filter data after refundable filter " + filterData.length);
        }
        if (isFreeMeal) {
          filterData = filterData.filter(data => {
            return !data.isLCC;
          });
          console.log("filter data after free meal filter " + filterData.length);
        }
        if (airLines.length > 0) {
          let arr2 = [];
          airLines.forEach((airlineIndex, index) => {
            let arr = filterData.filter((flightItem) => {
              if (flightItem.flightSegment[0][lastItem].airline.airlineCode === this.arrvialAirlines[airlineIndex].airlineCode) {
                return true;
              } else {
                return false;
              }
            })
            if (index == 0) {
              arr2 = arr
            } else {
              arr2 = arr2.concat(arr)
            }
            // if (index == 0) {
            //   filterData = arr;
            // } else {
            //   console.log("arr length in second time " + arr.length);
            //   filterData = filterData.concat(arr);
            // }
          })
          filterData = arr2;
        }
        // console.log("total data in list " + this.state.flightListData.length);
        // console.log(timeFilters1.length);
        // console.log(filterData);
        // console.log(filterData.length);
        if (timeFilters1.length == 0 && stopageFilter.length == 0 && !isRefundable && !isFreeMeal && airLines.length == 0) {
          if (!this.state.filterForDep) {
            this.setState({
              isFilterActive: false,
              filterForReturn: false,
              loading: !this.state.loading
            });
          } else {
            this.setState({
              filterForReturn: false,
              loading: !this.state.loading
            });
          }
        } else {
          this.setState({
            filteresFlightData: filterData,
            filterDataForTwoWayRet: filterData,
            isFilterActive: true,
            filterForReturn: true,
            selectedDestinationFlightIndex: filterData.length > 0 ? 0 : -1,
            loading: !this.state.loading
          });
        }
      }
    }
    else if (this.state.isInterNational) {
      this.setState({
        modelVisible: false,
        selectedFiltersDepart: dict
      });
      let timeFilters = dict.timeFilter;
      let timeFilters2 = dict.timeFilter2;
      let stopageFilter = dict.stopageFilter;
      let isRefundable = dict.refundable;
      let isFreeMeal = dict.freeMeal;
      let airLines = dict.airlines;
      console.log(dict);
      let filterData = this.state.completeResponseData[0];
      if (((timeFilters.length > 0 && timeFilters.length < 4)) && (timeFilters2.length > 0 && timeFilters2.length < 4)) {
        let filterDate = this.state.completeResponseData[0][0].flightSegment[0][0].origin.depTime;
        if (filterDate) {
          filterDate = filterDate.split("T")[0];
        }
        timeFilters.forEach((item, index) => {
          let timeArr = String(item).split(" - ");
          //let finaltime1 = "2019-04-04T" + timeArr[0] + ":00";
          let finaltime1 = filterDate + "T" + timeArr[0] + ":00";
          let finaltime2 = filterDate;
          if (timeArr[1] == "24:00") {
            finaltime2 = finaltime2 + "T" + "23:59" + ":59";
          } else {
            finaltime2 = finaltime2 + "T" + timeArr[1] + ":00";
          }
          console.log("time is  " + finaltime1);
          let deptTime1 = finaltime1;
          let depTime2 = finaltime2;
          // let depttimeStr2 = dateFormat(finaltime2, "HH:MM");
          let arr = this.state.completeResponseData[0].filter(data => {
            let flightDeptTime = data.flightSegment[0][0].origin.depTime;
            if (flightDeptTime >= deptTime1 && flightDeptTime <= depTime2) {
              console.log("true")
              return true;
            } else {
              return false;
            }
          });
          console.log(arr);
          if (index == 0) {
            filterData = arr;
          } else {
            console.log("arr length in second time " + arr.length);
            filterData = filterData.concat(arr);
          }
          console.log("filter data after time filter " + filterData.length);
        });
        filterDate = this.state.completeResponseData[0][0].flightSegment[1][0].origin.depTime;
        if (filterDate) {
          filterDate = filterDate.split("T")[0];
        }
        timeFilters2.forEach((item, index) => {
          let timeArr = String(item).split(" - ");
          //let finaltime1 = "2019-04-04T" + timeArr[0] + ":00";
          let finaltime1 = filterDate + "T" + timeArr[0] + ":00";
          let finaltime2 = filterDate;
          if (timeArr[1] == "24:00") {
            finaltime2 = finaltime2 + "T" + "23:59" + ":59";
          } else {
            finaltime2 = finaltime2 + "T" + timeArr[1] + ":00";
          }
          console.log("time is  " + finaltime1);
          let deptTime1 = finaltime1;
          let depTime2 = finaltime2;
          // let depttimeStr2 = dateFormat(finaltime2, "HH:MM");
          let arr = filterData.filter(data => {
            let flightDeptTime = data.flightSegment[1][0].origin.depTime;
            if (flightDeptTime >= deptTime1 && flightDeptTime <= depTime2) {
              console.log("true")
              return true;
            } else {
              return false;
            }
          });
          console.log(arr);
          if (index == 0) {
            filterData = arr;
          } else {
            console.log("arr length in second time " + arr.length);
            filterData = filterData.concat(arr);
          }
          console.log("filter data after time filter " + filterData.length);
        });
      }
      else if (timeFilters.length > 0 && timeFilters.length < 4) {
        let filterDate = this.state.completeResponseData[0][0].flightSegment[0][0].origin.depTime;
        if (filterDate) {
          filterDate = filterDate.split("T")[0];
        }
        timeFilters.forEach((item, index) => {
          let timeArr = String(item).split(" - ");
          //let finaltime1 = "2019-04-04T" + timeArr[0] + ":00";
          let finaltime1 = filterDate + "T" + timeArr[0] + ":00";
          let finaltime2 = filterDate;
          if (timeArr[1] == "24:00") {
            finaltime2 = finaltime2 + "T" + "23:59" + ":59";
          } else {
            finaltime2 = finaltime2 + "T" + timeArr[1] + ":00";
          }
          console.log("time is  " + finaltime1);
          let deptTime1 = finaltime1;
          let depTime2 = finaltime2;
          // let depttimeStr2 = dateFormat(finaltime2, "HH:MM");
          let arr = filterData.filter(data => {
            let flightDeptTime = data.flightSegment[0][0].origin.depTime;
            if (flightDeptTime >= deptTime1 && flightDeptTime <= depTime2) {
              console.log("true")
              return true;
            } else {
              return false;
            }
          });
          console.log(arr);
          if (index == 0) {
            filterData = arr;
          } else {
            console.log("arr length in second time " + arr.length);
            filterData = filterData.concat(arr);
          }
          console.log("filter data after time filter " + filterData.length);
        });
      }
      else if (timeFilters2.length > 0 && timeFilters2.length < 4) {
        let filterDate = this.state.completeResponseData[0][0].flightSegment[1][0].origin.depTime;
        if (filterDate) {
          filterDate = filterDate.split("T")[0];
        }
        timeFilters2.forEach((item, index) => {
          let timeArr = String(item).split(" - ");
          //let finaltime1 = "2019-04-04T" + timeArr[0] + ":00";
          let finaltime1 = filterDate + "T" + timeArr[0] + ":00";
          let finaltime2 = filterDate;
          if (timeArr[1] == "24:00") {
            finaltime2 = finaltime2 + "T" + "23:59" + ":59";
          } else {
            finaltime2 = finaltime2 + "T" + timeArr[1] + ":00";
          }
          console.log("time is  " + finaltime1);
          let deptTime1 = finaltime1;
          let depTime2 = finaltime2;
          // let depttimeStr2 = dateFormat(finaltime2, "HH:MM");
          let arr = this.state.completeResponseData[0].filter(data => {
            let flightDeptTime = data.flightSegment[1][0].origin.depTime;
            if (flightDeptTime >= deptTime1 && flightDeptTime <= depTime2) {
              console.log("true")
              return true;
            } else {
              return false;
            }
          });
          console.log(arr);
          if (index == 0) {
            filterData = arr;
          } else {
            console.log("arr length in second time " + arr.length);
            filterData = filterData.concat(arr);
          }
          console.log("filter data after time filter " + filterData.length);
        });
      }
      if (stopageFilter.length > 0 && stopageFilter.length < 3) {
        stopageFilter.sort((a, b) => { return a - b })
        let filterArr = [];
        stopageFilter.forEach((item, index) => {
          let arr = filterData.filter(data => {
            if (item == 0) {
              if ((data.flightSegment[0].length == 1) || (data.flightSegment[1].length == 1)) {
                return true;
              }
            } else if (item == 1) {
              if ((data.flightSegment[0].length == 2) || (data.flightSegment[1].length == 2)) {
                return true;
              }
            } else {
              if ((data.flightSegment[0].length > 2) || (data.flightSegment[1].length > 2)) {
                return true;
              }
            }
          });
          if (index == 0) {
            filterArr = arr;
          } else {
            console.log(arr)
            filterArr = filterArr.concat(arr);
          }
        });
        filterData = filterArr;
        console.log("filter data after stopage filter 3 " + filterData.length);
      }
      if (isRefundable) {
        filterData = filterData.filter(data => {
          return data.isRefundable == true;
        });
        console.log("filter data after refundable filter " + filterData.length);
      }
      if (isFreeMeal) {
        filterData = filterData.filter(data => {
          return !data.isLCC;
        });
        console.log("filter data after free meal filter " + filterData.length);
      }
      if (airLines.length > 0) {
        let arr2 = [];
        airLines.forEach((airlineIndex, index) => {
          let arr = filterData.filter((flightItem) => {
            if (flightItem.flightSegment[0][0].airline.airlineCode === this.internationalAirlines[airlineIndex].airlineCode) {
              return true;
            } else {
              return false;
            }
          })
          if (index == 0) {
            arr2 = arr
          } else {
            arr2 = arr2.concat(arr)
          }
          // if (index == 0) {
          //   filterData = arr;
          // } else {
          //   console.log("arr length in second time " + arr.length);
          //   filterData = filterData.concat(arr);
          // }
        })
        filterData = arr2;
      }
      // console.log("total data in list " + this.state.flightListData.length);
      // console.log(timeFilters.length);
      // console.log(filterData);
      // console.log(filterData.length);
      if (timeFilters.length == 0 && timeFilters2.length == 0 && stopageFilter.length == 0 && !isRefundable && !isFreeMeal && airLines.length == 0) {
        this.setState({
          isFilterActive: false,
          loading: !this.state.loading
        });
      } else {
        this.setState({
          filteresFlightData: filterData,
          isFilterActive: true,
          selectedDestinationFlightIndex: filterData.length > 0 ? 0 : -1,
          loading: !this.state.loading
        });
      }
    }
    else {
      this.setState({
        modelVisible: false,
        selectedFiltersDepart: dict
      });

      let timeFilters = dict.timeFilter;
      let stopageFilter = dict.stopageFilter;
      let isRefundable = dict.refundable;
      let isFreeMeal = dict.freeMeal;
      let airLines = dict.airlines;
      //console.log("FLIGHT_LIST_DATA:" + JSON.stringify(this.state.flightListData[0].flightSegment[0][0]))
      let filterDate = this.state.flightListData[0].flightSegment[0][0].origin.depTime;
      if (filterDate) {
        filterDate = filterDate.split("T")[0];
      }
      // console.log(dict);
      let filterData = this.state.flightListData;

      if (timeFilters.length > 0 && timeFilters.length < 4) {
        timeFilters.forEach((item, index) => {
          let timeArr = String(item).split(" - ");
          //let finaltime1 = "2019-04-04T" + timeArr[0] + ":00";
          let finaltime1 = filterDate + "T" + timeArr[0] + ":00";
          let finaltime2 = filterDate;
          if (timeArr[1] == "24:00") {
            finaltime2 = finaltime2 + "T" + "23:59" + ":59";
          } else {
            finaltime2 = finaltime2 + "T" + timeArr[1] + ":00";
          }
          console.log("time is  " + finaltime1);

          let deptTime1 = finaltime1;
          let depTime2 = finaltime2;
          // let depttimeStr2 = dateFormat(finaltime2, "HH:MM");
          let arr = this.state.flightListData.filter(data => {
            let flightDeptTime = data.flightSegment[0][0].origin.depTime;
            if (flightDeptTime >= deptTime1 && flightDeptTime <= depTime2) {
              console.log("true")
              return true;
            } else {
              return false;
            }
          });
          console.log(arr);
          if (index == 0) {
            filterData = arr;
          } else {
            console.log("arr length in second time " + arr.length);
            filterData = filterData.concat(arr);
          }
          console.log("filter data after time filter " + filterData.length);
        });
      }
      if (stopageFilter.length > 0 && stopageFilter.length < 3) {
        stopageFilter.sort((a, b) => { return a - b })
        let filterArr = [];
        stopageFilter.forEach((item, index) => {

          // console.log("DTTstopageFilter:", JSON.stringify(stopageFilter))
          // console.log("DTTflightSegment:", JSON.stringify(data.flightSegment))
          let arr = filterData.filter(data => {
            if (item == 0) {
              return data.flightSegment[0].length == 1;
            } else if (item == 1) {
              return data.flightSegment[0].length == 2;
            } else {
              return data.flightSegment[0].length > 2;
            }
          });

          if (index == 0) {
            filterArr = arr;
          } else {
            filterArr = filterArr.concat(arr);
          }
        });
        filterData = filterArr;
        console.log("filter data after stopage filter 4 " + filterData.length);
      }
      if (isRefundable) {
        filterData = filterData.filter(data => {
          return data.isRefundable == true;
        });
        console.log("filter data after refundable filter " + filterData.length);
      }
      if (isFreeMeal) {
        filterData = filterData.filter(data => {
          console.log("sss", data)
          return !data.isLCC;
        });
        console.log("filter data after free meal filter " + filterData.length);
      }
      if (airLines.length > 0) {
        let arr2 = []
        airLines.forEach((airlineIndex, index) => {
          let arr = filterData.filter((flightItem) => {
            if (flightItem.flightSegment[0][0].airline.airlineCode === this.depAirlines[airlineIndex].airlineCode) {
              return true;
            } else {
              return false;
            }
          })
          if (index == 0) {
            arr2 = arr
          } else {
            arr2 = arr2.concat(arr)
          }
        })
        filterData = arr2;
      }
      // console.log("total data in list " + this.state.flightListData.length);
      // console.log(timeFilters.length);
      // console.clear();
      // console.log(JSON.stringify(filterData));
      // console.log(filterData.length);
      if (timeFilters.length == 0 && stopageFilter.length == 0 && !isRefundable && !isFreeMeal && airLines.length == 0) {
        this.setState({
          isFilterActive: false,
          loading: !this.state.loading
        });
      } else {
        this.setState({
          filteresFlightData: filterData,
          isFilterActive: true,
          selectedOriginFlightIndex: 0,
          loading: !this.state.loading
        });
      }
    }
    //  this.setState({
    //       selectedOriginFlightIndex: -1,
    //       selectedDestinationFlightIndex: -1,
    //       originFlightItem: null,
    //       destinationFlightItem: null,
    //     })
  }



  setUniqueAirlines() {
    let exp;
    const map = new Map();
    const map2 = new Map();
    console.log("set uniquee")
    if (this.props.flightSearchParams.return_date == null) {
      //for single way airlines filter
      exp = 1;
    } else if (this.props.flightSearchParams.return_date != null && this.state.completeResponseData.length == 2) {
      //for two way domestic airlines filter
      exp = 2;
    } else {
      //for Internatinal flights
      exp = 3;
    }
    console.log(exp)
    switch (exp) {
      case 1: {
        console.log("in case first");
        const response = this.state.completeResponseData[0];
        for (const item of response) {
          if (!map.has(item.flightSegment[0][0].airline.airlineCode)) {
            map.set(item.flightSegment[0][0].airline.airlineCode, true)
            this.depAirlines.push({
              airlineCode: item.flightSegment[0][0].airline.airlineCode,
              airlineName: item.flightSegment[0][0].airline.airlineName,
              airlineImage: item.flightSegment[0][0].airline.image
            })
          }
        }
      }
        break;
      case 2:
        {
          const departData = this.state.completeResponseData[0];
          const arrivalData = this.state.completeResponseData[1];
          for (const item of departData) {
            if (!map.has(item.flightSegment[0][0].airline.airlineCode)) {
              map.set(item.flightSegment[0][0].airline.airlineCode, true)
              this.depAirlines.push({
                airlineCode: item.flightSegment[0][0].airline.airlineCode,
                airlineName: item.flightSegment[0][0].airline.airlineName,
                airlineImage: item.flightSegment[0][0].airline.image
              })
            }
          }
          for (const item of arrivalData) {
            if (!map2.has(item.flightSegment[0][0].airline.airlineCode)) {
              map2.set(item.flightSegment[0][0].airline.airlineCode, true)
              this.arrvialAirlines.push({
                airlineCode: item.flightSegment[0][0].airline.airlineCode,
                airlineName: item.flightSegment[0][0].airline.airlineName,
                airlineImage: item.flightSegment[0][0].airline.image
              })
            }
          }

        }
        break;
      case 3:
        {
          const response = this.state.completeResponseData[0];
          for (const item of response) {
            if (!map.has(item.flightSegment[0][0].airline.airlineCode)) {
              map.set(item.flightSegment[0][0].airline.airlineCode, true)
              this.internationalAirlines.push({
                airlineCode: item.flightSegment[0][0].airline.airlineCode,
                airlineName: item.flightSegment[0][0].airline.airlineName,
                airlineImage: item.flightSegment[0][0].airline.image
              })
            }
          }
        }
        break;
    }

  }

  getFlightsList(data) {
    let param = this.props.flightSearchParams;
    // this.setState({ spinner: true });
    let newParam = JSON.parse(JSON.stringify(param))
    delete newParam['destinationCountryCode'];
    delete newParam['originCountryCode'];
    if (data) { newParam.reload = true }
    console.log('parameters for flights list   ' + JSON.stringify(newParam));
    this.props.searchFlightAction(newParam);
  }


  compareTodayWithdate(date) {
    // let now = dateFormat(new Date(), "yyyy-mm-dd");
    let now = moment(new Date()).format("YYYY-MM-DD");
    let dateCompare = moment(date).format("YYYY-MM-DD");

    if (now == dateCompare) {
      return "Today";
    } else {
      return moment(date).format("DD MMM");
    }
  }

  arrivalSort(flightData) {
    console.log("calling arr sort and flightData is");
    console.log(flightData)
    if (this.state.sortArrIncr) {
      console.log("increment")
      flightData.sort((a, b) => {
        let alastflightIndex = a.flightSegment[0].length - 1;
        let blastflightIndex = b.flightSegment[0].length - 1;
        let aDate = new Date(a.flightSegment[0][alastflightIndex].stopPointArrivalTime);
        let bDate = new Date(b.flightSegment[0][blastflightIndex].stopPointArrivalTime);
        if (aDate < bDate) {
          return -1;
        } else if (aDate > bDate) {
          return 1;
        } else {
          return 0;
        }
      })

    } else {

      console.log("decrement")
      flightData.sort((a, b) => {
        let alastflightIndex = a.flightSegment[0].length - 1;
        let blastflightIndex = b.flightSegment[0].length - 1;
        let aDate = new Date(a.flightSegment[0][alastflightIndex].stopPointArrivalTime);
        let bDate = new Date(b.flightSegment[0][blastflightIndex].stopPointArrivalTime);
        if (bDate < aDate) {
          return -1;
        } else if (bDate > aDate) {
          return 1;
        } else {
          return 0;
        }
      })

    }

    // console.log("sorted flight Data is ")
    // console.log(flightData);
    // console.log("returning flight Data");
    return flightData;

  }

  priceSort(flightData) {
    // console.log("calling flightSort sort and flightData is");
    // console.log(flightData)
    if (this.state.sortPriceIncr) {
      flightData.sort((a, b) => {
        // let aFare = new Date(a.fare.offeredFare);
        // let bDate = new Date(b.fare.offeredFare);
        if (a.fare.offeredFare < b.fare.offeredFare) {
          return -1;
        } else if (a.fare.offeredFare > b.fare.offeredFare) {
          return 1;
        } else {
          return 0;
        }
      })
    } else {
      flightData.sort((a, b) => {
        // let aDate = new Date(a.flightSegment[0][0].origin.depTime);
        // let bDate = new Date(b.flightSegment[0][0].origin.depTime);
        if (a.fare.offeredFare > b.fare.offeredFare) {
          return -1;
        } else if (a.fare.offeredFare < b.fare.offeredFare) {
          return 1;
        } else {
          return 0;
        }
      })
    }
    console.log("sorted flight Data is ")
    console.log(flightData);
    console.log("returning flight Data");
    return flightData;
  }

  durationSort(flightData) {
    console.log("calling duration sort and flightData is");
    console.log(flightData)
    if (this.state.sortDurIncr) {
      flightData.sort((a, b) => {
        let atotalDuration = 0;
        for (i = 0; i < a.flightSegment[0].length; i++) {
          let item = a.flightSegment[0][i];
          atotalDuration = atotalDuration + item.duration;
        }
        let btotalDuration = 0;
        for (i = 0; i < b.flightSegment[0].length; i++) {
          let item = b.flightSegment[0][i];
          btotalDuration = btotalDuration + item.duration;
        }
        if (atotalDuration < btotalDuration) {
          return -1;
        } else if (atotalDuration > btotalDuration) {
          return 1;
        } else {
          return 0;
        }
      })
    } else {
      flightData.sort((a, b) => {
        let atotalDuration = 0;
        for (i = 0; i < a.flightSegment[0].length; i++) {
          let item = a.flightSegment[0][i];
          atotalDuration = atotalDuration + item.duration;
        }
        let btotalDuration = 0;
        for (i = 0; i < b.flightSegment[0].length; i++) {
          let item = b.flightSegment[0][i];
          btotalDuration = btotalDuration + item.duration;
        }
        if (atotalDuration < btotalDuration) {
          return 1;
        } else if (atotalDuration > btotalDuration) {
          return -1;
        } else {
          return 0;
        }
      })
    }
    // console.log("sorted flight Data is ")
    // console.log(flightData);
    // console.log("returning flight Data");
    return flightData;
  }

  depSort(flightData) {
    console.log("calling dep sort and flightData is");
    console.log(flightData)
    if (this.state.sortDepIncr) {
      flightData.sort((a, b) => {
        let aDate = new Date(a.flightSegment[0][0].origin.depTime);
        let bDate = new Date(b.flightSegment[0][0].origin.depTime);
        if (aDate < bDate) {
          return -1;
        } else if (aDate > bDate) {
          return 1;
        } else {
          return 0;
        }
      })
    } else {
      flightData.sort((a, b) => {
        let aDate = new Date(a.flightSegment[0][0].origin.depTime);
        let bDate = new Date(b.flightSegment[0][0].origin.depTime);
        if (bDate < aDate) {
          return -1;
        } else if (bDate > aDate) {
          return 1;
        } else {
          return 0;
        }
      })
    }
    console.log("sorted flight Data is ")
    console.log(flightData);
    console.log("returning flight Data");
    return flightData;
  }

  airlineNameSort(flightData) {
    // console.log("calling dep sort and flightData is");
    // console.log(flightData)
    if (this.state.sortAirlineIncr) {
      flightData.sort((a, b) => {

        if (a.airlineName < b.airlineName) {
          return -1;
        } else if (a.airlineName > a.airlineName) {
          return 1;
        } else {
          return 0;
        }
      })
    } else {

      flightData.sort((a, b) => {
        if (b.airlineName < a.airlineName) {
          return -1;
        } else if (b.airlineName > a.airlineName) {
          return 1;
        } else {
          return 0;
        }
      })
    }
    return flightData;
  }

  sortFlights() {
    let flightData = [];
    if (this.state.isFilterActive) {
      flightData = this.state.filteresFlightData;
      switch (this.state.selectedSort) {
        case 0:
          // for departure sort    
          flightData = this.depSort(flightData);
          break;
        case 1:
          // for Arrival sort
          flightData = this.arrivalSort(flightData);
          break;
        case 2:
          // for duration sort{}
          {
            flightData = this.durationSort(flightData);
            break;
          }
        case 3:
          // for price sort
          flightData = this.priceSort(flightData);
          break;
        case 4:
          {
            flightData = this.airlineNameSort(flightData);
            break;
          }
      }
      this.setState({
        filteresFlightData: flightData,
        loading: !this.state.loading,
      })
    } else {
      flightData = this.state.flightListData;
      switch (this.state.selectedSort) {
        case 0:
          // for dep sort
          flightData = this.depSort(flightData);
          break;
        case 1:
          // for Arrival sort
          flightData = this.arrivalSort(flightData);
          break;
        case 2:
          // for duration sort{}
          {
            flightData = this.durationSort(flightData);
            break;
          }
        case 3:
          // for price sort
          flightData = this.priceSort(flightData);
          break;
        case 4: {
          flightData = this.airlineNameSort(flightData);
          break;
        }
      }
      this.setState({
        flightListData: flightData,
        loading: !this.state.loading,
      })
    }
  }

  _renderListdate({ item, index }) {
    return (
      <View style={styles.styleDateListItemSeperView}>
        <Text style={{ fontSize: 12, color: "gray" }}>
          {this.compareTodayWithdate(item.departs_at)}
        </Text>
        <View
          style={[
            styles.styleListDotView,
            {
              backgroundColor:
                index === this.state.selectedIndex
                  ? colors.colorBlue
                  : "gray"
            }
          ]}
        />
        <Text style={{ fontSize: 12, color: "gray", marginTop: 8 }}>
          {item.currency} {item.price_per_adult}
        </Text>
      </View>
    );
  }

  calculateArrivalAndDepartureTime(item) {
    let data = item.flightSegment[0];

    // let deptTime = dateFormat(data[0].stopPointDepartureTime,'hh:mm')
    // let arrTime = dateFormat(data[0].stopPointArrivalTime,'hh:mm')
    // return deptTime + '-' + arrTime

    // console.log('calculated item is  '+ JSON.stringify(data[data.lenght-1].stopPointArrivalTime))
    // console.log('calculated items depart time is  '+ JSON.stringify(data[0].stopPointDepartureTime))
    if (data.lenght == 1) {
      console.log(
        "depart Time is " + moment(data[0].stopPointDepartureTime).format("HH:mm")
      );
      console.log(
        "Arrival Time is " + moment(data[0].stopPointArrivalTime).format("HH:mm")
      );

      let depTime = String(data[0].stopPointDepartureTime).split("T");
      let arTime = String(data[0].stopPointArrivalTime).split("T");

      return (
        depTime[1].substring(0, depTime[1].length - 3) +
        "-" +
        arTime[1].substring(0, arTime[1].length - 3)
      );
    } else {

      let depTime = String(data[0].stopPointDepartureTime).split("T");
      let arTime = String(data[data.length - 1].stopPointArrivalTime).split(
        "T"
      );

      return (
        depTime[1].substring(0, depTime[1].length - 3) +
        "-" +
        arTime[1].substring(0, arTime[1].length - 3)
      );
      // return deptTime + '-' + arrTime
    }
  }

  calculateDurationTime(data) {
    // console.log("calculateDurationTime:", JSON.stringify(data))
    let totalDuration = 0;
    for (i = 0; i < data.length; i++) {
      let item = data[i];
      totalDuration = totalDuration + item.duration;
    }
    return this.convertMinsIntoHandM(totalDuration);
    // let depart = moment(data[0].origin.depTime);
    // let arrival = moment(data[data.length - 1].destination.arrTime);
    // return this.convertMinsIntoHandM(arrival.diff(depart, 'minutes'))


  }

  convertMinsIntoHandM(n) {
    var num = n;
    var hours = num / 60;
    var rhours = Math.floor(hours);
    var minutes = (hours - rhours) * 60;
    var rminutes = Math.round(minutes);
    return rhours + "h " + rminutes + "m";
  }
  returnNonStopOrByOriginText(item) {
    let viaCode = "";
    if (item.length == 1) {
      return " ";
    } else {
      item.forEach((item, index) => {
        if (index != 0) {
          if (index == 1) {
            viaCode = item.origin.airport.airportCode
          } else {
            viaCode = viaCode + "-" + item.origin.airport.airportCode
          }

        }
      });
      return viaCode;
    }
  }

  returnNonStopOrByOrigin(item) {

    if (item.length == 1) {
      return "Non-Stop";
    } else {
      return ((item.length - 1) + " Stop")
    }

  }

  onPressSingleWayListItem(item, index) {
    let dict = this.props.flightSearchParams;
    if (dict.return_date && this.state.completeResponseData.length == 2) {
      if (this.state.selectedOriginIndex == 0) {
        requestAnimationFrame(() => {
          this.setState({
            selectedOriginFlightIndex: index,
            loading: !this.state.loading,
            originFlightItem: item
          }, () => { this.calulateTwoWayPrice() });
        })
      } else {
        requestAnimationFrame(() => {
          this.setState({
            selectedDestinationFlightIndex: index,
            loading: !this.state.loading,
            destinationFlightItem: item
          }, () => { this.calulateTwoWayPrice() });

        })
      }
    }
    else {
      let finalArr = []
      finalArr.push(item)
      let dictPassenger = {
        adult: dict.ADT,
        child: dict.CHD,
        infant: dict.INF
      };
      let data = {
        flightdata: finalArr,
        traceid: this.state.traceId,
        flightType: 0,
        isRoundTrip: this.state.isRoundTrip,
        isInterNational: this.state.isInterNational,
        isNavigateFromReviewPage: false
      }
      this.props.saveFlightItem(data);
      // alert("fare is :--" + Math.ceil(item.fare.offeredFare));
      NavigationServices.navigate('FlightDetails', {
        flightdata: finalArr,
        traceid: this.state.traceId,
        passengers: dictPassenger,
        flightType: 0,
        origin: dict.origin,
        destination: dict.destination,
        isRoundTrip: this.state.isRoundTrip,
        isInterNational: this.state.isInterNational,
        isNavigateFromReviewPage: false,
        totalFare: Math.ceil(item.fare.offeredFare),
        currency: item.fare.currency
      });
    }

  }

  onPressInternationalFlightListItem(item) {
    let dict = this.props.flightSearchParams;
    let finalArr = [item]
    let dictPassenger = {
      adult: dict.ADT,
      child: dict.CHD,
      infant: dict.INF
    };
    let data = {
      flightdata: finalArr,
      traceid: this.state.traceId,

      flightType: 1,

      isRoundTrip: this.state.isRoundTrip,
      isInterNational: this.state.isInterNational,
      isNavigateFromReviewPage: false,
    }
    this.props.saveFlightItem(data);
    // alert("fare is :--" + Math.ceil(item.fare.offeredFare));
    NavigationServices.navigate('FlightDetails', {
      flightdata: finalArr,
      traceid: this.state.traceId,
      passengers: dictPassenger,
      flightType: 1,
      origin: dict.origin,
      destination: dict.destination,
      isRoundTrip: this.state.isRoundTrip,
      isInterNational: this.state.isInterNational,
      isNavigateFromReviewPage: false,
      totalFare: Math.ceil(item.fare.offeredFare),
      currency: item.fare.currency
    });
  }

  getBackgroundColorForItem(index) {
    if (this.state.completeResponseData.length == 1) {
      return colors.colorWhite;
    }
    if (this.state.selectedOriginIndex == 0) {
      let color = this.state.selectedOriginFlightIndex == index
        ? "rgb(229,239,249)"
        : colors.colorWhite;
      // console.log(color)
      return color
    }
    return this.state.selectedDestinationFlightIndex == index
      ? "rgb(229,239,249)"
      : colors.colorWhite;
  }

  _renderFlightSingleWayListItem({ item, index }) {
    let dict = this.props.flightSearchParams;
    return (
      <CardView cardElevation={3} cardMaxElevation={3} cornerRadius={10}
        style={{
          marginHorizontal: 2,
          marginVertical: 5
        }}>
        <TouchableOpacity key={index}
          disabled={!(dict.return_date && this.state.completeResponseData.length == 2)}
          style={[styles.card, {
            backgroundColor: this.getBackgroundColorForItem(index)
          }]} onPress={() => this.onPressSingleWayListItem(item, index)}>
          {this._renderCommonListItem(item, 0, true)}
          {this.renderPriceTab(item, () => this.onPressSingleWayListItem(item, index))}
        </TouchableOpacity>
      </CardView>
    );
  }

  renderPriceTab(item, onPressSubmit) {
    return <View>
      <View
        style={{ height: 1, backgroundColor: colors.lightgrey }}>
      </View>
      <View style={{ width: '100%', padding: 10, flexDirection: 'row', }}>
        <View style={{ width: '30%', flexDirection: 'row', alignItems: 'center' }}>
          {item.fare.currency && <Text style={{ fontFamily: Fonts.bold, color: colors.colorBlue }}>
            {item.fare.currency}{" "}
          </Text>}
          <Text style={{ fontFamily: Fonts.bold, color: colors.colorBlack21 }}>
            {Math.ceil(item.fare.agencyFare)}
          </Text>
        </View>
        <View style={{ width: '40%', alignItems: 'center', justifyContent: 'center' }}>
          <Text style={{ fontSize: 12, fontFamily: Fonts.medium, color: colors.colorBlack21 }}>
            {item.isRefundable ? "Refundable" : "Non Refundable"}
          </Text>
        </View>
        <View style={{ width: '30%', alignItems: 'flex-end', justifyContent: 'center' }}>
          {this.state.completeResponseData.length < 2 &&
            <TouchableOpacity
              onPress={onPressSubmit}
              style={{ paddingVertical: 6, paddingHorizontal: 8, backgroundColor: colors.colorBlue, borderRadius: 7 }}>
              <Text style={{ color: 'white', fontFamily: Fonts.semiBold, fontSize: 12 }}>
                BOOK NOW
            </Text>
            </TouchableOpacity>}
        </View>

      </View>
    </View>

  }

  _renderInterNationalFlightListItem({ item, index }) {
     console.log("InterNational Item:", JSON.stringify(item))
    return (
      <CardView cardElevation={3} cardMaxElevation={3} cornerRadius={10}
        style={{
          marginHorizontal: 1,
          marginVertical: 5
        }}>
        <View style={styles.doubleCard}>
          <View style={{ flexDirection: "row", flex: 1, width: '100%' }}>
            <View style={{
              width: "100%",
              height: "100%",
              borderRightWidth: 1,
              borderRightColor: colors.lightgrey,
            }}>
              {this._renderCommonListItem(item, 0, false)}
              {this._renderCommonListItem(item, 1, false)}
              {this.renderPriceTab(item, () => this.onPressInternationalFlightListItem(item))}
            </View>
            {/* <View style={styles.cardContentCol2}>
            <View style={{ backgroundColor: colors.colorBlue, width: 75, elevation: 6, paddingTop: 5, paddingBottom: 5, borderRadius: 5, alignItems: "center" }}>
              <View style={{ flexDirection: "row", alignItems: "center", justifyContent: "center" }}>
                <Text style={{ color: colors.colorWhite, fontFamily: Fonts.bold, }}>
                  
                  {item.fare.currency}
                </Text>
                <Text style={{ color: colors.colorWhite, marginLeft: 3, textAlign: "right", fontFamily: Fonts.bold }}>
                  {Math.ceil(item.fare.offeredFare)}
                </Text>
              </View>
              <Text style={{ color: colors.colorWhite, textAlign: "center", fontSize: 10, fontFamily: Fonts.bold }}>BOOK</Text>
            </View>
            <View style={{ width: 55, justifyContent: "flex-end", alignItems: "center" }}>
              <Text numberOfLines={2} style={[styles.listItemGreyText, { marginTop: 5, alignSelf: "center", textAlign: "center", fontSize: 9 }]}>
                {item.isRefundable ? "Refundable" : "Non Refundable"}
              </Text>
            </View>
          </View> */}
          </View>
        </View>
      </CardView>
    );
  }

  _renderCommonListItem(item, sagmentIndex, isNational) {
    let airline = item.flightSegment[sagmentIndex][0].airline
    let operatingCarrier = airline.operatingCarrier;
    if (operatingCarrier == "" || operatingCarrier == null ||
      operatingCarrier == airline.airlineCode) {
      operatingCarrier = null
    }

    

    // console.log("myItem:", item)
    // console.log("sagmentIndex:", sagmentIndex)
    // console.log("isNational:", isNational)

    return (
      <View>
        <View style={[styles.cardHeader, { backgroundColor: colors.gray }]}>
          <View style={[styles.cardHeaderCol1, { width: isNational ? "100%" : "100%" }]}>
            <View style={styles.cardheaderFlightView}>
              <Image source={{ uri: imageBaseUrl + airline.image }}
                style={styles.cardheaderFlightImg}></Image>
              <View><Text style={[styles.price, { color: colors.colorBlack }]}>{airline.airlineName + " | "}{airline.airlineCode + "-" + airline.flightNumber} {operatingCarrier && <Text style={[styles.price, { color: colors.colorBlack, fontSize: 11 }]}>{" (Operated by: " + operatingCarrier + ")"}</Text>} </Text>

              </View>
            </View>
            <View style={styles.cardHeaderAmenties}>
              {item.isLCC == false &&
                <View style={{ paddingHorizontal: 4 }}>
                  <Image source={Images.ic_image_food} style={styles.flightAmenityImg} />
                </View>}
              {
                item.flightSegment[0][0].seatAvailable &&
                <View style={{ flexDirection: 'row', paddingHorizontal: 4 }}>
                  <Image source={Images.ic_image_seat} style={styles.flightAmenityImg} />
                  <View style={{
                    width: 18,
                    height: 20, padding: 2,
                    marginHorizontal: 4, backgroundColor: 'red', overflow: 'hidden',
                    borderRadius: 5, alignItems: 'center', justifyContent: 'center'
                  }}>
                    <Text style={{ color: 'white', fontSize: 12, fontFamily: Fonts.bold }}>{item.flightSegment[0][0].seatAvailable}</Text>
                  </View>
                </View>
              }
            </View>
          </View>
        </View>
        <View style={styles.cardContent}>
          <View style={[styles.cardContentCol1, { width: "100%" }]}>
            <View style={{ width: "15%" }}>
              <Text style={styles.estimate}>{moment(item.flightSegment[sagmentIndex][0].origin.depTime).format("HH:mm")}</Text>
              <Text style={{ fontFamily: Fonts.medium, color: colors.colorBlue }}>{item.flightSegment[sagmentIndex][0].origin.airport.airportCode}</Text>
            </View>
            {this.renderDashView(item.flightSegment[sagmentIndex], isNational)}
            <View style={{ width: "15%", alignItems: "flex-end" }}>
              <Text style={styles.estimate}>{moment(item.flightSegment[sagmentIndex][(item.flightSegment[sagmentIndex].length - 1)].destination.arrTime).format("HH:mm")}</Text>
              <Text style={[styles.stnName, { fontFamily: Fonts.medium, color: colors.colorBlue }]}>{item.flightSegment[sagmentIndex][(item.flightSegment[sagmentIndex].length - 1)].destination.airport.airportCode}</Text>
            </View>
          </View>
          <View style={[styles.cardContentCol1, { width: "100%" }]}>
            <View style={{ width: "50%" }}>
              <Text style={styles.stnName}>{moment(item.flightSegment[sagmentIndex][0].origin.depTime).format("DD MMM, YYYY")}</Text>
            </View>
            <View style={{ width: "50%", alignItems: "flex-end", alignSelf: 'flex-end' }}>
              <Text style={styles.stnName}>{moment(item.flightSegment[sagmentIndex][(item.flightSegment[sagmentIndex].length - 1)].destination.arrTime).format("DD MMM, YYYY")}</Text>
            </View>
          </View>
        </View>

      </View>
    )
  }

  renderDashView = (item) => {
    // if (!isNational) {
    return (
      <View style={{
        width: "70%",
        // backgroundColor: 'red',
        justifyContent: "center",
        alignItems: "center",
      }}>
        <View style={{
          width: "100%",
          height: 20,
          // backgroundColor: 'red',
          flexDirection: "row",
          justifyContent: "center",
          alignItems: "center",
        }}>
          <View style={{
            width: "45%",
            alignItems: "center"
            // backgroundColor: "blue"
          }}>
            <Text style={styles.stnName}>{this.returnNonStopOrByOrigin(item)}</Text>
            <View style={styles.dotView}>
              <View style={{ backgroundColor: colors.colorBlue, borderRadius: 5, height: 10, width: 10 }} />
              {/* <Image source={Images.imgbluedot} style={{ width: "10%", resizeMode: "contain" }}></Image> */}
              <Dash style={{
                width: "75%",
                height: 1,
              }} dashColor={colors.lightgrey} dashLength={5}
              />
            </View>
            {item.length > 1 ?
              <View style={{ width: '70%', alignItems: 'center', overflow: 'hidden' }}>
                <Text ellipsizeMode={'tail'} numberOfLines={1} style={[styles.stnName]}>{this.returnNonStopOrByOriginText(item)}</Text>
              </View> : <Text style={styles.stnName}>{" "}</Text>
            }
          </View>
          <View style={{
            width: "10%",
          }}>
            <Image source={Images.ic_image_planeblue} style={{ width: "100%", height: "100%", resizeMode: "contain" }}></Image>
          </View>
          <View style={{
            width: "45%",
            alignItems: "center"
          }}>
            <Text style={styles.stnName}>{this.calculateDurationTime(item)}</Text>
            <View style={styles.dotView}>
              <Dash style={{
                width: "75%",
                height: 1,
              }} dashColor={colors.lightgrey} dashLength={5}
              />
              <View style={{ backgroundColor: colors.colorBlue, borderRadius: 5, height: 10, width: 10 }} />
              {/* <Image source={Images.imgbluedot} style={{ width: "10%", resizeMode: "contain" }}></Image> */}
            </View>
            <Text style={styles.stnName}>{" "}</Text>
          </View>


        </View>


      </View>
    )
    // } else {
    //   return (
    //     <View style={{ width: "60%", alignItems: "center" }}>
    //       <Text style={styles.stnName}>{this.calculateDurationTime(item)}</Text>
    //       <View style={styles.dotView}>
    //         <Image source={Images.imgbluedot} style={{ width: "10%", resizeMode: "contain" }}></Image>
    //         <Dash style={{
    //           width: "75%",
    //           height: 1,
    //         }} dashColor={colors.lightgrey} dashLength={5}
    //         />
    //         <Image source={Images.imgbluedot} style={{ width: "10%", resizeMode: "contain" }}></Image>
    //       </View>
    //       <Text style={styles.stnName}>{this.returnNonStopOrByOrigin(item)}</Text>
    //     </View>
    //   )
    // }
  }

  _renderSagmentView() {
    let dict = this.props.flightSearchParams;
    if (dict.return_date && this.state.completeResponseData.length == 2) {
      return (
        <View style={[styles.styleSagmentVIew, { height: 60 }]}>
          <View style={styles.styleSagementButtonsView}>
            <TouchableHighlight
              style={[
                styles.styleSagmentButton,
                {
                  backgroundColor:
                    this.state.selectedOriginIndex == 0
                      ? colors.colorBlue
                      : "transparent"
                }
              ]}
              activeOpacity={0.9}
              underlayColor="transparent"
              onPress={() => {
                if (this.state.isFilterActive) {
                  if (this.state.filterForDep) {
                    this.setState({
                      selectedSort: 3,
                      sortPriceIncr: true,
                      filteresFlightData: this.state.filterDataForTwoWayDep,
                      selectedOriginIndex: 0,
                      loading: !this.state.loading
                    }, () => { this.scrollToitemindex() });
                  } else {
                    this.setState({
                      selectedSort: 3,
                      sortPriceIncr: true,
                      filteresFlightData: this.state.completeResponseData[0],
                      selectedOriginIndex: 0,
                      loading: !this.state.loading
                    }, () => { this.scrollToitemindex() });
                  }
                } else {
                  this.setState({
                    selectedSort: 3,
                    sortPriceIncr: true,
                    flightListData: this.state.completeResponseData[0],
                    selectedOriginIndex: 0,
                    loading: !this.state.loading
                  }, () => { this.scrollToitemindex() });
                }
              }}
            >
              <Text
                style={{
                  fontSize: 14,
                  color: this.state.selectedOriginIndex == 0 ? colors.colorWhite : colors.colorBlue,
                  fontFamily: Fonts.semiBold
                }}
              >
                {dict.origin + " - " + dict.destination}
              </Text>
            </TouchableHighlight>

            <TouchableHighlight
              style={[
                styles.styleSagmentButton,
                {
                  backgroundColor:
                    this.state.selectedOriginIndex == 1
                      ? colors.colorBlue
                      : "transparent"
                }
              ]}
              activeOpacity={0.9}
              underlayColor="transparent"
              onPress={() => {
                if (this.state.isFilterActive) {
                  if (this.state.filterForReturn) {
                    this.setState({
                      selectedSort: 3,
                      sortPriceIncr: true,
                      filteresFlightData: this.state.filterDataForTwoWayRet,
                      selectedOriginIndex: 1,
                      loading: !this.state.loading
                    }, () => { this.scrollToitemindex() });
                  } else {
                    this.setState({
                      selectedSort: 3,
                      sortPriceIncr: true,
                      filteresFlightData: this.state.completeResponseData[1],
                      selectedOriginIndex: 1,
                      loading: !this.state.loading
                    }, () => { this.scrollToitemindex() });
                  }
                } else {
                  this.setState({
                    selectedSort: 3,
                    sortPriceIncr: true,
                    flightListData: this.state.completeResponseData[1],
                    selectedOriginIndex: 1,
                    loading: !this.state.loading
                  }, () => { this.scrollToitemindex() });
                }
              }}
            >
              <Text
                style={{
                  fontSize: 14,
                  color:
                    this.state.selectedOriginIndex == 1
                      ? colors.colorWhite
                      : colors.colorBlue,
                  fontFamily: Fonts.semiBold
                }}
              >
                {dict.destination + " - " + dict.origin}
              </Text>
            </TouchableHighlight>
          </View>
        </View>
      );
    }
    return null;
  }

  _renderBookNowBtn() {
    let dict = this.props.flightSearchParams;
    if (dict.return_date && this.state.completeResponseData.length == 2) {
      let dict1 = this.state.originFlightItem
      return (
        <View style={styles.bottomVIew}>
          <View style={{ flexDirection: "row" }}>
            <View style={{ width: '30%', justifyContent: 'center' }}>
              <View style={{ flexDirection: "row", marginLeft: 5, overflow: 'hidden', alignItems: 'center' }}>
                <Image
                  style={styles.selectedflighticon}
                  source={{ uri: imageBaseUrl + this.state.originFlightItem.flightSegment[0][0].airline.image }}
                />
                <Image
                  style={[styles.selectedflighticon, { marginLeft: 15, }]}
                  source={{ uri: imageBaseUrl + this.state.destinationFlightItem.flightSegment[0][0].airline.image }}
                />
              </View>
            </View>
            <View style={{ width: '40%', alignItems: 'center', justifyContent: 'center' }}>
              <Text style={{ fontFamily: Fonts.bold, color: colors.colorWhite, fontSize: 16 }}>
                {/*₹*/} {dict1 && dict1.fare.currency + " "} {Math.ceil(this.state.twoWayTotalPrice)}
              </Text>
            </View>
            <View style={{ width: '30%', alignItems: 'flex-end', justifyContent: 'center' }}>

              <View style={styles.BookbtnStyle}>
                <TouchableOpacity
                  onPress={() => {
                    let arr1 = this.state.completeResponseData[0]
                    let arr2 = this.state.completeResponseData[1]
                    let dict1 = arr1[this.state.selectedOriginFlightIndex]
                    let finalArr = [this.state.originFlightItem, this.state.destinationFlightItem]
                    let dictPassenger = {
                      adult: dict.ADT,
                      child: dict.CHD,
                      infant: dict.INF
                    };
                    let data = {
                      flightdata: finalArr,
                      traceid: this.state.traceId,
                      flightType: 1,
                      isRoundTrip: this.state.isRoundTrip,
                      isInterNational: this.state.isInterNational,
                      isNavigateFromReviewPage: false,
                    }
                    this.props.saveFlightItem(data);
                    // alert("fare is :--" + Math.ceil(this.state.twoWayTotalPrice));
                    NavigationServices.navigate('FlightDetails', {
                      flightdata: finalArr,
                      traceid: this.state.traceId,
                      passengers: dictPassenger,
                      flightType: 1,
                      origin: dict.origin,
                      destination: dict.destination,
                      isRoundTrip: this.state.isRoundTrip,
                      isInterNational: this.state.isInterNational,
                      isNavigateFromReviewPage: false,
                      totalFare: Math.ceil(this.state.twoWayTotalPrice),
                      currency: dict1.fare.currency
                    });
                  }}>
                  <Text style={{ fontSize: 12, color: colors.colorBlue, fontFamily: Fonts.bold }}>
                    CONTINUE
                </Text>
                </TouchableOpacity>
              </View>
            </View>

          </View>

        </View>
      );
    }
    return <View />;
  }

  //departure view with 5 tab Airline Departure arrival duration and price 
  //based on which sorting will be performed 
  _renderdepartureView() {
    return (
      <View style={styles.tabs}>
        <TouchableOpacity style={[styles.tab, { display: "none" }]}
          onPress={() => {
            this.setState({
              sortAirlineIncr: !this.state.sortAirlineIncr,
              selectedSort: 4,
              spinner: true
            }, () => {
              setTimeout(() => { this.setState({ spinner: false }) }, 1000)
            })
            this.sortFlights()
          }}>
          <View
            style={{ flexDirection: "row", alignItems: "center" }}>
            <Text
              style={{
                fontSize: 12,
                color: (this.state.selectedSort == 4) ? colors.colorBlue : "grey",
                fontFamily: Fonts.semiBold
              }}
            >
              AirLines
        </Text>
            {this.state.selectedSort == 4
              ?
              <Image style={{ height: 12, width: 5, marginLeft: 5 }}
                source={
                  this.state.sortAirlineIncr
                    ? Images.ic_sorting_dec
                    : Images.ic_sorting_inc}
              />
              :
              null}
          </View>
        </TouchableOpacity>

        <TouchableOpacity
          underlayColor={"transparent"}
          style={styles.tab}
          onPress={() => {
            // requestAnimationFrame(()=>{
            console.log("departure click");
            this.setState({
              sortDepIncr: !this.state.sortDepIncr,
              selectedSort: 0,
              spinner: true
            }, () => {
              this.sortFlights()
              setTimeout(() => { this.setState({ spinner: false }) }, 1000)
            })
            // });

          }}
        >
          <Text style={styles.tabText}>Departure</Text>
          {this.state.selectedSort == 0
            ?
            <Image style={{ height: 12, width: 5, marginLeft: 5 }}
              source={
                this.state.sortDepIncr
                  ? Images.ic_sorting_dec
                  : Images.ic_sorting_inc}
            />
            :
            null}
        </TouchableOpacity>

        <TouchableOpacity style={[styles.tab, {
          borderLeftWidth: 1,
          borderRightWidth: 1,
          borderColor: "lightgray"
        }]} onPress={() => {
          this.setState({
            sortDurIncr: !this.state.sortDurIncr,
            spinner: true,
            selectedSort: 2
          }, () => {
            this.sortFlights();
            setTimeout(() => { this.setState({ spinner: false }) }, 1000)
          })
        }}>
          <Text style={styles.tabText}>Duration</Text>
          {this.state.selectedSort == 2
            ?
            <Image style={{ height: 12, width: 5, marginLeft: 5 }}
              source={
                this.state.sortDurIncr
                  ? Images.ic_sorting_dec
                  : Images.ic_sorting_inc}
            />
            :
            null}
        </TouchableOpacity>

        <TouchableOpacity style={[styles.tab, { display: "none" }]}
          underlayColor={"transparent"}
          onPress={() => {
            console.log("arrival click");
            this.setState({
              sortArrIncr: !this.state.sortArrIncr,
              selectedSort: 1,
              spinner: true
            }, () => {
              this.sortFlights()
              setTimeout(() => { this.setState({ spinner: false }) }, 1000)
            })
          }} >
          <View
            style={{ flexDirection: "row", alignItems: "center" }}>
            <Text style={{
              fontSize: 12,
              color: (this.state.selectedSort == 1) ? colors.colorBlue : "grey"
              ,
              fontFamily: "Montserrat-SemiBold"
            }}>
              Arrival
        </Text>
            {this.state.selectedSort == 1
              ?
              <Image style={{ height: 12, width: 5, marginLeft: 5 }}
                source={
                  this.state.sortArrIncr
                    ? Images.ic_sorting_dec
                    : Images.ic_sorting_inc}
              />
              :
              null}
          </View>

        </TouchableOpacity>

        <TouchableOpacity style={styles.tab}
          onPress={() => {
            this.setState({
              selectedSort: 3,
              spinner: true,
              sortPriceIncr: !this.state.sortPriceIncr,
            }, () => {
              this.sortFlights();
              setTimeout(() => { this.setState({ spinner: false }) }, 1000)
            })
          }}>
          <Text style={styles.tabText}>Price</Text>
          {this.state.selectedSort == 3
            ?
            <Image style={{ height: 12, width: 5, marginLeft: 5 }}
              source={
                this.state.sortPriceIncr
                  ? Images.ic_sorting_dec
                  : Images.ic_sorting_inc}
            />
            :
            null}
        </TouchableOpacity>
      </View>
    )
  }

  _renderItemSeperator = () => (
    <View style={{
      marginLeft: 5,
      marginRight: 5,
      height: 12,
      backgroundColor: colors.colorWhite
    }} />
  )

  filterFlight = () => {
    this.setState({ modelVisible: true });
  }

  render() {
    const { destination, origin, return_date } = this.props.flightSearchParams;
    let originCity
    let airlines
    if (this.state.isRoundTrip && this.state.selectedOriginIndex == 1) {
      originCity = destination;
      airlines = this.arrvialAirlines
    } else if (this.state.isRoundTrip && this.state.selectedOriginIndex == 0) {
      originCity = origin;
      airlines = this.depAirlines
    } else if (this.state.isInterNational) {
      originCity = origin;
      airlines = this.internationalAirlines
    } else {
      originCity = origin;
      airlines = this.depAirlines
    }
    // let dict = this.props.flightSearchParams;
    return (

      <View style={{ flex: 1, }}>
        <SafeAreaView backgroundColor={colors.colorBlue} />
        <Spinner
          style={{ flex: 1 }}
          visible={this.state.spinner}
          textContent={"Loading..."}
          textStyle={{ color: "white" }}
        />
        <View style={styles.container}>
          <FlightListHeader
            fireEvent={this.filterFlight}
            data={this.props.flightSearchParams}
            rightImg={Images.ic_image_filter} />
          {this._renderSagmentView()}
          {this._renderdepartureView()}
          <View style={styles.mainView}>
            <FlatList
              showsVerticalScrollIndicator={false}
              ref={(ref) => { this.flatListref = ref }}
              data={
                this.state.isFilterActive
                  ? this.state.filteresFlightData
                  : this.state.flightListData
              }
              extraData={this.state.loading}
              renderItem={
                (return_date && this.state.completeResponseData.length == 2) ||
                  return_date == null
                  ? this._renderFlightSingleWayListItem
                  : this._renderInterNationalFlightListItem
              }
              // ItemSeparatorComponent={return_date == null ? this._renderItemSeperator : null}
              keyExtractor={(item, index) => index.toString()}
            />
          </View>
          {this._renderBookNowBtn()}
        </View>
        {/* </ScrollView> */}
        <Modal
          transparent={false}
          animationType={"slide"}
          visible={this.state.modelVisible}>
          <View
            style={{ flex: 1 }}>
            <FlightFilters
              originCity={originCity}
              destCity={destination}
              airlines={airlines}
              isInterNational={this.state.isInterNational}
              onModelHide={() => {
                this.setState({
                  modelVisible: false
                });
              }}
              modelVisible={this.state.modelVisible}
              slectedFilters={this.state.isRoundTrip && this.state.selectedOriginIndex == 1 ? this.state.selectedFiltersArr : this.state.selectedFiltersDepart}
              onFilterSelection={filterDict => {
                this.onFilterSelection(filterDict);
              }}
            />
          </View>
        </Modal>
        <SafeAreaView backgroundColor={colors.colorBlue} />
      </View>
    );
  }
}


const mapStateToProps = state => {
  // console.log("FlightListReduxState:", JSON.stringify(state))
  return {
    flightSearchParams: state.FlightsReducer.flightSearchParams,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    saveFlightItem: (data) => dispatch(saveFlightItem(data)),
    searchFlightAction: (data) => dispatch(searchFlightAction(data))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(FlightList)