import { Dimensions, StyleSheet } from "react-native";
import { colors } from "../../../Utils/Colors";
import Fonts from '../../../Utils/Fonts';
const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;

export default styles = StyleSheet.create({
  doubleCard: {
    borderRadius: 10,
    overflow: "hidden",
    backgroundColor: colors.colorWhite,
  },
  bottomVIew: {
    height: 50,
    width: screenWidth,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    paddingRight: 14,
    paddingLeft: 14,
    backgroundColor: colors.colorBlue,
    //position: "absolute",
    bottom: 0,
    left: 0,
    right: 0
  },
  flightAmenityImg: {
    width: 20, height: 20,
    resizeMode: "contain",
  },
  cardHeaderAmenties: {
    width: "40%",
    height: "100%",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "flex-end",
    paddingLeft: 3,
    paddingRight: 10,
  },
  cardheaderFlightImg: {
    width: 20, height: 20,
    resizeMode: "contain", marginRight: 5
  },
  cardheaderFlightView: {
    height: "100%",
    width: "60%",
    flexDirection: "row",
    alignItems: "center"
  },
  bookBtnText: {
    color: colors.colorWhite,
    fontSize: 12,
    fontFamily: Fonts.semiBold
  },
  bookButton: {
    backgroundColor: colors.colorBlue,
    borderRadius: 7,
    padding: 7
  },
  cardHeaderCol2: {
    width: "30%",
    height: "100%",
    justifyContent: "center",
    alignItems: "center"
  },
  cardHeaderCol1: {
    width: "70%",
    height: "100%",
    flexDirection: "row"
  },
  stnName: {
    fontFamily: Fonts.regular,
    color: colors.colorBlack,
    fontSize: 14,
    lineHeight: 20,
  },
  dotView: {
    height: 10,
    width: "100%",
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center"
  },
  estimate: {
    fontFamily: Fonts.semiBold,
    color: colors.colorBlack,
    fontSize: 14
  },
  priceImg: {
    width: 12, height: 12, resizeMode: "contain"
  },
  priceText: {
    flexDirection: "row",
    color: colors.colorBlack,
    fontFamily: Fonts.bold,
    fontSize: 14
  },
  cardContentCol2: {
    width: "20%",
    justifyContent: "center",
    alignItems: "center"
  },
  cardContentCol1: {
    width: "70%",
    alignItems: "center",
    paddingTop: 5,
    flexDirection: "row",
    justifyContent: "space-between"
  },
  cardContent: {
    width: "100%",
    paddingVertical: 10,
    justifyContent: "space-between",
    alignItems: "center",
    paddingHorizontal: 10
  },
  cardHeader: {
    height: 35,
    width: "100%",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    backgroundColor: colors.gray,
    paddingLeft: 7
  },
  card: {
    borderRadius: 10,
    // height: 120,
    width: "100%",
    overflow: "hidden",
    // marginBottom: 20,
    backgroundColor: colors.colorWhite
  },
  mainView: {
    width: '100%',
    flex: 1,
    // height: screenHeight - 180,
    paddingHorizontal: 12,
    // paddingBottom:50
  },
  price: {
    color: colors.colorBlue,
    fontFamily: Fonts.medium,
    fontSize: 12
  },
  priceBtn: {
    width: 120,
    height: "100%",
    justifyContent: "center",
    alignItems: "center",
  },
  priceDevider: {
    width: .5,
    height: 40,
    backgroundColor: colors.colorBlack
  },
  priceView: {
    height: 50,
    width: 120,
    flexDirection: "row",
    alignItems: "center",
    borderTopWidth: 1.5,
  },
  pricesView: {
    width: screenWidth,
    height: 50,
    backgroundColor: colors.gray,
    position: "absolute",
    bottom: 0
  },
  tabs: {
    height: 40,
    width: "100%",
    flexDirection: "row",
    backgroundColor: colors.gray,
    flexDirection: "row"
  },
  tabText: {
    color: colors.colorBlack,
    fontSize: 14,
    fontFamily: Fonts.semiBold
  },
  tab: {
    width: screenWidth / 3,
    height: "100%",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center"
  },
  headerRowDevider: {
    height: 12, width: 1.5,
    backgroundColor: colors.colorWhite,
    marginLeft: 7,
    marginRight: 7,
    marginTop: 5
  },
  headerContentRowText: {
    color: colors.colorWhite,
    fontSize: 12,
    fontFamily: Fonts.medium,
    lineHeight: 20
  },
  headerContentIcon: {
    width: 20, height: 20,
    resizeMode: "contain",
    marginLeft: 10,
    marginRight: 10
  },
  headerStnName: {
    color: colors.colorWhite,
    fontSize: 14,
    fontFamily: Fonts.semiBold
  },
  headerContentRow: { width: "100%", flexDirection: "row", justifyContent: "center" },
  headerContent: {
    height: "100%",
    width: screenWidth - 120,
    padding: 8,
    justifyContent: "center"
  },
  backBtnText: {
    color: colors.colorWhite,
    fontFamily: Fonts.semiBold
  },
  container: {
    flex: 1,
  },
  header: {
    height: 70,
    width: screenWidth,
    backgroundColor: colors.colorBlue,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    paddingLeft: 12,
    paddingRight: 12
  },
  headerBackButton: {
    width: 60,
    // backgroundColor: "red",
    height: "100%",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "flex-start"
  },
  headerButton: {
    height: "100%",
    width: 40,
    // backgroundColor: "blue",
    justifyContent: "center",
    alignItems: "flex-end"
  },
  styleSuperView: {
    flex: 1,

  },
  styleDateListItemSeperView: {
    height: 80,
    width: (screenWidth - 30) / 4,
    justifyContent: "center",
    alignItems: "center"
  },
  styleListDotView: {
    height: 10,
    width: 10,
    borderRadius: 5,
    marginTop: 8
  },

  styleDepatureView: {
    flexDirection: "row",
    justifyContent: "center",
    marginTop: 10,
    borderRadius: 5,
    backgroundColor: "white",
    shadowColor: colors.colorShadow,
    shadowOpacity: 0.3,
    marginLeft: 5,
    marginRight: 5,
    elevation: 6,
    paddingTop: 10,
    paddingBottom: 10,
    shadowOffset: { width: 1, height: 1 }
    // shadowRadius: 5
  },
  styleDepartureText: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  styleFlightListView: {


    marginTop: 10,
    marginLeft: 5,
    marginRight: 5,
    marginBottom: 10,
    flex: 1,
    shadowColor: colors.colorShadow,
    shadowOffset: { width: 1, height: 1 },
    shadowOpacity: 0.2,
    shadowRadius: 3,
    elevation: 2
    // shadowColor: "black",
    // shadowOpacity: 0.5,
    // shadowOffset: { width: 2, height: 2 },
    // shadowRadius: 5
  },
  styleSagmentVIew: {
    justifyContent: "center",
    justifyContent: "center",
    marginTop: 10,
    marginLeft: 10,
    marginRight: 10
  },
  styleSagementButtonsView: {
    width: "100%",
    height: "70%",
    backgroundColor: "white",
    flexDirection: "row",
    borderRadius: 21,
    elevation: 6,
    shadowColor: colors.colorShadow,
    shadowOffset: { width: 1, height: 1 },
    shadowOpacity: 0.5
  },
  styleSagmentButton: {
    height: "100%",
    width: "50%",
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 21
  },

  // my style

  listItemText: {
    fontSize: 12,
    color: "black",
    fontFamily: "Montserrat-Bold"

  }, listItemGreyText: {

    fontSize: 12,
    marginTop: 7,
    color: "#696969",
    fontFamily: "Montserrat-Regular"
  },
  bookViewStyle: {
    backgroundColor: "rgba(25, 89, 189, 1)",
    padding: 10,
    marginLeft: 10,
    marginRight: 10,
    marginTop: 10,
    justifyContent: "center",
    borderRadius: 8
  },
  selectedflighticon:
  {
    height: 30,
    width: 30,
    borderRadius: 15,
    backgroundColor: "white"
  },
  BookbtnStyle:
  {
    backgroundColor: "white",
    paddingLeft: 10,
    paddingRight: 10,
    borderRadius: 8,
    justifyContent: "center",
    alignItems: "center",
    height: 30
  },
  styleNavBar: {
    height: ((Platform.OS == 'ios') ? 44 : 52),
    // height: ((Platform.OS == 'ios')? getStatusBarHeight(true):0) + ((Platform.OS == 'ios')? 44:52), 
    width: screenWidth,
    backgroundColor: 'transparent'
  },
  styleNavBarSubView: {
    height: ((Platform.OS == 'ios') ? 44 : 52),
    width: "100%",
    // marginTop: ((Platform.OS == 'ios')? getStatusBarHeight(true):0),
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: 'center',
    // backgroundColor:'green'
  },
  styleLeftBtn: {
    alignItems: "center",
    justifyContent: "center",
    width: 30,
    height: 30,
    marginLeft: 10
  },
  styleNavTitle: {
    fontSize: 14,
    fontWeight: '500',
    color: "black",
    alignSelf: 'center'
    // marginLeft: -50,

  },
  styleNavBarRightView: {
    height: 35,
    flexDirection: "row",
    alignSelf: "flex-end",
    justifyContent: "flex-end"
  }
})