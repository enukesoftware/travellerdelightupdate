import React, { Component } from "react";
import { DeviceEventEmitter, Dimensions, Image, InteractionManager, Keyboard, SectionList, StyleSheet, Text, TextInput, TouchableOpacity, View } from "react-native";
import { SafeAreaView } from "react-navigation";
import { connect } from "react-redux";
import { getAirportListAction, getNationalityAction, searchCityAction } from '../../Redux/Actions';
import { colors } from "../../Utils/Colors";
import Images from "../../Utils/Images";
import StringConstants from '../../Utils/StringConstants';
import NavigationServices from './../../Utils/NavigationServices';
const screenWidth = Dimensions.get('window').width;

class SearchAirport extends Component {
  constructor(props) {
    super(props);
    this.state = {
      airportName: "",
      isSource: null,
      forFlightSearch: null,
      data: [],
      searchData: [],
      isSeacrhActive: false,
      loading: false,
      onCitySelect: null,
      onNationalitySelect: null,
      forNationalitySearch: false,
    };

    this._renderNavBar = this._renderNavBar.bind(this);
    this._renderListItem = this._renderListItem.bind(this);
    this.onChangeText = this.onChangeText.bind(this);
    this._renderListItemForCities = this._renderListItemForCities.bind(this);
    this._renderNationalityListItem = this._renderNationalityListItem.bind(this);
    this.updateAirportDataFromApi = this.updateAirportDataFromApi.bind(this);
    this.updateCityDataFromApi = this.updateCityDataFromApi.bind(this);
    this.updateNationalityDataFromApi = this.updateNationalityDataFromApi.bind(this);
  }


  //----------------------- Components Life Cycle methods ------------
  componentDidMount() {
    const { navigation } = this.props;
    const isSource = navigation.getParam('isSource', null);
    const forFlightSearch = navigation.getParam('forFlightSearch', null);
    const onCitySelect = navigation.getParam('onCitySelect', null)
    const forNationalitySearch = navigation.getParam('forNationalitySearch', false);
    const onNationalitySelect = navigation.getParam('onNationalitySelect', null)
    this.setState({
      isSource: isSource,
      forFlightSearch: forFlightSearch,
      onCitySelect: onCitySelect,
      onNationalitySelect: onNationalitySelect,
      forNationalitySearch: forNationalitySearch,
    })
    //console.log("propr", JSON.stringify(this.props))

    DeviceEventEmitter.addListener(StringConstants.SEARCH_AIRPORT_EVENT, this.updateAirportDataFromApi)
    DeviceEventEmitter.addListener(StringConstants.SEARCH_CITY_EVENT, this.updateCityDataFromApi)
    DeviceEventEmitter.addListener(StringConstants.NATIONALITY_DATA_EVENT, this.updateNationalityDataFromApi)

    InteractionManager.runAfterInteractions(() => {
      if (forFlightSearch) {
        if (this.props.airPortListData && this.props.airPortListData.length != 0) {
          let data = this.props.airPortListData;
          data = data.filter((item) => {
            if (item.top_city == 1) {
              return true;
            } else {
              return false;
            }
          })
          console.log("filter data of Airport ");
          console.log(data);

          this.setState({
            data: [{ key: "Popular Cities", data: data }]
          })
        } else {

          this.props.getAirportListAction()
        }
      } else if (forNationalitySearch) {
        if (this.props.nationalityListData && this.props.nationalityListData.length != 0) {
          let data = this.props.nationalityListData;
          console.log("filter data of Nationality ");
          console.log(data);

          this.setState({
            data: [{ key: "Popular Cities", data: data }]
          })
        } else {

          this.props.getNationalityAction()
        }
      } else {
        this.setState({
          data: [{ key: "Popular Cities", data: [] }]
        });
      }
    })
  }

  componentWillUnmount() {
    DeviceEventEmitter.removeListener(StringConstants.SEARCH_AIRPORT_EVENT, this.updateAirportDataFromApi)
    DeviceEventEmitter.removeListener(StringConstants.SEARCH_CITY_EVENT, this.updateCityDataFromApi)
    DeviceEventEmitter.removeListener(StringConstants.NATIONALITY_DATA_EVENT, this.updateNationalityDataFromApi)
  }

  updateAirportDataFromApi(data) {
    if (!data && data.length < 1) return
    data = data.filter((item) => {
      if (item.top_city == 1) {
        return true;
      } else {
        return false;
      }
    })

    console.log("filter data of Airport");
    console.log(data);
    this.setState({
      data: [{ key: "Popular Cities", data: data }]
    })
  }

  updateCityDataFromApi(data) {
    if (!Array.isArray(data)) {
      data = []
    }
    console.log("CITY_DATA:" + JSON.stringify(data))
    this.setState({
      data: [{ Key: "Popular Cities", data: data }]
    })
  }

  updateNationalityDataFromApi(data) {
    this.setState({
      data: [{ Key: "Popular Cities", data: data }]
    })
  }

  //-------------- API Calling Methods ---------------

  getSearchData(searchStr) {
    // APIManager.getCities(searchStr, (success, response) => {
    //   if (success) {
    //     this.setState({
    //       data: [{ key: "Popular Cities", data: response }],
    //       loading: !this.state.loading
    //     });
    //   } else {
    //     alert(response);
    //   }
    // });

    this.props.searchCityAction(searchStr)

    // getCitiesApi(searchStr).then(res => {
    //   if (res.data) {
    //     this.setState({
    //       data: [{ Key: "Popular Cities", data: res.data }]
    //     })
    //   }
    // }).catch(err => { alert(err) })




  }

  onChangeText(name) {
    if (this.state.forFlightSearch) {
      console.log("FLIGHT")
      // let data = this.state.data[0].data;
      let data = this.props.airPortListData;
      if (!data && data.length < 1) return
      let filterData = data.filter(dict => {
        let airName = String(dict.airport_name);
        let cityName = String(dict.city_name);
        if (airName.toLowerCase().startsWith(name) || cityName.startsWith(name)
          || dict.airport_code.toLowerCase().startsWith(name.toLowerCase())) {
          return true;
        } else {
          return false;
        }
      });
      // console.log(JSON.stringify(filterData.length))
      let dict = { key: "", data: filterData };
      let arr = [dict];
      this.setState({
        searchData: arr,
        isSeacrhActive: name.trim().length == 0 ? false : true,
        airportName: name
      });
    } else if (this.state.forNationalitySearch) {

      let data = this.props.nationalityListData;

      if (!data && data.length < 1) return
      let filterData = data.filter(dict => {
        let nationalityName = String(dict.name);
        let nationalityCode = String(dict.code);
        if (nationalityName.toLowerCase().includes(name.toLowerCase()) || nationalityCode.toLowerCase().includes(name.toLowerCase())) {
          return true;
        } else {
          return false;
        }
      });


      // console.log(JSON.stringify(filterData.length))
      let dict = { key: "", data: filterData };
      let arr = [dict];
      this.setState({
        searchData: arr,
        isSeacrhActive: name.trim().length == 0 ? false : true,
        airportName: name
      });
    } else {
      this.setState({
        airportName: name
      })
      if (name.trim().length == 0) {
        this.setState({
          data: [{ key: "Popular Cities", data: [] }],
          loading: !this.state.loading
        })
      }
      else {
        this.getSearchData(name);
      }

    }
  }

  _renderNavBar() {
    return (
      <View style={styles.header}>
        <TouchableOpacity style={styles.headerBtn} onPress={() => {
          NavigationServices.goBack();
        }}>
          <Image source={Images.ic_image_close} style={{ width: 20, height: 20, resizeMode: "contain" }}></Image>
        </TouchableOpacity>
        <TextInput style={{
          width: screenWidth - 110,
          height: 50
        }}
          placeholder={this.state.forNationalitySearch ? "Enter Nationality" : this.state.forFlightSearch ? "Enter Airport Name" : "Enter City Name"}
          placeholderTextColor={colors.colorBlack}
          value={this.state.airportName}
          onChangeText={name => this.onChangeText(name)}
        ></TextInput>
        <TouchableOpacity style={styles.headerBtn}>
          <Text style={{
            fontSize: 14,
            fontFamily: Fonts.medium,
            color: colors.colorBlue
          }} onPress={() => this.setState({ airportName: "" })}>Clear</Text>
        </TouchableOpacity>
      </View>
    );
  }

  _renderListItemForCities({ item, index, section }) {
    return (
      <View>
        <TouchableOpacity style={[styles.item, { justifyContent: 'center' }]}
          onPress={() => {
            Keyboard.dismiss()
            // DeviceEventEmitter.emit('citySelected',item)
            this.state.onCitySelect(item)
            NavigationServices.goBack();
          }}>
          <View style={{
            flexDirection: "row",
            justifyContent: "space-between",
          }}>
            <Text style={styles.name}>{item.name}</Text>
            <Text style={styles.code}>{item.country}</Text>
          </View>
        </TouchableOpacity>
        <View style={styles.border}></View>
      </View>

    );

  }

  _renderListItem({ item, index, section }) {
    if (this.state.isSource === null || this.state.forFlightSearch === null) return;
    return (
      <View>
        <TouchableOpacity style={styles.item} onPress={() => {
          let dict = item;
          dict["isSource"] = this.state.isSource;
          DeviceEventEmitter.emit(StringConstants.AIRPORT_SELECT, dict);
          NavigationServices.goBack();
        }}>
          <View style={{
            flexDirection: "row",
            justifyContent: "space-between",
          }}>
            <Text style={styles.name}>{item.city_name}{/* , {item.country_name} */}</Text>
            <Text style={styles.code}>{item.airport_code}</Text>
          </View>
          <Text style={styles.fullName}>{item.airport_name}</Text>
        </TouchableOpacity>
        <View style={styles.border}></View>
      </View>
    );
  }

  _renderNationalityListItem({ item, index, section }) {
    return (
      <View>
        <TouchableOpacity style={[styles.item, { justifyContent: 'center' }]}
          onPress={() => {
            Keyboard.dismiss()
            this.state.onNationalitySelect(item)
            NavigationServices.goBack();
          }}>
          <View style={{
            flexDirection: "row",
            justifyContent: "space-between",
          }}>
            <Text style={styles.name}>{item.name}</Text>
            <Text style={styles.code}>{item.code}</Text>
          </View>
        </TouchableOpacity>
        <View style={styles.border}></View>
      </View>
    );
  }

  _renderSectionHeader({ section }) {
    return (
      <View style={{ height: 20, backgroundColor: "rgba(244,248,254,1)" }}>
        <Text style={{ fontSize: 16, color: "black" }}>{section.key}</Text>
      </View>
    );
  }

  render() {
    return (
      <View style={{
        flex: 1,
      }}>
        <SafeAreaView backgroundColor={colors.colorBlue} />
        {this._renderNavBar()}
        <View style={styles.container}>
          {
            <SectionList
              sections={
                this.state.isSeacrhActive
                  ? this.state.searchData
                  : this.state.data
              }
              renderItem={
                this.state.forNationalitySearch ? this._renderNationalityListItem
                  : this.state.forFlightSearch ? this._renderListItem
                    : this._renderListItemForCities
              }
              extraData={this.state.loading}
              // renderSectionHeader={this._renderSectionHeader}
              keyExtractor={(item, index) => index.toString()}
            />}
        </View>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    airPortListData: state.airportListDataReducer,
    nationalityListData: state.nationalityDataReducer,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getAirportListAction: () => dispatch(getAirportListAction()),
    searchCityAction: (data) => dispatch(searchCityAction(data)),
    getNationalityAction: () => dispatch(getNationalityAction())
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SearchAirport)

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.colorWhite
  },
  styleListAirportCodeSuperView: {
    height: "100%",
    width: "20%",
    justifyContent: "center",
    marginLeft: 10
  },
  styleAirportCodeView: {
    borderColor: "gray",
    borderWidth: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "white",
    width: "70%"
  },
  border: {
    backgroundColor: "lightgrey",
    height: 1,
    marginLeft: 15,
    marginRight: 15
  },
  fullName: {
    color: colors.colorBlack,
    fontFamily: Fonts.regular,
    fontSize: 12,
    marginTop: 5
  },
  code: {
    color: colors.colorBlack,
    fontFamily: Fonts.regular,
    fontSize: 14
  },
  name: {
    color: colors.colorBlack,
    fontFamily: Fonts.semiBold,
    fontSize: 14
  },
  item: {
    height: 60,
    width: screenWidth,
    paddingLeft: 15,
    paddingRight: 15,
    padding: 10
  },
  header: {
    height: 50,
    width: "100%",
    backgroundColor: "rgb(246,245,246)",
    flexDirection: "row",
    alignItems: "center"
  },
  mainContainer: {
    marginTop: 50,
    flex: 1,
    backgroundColor: colors.colorWhite
  },
  headerBtn: {
    width: 50,
    height: "100%",
    alignItems: "center",
    justifyContent: "center",
    // backgroundColor: "red"
  }
})
