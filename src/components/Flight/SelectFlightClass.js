import React, { Component } from "react";
import { DeviceEventEmitter, FlatList, Modal, StyleSheet, Text, TouchableHighlight, View } from "react-native";
import { colors } from "../../Utils/Colors";
import { commonstyle } from "../../Utils/Commonstyle";
import Constants from "../../Utils/Constants";
import Fonts from "../../Utils/Fonts";


export default class SelectFlightClass extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      selectedClassKey: this.props.selectedClassKey,
      selectedIndex: 0
    };

    this._renderListItem = this._renderListItem.bind(this)
  }

  _renderListItem({ item, index }) {
    return (
      <TouchableHighlight
        activeOpacity={0.9}
        underlayColor='transparent'
        onPress={() => {
          this.setState({
            selectedIndex: index,
            selectedClassKey: item.key
          })
        }}
      >
        <View style={styles.styleListItemView}>
          <View
            style={
              (this.state.selectedIndex) == index
                ? styles.styleListSelectedRadioBtn
                : styles.styleListRadioUnSelect
            }
          />
          <Text style={[commonstyle.blackText, { marginLeft: 20, fontSize: 15 }]}>
            {item.value}
          </Text>
        </View>
      </TouchableHighlight>
    );
  }

  render() {
    return (
      // <View>
      <Modal
        animationType="slide"
        transparent={true}
        visible={this.props.modalVisible}
        onRequestClose={() => {
          // Alert.alert('Modal has been closed.');
        }}
      >
        <View style={styles.container}>
          <View style={styles.stylePopUpView}>
            <View style={styles.styleTitleView}>
              <Text
                style={[commonstyle.blackBold, { fontSize: 18, }]}
              >Class
                </Text>
            </View>

            <FlatList
              style={{ marginLeft: 15, marginRight: 15 }}
              data={Constants.CabinClasses}
              extraData={this.state}
              renderItem={this._renderListItem}
              keyExtractor={(item, index) => String(index)}
            />

            <View style={styles.styleBottomBtnsView}>

              <TouchableHighlight
                style={styles.styleCancelOrDoneBtnView}
                activeOpacity={0.9}
                underlayColor="transparent"
                onPress={() => {
                  DeviceEventEmitter.emit("TravellerClass", null);
                  this.props.closeModal();
                }}
              >
                <Text style={[commonstyle.blackSemiBold, { fontSize: 14 }]}>
                  Cancel
                  </Text>
              </TouchableHighlight>

              <TouchableHighlight
                style={styles.styleCancelOrDoneBtnView}
                underlayColor="transparent"
                activeOpacity={0.9}
                onPress={() => {
                  //alert("SelectedIndex: " + this.state.selectedIndex)

                  this.props.closeModal();
                  DeviceEventEmitter.emit('TravellerClass', Constants.CabinClasses[this.state.selectedIndex]);
                }}
              >
                <Text style={{ color: colors.colorBlue, fontSize: 14, fontFamily: Fonts.semiBold }}>Done</Text>
              </TouchableHighlight>

            </View>
          </View>
        </View>
      </Modal>
      // </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "rgba(0,0,0,0.5)",
    height: "100%",
    justifyContent: "center",
    alignItems: "center"
  },
  stylePopUpView: {
    width: "80%",
    backgroundColor: colors.colorWhite,
    borderRadius: 15
  },
  styleTitleView: {
    margin: 15,
    height: 30,
    justifyContent: "center"
  },
  styleBottomBtnsView: {
    flexDirection: "row",
    justifyContent: "flex-end",
    margin: 15,
    height: 35,
    paddingRight: 10
  },
  styleCancelOrDoneBtnView: {
    height: "100%",
    width: 60,
    justifyContent: "center",
    alignItems: "center"
  },
  styleListItemView: {
    width: "100%",
    height: 50,
    flexDirection: "row",
    alignItems: "center"
  },
  styleListSelectedRadioBtn: {
    height: 20,
    width: 20,
    borderRadius: 10,
    backgroundColor: colors.colorBlue
  },
  styleListRadioUnSelect: {
    height: 20,
    width: 20,
    borderRadius: 10,
    borderWidth: 3,
    borderColor: colors.colorBlue
  }
});
