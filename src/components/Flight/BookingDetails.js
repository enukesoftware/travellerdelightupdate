import moment from 'moment'
import React, { Component } from 'react'
import { DeviceEventEmitter, Dimensions, Image, SafeAreaView, ScrollView, StyleSheet, Text, View,TouchableOpacity } from 'react-native'
import CardView from 'react-native-cardview'
import Dash from "react-native-dash"
import { connect } from "react-redux"
import { getFlightBookingDetailsAction } from '../../Redux/Actions'
import { colors } from '../../Utils/Colors'
import Fonts from '../../Utils/Fonts'
import Images from '../../Utils/Images'
import StringConstants from '../../Utils/StringConstants'
import { convertMinsIntoHandM, MinutesToHours } from '../../Utils/Utility'
import CommonHeader from '../custom/CommonHeader'
import { getCabinClass } from "../../Utils/Utility";
import NavigationServices from './../../Utils/NavigationServices';



const screenWidth = Dimensions.get('window').width
const screenHeight = Dimensions.get('window').height

const gender = {
  "male": "Male",
  "female": "Female"
}

class BookingDetails extends Component {
  constructor(props) {
    super(props)
    this.state = {
      bookingDataItem: null,
      outBoundFlights: null,
      inBoundFlights: null,
      children: null,
      adults: null,
      bookingId: '',
      email: '',
      phone: '',
      PNR: '',
      price: null,
      id:null

    }
    this.updateFlightDetails = this.updateFlightDetails.bind(this)
  }

  componentDidMount() {
    DeviceEventEmitter.addListener(StringConstants.FLIGHT_BOOKING_DETAILS_EVENT, this.updateFlightDetails)
    const id = this.props.navigation.getParam('id', null);
    this.setState({   id:id  })
    this.props.getFlightBookingDetailsAction(id)

  }

  componentWillUnmount() {
    DeviceEventEmitter.removeListener(StringConstants.FLIGHT_BOOKING_DETAILS_EVENT, this.updateFlightDetails)

  }

  updateFlightDetails(data) {
    // console.log("FLIGHT_DETAIL_RESP",JSON.stringify(data))
    console.log("FLIGHT_DETAIL_RESP_JSON", JSON.stringify(JSON.parse(data.res_json).Response))
    const resp_json = JSON.parse(data.res_json).Response
    const PNR = resp_json.PNR
    const bookingId = resp_json.BookingId
    const email = data.email
    const phone = data.phone
    const price = data.price

    let inBoundFlights = []
    const outBoundFlights = resp_json.FlightItinerary.Segments.filter((item) => {
      if (item.TripIndicator == 1) return true
      if (item.TripIndicator == 2) {
        inBoundFlights.push(item)
        return false
      }
    })
    let children = []
    let infants = []
    let adults = resp_json.FlightItinerary.Passenger.filter((item) => {
      if (item.PaxType == 1) return true
      if (item.PaxType == 2) {
        children.push(item)
        return false;
      }
      if (item.PaxType == 3) {
        infants.push(item)
        return false;
      }
      return false;
    })
    console.log("INBOUND FLIGHTS", JSON.stringify(inBoundFlights))
    console.log("OUTBOUND FLIGHTS", JSON.stringify(outBoundFlights))
    console.log("CHILDREN", JSON.stringify(children))
    console.log("ADULTS", JSON.stringify(adults))
    console.log("INFANTS", JSON.stringify(infants))

    this.setState({
      inBoundFlights: inBoundFlights.length > 0 ? inBoundFlights : null,
      outBoundFlights: outBoundFlights,
      adults: adults.length > 0 ? adults : null,
      children: children.length > 0 ? children : null,
      infants: infants.length > 0 ? infants : null,
      email: email,
      phone: phone,
      PNR: PNR,
      bookingId: bookingId,
      price: price
    })
  }



  renderAirlineData(item) {
    return (
      <CardView style={{
        //overflow: 'hidden',
        marginHorizontal: 5,
        marginBottom: 20,
        backgroundColor: colors.colorWhite,
      }}
        cardElevation={2}
        cardMaxElevation={3}
        cornerRadius={10}>
        <View style={{ width: '100%', overflow: 'hidden', borderRadius: 10, }}>
          <View style={{ width: '100%', backgroundColor: '#e6e6e6', height: 45, flexDirection: 'row', padding: 10 }}>
            <View style={{ width: '50%', alignItems: 'center', flexDirection: 'row', height: '100%' }}>
              <View style={{ width: 25, height: 25, borderRadius: 15, backgroundColor: colors.colorBlue, alignItems: 'center', justifyContent: 'center' }}>
                <Image source={Images.ic_image_planeblue} style={{ width: 15, height: 15, tintColor: colors.colorWhite, resizeMode: 'center' }} />
              </View>
              <Text style={{ fontFamily: Fonts.medium, fontSize: 13, color: colors.colorBlack, paddingLeft: 7 }}>
                {"PNR : "}{this.state.bookingDataItem.pnr}
              </Text>

            </View>
            <View style={{ width: '50%' }}></View>
          </View>
          <View style={{ width: '100%', paddingVertical: 15, paddingHorizontal: 10 }}>
            <View style={{ width: '100%', flexDirection: 'row' }}>
              <View style={{ width: '28%' }}>
                <Text style={{ fontFamily: Fonts.medium, fontSize: 13, color: colors.colorBlack, padding: 1 }}>
                  {moment(item.depart_at).format("HH:mm")}
                </Text>
                <Text style={{ fontFamily: Fonts.medium, fontSize: 10, color: colors.colorBlue, padding: 1 }}>
                  {item.origin}
                </Text>
                <Text style={{ fontFamily: Fonts.regular, fontSize: 10, color: colors.colorBlack21, padding: 1 }}>
                  {moment(item.depart_at).format("DD MMM, YYYY")}
                </Text>
              </View>
              <View style={{ width: '44%', alignItems: 'center', justifyContent: 'center', }}>
                <View style={{ width: '100%', alignItems: 'center', justifyContent: 'center', }}>
                  <View style={{ width: 7, height: 7, borderRadius: 5, backgroundColor: colors.colorBlue, position: 'absolute', left: 0 }} />
                  <View style={{ width: 7, height: 7, borderRadius: 5, backgroundColor: colors.colorBlue, position: 'absolute', right: 0 }} />
                  <Dash dashColor={colors.colorBlue} dashLength={1} dashGap={2} dashThickness={0.8} style={{ position: 'absolute', left: 0, width: '100%', }} />
                  <Image source={Images.ic_image_planeblue} style={{ width: 15, height: 15, resizeMode: 'center' }} />
                </View>
                <Text style={{ fontFamily: Fonts.medium, fontSize: 10, color: colors.colorBlack21, padding: 2 }}>
                  {MinutesToHours(item.travel_time)}
                </Text>
              </View>

              <View style={{ width: '28%', alignItems: 'flex-end' }}>
                <Text style={{ fontFamily: Fonts.medium, fontSize: 13, color: colors.colorBlack, padding: 1 }}>
                  {moment(item.arrive_at).format("HH:mm")}
                </Text>
                <Text style={{ fontFamily: Fonts.medium, fontSize: 10, color: colors.colorBlue, padding: 1 }}>
                  {item.destination}
                </Text>
                <Text style={{ fontFamily: Fonts.regular, fontSize: 10, color: colors.colorBlack21, padding: 1 }}>
                  {moment(item.arrive_at).format("DD MMM, YYYY")}
                </Text>
              </View>

            </View>

          </View>

        </View></CardView>
    )
  }

  renderPassengerDetail() {
    const { adults, children, infants } = this.state
    if (adults || children || infants)
      return (
        <View style={{ width: '100%', overflow: 'hidden', flexDirection: 'row', padding: 15, }}>
          <View style={{ width: '30%', alignItems: 'flex-start', justifyContent: 'flex-start' }} >
            <Text style={{ fontFamily: Fonts.medium, fontSize: 13, lineHeight: 20, color: colors.colorBlack }}>Travellers</Text>
          </View>
          <View style={{ width: '70%', }}>
            {adults && adults.length > 0 && <>
              <Text style={{ fontFamily: Fonts.medium, fontSize: 13, color: colors.colorBlack21, lineHeight: 20 }}>Adults ({adults.length})</Text>
              {adults.map((item, index) => {
                return (
                  <View style={{ flexDirection: 'row', width: '100%', marginTop: 5, alignItems: 'center' }}>
                    <Image style={{ height: 10, width: 10, resizeMode: 'contain' }} source={Images.ic_user_black} />
                    <Text style={{ fontFamily: Fonts.medium, paddingLeft: 5, fontSize: 12, color: colors.colorBlack62, lineHeight: 18 }}>{item.Title + " " + item.FirstName + " " + item.LastName}</Text>
                  </View>
                )
              })}</>}
            {children && children.length > 0 && <>
              <Text style={{ fontFamily: Fonts.medium, fontSize: 13, color: colors.colorBlack21, lineHeight: 20, marginTop: 10 }}>Children ({children.length})</Text>
              {children.map((item, index) => {
                return (
                  <View style={{ flexDirection: 'row', width: '100%', marginTop: 5, alignItems: 'center' }}>
                    <Image style={{ height: 10, width: 10, resizeMode: 'contain' }} source={Images.ic_user_black} />
                    <Text style={{ fontFamily: Fonts.medium, paddingLeft: 5, fontSize: 12, color: colors.colorBlack62, lineHeight: 18 }}>{item.Title + " " + item.FirstName + " " + item.LastName}</Text>
                  </View>
                )
              })}</>}

            {infants && infants.length > 0 && <>
              <Text style={{ fontFamily: Fonts.medium, fontSize: 13, color: colors.colorBlack21, lineHeight: 20, marginTop: 10 }}>Infants ({infants.length})</Text>
              {infants.map((item, index) => {
                return (
                  <View style={{ flexDirection: 'row', width: '100%', marginTop: 5, alignItems: 'center' }}>
                    <Image style={{ height: 10, width: 10, resizeMode: 'contain' }} source={Images.ic_user_black} />
                    <Text style={{ fontFamily: Fonts.medium, paddingLeft: 5, fontSize: 12, color: colors.colorBlack62, lineHeight: 18 }}>{item.Title + " " + item.FirstName + " " + item.LastName}</Text>
                  </View>
                )
              })}</>}
          </View>
        </View>
      )
  }

  _renderEmailDetails() {
    return (
      <View style={{ width: '100%', overflow: 'hidden', flexDirection: 'row', padding: 15, }}>
        <View style={{ width: '30%', alignItems: 'flex-start', justifyContent: 'flex-start' }} >
          <Text style={{ fontFamily: Fonts.medium, fontSize: 13, lineHeight: 20, color: colors.colorBlack }}>Email</Text>
        </View>
        <View style={{ width: '70%' }}>
          <Text style={{ fontFamily: Fonts.medium, fontSize: 13, color: colors.colorBlack21, lineHeight: 20 }}>{this.state.email}</Text>
        </View>
      </View>
    )
  }

  _renderPhoneDetails() {
    return (
      <View style={{ width: '100%', overflow: 'hidden', flexDirection: 'row', padding: 15, }}>
        <View style={{ width: '30%', alignItems: 'flex-start', justifyContent: 'flex-start' }} >
          <Text style={{ fontFamily: Fonts.medium, fontSize: 13, lineHeight: 20, color: colors.colorBlack }}>Phone</Text>
        </View>
        <View style={{ width: '70%' }}>
          <Text style={{ fontFamily: Fonts.medium, fontSize: 13, color: colors.colorBlack21, lineHeight: 20 }}>{this.state.phone}</Text>
        </View>
      </View>
    )
  }
  _renderDownloadTicket() {
    return (
      <View style={{ width: '100%', overflow: 'hidden', flexDirection: 'row', padding: 15, }}>
        <View style={{ width: '30%', alignItems: 'flex-start', justifyContent: 'flex-start' }} >
          <Text style={{ fontFamily: Fonts.medium, fontSize: 13, lineHeight: 20, color: colors.colorBlack }}>Ticket</Text>
        </View>
        <View style={{ width: '70%' }}>
        <TouchableOpacity
        onPress={()=>{
                  NavigationServices.navigate("ViewTicket",{bookingId:this.state.id})
                }} >
          <Text style={{ fontFamily: Fonts.medium, fontSize: 13, color: colors.colorBlack21, lineHeight: 20 }}>Click here to view ticket</Text>
        </TouchableOpacity>
        </View>
      </View>
    )
  }
  _renderPNR() {
    return (
      <View style={{ width: '100%', overflow: 'hidden', flexDirection: 'row', padding: 15, }}>
        <View style={{ width: '30%', alignItems: 'flex-start', justifyContent: 'flex-start' }} >
          <Text style={{ fontFamily: Fonts.medium, fontSize: 13, lineHeight: 20, color: colors.colorBlack }}>PNR</Text>
        </View>
        <View style={{ width: '70%' }}>
          <Text style={{ fontFamily: Fonts.medium, fontSize: 13, color: colors.colorBlack21, lineHeight: 20 }}>{this.state.PNR}</Text>
        </View>
      </View>
    )
  }

  _renderBookingId() {
    return (
      <View style={{ width: '100%', overflow: 'hidden', flexDirection: 'row', padding: 15, }}>
        <View style={{ width: '30%', alignItems: 'flex-start', justifyContent: 'flex-start' }} >
          <Text style={{ fontFamily: Fonts.medium, fontSize: 13, lineHeight: 20, color: colors.colorBlack }}>Booking ID</Text>
        </View>
        <View style={{ width: '70%' }}>
          <Text style={{ fontFamily: Fonts.medium, fontSize: 13, color: colors.colorBlack21, lineHeight: 20 }}>{this.state.bookingId}</Text>
        </View>
      </View>
    )
  }


  renderTotalPrice() {
    return (
      <CardView style={{
        //overflow: 'hidden',    
        marginHorizontal: 5,
        marginBottom: 20,
        backgroundColor: colors.colorWhite,
      }}
        cardElevation={2}
        cardMaxElevation={3}
        cornerRadius={10}>
        <View style={{ width: '100%', overflow: 'hidden', borderRadius: 10, }}>
          <View style={{ width: '100%', height: 45, flexDirection: 'row', padding: 10, }}>
            <View style={{ width: '50%', alignItems: 'center', flexDirection: 'row', height: '100%' }}>
              <View style={{ width: 25, height: 25, borderRadius: 15, backgroundColor: colors.colorBlue, alignItems: 'center', justifyContent: 'center' }}>
                <Image source={Images.img_primary_contact} style={{ width: 25, height: 25, resizeMode: 'cover' }} />
              </View>
              <Text style={{ fontFamily: Fonts.medium, fontSize: 13, color: colors.colorBlack, paddingLeft: 7 }}>
                Total Paid
              </Text>

            </View>
            <View style={{ width: '50%', alignItems: 'flex-end', justifyContent: 'center' }}>

              <Text style={{ fontFamily: Fonts.bold, fontSize: 15, color: colors.colorBlack, }}>
                {this.state.bookingDataItem.price.currency ? (
                  this.state.bookingDataItem.price.currency == "INR" ? "₹ " : this.state.bookingDataItem.price.currency
                ) : ""
                }{this.state.bookingDataItem.price.grant_total}
              </Text>

            </View>
          </View>
        </View>
      </CardView>)
  }

  renderFlights(flights) {
    if (flights)
      return (<CardView style={{
        backgroundColor: colors.colorWhite,
        marginVertical: 10,
        marginHorizontal: 1
      }}
        cardElevation={2}
        cardMaxElevation={3}
        cornerRadius={10}>
        <View style={{ borderTopLeftRadius: 10, borderTopRightRadius: 10, overflow: "hidden" }}>
          {
            flights.map((item, index) => (
              this.renderFlightItem(item, index, flights)
            ))
          }
        </View>
      </CardView>)
  }

  renderFlightItem(item, index, flights) {
    const { Airline } = item
    let airlineName = Airline.AirlineName + " | " + Airline.AirlineCode + "-" + Airline.FlightNumber;
    let fareClass = Airline.FareClass
    // let cabinClass = getCabinClass(item.cabinClass, "key", "value")
    let operatingCarrier = Airline.OperatingCarrier
    if (operatingCarrier == "" || operatingCarrier == null ||
      operatingCarrier == Airline.AirlineCode) {
      operatingCarrier = null
    }
    //operatingCarrier = "SG"
    let destTerminal = item.Destination.Airport.Terminal;
    let originTerminal = item.Origin.Airport.Terminal;
    const data = flights
    const length = data.length

    return (
      <View style={{ paddingHorizontal: 0 }} >
        <View style={{ marginBottom: 10 }} >
          <View style={{ width: '100%', height: 45, flexDirection: 'row', alignItems: 'center', paddingHorizontal: 10, paddingVertical: 5 }}>

            <Text style={[styles.price, { fontFamily: Fonts.semiBold, fontSize: 13, paddingLeft: 10 }]}>{airlineName}</Text>
              {operatingCarrier && <Text style={{ color: colors.colorBlack, fontSize: 11, fontFamily: Fonts.medium }}>{" (Operated by: " + operatingCarrier + ")"}</Text>}
              {item.CabinClass ? <Text style={{ color: colors.colorBlack, fontSize: 11, fontFamily: Fonts.medium,textAlign:'right',flex:1 }}>{getCabinClass(item.CabinClass,"key","shortValue")}</Text>:null}
            
          </View>
          <Dash style={{
            height: 2,
          }} dashColor={colors.lightgrey} dashLength={2}
          />
          <View style={{ width: "100%", padding: 12, paddingBottom: 0, flexDirection: "row", }}>
            <View style={{ width: "35%", }}>
              <Text style={styles.estimate}>{moment(item.Origin.DepTime).format("HH:mm")}</Text>
              <Text style={[styles.stnName, { color: colors.colorBlue }]}>{item.Origin.Airport.CityName}</Text>
            </View>
            <View style={{ width: "30%", height: '100%', flexDirection: "row", justifyContent: "space-between", alignItems: "center", }}>
              <View style={{ width: "43%", alignItems: "center" }}>
                <View style={styles.dotView}>
                  <View style={{ backgroundColor: colors.colorBlue, borderRadius: 3, height: 6, width: 6 }} />
                  <Dash style={{ width: "75%", height: 1, }} dashColor={colors.lightgrey} dashLength={5} />
                </View>
              </View>
              <View style={{ width: "14%", height: 20, }}>
                <Image source={Images.ic_image_planeblue} style={{ width: "100%", height: "100%", resizeMode: "contain" }}></Image>
              </View>
              <View style={{ width: "43%", alignItems: "center" }}>
                <View style={styles.dotView}>
                  <Dash style={{ width: "75%", height: 1, }} dashColor={colors.lightgrey} dashLength={5} />
                  <View style={{ backgroundColor: colors.colorBlue, borderRadius: 3, height: 6, width: 6 }} />
                </View>
              </View>
            </View>
            <View style={{ width: "35%", alignItems: "flex-end", }}>
              <Text style={styles.estimate}>{moment(item.Destination.ArrTime).format("HH:mm")}</Text>
              <Text style={[styles.stnName, { color: colors.colorBlue }]}>{item.Destination.Airport.CityName}</Text>
            </View>
          </View>
          <View style={{
            width: "100%", padding: 12,
            flexDirection: "row", paddingTop: 0,
            justifyContent: "space-between", paddingBottom: 0
          }}>
            <View style={{ width: "35%" }}>
              <Text style={[styles.stnName, { fontFamily: Fonts.medium }]}>{moment(item.Origin.DepTime).format("DD MMM, YYYY")}</Text>
            </View>
            <View style={{ width: "30%", alignItems: "center", }}>
              <Text style={[styles.stnName, { fontFamily: Fonts.medium, marginTop: -12 }]}>{convertMinsIntoHandM(item.Duration)}</Text>
            </View>
            <View style={{ width: "35%", alignItems: "flex-end" }}>
              <Text style={[styles.stnName, { fontFamily: Fonts.medium, }]}>{moment(item.Destination.ArrTime).format("DD MMM, YYYY")}</Text>
            </View>
          </View>
          <View style={{
            width: "100%", paddingHorizontal: 12,
            flexDirection: "row", paddingTop: 0,
            justifyContent: "space-between",
          }}>
            <View style={{ width: "50%" }}>
              <Text style={[styles.stnName, { textAlign: 'left' }]}>{item.Origin.Airport.AirportName}</Text>
              {originTerminal !== "" && <Text style={[styles.stnName, { textAlign: 'left' }]}>
                Terminal - {originTerminal}
              </Text>}
            </View>
            <View style={{ width: "50%", alignItems: "flex-end" }}>
              <Text style={[styles.stnName, { textAlign: 'right' }]}>{item.Destination.Airport.AirportName}</Text>
              {destTerminal !== "" && <Text style={[styles.stnName, { textAlign: 'right' }]}>
                Terminal - {destTerminal}
              </Text>}
            </View>
          </View>
        </View>

        {length != index + 1 &&
          <View style={{ backgroundColor: '#f5f5f5', paddingVertical: 10, alignItems: 'center' }}>
            <Text style={{ fontFamily: Fonts.medium }}>
              {convertMinsIntoHandM((moment.duration((moment(flights[index + 1].Origin.DepTime)).diff(moment(item.Destination.ArrTime)))).asMinutes())} Layover
        </Text>
          </View>}



        {/* <View style={{ flexDirection: 'row', alignItems: 'flex-start', width: '100%', paddingVertical: 5, paddingHorizontal: 15 }}>
          <View style={{ flexDirection: 'row', alignItems: 'flex-start', width: '60%', paddingVertical: 10 }}>
            <Text style={{ paddingLeft: 5, fontFamily: Fonts.medium, fontSize: 11.5, color: colors.colorBlack21 }} >Cabin Class - </Text>
            <Text style={{ paddingLeft: 5, fontFamily: Fonts.medium, fontSize: 11, color: colors.colorBlack62, flex: 1 }} >{cabinClass || "Economy"}</Text>
          </View>
          <View style={{ flexDirection: 'row', alignItems: 'flex-start', width: '40%', paddingVertical: 10 }}>
            <Text style={{ paddingLeft: 5, fontFamily: Fonts.medium, fontSize: 11.5, color: colors.colorBlack21 }} >Fare Class - </Text>
            <Text style={{ paddingLeft: 5, fontFamily: Fonts.medium, fontSize: 11, color: colors.colorBlack62 }} >{fareClass || "A"}</Text>
          </View>
        </View>

        {this.renderCabinDetails(item, index, section)}
        {this.renderCancelPolicy(item, index, sectionIndex)} */}

      </View>

    )
  }

  renderFareDetails() {
    const { price } = this.state

    if (price) return (
      <CardView
        style={{
          backgroundColor: colors.colorWhite,
          marginVertical: 10,
          marginHorizontal: 1
        }}
        cardElevation={2}
        cardMaxElevation={3}
        cornerRadius={8}>
        <View style={{ borderRadius: 8, overflow: 'hidden' }}>
          <View style={{ padding: 10, paddingHorizontal: 15, alignItems: 'center', flexDirection: 'row' }}>
            <Text style={{ fontFamily: Fonts.medium, fontSize: 16, color: colors.colorBlack21 }}>Fare Details</Text>
          </View>

          <View style={{ height: 1, backgroundColor: colors.lightgrey, width: '100%' }} />
          <View style={{ paddingVertical: 10, }}>
            <View style={styles.fullView}>
              <View style={{ justifyContent: 'center', alignItems: 'flex-start', width: '60%' }}>
                <Text style={{ fontFamily: Fonts.medium, color: colors.colorBlack21, fontSize: 15, paddingHorizontal: 2 }}>Base Fare</Text>
              </View>
              <View style={{ justifyContent: 'center', alignItems: 'flex-end', width: '40%' }}>
                <Text style={{ fontFamily: Fonts.medium, color: colors.colorBlack21, fontSize: 15, paddingHorizontal: 2 }}>{price.currency + " " + price.price}</Text>
              </View>
            </View>
          </View>

          <View style={{ height: 1, backgroundColor: colors.lightgrey, width: '100%' }} />
          <View style={{ paddingVertical: 10, }}>
            <View style={styles.fullView}>
              <View style={{ justifyContent: 'center', alignItems: 'flex-start', width: '60%' }}>
                <Text style={{ fontFamily: Fonts.medium, color: colors.colorBlack21, fontSize: 15, paddingHorizontal: 2 }}>Tax</Text>
              </View>
              <View style={{ justifyContent: 'center', alignItems: 'flex-end', width: '40%' }}>
                <Text style={{ fontFamily: Fonts.medium, color: colors.colorBlack21, fontSize: 15, paddingHorizontal: 2 }}>{price.currency + " " + price.tax}</Text>
              </View>
            </View>
          </View>

          <View style={{ height: 1, backgroundColor: colors.lightgrey, width: '100%' }} />
          <View style={{ paddingVertical: 10, }}>
            <View style={styles.fullView}>
              <View style={{ justifyContent: 'center', alignItems: 'flex-start', width: '60%' }}>
                <Text style={{ fontFamily: Fonts.medium, color: colors.colorBlack21, fontSize: 15, paddingHorizontal: 2 }}>Baggage Price</Text>
              </View>
              <View style={{ justifyContent: 'center', alignItems: 'flex-end', width: '40%' }}>
                <Text style={{ fontFamily: Fonts.medium, color: colors.colorBlack21, fontSize: 15, paddingHorizontal: 2 }}>{price.currency + " " + price.baggage_price}</Text>
              </View>
            </View>
          </View>

          <View style={{ height: 1, backgroundColor: colors.lightgrey, width: '100%' }} />
          <View style={{ paddingVertical: 10, }}>
            <View style={styles.fullView}>
              <View style={{ justifyContent: 'center', alignItems: 'flex-start', width: '60%' }}>
                <Text style={{ fontFamily: Fonts.medium, color: colors.colorBlack21, fontSize: 15, paddingHorizontal: 2 }}>Meal Price</Text>
              </View>
              <View style={{ justifyContent: 'center', alignItems: 'flex-end', width: '40%' }}>
                <Text style={{ fontFamily: Fonts.medium, color: colors.colorBlack21, fontSize: 15, paddingHorizontal: 2 }}>{price.currency + " " + price.meal_price}</Text>
              </View>
            </View>
          </View>


          <View style={{ height: 1, backgroundColor: colors.lightgrey, width: '100%' }} />
          <View style={{ paddingVertical: 10, }}>
            <View style={styles.fullView}>
              <View style={{ justifyContent: 'center', alignItems: 'flex-start', width: '60%' }}>
                <Text style={{ fontFamily: Fonts.medium, color: colors.colorBlack21, fontSize: 15, paddingHorizontal: 2 }}>Other Charges</Text>
              </View>
              <View style={{ justifyContent: 'center', alignItems: 'flex-end', width: '40%' }}>
                <Text style={{ fontFamily: Fonts.medium, color: colors.colorBlack21, fontSize: 15, paddingHorizontal: 2 }}>{price.currency + " " + price.other_charges}</Text>
              </View>
            </View>
          </View>

          <View style={{ height: 1, backgroundColor: colors.lightgrey, width: '100%' }} />
          <View style={{ paddingVertical: 10, }}>
            <View style={styles.fullView}>
              <View style={{ justifyContent: 'center', alignItems: 'flex-start', width: '60%' }}>
                <Text style={{ fontFamily: Fonts.medium, color: colors.colorBlack21, fontSize: 15, paddingHorizontal: 2 }}>Total Fare</Text>
              </View>
              <View style={{ justifyContent: 'center', alignItems: 'flex-end', width: '40%' }}>
                <Text style={{ fontFamily: Fonts.medium, color: colors.colorBlack21, fontSize: 15, paddingHorizontal: 2 }}>{price.currency + " " + price.grant_total}</Text>
              </View>
            </View>
          </View>

        </View>
      </CardView>
    )
  }



  render() {
    const { inBoundFlights, outBoundFlights } = this.state
    if(!outBoundFlights) return <View/>
    return (
      <View style={styles.container}>
        <SafeAreaView backgroundColor={colors.colorBlue} />
        <CommonHeader title={"Booking Details"} />
        <View style={{ paddingHorizontal: 15, flex: 1 }} >
          <ScrollView showsVerticalScrollIndicator={false} style={{ flex: 1 }}>
            <View style={{ marginTop: 20 }}>
              {/* {this.state.bookingDataItem.flights.map((item, index) => {
              return this.renderAirlineData(item)
            })} */}
              {this.renderFlights(outBoundFlights)}
              {this.renderFlights(inBoundFlights)}


              <CardView
                style={{
                  backgroundColor: colors.colorWhite,
                  marginVertical: 10,
                  marginHorizontal: 1
                }}
                cardElevation={2}
                cardMaxElevation={3}
                cornerRadius={8}>
                <View style={{ borderRadius: 8, overflow: 'hidden' }}>
                  {this.renderPassengerDetail()}
                  <Dash style={{ width: '100%', height: 0.5, }} dashColor={colors.colorGrey} dashLength={2} dashThickness={1} />
                  {this._renderPNR()}
                  <Dash style={{ width: '100%', height: 0.5, }} dashColor={colors.colorGrey} dashLength={2} dashThickness={1} />
                  {this._renderBookingId()}
                  <Dash style={{ width: '100%', height: 0.5, }} dashColor={colors.colorGrey} dashLength={2} dashThickness={1} />
                  {this._renderEmailDetails()}
                  <Dash style={{ width: '100%', height: 0.5, }} dashColor={colors.colorGrey} dashLength={2} dashThickness={1} />
                  {this._renderPhoneDetails()}
                  <Dash style={{ width: '100%', height: 0.5, }} dashColor={colors.colorGrey} dashLength={2} dashThickness={1} />
                  {this._renderDownloadTicket()}
                </View>
              </CardView>

              {this.renderFareDetails()}
            </View>

            {/* {this.renderPassengerDetail()}

            {this.renderContactDetail()}

            {this.renderTotalPrice()} */}


          </ScrollView>

        </View>
        <SafeAreaView />

      </View>
    )
  }

}


const mapStateToProps = state => {
  return {
  }
}

const mapDispatchToProps = dispatch => {
  return {
    getFlightBookingDetailsAction: (data) => dispatch(getFlightBookingDetailsAction(data))
  }
}

export default connect(null, mapDispatchToProps)(BookingDetails)

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f5f5f5'
  },
  estimate: {
    fontFamily: Fonts.semiBold,
    color: colors.colorBlack,
    fontSize: 14
  },
  stnName: {
    fontFamily: Fonts.regular,
    color: colors.colorBlack,
    fontSize: 14,
    lineHeight: 20
  },
  dotView: {
    height: 10,
    width: "100%",
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center"
  },
  fullView: { flexDirection: 'row', alignItems: 'center', width: '100%', paddingHorizontal: 20 }



})
