import { Dimensions, StyleSheet } from "react-native";
import { colors } from "../../../Utils/Colors";
import Fonts from "../../../Utils/Fonts";
const screenWidth = Dimensions.get("window").width;


export const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "rgba(244,248,254,1)"
  },
  itemContainer: {
    margin: 10,
    backgroundColor: "white",
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 10,
    shadowColor: colors.colorShadow,
    shadowOpacity: 0.3,
    shadowOffset: { width: 1, height: 2 },
    shadowRadius: 3,
    elevation: 4,
    padding: 15
  },
  Buttons: {
    height: 50,
    width: "90%",
    backgroundColor: "rgb(0,104,208)",
    margin: 10,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 7,
    alignSelf: "center",
  },
  modalclosebtn: {
    alignSelf: "center", marginTop: 10,
    backgroundColor: "rgb(0,104,208)",
    padding: 20, paddingTop: 8,
    paddingBottom: 8, borderRadius: 5
  },
  headerBtn: {
    width: 50,
    height: "100%",
    justifyContent: "center",
    alignItems: "center"
  },
  header: {
    height: 50,
    width: "100%",
    backgroundColor: "rgb(246,245,246)",
    flexDirection: "row",
    alignItems: "center",
    // backgroundColor: "yellow"
  },
  headerTitle: {
    width: screenWidth - 100,
    height: 50,
    justifyContent: "center",
    alignItems: "center",
    // backgroundColor: "red"
  },
  titleText: {
    fontSize: 14,
    fontFamily: Fonts.medium,
    color: colors.colorBlack
  },
  dotView: {
    height: 10,
    width: "100%",
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center"
  },
  estimate: {
    fontFamily: Fonts.semiBold,
    color: colors.colorBlack,
    fontSize: 14
  },
  stnName: {
    fontFamily: Fonts.regular,
    color: colors.colorBlack,
    fontSize: 14,
    lineHeight: 20
  },
  cardheaderFlightImg: {
    width: 20, height: 20,
    resizeMode: "contain", marginRight: 5
  },
  container: {
    flex: 1,
  },
  mainContainer: {
    flex: 1,
    backgroundColor: "#f5f5f5"
  },
  price: {
    color: colors.colorBlack,
    fontFamily: Fonts.medium,
    fontSize: 12
  },
});
