import moment from "moment";
import React, { Component } from "react";
import { DeviceEventEmitter, Image, Modal, Platform, Text, TextInput, TouchableHighlight, TouchableOpacity, View, Alert } from "react-native";
import CardView from 'react-native-cardview';
import Dash from "react-native-dash";
import { FlatList } from "react-native-gesture-handler";
import { WebView } from 'react-native-webview';
import { NavigationEvents, SafeAreaView, ScrollView } from "react-navigation";
import { connect } from "react-redux";
import { doLoginAction, getFlightRuleAction } from '../../../Redux/Actions';
import { imageBaseUrl } from "../../../Utils/APIManager/APIConstants";
import { colors } from "../../../Utils/Colors";
import { commonstyle } from "../../../Utils/Commonstyle";
import Fonts from "../../../Utils/Fonts";
import Images from "../../../Utils/Images";
import StringConstants from '../../../Utils/StringConstants';
import { convertMinsIntoHandM, getCabinClass, isEmailValid, MyAlert } from "../../../Utils/Utility";
import BookingBelt from "../../custom/BookingBelt";
import BottomStrip from "../../custom/BottomStrip";
import FlightListHeader from "../../custom/FlightListHeader";
import NavigationServices from './../../../Utils/NavigationServices';
import { styles } from "./Styles";
import MyLoader from "../../custom/MyLoader";
import Spinner from "react-native-loading-spinner-overlay";


const INJECTEDJAVASCRIPT = `const meta = document.createElement('meta'); meta.setAttribute('content', 'initial-scale=0.5, maximum-scale=0.5, user-scalable=1'); meta.setAttribute('name', 'viewport'); document.getElementsByTagName('head')[0].appendChild(meta); `

class FlightDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      totalFare: 0,
      isNavigateFromReviewPage: null,
      flightType: null,
      origin: null,
      destination: null,
      isRoundTrip: null,
      isInterNational: null,
      switch: false,
      nonRefundable: true,
      spinner: false,
      visible: false,
      loginModal: false,
      isSigninTicked: false,
      disabled: false,
      phone: '',
      email: '',
      phoneN: '',
      emailN: '',
      password: '',
      flightData: [],
      finalData: [],
      seactionHeaderDict: {
        origin: "",
        destination: "",
        isRefundable: "",
        dept_date: "",
        stops: 0,
        duration: "",

        //if isLcc true then meal is paid
        isLCC: false
      },
      listDict: {
        key: "",
        data: []
      },
      traceId: "",
      passengerDetails: null,
      loading: false,
      html: "",
      mealType: "",
      currency: "",
    };
    this._renderListItem = this._renderListItem.bind(this);
    this.updateFlightRuleFromApi = this.updateFlightRuleFromApi.bind(this);
    this.updateLoginDetail = this.updateLoginDetail.bind(this);
  }

  //MARK:------------------------- Component Lifecycle Methods ----------------
  componentDidMount() {
    let { navigation } = this.props;
    console.log("flight details hello");
    let flightData = navigation.getParam('flightdata', null);
    let passengers = navigation.getParam('passengers', null);
    let totalFare = navigation.getParam("totalFare", 0);
    console.log("totalFare:--" + totalFare);
    console.log("flightData:-", JSON.stringify(flightData));
    let traceId = navigation.getParam('traceid', null);
    console.log(JSON.stringify(passengers));
    let flightType = navigation.getParam('flightType', null);
    let origin = navigation.getParam('origin', null);
    let destination = navigation.getParam('destination', null);
    let isRoundTrip = navigation.getParam('isRoundTrip', null);
    let isInterNational = navigation.getParam('isInterNational', null);
    let currency = navigation.getParam('currency', null);
    let isNavigateFromReviewPage = navigation.getParam('isNavigateFromReviewPage', null);
    let finalData = [];
    let isRefundable = 'Non-Refundable'
    if (flightData.length == 2) {
      finalData = this.domesticRoundTripData(flightData);
      if (flightData[0].isRefundable && flightData[1].isRefundable) {
        isRefundable = "Refundable"
      }
    } else {
      let dict = flightData[0];
      // let dict = dict1[0]
      if (dict.isRefundable) {
        isRefundable = "Refundable"
      }
      let flightSagments = dict.flightSegment;

      if (flightSagments.length > 1) {
        let dataArr = this.calculateInterNationalRoundTrip(
          dict,
          flightSagments
        );
        finalData = dataArr;
      } else {
        let listDict = this.calculateDomesticSingle(dict, flightSagments);
        finalData.push(listDict);
      }
    }

    this.setState({
      totalFare,
      isRefundable: isRefundable,
      flightData: flightData,
      finalData: finalData,
      traceId: traceId,
      passengerDetails: passengers,
      flightType: flightType,
      origin: origin,
      destination: destination,
      isRoundTrip: isRoundTrip,
      isInterNational: isInterNational,
      isNavigateFromReviewPage: isNavigateFromReviewPage,
      currency: currency ? currency : "INR"
    }, () => {
      DeviceEventEmitter.addListener(StringConstants.FLIGHT_RULE_EVENT, this.updateFlightRuleFromApi)
      DeviceEventEmitter.addListener(StringConstants.IS_LOGINEVENT, this.updateLoginDetail)
      //this.getFlightRule()
      // console.log("DJ", this.state.finalData)
    });
  }

  componentWillUnmount() {
    DeviceEventEmitter.removeListener(StringConstants.FLIGHT_RULE_EVENT, this.updateFlightRuleFromApi)
    DeviceEventEmitter.removeListener(StringConstants.IS_LOGINEVENT, this.updateLoginDetail)
  }

  updateFlightRuleFromApi(res) {
    const data = res.response;
    const sectionIndex = res.sectionIndex
    const index = res.index
    let cancellationPolicyArray = [];
    // console.log("CancePol:", JSON.stringify(data))
    if (data) {
      let hml = "";
      data.forEach((element, index) => {
        var num = index + 1;
        element.forEach((element2, index2) => {
          var num2 = index2 + 1;
          //hml = hml + "<H1 style='text-align:center;font-size:20px;'>Flight Rule for flight " + " " + num2 + "</H1>" + element2.FareRuleDetail
          cancellationPolicyArray.push("<H1 style='text-align:center;font-size:20px;'>Flight From " + element2.Origin + " to " + element2.Destination + "</H1>" + element2.FareRuleDetail)
        });
        // hml = hml + "<H1 style='text-align:center;font-size:15px;'>Flight Rule for flight " + num + "</H1>" + element.FareRuleDetail
      });
      // this.setState({
      //   html: hml,
      //   visible: true,
      //   cancellationPolicyArray
      // })
      let n = 0
      let list = this.state.finalData
      if (list.length > 0) {
        list.forEach((element) => {
          element.data.forEach((element, index) => {
            if (cancellationPolicyArray.length > n) {
              element.cancellationPolicy = cancellationPolicyArray[n]
              n++;
            }
          })
        });
      }
      console.log("this.state.finalData:", JSON.stringify(this.state.finalData))
      console.log("sectionIndex", JSON.stringify(sectionIndex))
      console.log("index", JSON.stringify(index))
      this.setState({
        finalData: list,
        html: this.state.finalData[sectionIndex].data[index].cancellationPolicy,
        visible: true,
      })


    }
  }

  getFlightRule(sectionIndex, index) {
    // this.setState({
    //   visible: true
    // })
    // return
    let flight = [{
      index: this.state.flightData[0].index,
      isLCC: this.state.flightData[0].isLCC
    }]
    if (this.state.flightData.length > 1) {
      flight.push({
        index: this.state.flightData[1].index,
        isLCC: this.state.flightData[1].isLCC
      })
    }
    let param = {
      type: this.state.flightData[0].source,
      flight: flight,
      traceId: this.state.traceId,
      sectionIndex: sectionIndex,
      index: index
    }
    console.log("get flight rule");
    console.log(param);

    this.props.getFlightRuleAction(param)
  }

  //MARK:-------------------- Calculate Domestic Single Way Data ------------

  calculateDomesticSingle(dict, flightSagments) {
    let headerDict = this.state.seactionHeaderDict;
    let listDict = this.state.listDict;

    headerDict.origin = flightSagments[0][0].origin.airport.airportCode;
    headerDict.destination = dict.destination;
    headerDict.dept_date = flightSagments[0][0].stopPointDepartureTime;
    headerDict.isRefundable = dict.isRefundable;
    headerDict.stops = flightSagments.length - 1;
    headerDict.isLCC = dict.isLCC;

    let duration = 0;
    flightSagments[0].forEach(item => {
      duration = duration + item.duration;
    });

    headerDict.duration = duration;

    listDict.key = headerDict;
    listDict.data = flightSagments[0];
    return listDict;
  }

  //MARK:-------------- Calculate InterNational round Trip Data --------------

  calculateInterNationalRoundTrip(dict, flightSagments) {
    let finalData = [];
    flightSagments.forEach(objArr => {
      let headerDict = {
        origin: "",
        destination: "",
        isRefundable: "",
        dept_date: "",
        stops: 0,
        duration: "",
        isLCC: dict.isLCC
      };
      let listDict = {
        key: "",
        data: []
      };

      let flightOriginDict = objArr[0];
      let flightDestdict = objArr[objArr.length - 1];

      headerDict.origin = flightOriginDict.origin.airport.airportCode;
      headerDict.destination = flightDestdict.destination.airport.airportCode;

      headerDict.dept_date = flightOriginDict.stopPointDepartureTime;

      headerDict.isRefundable = dict.isRefundable;
      headerDict.stops = objArr.length - 1;


      let duration = 0;
      objArr.forEach(item => {
        duration = duration + item.duration;
      });

      headerDict.duration = duration;

      listDict.key = headerDict;
      listDict.data = objArr;

      finalData.push(listDict);
    });
    return finalData;
  }

  //MARK:----------------- Domestic Round Trip Data --------------
  domesticRoundTripData(flightData) {
    let finalData = [];
    flightData.forEach((obj, index) => {
      let flightSegment = obj.flightSegment[0];
      let headerDict = {
        origin: "",
        destination: "",
        isRefundable: "",
        dept_date: "",
        stops: 0,
        duration: "",
        isLCC: obj.isLCC
      };
      let listDict = {
        key: "",
        data: []
      };

      headerDict.origin = flightSegment[0].origin.airport.airportCode;
      headerDict.destination = obj.destination;
      headerDict.dept_date = flightSegment[0].stopPointDepartureTime;
      headerDict.isRefundable = obj.isRefundable;
      headerDict.stops = flightSegment.length - 1;

      let duration = 0;

      flightSegment.forEach(item => {
        duration = duration + item.duration;
      });

      headerDict.duration = duration;

      listDict.key = headerDict;
      listDict.data = flightSegment;

      finalData.push(listDict);
    });

    return finalData;
  }



  _renderListItem({ item, index, section }) {
    let operatingCarrier = item.airline.operatingCarrier;
    let fareClass = item.airline.fareClass
    if (operatingCarrier == "" || operatingCarrier == null ||
      operatingCarrier == item.airline.airlineCode) {
      operatingCarrier = null
    }
    // console.clear();
    // console.log(section)
    // console.log("item", JSON.stringify(item))
    // console.log("item", JSON.stringify(this.state.finalData))

    let destTerminal = item.destination.airport.terminal;
    let originTerminal = item.origin.airport.terminal;
    return (
      <View style={[commonstyle.itemBackground, { padding: 0 }]} >
        <View style={commonstyle.cardHeader}>
          <View style={commonstyle.cardHeaderCol}>
            <Image source={{ uri: imageBaseUrl + item.airline.image }}
              style={styles.cardheaderFlightImg}></Image>
            <Text style={[styles.price, { fontFamily: Fonts.semiBold, fontSize: 13 }]}>{item.airline.airlineName + " | "}{item.airline.airlineCode + "-" + item.airline.flightNumber}
              {operatingCarrier && <Text style={[styles.price, { color: colors.colorBlack, fontSize: 11 }]}>{" (Operated by: " + operatingCarrier + ")"}</Text>}
            </Text>
          </View>
          <View style={[commonstyle.cardHeaderCol, {
            justifyContent: "flex-end"
          }]}>

            <Text style={styles.price}>{(section.key.isRefundable ? "Refundable" : "Non-Refundable") + " | "}{section.key.stops ? section.key.stops + " stop" : "Nonstop"}</Text>
          </View>
        </View>
        <View style={{
          width: "100%",
          padding: 12, paddingBottom: 0,
          flexDirection: "row",
          alignItems: "center"
        }}>
          <View style={{
            width: "25%",
            // backgroundColor: "red",
          }}>
            <Text style={styles.estimate}>{moment(item.origin.depTime).format("HH:mm")}</Text>
            <Text style={[styles.stnName, { color: colors.colorBlue }]}>{item.origin.airport.cityName}</Text>
          </View>
          <View style={{
            width: "50%",
            height: 15,
            flexDirection: "row",
            justifyContent: "space-between",
            alignItems: "center",
          }}>
            <View style={{
              width: "45%",
              alignItems: "center"
              // backgroundColor: "blue"
            }}>
              <View style={styles.dotView}>
              <View style={{ backgroundColor: colors.colorBlue, borderRadius: 5, height: 10, width: 10 }} />
                {/* <Image source={Images.imgbluedot} style={{ width: "10%", resizeMode: "contain" }}></Image> */}
                <Dash style={{
                  width: "75%",
                  height: 1,
                }} dashColor={colors.lightgrey} dashLength={5}
                />
              </View>
            </View>
            <View style={{
              width: "10%",
              height: 20,
              marginTop: 3
            }}>
              <Image source={Images.ic_image_planeblue} style={{ width: "100%", height: "100%", resizeMode: "contain" }}></Image>
            </View>
            <View style={{
              width: "45%",
              alignItems: "center"
            }}>
              <View style={styles.dotView}>
                <Dash style={{
                  width: "75%",
                  height: 1,
                }} dashColor={colors.lightgrey} dashLength={5}
                />
                <View style={{ backgroundColor: colors.colorBlue, borderRadius: 5, height: 10, width: 10 }} />
                {/* <Image source={Images.imgbluedot} style={{ width: "10%", resizeMode: "contain" }}></Image> */}
              </View>
            </View>
          </View>
          <View style={{
            width: "25%",
            alignItems: "flex-end",
            // backgroundColor: "red",
          }}>
            <Text style={styles.estimate}>{moment(item.destination.arrTime).format("HH:mm")}</Text>
            <Text style={[styles.stnName, { color: colors.colorBlue }]}>{item.destination.airport.cityName}</Text>
          </View>
        </View>
        <View style={{
          width: "100%", padding: 12,
          flexDirection: "row", paddingTop: 0,
          justifyContent: "space-between",
          alignItems: "center", paddingBottom: 0
        }}>
          <View style={{ width: "35%" }}>
            <Text style={styles.stnName}>{moment(item.origin.depTime).format("DD MMM, YYYY")}</Text>
          </View>
          <View style={{
            width: "30%",
            alignItems: "center",
          }}>
            <Text style={[styles.stnName, { fontFamily: Fonts.medium, marginTop: -12 }]}>{convertMinsIntoHandM(item.duration)}</Text>
          </View>
          <View style={{ width: "35%", alignItems: "flex-end" }}>
            <Text style={[styles.stnName, { fontFamily: Fonts.medium, }]}>{moment(item.destination.arrTime).format("DD MMM, YYYY")}</Text>
          </View>
        </View>
        <View style={{
          width: "100%", padding: 12,
          flexDirection: "row", paddingTop: 0,
          justifyContent: "space-between",
          alignItems: "center",
        }}>
          <View style={{ width: "50%" }}>
            <Text style={[styles.stnName]}>{item.origin.airport.airportName}</Text>
            {originTerminal !== "" && <Text style={styles.stnName}>
              Terminal - {originTerminal}
            </Text>}
          </View>
          <View style={{ width: "50%", alignItems: "flex-end" }}>
            <Text style={[styles.stnName]}>{item.destination.airport.airportName}</Text>
            {destTerminal !== "" && <Text style={styles.stnName}>
              Terminal - {destTerminal}
            </Text>}
          </View>
        </View>
        <Dash style={{ height: 1, width: "100%", }} dashColor={colors.lightgrey}
          dashLength={5} />
        <View style={{ paddingHorizontal: 15, paddingBottom: 15, flexDirection: "row", width: '100%' }}>
          <View style={{ width: "100%", justifyContent: "space-between" }}>
            {item.baggage &&
              <View style={{ flexDirection: "row", alignItems: "center", paddingTop: 15 }}>
                <Image style={{ height: 20, width: 20, tintColor: colors.colorBlue }}
                  source={Images.ic_image_baggage}
                  resizeMode="contain" />
                <Text style={[commonstyle.blackText, { marginLeft: 5, fontSize: 12 }]}>
                  {item.baggage && item.baggage + " Check-In"}   {item.cabinBaggage && ", " + item.cabinBaggage + " Cabin"}
                </Text>
              </View>
            }
            <View style={{
              flexDirection: "row",
              alignItems: "center",
              paddingTop: 10,
              width: '70%',
            }} >
              <View style={{
                flexDirection: "row",
                alignItems: "center",
                width: '50%',
              }} >
                <Image style={{ height: 20, width: 20, tintColor: colors.colorBlue }}
                  source={Images.ic_paid_meal}
                  resizeMode="contain"
                />
                <Text style={[commonstyle.blackText, { marginLeft: 5, fontSize: 12, color: "black" }]}>
                  Food {section.key.isLCC ? "Paid" : "Free"}
                </Text>
              </View>
              {
                item.seatAvailable &&

                <View style={{
                  flexDirection: "row",
                  alignItems: "center",
                  width: '50%'
                }} >
                  <Image style={{ height: 20, width: 20 }}
                    source={Images.ic_image_seat}
                    resizeMode="contain"
                  />
                  <Text style={[commonstyle.blackText, { marginLeft: 5, fontSize: 12, color: "black" }]}>
                    {item.seatAvailable}{item.seatAvailable > 1 ? " seats " : " seat "} available
              </Text>
                </View>}
            </View>


          </View>
        </View>
      </View>
    );
  }

  continue = () => {
    // console.log("USERDATA:", JSON.stringify(this.props.userData))
    if (this.props.isLogin && this.props.userData) {
      let phone = "NA"
      let email = "NA"
      let userId = null
      if (this.props.userData.address && this.props.userData.address.phone) phone = this.props.userData.address.phone
      if (this.props.userData.email) email = this.props.userData.email
      if (this.props.userData.id) userId = this.props.userData.id
      NavigationServices.navigate('PassengerDetails', {
        flightdata: this.state.flightData,
        traceid: this.state.traceId,
        passengers: this.state.passengerDetails,
        flightType: this.state.flightType,
        origin: this.state.origin,
        destination: this.state.destination,
        isRoundTrip: this.state.isRoundTrip,
        isInterNational: this.state.isInterNational,
        currency: this.state.currency,
        phone: phone,
        email: email,
        userId: userId
      })
    }
    else {
      this.setState({
        loginModal: true
      })
    }
  }

  _renderSection = ({ item, index }) => {
    //const section = JSON.parse(JSON.stringify(item));
    const section = item;
    const sectionIndex = index;
    // console.log("renderSection", JSON.stringify(section))


    return (
      <CardView style={{
        backgroundColor: colors.colorWhite,
        margin: 10,
      }}
        cardElevation={2}
        cardMaxElevation={3}
        cornerRadius={10}>
        <View style={{ borderTopLeftRadius: 10, borderTopRightRadius: 10, overflow: "hidden" }}>
          {
            section.data.map((item, index) => (
              this.renderFlightItem(item, index, section, sectionIndex)
            ))
          }
        </View>
      </CardView>
    )
  }

  renderFlightItem(item, index, section, sectionIndex) {
    let operatingCarrier = item.airline.operatingCarrier;
    let fareClass = item.airline.fareClass
    let cabinClass = getCabinClass(item.cabinClass, "key", "value")
    if (operatingCarrier == "" || operatingCarrier == null ||
      operatingCarrier == item.airline.airlineCode) {
      operatingCarrier = null
    }
    let destTerminal = item.destination.airport.terminal;
    let originTerminal = item.origin.airport.terminal;
    const data = section.data
    const length = data.length
    return (
      <View style={{ paddingHorizontal: 0 }} >
        <View style={{ paddingHorizontal: 5, marginBottom: 10 }} >
          <View style={{ width: '100%', height: 45, flexDirection: 'row', alignItems: 'center', paddingHorizontal: 10, paddingVertical: 5 }}>
            <View style={{ padding: 2, borderRadius: 20, borderWidth: 1, borderColor: colors.lightgrey }}>
              <Image source={{ uri: imageBaseUrl + item.airline.image }}
                style={{ width: 30, height: 30, resizeMode: 'contain' }} /></View>
            <Text style={[styles.price, { fontFamily: Fonts.semiBold, fontSize: 13, paddingLeft: 10 }]}>{item.airline.airlineName + " | "}{item.airline.airlineCode + "-" + item.airline.flightNumber}
              {operatingCarrier && <Text style={[styles.price, { color: colors.colorBlack, fontSize: 11 }]}>{" (Operated by: " + operatingCarrier + ")"}</Text>}
            </Text>
          </View>
          <View style={{
            width: "100%",
            padding: 12, paddingBottom: 0,
            flexDirection: "row",
          }}>
            <View style={{
              width: "35%",
              // backgroundColor: "red",
            }}>
              <Text style={styles.estimate}>{moment(item.origin.depTime).format("HH:mm")}</Text>
              <Text style={[styles.stnName, { color: colors.colorBlue }]}>{item.origin.airport.cityName}</Text>
            </View>
            <View style={{
              width: "30%",
              height: '100%',
              //height: 15,
              flexDirection: "row",
              justifyContent: "space-between",
              alignItems: "center",
            }}>
              <View style={{
                width: "43%",
                alignItems: "center"
                // backgroundColor: "blue"
              }}>
                <View style={styles.dotView}>
                  <View style={{ backgroundColor: colors.colorBlue, borderRadius: 3, height: 6, width: 6 }} />
                  {/* <Image source={Images.imgbluedot} style={{ width: "10%", resizeMode: "contain" }}></Image> */}
                  <Dash style={{
                    width: "75%",
                    height: 1,
                  }} dashColor={colors.lightgrey} dashLength={5}
                  />
                </View>
              </View>
              <View style={{
                width: "14%",
                height: 20,
              }}>
                <Image source={Images.ic_image_planeblue} style={{ width: "100%", height: "100%", resizeMode: "contain" }}></Image>
              </View>
              <View style={{
                width: "43%",
                alignItems: "center"
              }}>
                <View style={styles.dotView}>
                  <Dash style={{
                    width: "75%",
                    height: 1,
                  }} dashColor={colors.lightgrey} dashLength={5}
                  />
                  <View style={{ backgroundColor: colors.colorBlue, borderRadius: 3, height: 6, width: 6 }} />

                  {/* <Image source={Images.imgbluedot} style={{ width: "10%", resizeMode: "contain" }}></Image> */}
                </View>
              </View>
            </View>
            <View style={{
              width: "35%",
              alignItems: "flex-end",
              // backgroundColor: "red",
            }}>
              <Text style={styles.estimate}>{moment(item.destination.arrTime).format("HH:mm")}</Text>
              <Text style={[styles.stnName, { color: colors.colorBlue }]}>{item.destination.airport.cityName}</Text>
            </View>
          </View>
          <View style={{
            width: "100%", padding: 12,
            flexDirection: "row", paddingTop: 0,
            justifyContent: "space-between", paddingBottom: 0
          }}>
            <View style={{ width: "35%" }}>
              <Text style={[styles.stnName, { fontFamily: Fonts.medium }]}>{moment(item.origin.depTime).format("DD MMM, YYYY")}</Text>
            </View>
            <View style={{
              width: "30%",
              alignItems: "center",
            }}>
              <Text style={[styles.stnName, { fontFamily: Fonts.medium, marginTop: -12 }]}>{convertMinsIntoHandM(item.duration)}</Text>
            </View>
            <View style={{ width: "35%", alignItems: "flex-end" }}>
              <Text style={[styles.stnName, { fontFamily: Fonts.medium, }]}>{moment(item.destination.arrTime).format("DD MMM, YYYY")}</Text>
            </View>
          </View>
          <View style={{
            width: "100%", paddingHorizontal: 12,
            flexDirection: "row", paddingTop: 0,
            justifyContent: "space-between",
          }}>
            <View style={{ width: "50%" }}>
              <Text style={[styles.stnName, { textAlign: 'left' }]}>{item.origin.airport.airportName}</Text>
              {originTerminal !== "" && <Text style={[styles.stnName, { textAlign: 'left' }]}>
                Terminal - {originTerminal}
              </Text>}
            </View>
            <View style={{ width: "50%", alignItems: "flex-end" }}>
              <Text style={[styles.stnName, { textAlign: 'right' }]}>{item.destination.airport.airportName}</Text>
              {destTerminal !== "" && <Text style={[styles.stnName, { textAlign: 'right' }]}>
                Terminal - {destTerminal}
              </Text>}
            </View>
          </View>
        </View>

        <Dash style={{
          height: 2,
        }} dashColor={colors.lightgrey} dashLength={2}
        />

        <View style={{ flexDirection: 'row', alignItems: 'flex-start', width: '100%', paddingVertical: 5, paddingHorizontal: 15 }}>
          <View style={{ flexDirection: 'row', alignItems: 'flex-start', width: '60%', paddingVertical: 10 }}>
            <Text style={{ paddingLeft: 5, fontFamily: Fonts.medium, fontSize: 11.5, color: colors.colorBlack21 }} >Cabin Class - </Text>
            <Text style={{ paddingLeft: 5, fontFamily: Fonts.medium, fontSize: 11, color: colors.colorBlack62, flex: 1 }} >{cabinClass || "Economy"}</Text>
          </View>
          <View style={{ flexDirection: 'row', alignItems: 'flex-start', width: '40%', paddingVertical: 10 }}>
            <Text style={{ paddingLeft: 5, fontFamily: Fonts.medium, fontSize: 11.5, color: colors.colorBlack21 }} >Fare Class - </Text>
            <Text style={{ paddingLeft: 5, fontFamily: Fonts.medium, fontSize: 11, color: colors.colorBlack62 }} >{fareClass || "A"}</Text>
          </View>
        </View>

        {this.renderCabinDetails(item, index, section)}
        {this.renderCancelPolicy(item, index, sectionIndex)}
        {
          length != index + 1 &&
          <View style={{ backgroundColor: '#f5f5f5', paddingVertical: 10, alignItems: 'center' }}>
            <Text style={{ fontFamily: Fonts.medium }}>
              {convertMinsIntoHandM((moment.duration((moment(data[index + 1].origin.depTime)).diff(moment(item.destination.arrTime)))).asMinutes())} Layover
            </Text>
          </View>
        }
      </View>
    );

  }

  renderCabinDetails(item, index, section) {
    let cabinDetailData = []
    if (item.cabinBaggage) {
      cabinDetailData.push({
        image: Images.ic_cabin_bag,
        text: "Cabin Bag - " + item.cabinBaggage
      })
    }
    if ((section.key.isLCC) != null || (section.key.isLCC) != "") {
      cabinDetailData.push({
        image: Images.ic_meal,
        text: "Food " + (section.key.isLCC ? "Paid" : "Free")
      })
    }
    if (item.baggage) {
      cabinDetailData.push({
        image: Images.ic_cabin_bag,
        text: "Check In Bag - " + item.baggage
      })
    }
    if (item.seatAvailable) {
      cabinDetailData.push({
        image: Images.ic_cabin_bag,
        text: item.seatAvailable + (item.seatAvailable > 1 ? " seats" : " seat") + " Available"
      })
    }
    // console.log("Cabin:", JSON.stringify(cabinDetailData))

    return (
      <>
        <Dash style={{
          height: 2,
        }} dashColor={colors.lightgrey} dashLength={2}
        />
        <FlatList
          showsVerticalScrollIndicator={false}
          scrollEnabled={false}
          numColumns={2}
          style={{ paddingVertical: 5, paddingHorizontal: 20 }}
          listKey={(item, index) => 'D' + index.toString()}
          data={
            cabinDetailData
          }
          renderItem={({ item, index }) => {
            return (
              <View style={{ flexDirection: 'row', alignItems: 'center', width: '50%', paddingVertical: 10 }}>
                <Image style={{ height: 16, width: 16 }} resizeMode={'contain'} source={item.image} />
                <Text style={{ paddingLeft: 5, fontFamily: Fonts.medium, fontSize: 11.5, color: colors.colorBlack62 }} >{item.text}</Text>
              </View>)
          }}
        //keyExtractor={(item, index) => index.toString()}
        />
      </>
    )

  }

  renderCancelPolicy(item, index, sectionIndex) {
    return (
      <>
        <Dash style={{
          height: 2,
        }} dashColor={colors.lightgrey} dashLength={2}
        />
        <View style={{ alignItems: 'center', justifyContent: 'center', padding: 12 }}>
          <TouchableOpacity onPress={() => {
            if (item.cancellationPolicy) {
              this.setState({
                html: item.cancellationPolicy,
                visible: true,
              })
            }

            else {
              this.setState({
                html: 'loading....',
                visible: true,
              }, () => setTimeout(() => {
                this.getFlightRule(sectionIndex, index)
              }, 200))
            }
          }}>
            <Text style={{ fontFamily: Fonts.bold, fontSize: 12, color: colors.colorBlue }}>
              BOOKING & CANCELLATION POLICY >>
            </Text>
          </TouchableOpacity>
        </View>
      </>
    )

  }

  guestLogin() {
    const { phone, email, password, isSigninTicked } = this.state
    if (!email) {
      const emailError = 'Enter an Email ID ' + (!isSigninTicked ? 'for Guest Login' : '')
      MyAlert("Email Id Required", emailError)
      return
    }
    if (!isEmailValid(email)) {
      MyAlert("Invalid Credentials", 'Please enter a valid email id')
      return
    }

    if (isSigninTicked) {
      if (!password) {
        MyAlert("Password Required", 'Enter Password')
        return
      }
      if (password.length < 6) {
        MyAlert("Invalid Credentials", 'The password must be at least 6 characters.')
        return
      }
      this.props.doLoginAction({
        email: email,
        password: password,
        loginFrom: "FlightDetails"
      }, this.props)
    } else {
      if (phone.trim().length == 0) {
        MyAlert("Phone Number Required", 'Enter a Phone Number for Guest Login.')
        return
      }
      if (phone.trim().length < 10) {
        MyAlert("Invalid Credentials", 'Please enter valid Phone Number')
        // alert('Please Enter valid Phone Number')
        return
      }
      this.setState({
        loginModal: false,
        email: '',
        phone: '',
        password: ''
      }, () => {
        NavigationServices.navigate('PassengerDetails', {
          flightdata: this.state.flightData,
          traceid: this.state.traceId,
          passengers: this.state.passengerDetails,
          flightType: this.state.flightType,
          origin: this.state.origin,
          destination: this.state.destination,
          isRoundTrip: this.state.isRoundTrip,
          isInterNational: this.state.isInterNational,
          currency: this.state.currency,
          phone: this.state.phoneN,
          email: this.state.emailN
        })
      })


    }
  }
  updateLoginDetail(data) {
    if (data === true && this.props.userData) {
      this.setState({
        loginModal: false,
        password: '',
        email: ''
      }, () => {
        let phone = "NA"
        let email = "NA"
        let userId = null
        if (this.props.userData.address && this.props.userData.address.phone) phone = this.props.userData.address.phone
        if (this.props.userData.email) email = this.props.userData.email
        if (this.props.userData.id) userId = this.props.userData.id

        setTimeout(() => {
          NavigationServices.navigate('PassengerDetails', {
            flightdata: this.state.flightData,
            traceid: this.state.traceId,
            passengers: this.state.passengerDetails,
            flightType: this.state.flightType,
            origin: this.state.origin,
            destination: this.state.destination,
            isRoundTrip: this.state.isRoundTrip,
            isInterNational: this.state.isInterNational,
            currency: this.state.currency,
            phone: phone,
            email: email,
            userId: userId
          })
        }, 300);
      })
    }
  }

  renderLoginModal() {
    return (
      <Modal
        animationType="slide"
        transparent={true}
        visible={this.state.loginModal}
        onRequestClose={() => { this.setState({ loginModal: false }) }}>
        <MyLoader />
        <TouchableHighlight
          underlayColor={'rgba(0,0,0,0.5)'}
          activeOpacity={1}
          onPress={() => this.setState({ loginModal: false })} style={{ backgroundColor: "rgba(0,0,0,0.5)", justifyContent: "center", flex: 1, alignItems: "center" }}>

          <TouchableHighlight onPress={() => { }}
            underlayColor={'white'}
            activeOpacity={1}
            style={{ width: "80%", backgroundColor: colors.colorWhite, borderRadius: 15, padding: 15, }}>
            <View>
              <View style={{ flexDirection: 'row', alignItems: 'center', height: 30, width: '100%' }}>
                {/* <View style={{ width: '20%', alignItems: 'center', justifyContent: 'center' }}>
                  <TouchableOpacity onPress={() => this.setState({ loginModal: false })}>
                    <Image tintColor={colors.colorBlue} style={{ height: 15, width: 15 }} source={Images.ic_image_back} resizeMode={'contain'} />
                  </TouchableOpacity>
                </View> */}
                <View style={{ width: '100%', justifyContent: "center", alignItems: 'center' }}>
                  <Text
                    style={{ fontFamily: Fonts.bold, color: colors.colorBlue, fontSize: 14, }}
                  >{this.state.isSigninTicked ? 'Sign in' : 'Check-out as a Guest'}</Text>
                </View>
              </View>
              <View style={{ padding: 10 }}>
                <TextInput
                  style={{ height: 34, margin: 4, fontSize: 12, padding: 4, fontFamily: Fonts.regular, borderBottomColor: colors.lightgrey, borderBottomWidth: 1 }}
                  value={this.state.email}
                  onChangeText={text => this.setState({ email: text, emailN: text })}
                  placeholder={'Email'}
                  autoCapitalize={"none"}
                  keyboardType={'email-address'}
                  autoCorrect={false}
                  placeholderTextColor={colors.colorBlack21}
                />

                {!this.state.isSigninTicked && <TextInput
                  style={{ height: 34, margin: 4, fontSize: 12, padding: 4, fontFamily: Fonts.regular, borderBottomColor: colors.lightgrey, borderBottomWidth: 1 }}
                  value={this.state.phone}
                  onChangeText={text => this.setState({ phone: text, phoneN: text })}
                  placeholder={'Phone'}
                  autoCapitalize={"none"}
                  keyboardType={'phone-pad'}
                  autoCorrect={false}
                  placeholderTextColor={colors.colorBlack21}
                  maxLength={10}
                />}
                {this.state.isSigninTicked &&
                  <TextInput
                    style={{ height: 34, fontSize: 12, margin: 4, padding: 4, fontFamily: Fonts.regular, borderBottomColor: colors.lightgrey, borderBottomWidth: 1 }}
                    value={this.state.password}
                    onChangeText={text => this.setState({ password: text })}
                    placeholder={"Password"}
                    autoCapitalize={"none"}
                    autoCorrect={false}
                    secureTextEntry={true}
                    placeholderTextColor={colors.colorBlack21}
                  />}
                <View style={{ paddingVertical: 10, paddingHorizontal: 8, marginBottom: 1, }}>
                  <TouchableOpacity onPress={() => this.setState({ isSigninTicked: !this.state.isSigninTicked, password: '', phone: '', })} style={{ alignItems: 'center', flexDirection: 'row', alignSelf: 'baseline' }}>
                    <View style={{ width: 17, height: 17, backgroundColor: this.state.isSigninTicked ? colors.colorBlue : 'white', borderRadius: 5, borderColor: this.state.isSigninTicked ? colors.colorBlue : colors.colorBlack62, alignItems: 'center', justifyContent: 'center', borderWidth: 1 }}>
                      {this.state.isSigninTicked && <Image resizeMode={'contain'} style={{ width: 14, height: 14, zIndex: 2 }} source={Images.ic_image_checked} />}
                    </View>
                    <Text style={{ fontFamily: Fonts.medium, color: colors.colorBlack62, fontSize: 12, marginLeft: 8 }}>Existing User? Sign In</Text>
                  </TouchableOpacity>
                </View>

                <View style={{ alignItems: 'center', justifyContent: 'center', paddingTop: 14 }}>
                  <TouchableOpacity
                    onPress={() => this.guestLogin()}
                    style={{ alignItems: 'center', justifyContent: 'center', paddingHorizontal: 10, paddingVertical: 8, backgroundColor: colors.colorBlue, borderRadius: 5 }}>
                    <Text style={{ fontFamily: Fonts.bold, color: colors.colorWhite, fontSize: 14 }}>{this.state.isSigninTicked ? "Sign In" : "BOOK AS GUEST"}</Text>
                  </TouchableOpacity>
                </View>

              </View>
            </View>

          </TouchableHighlight>

        </TouchableHighlight>
      </Modal>

    )
  }




  render() {
    if (this.state.flightData == null) { return <View></View> }
    let { visible, html, isNavigateFromReviewPage, totalFare, currency } = this.state;
    return (
      <View style={styles.container} forceInset={{ bottom: "always" }}>
        <SafeAreaView backgroundColor={colors.colorBlue} />
        <NavigationEvents
          onWillFocus={() => {
            let { navigation } = this.props;
            let isNavigateFromReviewPage = navigation.getParam('isNavigateFromReviewPage', null);
            this.setState({
              isNavigateFromReviewPage
            }, () => {
              console.log("isNavigateFromReviewPage:=" + isNavigateFromReviewPage);
            })
            //Call whatever logic or dispatch redux actions and update the screen!
          }} />

        <FlightListHeader noBack={visible}
          data={this.props.flightSearchParams} />
        <View style={styles.mainContainer}>
          <ScrollView showsVerticalScrollIndicator={false}>
            <BookingBelt type={1} style={{ paddingBottom: 5 }} />

            <FlatList
              showsVerticalScrollIndicator={false}
              scrollEnabled={false}
              ref={(ref) => { this.flatListRef = ref }}
              data={
                this.state.finalData
              }
              extraData={this.state.loading}
              renderItem={({ item, index }) => this._renderSection({ item, index })}

              keyExtractor={(item, index) => index.toString()}
            />


          </ScrollView>

          <Modal transparent={true} visible={visible} animationType={'slide'} >
            <View style={styles.container}>
              <SafeAreaView />
              <MyLoader />
              <View style={{ height: (Platform.OS === 'ios') ? 44 : 56 }}></View>
              <View style={styles.header}>
                <TouchableOpacity style={styles.headerBtn} onPress={() => {
                  this.setState({
                    visible: false
                  })
                }}>
                  <Image source={Images.ic_image_close} style={{ width: 20, height: 20, resizeMode: "contain" }}></Image>
                </TouchableOpacity>
                <View style={styles.headerTitle}>
                  <Text style={styles.titleText}>Cancellation And Baggage Policy</Text>
                </View>
              </View>
              <View style={{
                flex: 1, backgroundColor: colors.colorWhite,
                padding: 10
              }}>
                <WebView
                  originWhitelist={['*']}
                  source={{ html: html }}
                  scalesPageToFit={Platform.OS == 'android' ? false : true}
                  injectedJavaScript={INJECTEDJAVASCRIPT}
                />
              </View>
              <SafeAreaView />
            </View>
          </Modal>
          {this.renderLoginModal()}
          <Spinner
            style={{ flex: 1, zIndex: 100, }}
            visible={this.props.isLoading}
            textContent={this.props.loadingMsg}
            textStyle={{ color: "white", fontFamily: Fonts.bold, textAlign: 'center', fontSize: 13 }}
          />
        </View>
        <BottomStrip bottomStripStyle={{ position: 'relative' }} middleText={this.state.isRefundable} price={currency + " " + totalFare} fireEvent={this.continue} />

        <SafeAreaView backgroundColor={colors.colorBlue} />
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  // console.log("USERDATA:", JSON.stringify(state.userDataReducer))
  return {
    isLogin: state.isLoginReducer,
    userData: state.userDataReducer,
    flightSearchParams: state.FlightsReducer.flightSearchParams,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getFlightRuleAction: (data) => dispatch(getFlightRuleAction(data)),
    doLoginAction: (data) => dispatch(doLoginAction(data)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(FlightDetails)
