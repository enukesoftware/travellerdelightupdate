import moment from "moment";
import React, { Component } from "react";
import { Animated, DeviceEventEmitter, Dimensions, Image, InteractionManager, StatusBar, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import { CalendarList } from "react-native-calendars";
import { SafeAreaView } from "react-navigation";
import { colors } from "../Utils/Colors";
import Fonts from "../Utils/Fonts";
import Images from "../Utils/Images";
import StringConstants from "../Utils/StringConstants";
import NavigationServices from './../Utils/NavigationServices';

const screenWidth = Dimensions.get('window').width;

export default class Calender extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedIndex: 0,
      isOneWayActive: null,
      selectedReturnDate: null,
      isSelectDepartDate: true,
      selectedDepartDate: null,
      interactionFinished: false,
      isForHotel: null,
      forReturnDate: null
    };
    this.springValue = new Animated.Value(0.3)
    this._renderSelectedDateView = this._renderSelectedDateView.bind(this);
  }

  componentDidMount() {
    const { navigation } = this.props;
    const forReturnDate = navigation.getParam('forReturnDate', null);
    const departDate = navigation.getParam('departDate', departDate);
    const isForHotel = navigation.getParam('isForHotel', null);
    const isOneWayActive = navigation.getParam('isOneWayActive', null);
    const returnDate = navigation.getParam('returnDate', null);
    this.setState({
      forReturnDate: forReturnDate,
      selectedDepartDate: departDate,
      isForHotel: isForHotel,
      isOneWayActive: isOneWayActive,
      selectedReturnDate: returnDate
    }, () => {
      console.log("isOneWayActive is:--" + this.state.isOneWayActive);
      console.log("selectedIndex is:--" + this.state.selectedIndex);
    })



    InteractionManager.runAfterInteractions(() => {
      console.log("forReturnDate:==" + forReturnDate);
      if (forReturnDate) {
        console.log("forreturnDate:==" + returnDate);
        if (returnDate != null) {
          this.setState({
            selectedIndex: 1,
            selectedReturnDate: moment(returnDate).format('YYYY-MM-DD')
          })
        } else {
          this.setState({
            selectedIndex: 1,
            selectedReturnDate: moment(moment(departDate).add(1, 'd')).format('YYYY-MM-DD'),
          })
        }

      }


      this.setState({
        interactionFinished: true,
        selectedDepartDate: moment(this.state.selectedDepartDate).format('YYYY-MM-DD'),
        // selectedReturnDate: dateFormat(this.state.selectedReturnDate, 'YYYY-MM-DD')
      })
    });


  }

  getDate(date) {
    return moment(date).format("ddd, MMM Do");
  }

  _renderSelectedDateView() {
    return (
      <View
        style={{
          flexDirection: "row",
          width: "100%",
          height: 60,
          backgroundColor: "white"
        }}
      >
        <TouchableOpacity
          style={{
            flex: 1,
            justifyContent: "center",
            // alignItems: "center",
          }}
          underlayColor="transparent"
          activeOpacity={0.9}
          onPress={() => {
            this.setState({ selectedIndex: 0, selectedReturnDate: null });
          }}
        >
          <Text style={styles.normalText}>Departure</Text>
          <Text style={styles.boldText}>
            {this.getDate(this.state.selectedDepartDate)}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.iconButton} disabled={true}>
          <Image source={Images.ic_image_calender}
            style={styles.icon}></Image>
        </TouchableOpacity>

        <TouchableOpacity
          style={{
            flex: 1,
            justifyContent: "center",
            alignItems: "flex-end",
          }}
          underlayColor="transparent"
          activeOpacity={0.9}
          onPress={() => {
            let departdate = new Date(this.state.selectedDepartDate);
            let returndate;
            if (!this.state.selectedReturnDate)
              returndate = moment(departdate).add(1, 'days').format("YYYY-MM-DD");
            else returndate = this.state.selectedReturnDate
            this.setState({
              selectedIndex: 1,
              selectedReturnDate: returndate,
              isOneWayActive: false
            })
          }}
        >
          <Text style={styles.normalText}>Return</Text>
          <Text style={[styles.boldText, { color: this.state.selectedReturnDate ? colors.colorBlack : colors.lightgrey }]}>{this.state.selectedReturnDate ? this.getDate(this.state.selectedReturnDate) : "Select Date"}</Text>
        </TouchableOpacity>
      </View>
    );
  }

  _renderNavBar() {
    return (
      <View style={styles.header}>
        <TouchableOpacity style={styles.headerBtn} onPress={() => {
          NavigationServices.goBack();
        }}>
          <Image source={Images.ic_image_close} style={{ width: 20, height: 20, resizeMode: "contain" }}></Image>
        </TouchableOpacity>
        <View style={{
          width: screenWidth - 110,
          height: 50, alignItems: "center", justifyContent: "center"
        }}>
          <Text style={{ color: colors.colorBlack, fontSize: 14, fontFamily: Fonts.semiBold }}>Select Travel Dates</Text>
        </View>
      </View>
    );
  }

  render() {

    let selectedDate = (this.state.selectedIndex == 0) ? this.state.selectedDepartDate : (this.state.selectedReturnDate) ? this.state.selectedReturnDate : ''
    // let selectedDate = (this.state.selectedIndex == 0 )? this.state.selectedDepartDate:this.state.selectedReturnDate 

    let markingDates = {}

    markingDates[selectedDate] = { selected: true, selectedColor: colors.colorBlue }


    if (this.state.selectedIndex == 1) {
      if (!this.state.isOneWayActive) {
        markingDates[this.state.selectedDepartDate] = { selected: true, selectedColor: "#9ECEFF" }
        markingDates[this.state.selectedReturnDate] = { selected: true, selectedColor: colors.colorBlue }
      }
    }

    return (
      <SafeAreaView style={{ flex: 1, backgroundColor: colors.colorBlue }}
      >
        <StatusBar barStyle="light-content" />
        <View style={styles.container}>
          {this._renderNavBar()}
          <View
            style={{
              padding: 14,
              justifyContent: "space-between",
              borderBottomColor: colors.lightgrey,
              borderBottomWidth: 1
              // height: screenHeight - (getStatusBarHeight(true) + 84)
            }}
          >
            {this._renderSelectedDateView()}

          </View>
          <View style={styles.styleCalendarSuperView}>

            <View style={styles.styleCalendarSuperView}>
              {this.state.interactionFinished && <CalendarList
                // pastScrollRange={50}
                current={this.state.forReturnDate && this.state.selectedReturnDate ? this.state.selectedReturnDate : this.state.selectedDepartDate}
                minDate={this.state.selectedIndex == 0 ? new Date() : this.state.selectedDepartDate}
                onDayPress={(date) => {
                  console.log("isOneWayActive is:--" + this.state.isOneWayActive);
                  console.log("selectedIndex is:--" + this.state.selectedIndex);
                  if (this.state.selectedIndex == 0) {
                    if (!this.state.isOneWayActive) {

                      console.log("date is " + date)

                      let departdate = new Date(date.dateString);
                      let retundate = new Date();
                      retundate = moment(departdate).add(1, 'days').format("YYYY-MM-DD");

                      // retundate.setDate(departdate.getDate() + 1);
                      // retundate = moment(retundate).format("YYYY-MM-DD");

                      console.log(retundate)

                      this.setState({
                        selectedDepartDate: date.dateString,
                        selectedIndex: 1,
                        selectedReturnDate: retundate
                      })

                    } else {
                      this.setState({
                        selectedDepartDate: date.dateString,
                      }, () => {
                        console.log("selectedDepartDate is:--" + this.state.selectedDepartDate);
                        console.log("selectedReturnDate is:--" + this.state.selectedReturnDate);
                      })
                    }

                  } else {
                    this.setState({
                      selectedReturnDate: date.dateString
                    }, () => {
                      console.log("selectedReturnDate 1 is:--" + this.state.selectedReturnDate);
                    })
                  }
                }}
                markedDates={markingDates}
                // markingType='period'
                theme={{
                  backgroundColor: "transparent",
                  calendarBackground: "transparent",
                }}

              />}
            </View>
          </View>
          <View
            style={{ margin: 10,alignItems:'center' }}>
            <TouchableOpacity style={{width:'95%',height:45,backgroundColor:colors.colorBlue,borderRadius:8,alignItems:'center',justifyContent:'center'}}
             onPress={() => {
                // let departDate = Date.parse(this.state.selectedDepartDate)
                // let returnDate = Date.parse(this.state.selectedReturnDate)
                let dict = {
                  departDate: this.state.selectedDepartDate,
                  returnDate: this.state.selectedReturnDate,
                  isOneWayActive: this.state.isOneWayActive
                }
                DeviceEventEmitter.emit(StringConstants.DATE_SELECT, dict)
                NavigationServices.goBack();
              }}>
            <Text style={{color:colors.colorWhite,fontFamily:Fonts.bold,fontSize:16}}>OK</Text>
            </TouchableOpacity>
            
          </View>
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  iconButton: {
    width: 50,
    height: "100%",
    paddingTop: 5,
    alignItems: "center",
  }, icon: {
    width: 30, height: 30,
    resizeMode: "contain",
    marginTop: 7
  },
  boldText: {
    fontFamily: Fonts.bold,
    fontSize: 14,
    color: colors.colorBlack,
    letterSpacing: .5
  },
  normalText: {
    fontFamily: Fonts.regular,
    fontSize: 12,
    color: colors.colorBlack,
    marginBottom: 5
  },
  header: {
    height: 50,
    width: "100%",
    backgroundColor: "rgb(246,245,246)",
    flexDirection: "row",
    alignItems: "center"
  },
  mainContainer: {
    marginTop: 50,
    flex: 1,
    backgroundColor: colors.colorWhite
  },
  headerBtn: {
    width: 50,
    height: "100%",
    alignItems: "center",
    justifyContent: "center",
    // backgroundColor: "red"
  },
  container: {
    flex: 1,
    backgroundColor: colors.colorWhite
  },
  styleCalendarSuperView: {
    flex: 1,
    marginTop: 15,
    backgroundColor: "white",
    margin: 5,
  },

});
