import React, { Component } from "react";
import { Dimensions, Image, Platform, SafeAreaView, ScrollView, Text, TouchableOpacity, View } from "react-native";
import RangeSlider from 'rn-range-slider';
import { colors } from "../../Utils/Colors";
import Fonts from "../../Utils/Fonts";
import Images from "../../Utils/Images";
const screenWidth = Dimensions.get("screen").width;
const screenHeight = Dimensions.get('screen').height;

export default class HotelFilters extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedMinPrice: 0,
      selectedMaxPrice: null,
      minPrice: 0, maxPrice: 0,
      currencycode: null,
      sortingStatus: 0,
      is1starSelected: false,
      is2starSelected: false,
      is3starSelected: false,
      is4starSelected: false,
      is5starSelected: false,
      isScroll:true

    }
  }

  componentDidMount() {
    console.log("Hotel Filters:--" + JSON.stringify(this.props));
    let { selectedFilters } = this.props
    if (selectedFilters) {
      let { minPrice, maxPrice, selectedMaxPrice, selectedMinPrice } = this.state
      if (selectedFilters.minPrice) {
        minPrice = Math.ceil(selectedFilters.minPrice)
      }
      if (selectedFilters.maxPrice) {
        maxPrice = Math.ceil(selectedFilters.maxPrice)
      }
      if (selectedFilters.selectedMinPrice) {
        selectedMinPrice = Math.ceil(selectedFilters.selectedMinPrice)
      }
      if (selectedFilters.selectedMaxPrice) {
        selectedMaxPrice = Math.ceil(selectedFilters.selectedMaxPrice)
      }
      this.setState({
        selectedMinPrice: selectedMinPrice,
        selectedMaxPrice: selectedMaxPrice,
        minPrice: minPrice,
        maxPrice: maxPrice,
        currencycode: selectedFilters.currencycode,
        sortingStatus: selectedFilters.sortingStatus,
        is1starSelected: selectedFilters.is1starSelected,
        is2starSelected: selectedFilters.is2starSelected,
        is3starSelected: selectedFilters.is3starSelected,
        is4starSelected: selectedFilters.is4starSelected,
        is5starSelected: selectedFilters.is5starSelected,
      })
    }
  }

  renderRangeSlider() {
    const { minPrice, maxPrice, selectedMaxPrice, selectedMinPrice, currencycode } = this.state
    let min = 0
    let max = 10000
    let initialLowValue = 0
    let initialHighValue = 10000
    let radioValue = 10

    // if (minPrice) {
    //   min = parseInt(minPrice)
    //   initialLowValue = parseInt(minPrice)
    // }
    if (maxPrice) {
      max = parseInt(maxPrice)
      initialHighValue = parseInt(maxPrice)
    }
    if (selectedMinPrice) {
      initialLowValue = parseInt(selectedMinPrice)
    }
    if (selectedMaxPrice) {
      initialHighValue = parseInt(selectedMaxPrice)
    }

    if (selectedMinPrice != null && selectedMaxPrice != null) {
      if (selectedMinPrice < 1000 && selectedMaxPrice == 1000) {
        radioValue = 1
      }
      if (selectedMinPrice == 1000 && selectedMaxPrice == 2000) {
        radioValue = 2
      }
      if (selectedMinPrice == 2000 && selectedMaxPrice == 3000) {
        radioValue = 3
      }
      if (selectedMinPrice == 3000 && selectedMaxPrice == 4000) {
        radioValue = 4
      }
      if (selectedMinPrice == 4000 && selectedMaxPrice > 4000) {
        radioValue = 5
      }
    }
    // console.log("selectedMinPrice" + selectedMinPrice)
    // console.log("selectedMaxPrice" + selectedMaxPrice)
    // console.log("RadioValue" + radioValue)
    //alert(initialHighValue)
    if (maxPrice) return (
      <View style={{ alignItems: 'center', marginBottom: 10 }}>

        <View style={{ width: '100%', paddingHorizontal: 15, paddingVertical: 10 }}>
          <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center' }} onPress={() => { this.changePriceRangeRadio(1) }}>
            <View style={{ width: 20, height: 20, borderRadius: 10, alignItems: 'center', justifyContent: 'center', borderColor: colors.colorBlue, borderWidth: 1 }}>
              <View style={{ width: 10, height: 10, borderRadius: 5, backgroundColor: radioValue == 1 ? colors.colorBlue : 'white' }} />
            </View>
            <View style={{ paddingHorizontal: 15, alignItems: 'flex-start' }}>
              <Text style={{ fontFamily: Fonts.medium, color: colors.colorBlack21, fontSize: 13 }}>Less Than {currencycode && currencycode + " "}1000</Text>
            </View>
          </TouchableOpacity>
        </View>

        <View style={{ width: '100%', paddingHorizontal: 15, paddingVertical: 10 }}>
          <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center' }} onPress={() => { this.changePriceRangeRadio(2) }}>
            <View style={{ width: 20, height: 20, borderRadius: 10, alignItems: 'center', justifyContent: 'center', borderColor: colors.colorBlue, borderWidth: 1 }}>
              <View style={{ width: 10, height: 10, borderRadius: 5, backgroundColor: radioValue == 2 ? colors.colorBlue : 'white' }} />
            </View>
            <View style={{ paddingHorizontal: 15, alignItems: 'flex-start' }}>
              <Text style={{ fontFamily: Fonts.medium, color: colors.colorBlack21, fontSize: 13 }}>{currencycode && currencycode + " "}1000 - {currencycode && currencycode + " "}2000</Text>
            </View>
          </TouchableOpacity>
        </View>

        <View style={{ width: '100%', paddingHorizontal: 15, paddingVertical: 10 }}>
          <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center' }} onPress={() => { this.changePriceRangeRadio(3) }}>
            <View style={{ width: 20, height: 20, borderRadius: 10, alignItems: 'center', justifyContent: 'center', borderColor: colors.colorBlue, borderWidth: 1 }}>
              <View style={{ width: 10, height: 10, borderRadius: 5, backgroundColor: radioValue == 3 ? colors.colorBlue : 'white' }} />
            </View>
            <View style={{ paddingHorizontal: 15, alignItems: 'flex-start' }}>
              <Text style={{ fontFamily: Fonts.medium, color: colors.colorBlack21, fontSize: 13 }}>{currencycode && currencycode + " "}2000 - {currencycode && currencycode + " "}3000</Text>
            </View>
          </TouchableOpacity>
        </View>

        <View style={{ width: '100%', paddingHorizontal: 15, paddingVertical: 10 }}>
          <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center' }} onPress={() => { this.changePriceRangeRadio(4) }}>
            <View style={{ width: 20, height: 20, borderRadius: 10, alignItems: 'center', justifyContent: 'center', borderColor: colors.colorBlue, borderWidth: 1 }}>
              <View style={{ width: 10, height: 10, borderRadius: 5, backgroundColor: radioValue == 4 ? colors.colorBlue : 'white' }} />
            </View>
            <View style={{ paddingHorizontal: 15, alignItems: 'flex-start' }}>
              <Text style={{ fontFamily: Fonts.medium, color: colors.colorBlack21, fontSize: 13 }}>{currencycode && currencycode + " "}3000 - {currencycode && currencycode + " "}4000</Text>
            </View>
          </TouchableOpacity>
        </View>

        <View style={{ width: '100%', paddingHorizontal: 15, paddingVertical: 10 }}>
          <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center' }} onPress={() => { this.changePriceRangeRadio(5) }}>
            <View style={{ width: 20, height: 20, borderRadius: 10, alignItems: 'center', justifyContent: 'center', borderColor: colors.colorBlue, borderWidth: 1 }}>
              <View style={{ width: 10, height: 10, borderRadius: 5, backgroundColor: radioValue == 5 ? colors.colorBlue : 'white' }} />
            </View>
            <View style={{ paddingHorizontal: 15, alignItems: 'flex-start' }}>
              <Text style={{ fontFamily: Fonts.medium, color: colors.colorBlack21, fontSize: 13 }}>{currencycode && currencycode + " "}4000 and above</Text>
            </View>
          </TouchableOpacity>
        </View>


        <View style={{ width: '100%', padding: 15, flexDirection: 'row' }}>
          <View style={{ width: '50%', paddingHorizontal: 15, alignItems: 'flex-start' }}>
            <Text style={{ fontFamily: Fonts.medium, color: colors.colorBlack21, fontSize: 13 }}>MIN: {currencycode && currencycode} {selectedMinPrice ? selectedMinPrice : initialLowValue}</Text>
          </View>
          <View style={{ width: '50%', paddingHorizontal: 15, alignItems: 'flex-end' }}>
            <Text style={{ fontFamily: Fonts.medium, color: colors.colorBlack21, fontSize: 13 }}>MAX: {currencycode && currencycode} {selectedMaxPrice ? selectedMaxPrice : initialHighValue}</Text>
          </View>
        </View>
        <RangeSlider
          ref={component => this._rangeSlider = component}
          style={{ width: Dimensions.get('window').width - 40, height: 60 }}
          gravity={'center'}
          min={min}
          //style={{ backgroundColor: 'red' }}
          max={max}
          step={1}
          textFormat={(currencycode && currencycode) + " %d"}
          thumbRadius={8}
          textSize={14}
          thumbBorderWidth={0}
          thumbColor={'#989898'}
          labelBackgroundColor={colors.colorBlue}
          labelBorderColor={colors.colorBlue}
          initialLowValue={initialLowValue}
          initialHighValue={initialHighValue}
          selectionColor={colors.colorBlue}
          blankColor={colors.colorGrey}
          lineWidth={4}
          labelFontSize={8}
          labelBorderRadius={10}
          onValueChanged={(low, high, fromUser) => {
            //alert(JSON.stringify(fromUser))
            this.selectedMinPrice = low
            this.selectedMaxPrice = high
            // this.setState({ selectedMinPrice: low, selectedMaxPrice: high })
          }}
          onTouchStart={()=>{
            this.setState({isScroll:false})
          }}
          onTouchEnd={() => {
            this.setState({ selectedMinPrice: this.selectedMinPrice, selectedMaxPrice: this.selectedMaxPrice,isScroll:true })
          }}
        />

      </View>)
  }

  changePriceRangeRadio(radioValue) {
    switch (radioValue) {
      case 1:
        this.setState({ selectedMinPrice: 0, selectedMaxPrice: 1000 }, () => {
          this._rangeSlider.setLowValue(0);
          this._rangeSlider.setHighValue(1000);
        });
        break;
      case 2:
        this.setState({ selectedMinPrice: 1000, selectedMaxPrice: 2000 }, () => {
          this._rangeSlider.setLowValue(1000);
          this._rangeSlider.setHighValue(2000);
        });
        break;
      case 3:
        this.setState({ selectedMinPrice: 2000, selectedMaxPrice: 3000 }, () => {
          this._rangeSlider.setLowValue(2000);
          this._rangeSlider.setHighValue(3000);
        });
        break;
      case 4:
        this.setState({ selectedMinPrice: 3000, selectedMaxPrice: 4000 }, () => {
          this._rangeSlider.setLowValue(3000);
          this._rangeSlider.setHighValue(4000);
        });
        break;
      case 5:
        this.setState({ selectedMinPrice: 4000, selectedMaxPrice: this.state.maxPrice }, () => {
          this._rangeSlider.setLowValue(4000);
          this._rangeSlider.setHighValue(this.state.maxPrice);
        });
        break;
    }
  }

  renderSortingFilter() {
    return (
      <View style={{ width: '100%', paddingHorizontal: 15, paddingVertical: 10 }}>
        <View style={{ flexDirection: "row", justifyContent: "space-evenly" }}>
          <TouchableOpacity
            underlayColor={"transparent"}
            onPress={() => {
              this.setState({
                sortingStatus: 1,
              })
            }}>
            <View style={{ alignItems: 'center' }}>
              <View style={{ padding: 5, width: 50, alignItems: 'center', justifyContent: 'center' }}>
                <Image style={{ width: 18, height: 18, tintColor: this.state.sortingStatus == 1 ? colors.colorBlue : "#626262" }} source={Images.icon_high} resizeMode={'contain'} />
              </View>
              <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                <Text style={{ fontFamily: Fonts.regular, textAlign: 'center', fontSize: 10, color: this.state.sortingStatus == 1 ? colors.colorBlue : "#626262" }}>{"Price\nLow To High"}</Text>
              </View>
            </View>
          </TouchableOpacity>
          <View style={{ width: 1, height: 40, backgroundColor: 'grey' }} />

          <TouchableOpacity
            underlayColor={"transparent"}
            onPress={() => {
              this.setState({
                sortingStatus: 2,
              })
            }}>
            <View style={{ alignItems: 'center' }}>
              <View style={{ padding: 5, width: 50, alignItems: 'center', justifyContent: 'center' }}>
                <Image style={{ width: 18, height: 18, tintColor: this.state.sortingStatus == 2 ? colors.colorBlue : "#626262" }} source={Images.icon_low} resizeMode={'contain'} />
              </View>
              <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                <Text style={{ fontFamily: Fonts.regular, textAlign: 'center', fontSize: 10, color: this.state.sortingStatus == 2 ? colors.colorBlue : "#626262" }}>{"Price\nHigh To Low"}</Text>
              </View>
            </View>
          </TouchableOpacity>
          <View style={{ width: 1, height: 40, backgroundColor: 'grey' }} />

          <TouchableOpacity
            underlayColor={"transparent"}
            onPress={() => {
              this.setState({
                sortingStatus: 3,
              })
            }}>
            <View style={{ alignItems: 'center' }}>
              <View style={{ padding: 5, width: 50, alignItems: 'center', justifyContent: 'center' }}>
                <Image style={{ width: 18, height: 18, tintColor: this.state.sortingStatus == 3 ? colors.colorBlue : "#626262" }} source={Images.icon_rating} resizeMode={'contain'} />
              </View>
              <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                <Text style={{ fontFamily: Fonts.regular, textAlign: 'center', fontSize: 10, color: this.state.sortingStatus == 3 ? colors.colorBlue : "#626262" }}>{"User\nRating"}</Text>
              </View>
            </View>
          </TouchableOpacity>
          <View style={{ width: 1, height: 40, backgroundColor: 'grey' }} />

          <TouchableOpacity
            underlayColor={"transparent"}
            onPress={() => {
              this.setState({
                sortingStatus: 4,
              })
            }}>
            <View style={{ alignItems: 'center' }}>
              <View style={{ padding: 5, width: 50, alignItems: 'center', justifyContent: 'center' }}>
                <Image style={{ width: 18, height: 18, tintColor: this.state.sortingStatus == 4 ? colors.colorBlue : "#626262" }} source={Images.star} resizeMode={'contain'} />
              </View>
              <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                <Text style={{ fontFamily: Fonts.regular, textAlign: 'center', fontSize: 10, color: this.state.sortingStatus == 4 ? colors.colorBlue : "#626262" }}>{"Hotel\nPopularity"}</Text>
              </View>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    )
  }


  render() {
    const { is1starSelected, is2starSelected, is3starSelected, is4starSelected, is5starSelected, selectedMinPrice, selectedMaxPrice, sortingStatus } = this.state;
    return (
      <>
        <SafeAreaView backgroundColor={'transparent'} />
        <View style={{ flex: 1, backgroundColor: 'white', marginTop: (Platform.OS === 'ios') ? 44 : 56 }} >
         
            <View style={{ height: 50, width: "100%", backgroundColor: colors.gray, flexDirection: "row", alignItems: "center", padding: 10, paddingHorizontal: 20 }}>
              <View style={{ width: '20%', justifyContent: 'center' }}>
                <TouchableOpacity
                  onPress={() => this.props.onFilterSelection(null)}>
                  <Image
                    style={{ height: 15, width: 15, tintColor: colors.colorBlue, resizeMode: 'contain' }}
                    source={Images.ic_image_close} />
                </TouchableOpacity>
              </View>
              <View style={{ width: '60%', justifyContent: 'center', alignItems: 'center' }}>
                <Text style={{ fontFamily: Fonts.medium, fontSize: 14, color: colors.colorBlack21 }}>Filters</Text>
              </View>
              <View style={{ width: '20%', justifyContent: 'center', alignItems: 'flex-end' }}>
                <TouchableOpacity
                  onPress={() => {
                    this.setState({
                      selectedMinPrice: 0,
                      selectedMaxPrice: this.state.maxPrice,
                      sortingStatus: 0,
                      is1starSelected: false,
                      is2starSelected: false,
                      is3starSelected: false,
                      is4starSelected: false,
                      is5starSelected: false,
                    }, () => {
                      this._rangeSlider.setLowValue(0);
                      this._rangeSlider.setHighValue(this.state.maxPrice);
                    })
                  }}>
                  <Text style={{ fontFamily: Fonts.medium, fontSize: 14, color: colors.colorBlue }}>Reset</Text>
                </TouchableOpacity>
              </View>
            </View>
            <ScrollView scrollEnabled={this.state.isScroll} style={{ flex: 1 }}>
            {/* Rating Filter */}
            <View style={{ width: "100%", backgroundColor: colors.gray, flexDirection: "row", alignItems: "center", padding: 10, paddingHorizontal: 20, marginHorizontal: 3 }}>
              <Text style={{ fontFamily: Fonts.medium, fontSize: 12, color: colors.colorBlack21 }}>Rating</Text>
            </View>

            {
              <View style={{ marginTop: 20, flexDirection: "row", justifyContent: "space-evenly", marginBottom: 20 }}>


                <TouchableOpacity
                  onPress={() => this.setState({ is1starSelected: !this.state.is1starSelected })}>
                  <View style={{ height: 50, alignItems: 'center', width: 50, borderWidth: 0.8, borderColor: this.state.is1starSelected ? colors.colorBlue : colors.colorBlack62 }}>
                    <View style={{ height: 24, padding: 5, width: 50, alignItems: 'center', justifyContent: 'center' }}>
                      <Image style={{ width: 15, height: 15, tintColor: this.state.is1starSelected ? colors.colorBlue : colors.colorBlack62 }} source={this.state.is1starSelected ? Images.star : Images.icon_holo_star} resizeMode={'contain'} />
                    </View>
                    <View style={{ height: 1, backgroundColor: this.state.is1starSelected ? colors.colorBlue : colors.colorBlack62, width: 40, }}>

                    </View>
                    <View style={{ height: 25, width: 50, alignItems: 'center', justifyContent: 'center' }}>
                      <Text style={{ fontFamily: Fonts.regular, fontSize: 11, color: this.state.is1starSelected ? colors.colorBlue : colors.colorBlack62 }}>{"1 Star"}</Text>
                    </View>
                  </View>


                </TouchableOpacity>

                <TouchableOpacity
                  onPress={() => this.setState({ is2starSelected: !this.state.is2starSelected })}>
                  <View style={{ height: 50, alignItems: 'center', width: 50, borderWidth: 0.8, borderColor: this.state.is2starSelected ? colors.colorBlue : colors.colorBlack62 }}>
                    <View style={{ height: 24, padding: 5, width: 50, alignItems: 'center', justifyContent: 'center' }}>
                      <Image style={{ width: 15, height: 15, tintColor: this.state.is2starSelected ? colors.colorBlue : colors.colorBlack62 }} source={this.state.is2starSelected ? Images.star : Images.icon_holo_star} resizeMode={'contain'} />
                    </View>
                    <View style={{ height: 1, backgroundColor: this.state.is2starSelected ? colors.colorBlue : colors.colorBlack62, width: 40, }}>

                    </View>
                    <View style={{ height: 25, width: 50, alignItems: 'center', justifyContent: 'center' }}>
                      <Text style={{ fontFamily: Fonts.regular, fontSize: 11, color: this.state.is2starSelected ? colors.colorBlue : colors.colorBlack62 }}>{"2 Star"}</Text>
                    </View>
                  </View>


                </TouchableOpacity>

                <TouchableOpacity
                  onPress={() => this.setState({ is3starSelected: !this.state.is3starSelected })}
                  underlayColor={"transparent"}>
                  <View style={{ height: 50, alignItems: 'center', width: 50, borderWidth: 0.8, borderColor: this.state.is3starSelected ? colors.colorBlue : colors.colorBlack62, }}>
                    <View style={{ height: 24, padding: 5, width: 50, alignItems: 'center', justifyContent: 'center' }}>
                      <Image style={{ width: 15, height: 15, tintColor: this.state.is3starSelected ? colors.colorBlue : colors.colorBlack62 }} source={this.state.is3starSelected ? Images.star : Images.icon_holo_star} resizeMode={'contain'} />
                    </View>
                    <View style={{ height: 1, backgroundColor: this.state.is3starSelected ? colors.colorBlue : colors.colorBlack62, width: 40, }}>

                    </View>
                    <View style={{ height: 25, width: 50, alignItems: 'center', justifyContent: 'center' }}>
                      <Text style={{ fontFamily: Fonts.regular, fontSize: 11, color: this.state.is3starSelected ? colors.colorBlue : colors.colorBlack62 }}>{"3 Star"}</Text>
                    </View>
                  </View>
                </TouchableOpacity>

                <TouchableOpacity
                  onPress={() => this.setState({ is4starSelected: !this.state.is4starSelected })}
                  underlayColor={"transparent"}>
                  <View style={{ height: 50, alignItems: 'center', width: 50, borderWidth: 0.8, borderColor: this.state.is4starSelected ? colors.colorBlue : colors.colorBlack62, }}>
                    <View style={{ height: 24, padding: 5, width: 50, alignItems: 'center', justifyContent: 'center' }}>
                      <Image style={{ width: 15, height: 15, tintColor: this.state.is4starSelected ? colors.colorBlue : colors.colorBlack62 }} source={this.state.is4starSelected ? Images.star : Images.icon_holo_star} resizeMode={'contain'} />
                    </View>
                    <View style={{ height: 1, backgroundColor: this.state.is4starSelected ? colors.colorBlue : colors.colorBlack62, width: 40, }}>

                    </View>
                    <View style={{ height: 25, width: 50, alignItems: 'center', justifyContent: 'center' }}>
                      <Text style={{ fontFamily: Fonts.regular, fontSize: 11, color: this.state.is4starSelected ? colors.colorBlue : colors.colorBlack62 }}>{"4 Star"}</Text>
                    </View>
                  </View>
                </TouchableOpacity>

                <TouchableOpacity
                  onPress={() => this.setState({ is5starSelected: !this.state.is5starSelected })}
                  underlayColor={"transparent"}>
                  <View style={{ height: 50, alignItems: 'center', width: 50, borderWidth: 0.8, borderColor: this.state.is5starSelected ? colors.colorBlue : colors.colorBlack62, }}>
                    <View style={{ height: 24, padding: 5, width: 50, alignItems: 'center', justifyContent: 'center' }}>
                      <Image style={{ width: 15, height: 15, tintColor: this.state.is5starSelected ? colors.colorBlue : colors.colorBlack62 }} source={this.state.is5starSelected ? Images.star : Images.icon_holo_star} resizeMode={'contain'} />
                    </View>
                    <View style={{ height: 1, backgroundColor: this.state.is5starSelected ? colors.colorBlue : colors.colorBlack62, width: 40, }}>

                    </View>
                    <View style={{ height: 25, width: 50, alignItems: 'center', justifyContent: 'center' }}>
                      <Text style={{ fontFamily: Fonts.regular, fontSize: 11, color: this.state.is5starSelected ? colors.colorBlue : colors.colorBlack62 }}>{"5 Star"}</Text>
                    </View>
                  </View>
                </TouchableOpacity>
              </View>
            }

            {/* Price Range Filter */}

            <View style={{ width: "100%", backgroundColor: colors.gray, flexDirection: "row", alignItems: "center", padding: 10, paddingHorizontal: 20, marginHorizontal: 3 }}>
              <Text style={{ fontFamily: Fonts.medium, fontSize: 12, color: colors.colorBlack21 }}>Price Range</Text>
            </View>
            {
              this.renderRangeSlider()
            }

            {/* Sorting Filter */}
            <View style={{ width: "100%", backgroundColor: colors.gray, flexDirection: "row", alignItems: "center", padding: 10, paddingHorizontal: 20, marginHorizontal: 3 }}>
              <Text style={{ fontFamily: Fonts.medium, fontSize: 12, color: colors.colorBlack21 }}>Sort by</Text>
            </View>
            {
              this.renderSortingFilter()
            }

          </ScrollView>
        </View>

        <View style={{width:'100%',backgroundColor:'white'}}>

        <TouchableOpacity
          onPress={
            () => this.props.onFilterSelection({
              is1starSelected: is1starSelected,
              is2starSelected: is2starSelected,
              is3starSelected: is3starSelected,
              is4starSelected: is4starSelected,
              is5starSelected: is5starSelected,
              selectedMinPrice: selectedMinPrice,
              selectedMaxPrice: selectedMaxPrice,
              sortingStatus: sortingStatus
            })
          }
          style={{ width: '100%', alignItems: 'center', justifyContent: 'center', backgroundColor: colors.colorBlue, padding: 10 }}>
          <Text style={{ fontSize: 15, fontFamily: Fonts.semiBold, color: colors.colorWhite }}>Apply Filters</Text>
        </TouchableOpacity>
        </View>
        <SafeAreaView backgroundColor={colors.colorBlue} />
      </>
    );
  }
}



