import { StyleSheet } from "react-native";
import { colors } from "../../../Utils/Colors";

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "rgba(244,248,254,1)",
  },
  itemContainer: {
    padding: 15,
    backgroundColor: 'white',
  }, 
  imageContainer: {
    width: 90,
    justifyContent: 'center',
    alignItems: 'center'
  }, confirmButtonStyle: {
    height: 50,
    width: "90%",
    backgroundColor: colors.colorBlue,
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 7,
    bottom: 15
  }, bookingIdContainer: {
    padding: 15,
    margin: 10,
    backgroundColor: colors.colorBlue,
    borderRadius: 10,
    justifyContent: "center",
    alignItems: "center",
    shadowColor: "black",
    shadowOpacity: 0.3,
    shadowOffset: { width: 1, height: 2 },
    shadowRadius: 3,
    elevation: 6,
  },
  dotView: {
    width: '40%',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  leftDot: {
    backgroundColor: colors.colorBlue,
    borderRadius: 5,
    position: 'absolute',
    left: -1,
    height: 8,
    width: 8,
  },
  rightDot: {
    backgroundColor: colors.colorBlue,
    borderRadius: 5,
    position: 'absolute',
    right: -1,
    height: 8,
    width: 8,
  },
  estimate: {
    fontFamily: Fonts.semiBold,
    color: colors.colorBlack21,
    fontSize: 14,
  },
  dashStyle: {
    width: '100%',
    height: 0.5,
    position: 'absolute',
    left: 0,
  },
  checkIn: {
    fontFamily: Fonts.regular,
    fontSize: 12,
    lineHeight: 17,
    color: colors.colorBlack21,
  },
  textAreaContainer: {
    borderColor: colors.colorGrey,
    borderWidth: 0.8,
    marginHorizontal:8,
    borderRadius:7,
    paddingHorizontal:3
  },
  textArea: {
    height: 80,
    justifyContent: "flex-start",
    textAlignVertical: "top",
    fontSize:11
  }

});