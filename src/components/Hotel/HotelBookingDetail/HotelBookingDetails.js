import moment from "moment";
import React, { Component } from "react";
import { DeviceEventEmitter, Image, Modal, SafeAreaView, ScrollView, Text, TextInput, TouchableHighlight, TouchableOpacity, View } from 'react-native';
import CardView from 'react-native-cardview';
import Dash from "react-native-dash";
import { connect } from "react-redux";
import { cancelHotelBookingAction, getHotelBookingDetailsAction } from "../../../Redux/Actions";
import { colors } from "../../../Utils/Colors";
import Fonts from "../../../Utils/Fonts";
import Images from '../../../Utils/Images';
import { MyAlert } from '../../../Utils/Utility';
import StringConstants from "../../../Utils/StringConstants";
import CommonHeader from "../../custom/CommonHeader";
import RatingBar from "../../custom/RatingBar";
import { styles } from "./Style";
import MyLoader from '../../custom/MyLoader'
import NavigationServices from '../../../Utils/NavigationServices';

class HotelBookingDetails extends Component {
  constructor(props) {
    super(props)
    this.state = {
      id: null,
      pdfLink: "",
      BookingDetails: null,
      hotelAddress: 'PD-13 Bramhpuri Rd, Shankar Nagar, Krishna Colony, Jaipur, Rajasthan 302002',
      adults: null,
      children: null,
      hotel_address_line_1: '',
      hotel_address_line_2: '',
      cancellationModal: false,
      isCOP: false,
      isODP: false,
      isSCBM: false,
      isCBEPD: false,
      isDF: false,
      isGC: false,
      reason: '',
      isCancellable: false
    }

    this.updateHotelBookingDetailsFromApi = this.updateHotelBookingDetailsFromApi.bind(this)
    this.updateCancelStatus = this.updateCancelStatus.bind(this)
  }
  getBookingData(id) {
    this.props.getHotelBookingDetailsAction(id)

  }

  componentDidMount() {
    DeviceEventEmitter.addListener(StringConstants.HOTEL_BOOKING_DETAILS_EVENT, this.updateHotelBookingDetailsFromApi)
    DeviceEventEmitter.addListener(StringConstants.CANCEL_HOTEL_BOOKING_EVENT, this.updateCancelStatus)
    const id = this.props.navigation.getParam('id', null);
    this.setState({ id: id })
    this.getBookingData(id)

  }

  componentWillUnmount() {
    DeviceEventEmitter.removeListener(StringConstants.HOTEL_BOOKING_DETAILS_EVENT, this.updateHotelBookingDetailsFromApi)
    DeviceEventEmitter.removeListener(StringConstants.CANCEL_HOTEL_BOOKING_EVENT, this.updateCancelStatus)

  }

  updateHotelBookingDetailsFromApi(data) {
    // console.log("HOTEL_BOOKING_DETAIL_RES:", JSON.stringify(data))
    if (data) {
      //console.log("HOTEL_BOOKING_DETAIL_RES:", JSON.stringify(data))
      let starRating = 2
      //let cancellation = null
      try {
        const res_json = JSON.parse(JSON.stringify(JSON.parse(data.res_json).GetBookingDetailResult))
        starRating = res_json.StarRating;
        console.log("HOTEL_BOOKING_DETAIL_RES:", JSON.stringify(data))
        console.log("HOTEL_BOOKING_DETAIL_RES_JSON:", JSON.stringify(res_json))
      }
      catch (e) {
        // const starRating = 2
      }
      delete data['res_json']

      // if (data.cancellation && data.cancellation.res_json) {
      //   const cancelResJson = JSON.parse(data.cancellation.res_json)
      //   console.log("cancelResJson", JSON.stringify(cancelResJson))
      // }

      const checkInDate = moment(data.check_in).format("YYYY-MM-DD")
      const todayDate = moment(new Date()).format("YYYY-MM-DD")
      const status = data.status
      const cancellation = data.cancellation
      let isCancellable = false
      if (!(checkInDate < todayDate) && status == 1 && !cancellation) {
        isCancellable = true
      }





      let children = []
      let adults = data.guests.filter((item) => {
        if (item.guest_type == 1) return true
        if (item.guest_type == 2) {
          children.push(item)
          return false;
        }
        return false;
      })


      let roomTypeArrays = [],
        currentType = "",
        currIndex = 0
      data.rooms.forEach(element => {
        if (currentType != element.room_type_code) {
          let a = [];
          a.push(element)
          roomTypeArrays[currIndex] = a
          currIndex++
        }
        else {
          roomTypeArrays[currIndex - 1].push(element)
        }
        currentType = element.room_type_code

      });
      console.log("roomTypeArrays", JSON.stringify(roomTypeArrays))
      // let adults = data.guests.filter((item) => {
      //   if (item.guest_type == 1) return true
      //   if (item.guest_type == 2) {
      //     children.push(item)
      //     return false;
      //   }
      //   return false;
      // })

      this.setState({
        BookingDetails: data,
        children: children.length > 0 ? children : null,
        adults: adults.length > 0 ? adults : null,
        roomTypeArrays: roomTypeArrays,
        starRating: starRating,
        isCancellable: isCancellable
      })
    }
  }

  updateCancelStatus(response) {
    console.log("Response:", JSON.stringify(response))
    if (response.message && response.status) {
      console.log("Status:", JSON.stringify(response.status))
      if (response.status != 4) {
        console.log("Message:", JSON.stringify(response.message))
        this.setState({ isCancellable: false, cancellationModal: false }, () => {
          setTimeout(() => {
            console.log("TimeOutMessage:", JSON.stringify(response.message))
            MyAlert("Success", response.message)

          }, 200);
        })
      }
    }
  }

  _renderPriceDetail() {
    const currency = this.state.BookingDetails.amounts[0].currency
    const grand_total = Math.ceil(this.state.BookingDetails.amounts[0].grand_total)
    return (
      <View style={{ width: '100%', overflow: 'hidden', flexDirection: 'row', padding: 15, }}>
        <View style={{ width: '30%', alignItems: 'flex-start', justifyContent: 'flex-start' }} >
          <Text style={{ fontFamily: Fonts.medium, fontSize: 13, lineHeight: 20, color: colors.colorBlack }}>Amount</Text>
        </View>
        <View style={{ width: '70%' }}>
          <Text style={{ fontFamily: Fonts.medium, fontSize: 11, color: colors.colorBlack21, lineHeight: 20, textAlign: 'left', flex: 1 }}>{currency} {grand_total}</Text>
        </View>
      </View>
    )
  }

  _renderCancellationStatus() {
    const status = parseInt(this.state.BookingDetails.cancellation.status)
    // let retColor = null
    let st
    switch (status) {
      case 3:
        st = "Cancelled"
        break
      case 4:
        st = "Failed"
      default:
        st = "Cancellation Pending"

    }
    return (
      <View style={{ width: '100%', overflow: 'hidden', flexDirection: 'row', padding: 15, }}>
        <View style={{ width: '30%', alignItems: 'flex-start', justifyContent: 'flex-start' }} >
          <Text style={{ fontFamily: Fonts.medium, fontSize: 13, lineHeight: 20, color: colors.colorBlack }}>Status</Text>
        </View>
        <View style={{ width: '70%' }}>
          <Text style={{ fontFamily: Fonts.medium, fontSize: 11, color: colors.colorBlack21, lineHeight: 20, textAlign: 'left', flex: 1 }}>{st}</Text>
        </View>
      </View>
    )
  }

  _renderConfirmButton() {
    return (
      <TouchableOpacity
        style={styles.confirmButtonStyle}
        underlayColor={"transparent"}
        onPress={() => {
          // this.validateEntryFields()
        }}>
        <Text
          style={{ color: 'white' }}>
          Continue
                     </Text>
      </TouchableOpacity>
    )
  }

  _renderCheckInItem() {
    //  const checkIn = dateFormat(this.state.BookingDetails.check_in, "ddd, dd mmm yyyy")
    //  const checkOut = dateFormat(this.state.BookingDetails.check_out, "ddd, dd mmm yyyy")
    const checkIn = moment(this.state.BookingDetails.check_in).format("DD MMM, YYYY")
    const checkInDay = moment(this.state.BookingDetails.check_in).format("dddd")
    const checkOut = moment(this.state.BookingDetails.check_out).format("DD MMM, YYYY")
    const checkOutDay = moment(this.state.BookingDetails.check_out).format("dddd")
    return (
      <View style={{ width: '100%', flexDirection: 'row', padding: 15 }}>
        <View style={{ width: '30%' }}>
          <Text style={styles.checkIn}>Check In</Text>
          <Text style={styles.estimate}>{checkIn}</Text>
          <Text style={styles.checkIn}>{checkInDay}</Text>
        </View>
        <View style={styles.dotView}>
          <View style={styles.leftDot} />
          <View style={styles.rightDot} />
          <Dash
            style={styles.dashStyle}
            dashColor={colors.colorBlue}
            dashLength={2}
            dashThickness={1}
          />
          <View style={{ alignItems: 'center', justifyContent: 'center', backgroundColor: 'white', }}>
            <Image source={Images.ic_hotel_icon} tintColor={colors.colorBlue} style={{ width: 17, height: 17, resizeMode: 'contain', tintColor: 'colors.colorBlue' }} />
          </View>
        </View>
        <View style={{ width: '30%', alignItems: 'flex-end' }}>
          <Text style={styles.checkIn}>Check Out</Text>
          <Text style={[styles.estimate, { textAlign: 'right' }]}>{checkOut}</Text>
          <Text style={styles.checkIn}>{checkOutDay}</Text>
        </View>
      </View>
    )
  }
  _renderRoomItem() {
    const { roomTypeArrays } = this.state
    return (
      <View style={{ width: '100%', overflow: 'hidden', flexDirection: 'row', padding: 15, }}>
        <View style={{ width: '30%', alignItems: 'flex-start', justifyContent: 'flex-start' }} >
          <Text style={{ fontFamily: Fonts.medium, fontSize: 13, lineHeight: 20, color: colors.colorBlack }}>Rooms</Text>
        </View>
        <View style={{ width: '70%' }}>
          {roomTypeArrays.map((item) => {
            return <Text style={{ fontFamily: Fonts.medium, fontSize: 13, color: colors.colorBlack62, lineHeight: 20 }}>{item[0].room_type} <Text style={{ fontFamily: Fonts.semiBold, fontSize: 14, color: colors.colorBlack62, lineHeight: 20 }}>({item.length})</Text></Text>
          })}
        </View>
      </View>
    )
  }

  _renderRoomItem2() {
    return (
      <View style={styles.itemContainer}>
        <Text style={{ fontSize: 15, fontWeight: '600', color: 'rgb(76,76,76)' }}>
          {/* {this.state.BookingDetails.name} */}
          {/* {room name required from api end} */}
          Room name
            </Text>

        {/* {Object.keys(this.state.roomDetails.amenities).length>0 ? <Text style={{color:'rgb(76,76,76)',fontSize:12,marginTop:10}}>{this.state.roomDetails.amenities[0]}</Text> : <Text>{""}</Text>} */}

        <Text style={{ marginTop: 5, color: 'rgb(76,76,76)', fontWeight: '500' }}>
          {this.state.BookingDetails.room} Room   {Object.keys(this.state.BookingDetails.guests).length} Guest
            </Text>
      </View>
    )
  }
  _renderHotelItem() {
    const { BookingDetails } = this.state
    return (
      <View style={styles.itemContainer}>
        <View style={{ flexDirection: 'row' }}>

          <View style={{ flex: 1, marginLeft: 0 }}>
            <Text style={{ fontSize: 14, fontFamily: Fonts.semiBold, color: colors.colorBlack21 }}>
              {BookingDetails.hotel_name}
            </Text>
            <View style={{ width: 60, marginVertical: 5 }}>
              {this.state.starRating && <RatingBar count={this.state.starRating} starSize={12} emptyStarColor={colors.colorBlue} fullStarColor={colors.colorBlue} />}
            </View>
            <Text style={{ color: colors.colorBlack62, fontSize: 10, lineHeight: 16, fontFamily: Fonts.medium }}>{BookingDetails.hotel_address_line_1} </Text>
            <Text style={{ color: colors.colorBlack62, fontSize: 10, lineHeight: 16, fontFamily: Fonts.medium }}>{BookingDetails.hotel_address_line_2} </Text>
          </View>
        </View>
      </View>
    )
  }
  _renderBookingItem() {
    return (
      <View style={styles.bookingIdContainer}>
        <Text style={{ color: "white", fontWeight: "600" }}>
          BOOKING ID
                </Text>
        <Text style={{ color: "white", marginTop: 5 }}>
          {this.state.BookingDetails.booking_id}
        </Text>
      </View>
    )
  }

  _renderEmailDetails() {
    return (
      <View style={{ width: '100%', overflow: 'hidden', flexDirection: 'row', padding: 15, }}>
        <View style={{ width: '30%', alignItems: 'flex-start', justifyContent: 'flex-start' }} >
          <Text style={{ fontFamily: Fonts.medium, fontSize: 13, lineHeight: 20, color: colors.colorBlack }}>Email</Text>
        </View>
        <View style={{ width: '70%' }}>
          <Text style={{ fontFamily: Fonts.medium, fontSize: 11, color: colors.colorBlack21, lineHeight: 20, textAlign: 'left', flex: 1 }}>{this.state.BookingDetails.email}</Text>
        </View>
      </View>
    )
  }

  _renderBookingId() {
    return (
      <View style={{ width: '100%', overflow: 'hidden', flexDirection: 'row', padding: 15, }}>
        <View style={{ width: '30%', alignItems: 'flex-start', justifyContent: 'flex-start' }} >
          <Text style={{ fontFamily: Fonts.medium, fontSize: 13, lineHeight: 20, color: colors.colorBlack }}>Booking ID</Text>
        </View>
        <View style={{ width: '70%' }}>
          <Text style={{ fontFamily: Fonts.medium, fontSize: 13, color: colors.colorBlack21, lineHeight: 20 }}>{this.state.BookingDetails.booking_id}</Text>
        </View>
      </View>
    )
  }

  _renderPhoneDetails() {
    return (
      <View style={{ width: '100%', overflow: 'hidden', flexDirection: 'row', padding: 15, }}>
        <View style={{ width: '30%', alignItems: 'flex-start', justifyContent: 'flex-start' }} >
          <Text style={{ fontFamily: Fonts.medium, fontSize: 13, lineHeight: 20, color: colors.colorBlack }}>Phone</Text>
        </View>
        <View style={{ width: '70%' }}>
          <Text style={{ fontFamily: Fonts.medium, fontSize: 13, color: colors.colorBlack21, lineHeight: 20 }}>{this.state.BookingDetails.mobile}</Text>
        </View>
      </View>
    )
  }

  _renderDownloadTicket() {
    return (
      <View style={{ width: '100%', overflow: 'hidden', flexDirection: 'row', padding: 15, }}>
        <View style={{ width: '30%', alignItems: 'flex-start', justifyContent: 'flex-start' }} >
          <Text style={{ fontFamily: Fonts.medium, fontSize: 13, lineHeight: 20, color: colors.colorBlack }}>Ticket</Text>
        </View>
        <View style={{ width: '70%' }}>
          <TouchableOpacity
            onPress={() => {
              NavigationServices.navigate("ViewTicket", { bookingId: this.state.id, isHotel: true })
            }} >
            <Text style={{ fontFamily: Fonts.medium, fontSize: 13, color: colors.colorBlack21, lineHeight: 20 }}>Click here to view ticket</Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }

  renderGuestDetail() {
    const { adults, children } = this.state
    return (
      <View style={{ width: '100%', overflow: 'hidden', flexDirection: 'row', padding: 15, }}>
        <View style={{ width: '30%', alignItems: 'flex-start', justifyContent: 'flex-start' }} >
          <Text style={{ fontFamily: Fonts.medium, fontSize: 13, lineHeight: 20, color: colors.colorBlack }}>Guests</Text>
        </View>
        <View style={{ width: '70%', }}>
          {adults && adults.length > 0 && <>
            <Text style={{ fontFamily: Fonts.medium, fontSize: 13, color: colors.colorBlack21, lineHeight: 20 }}>Adults ({adults.length})</Text>
            {adults.map((item, index) => {
              return (
                <View style={{ flexDirection: 'row', width: '100%', marginTop: 5, alignItems: 'center' }}>
                  <Image style={{ height: 10, width: 10, resizeMode: 'contain' }} source={Images.ic_user_black} />
                  <Text style={{ fontFamily: Fonts.medium, paddingLeft: 5, fontSize: 12, color: colors.colorBlack62, lineHeight: 18 }}>{item.title + " " + item.first_name + " " + item.last_name}</Text>
                </View>
              )
            })}</>}
          {children && children.length > 0 && <>
            <Text style={{ fontFamily: Fonts.medium, fontSize: 13, color: colors.colorBlack21, lineHeight: 20, marginTop: 10 }}>Children ({children.length})</Text>
            {children.map((item, index) => {
              return (
                <View style={{ flexDirection: 'row', width: '100%', marginTop: 5, alignItems: 'center' }}>
                  <Image style={{ height: 10, width: 10, resizeMode: 'contain' }} source={Images.ic_user_black} />
                  <Text style={{ fontFamily: Fonts.medium, paddingLeft: 5, fontSize: 12, color: colors.colorBlack62, lineHeight: 18 }}>{item.title + " " + item.first_name + " " + item.last_name}</Text>
                </View>
              )
            })}</>}
        </View>
      </View>
    )
  }

  cancellationPolicyItem() {

    return (
      <CardView
        style={{
          backgroundColor: colors.colorWhite,
          marginHorizontal: 10,
          marginVertical: 5
        }}
        cardElevation={2}
        cardMaxElevation={3}
        cornerRadius={8}>
        <View style={{ borderRadius: 8, overflow: 'hidden' }}>

          <TouchableOpacity
            onPress={() => {
              const Policy = this.state.BookingDetails.messages[0].message.replace(/[^a-zA-Z0-9\%,\-.:^ ]/g, '').replace(/[\^]/g, '. ');
              alert(Policy)
            }} style={{ width: '100%', overflow: 'hidden', borderRadius: 10, }}>
            <View style={{ width: '100%', height: 45, flexDirection: 'row', padding: 10 }}>
              <View style={{ width: '50%', alignItems: 'center', flexDirection: 'row', height: '100%' }}>
                <View style={{ width: 25, height: 25, borderRadius: 15, backgroundColor: colors.colorBlue, alignItems: 'center', justifyContent: 'center' }}>
                  <Image source={Images.ic_image_cancellation} style={{ width: 25, height: 25, resizeMode: 'cover' }} />
                </View>
                <Text style={{ fontFamily: Fonts.medium, fontSize: 13, color: colors.colorBlack, paddingLeft: 7 }}>
                  Cancellation Policy
              </Text>

              </View>
              <View style={{ width: '50%', alignItems: 'flex-end', justifyContent: 'center' }}>

                <Text style={{ fontFamily: Fonts.bold, fontSize: 15, color: colors.colorBlack, }}>

                </Text>

              </View>
            </View>
          </TouchableOpacity>
        </View></CardView>

    )
  }

  renderCancelBookingButton() {
    const checkInDate = moment(this.state.BookingDetails.check_in).format("YYYY-MM-DD")
    const todayDate = moment(new Date()).format("YYYY-MM-DD")
    const status = this.state.BookingDetails.status
    const cancellation = this.state.BookingDetails.cancellation
    if (this.state.isCancellable)
      return (
        <View style={{ width: "100%", alignItems: 'center' }}>
          <TouchableOpacity onPress={() => this.setState({ cancellationModal: true })}
            style={{ marginTop: 15, marginBottom: 25, backgroundColor: colors.colorBlue, color: colors.colorWhite, paddingHorizontal: 15, paddingVertical: 10, borderRadius: 7 }}>
            <Text style={{ fontFamily: Fonts.medium, color: colors.colorWhite, fontSize: 12 }}>Cancel Booking</Text>
          </TouchableOpacity>
        </View>)

  }

  cancelBooking() {
    const booking_id = this.state.BookingDetails.booking_id
    const { isCOP, isODP, isSCBM, isCBEPD, isDF, isGC } = this.state
    let reason = ''
    if (isCOP) {
      reason = reason + "Change of Plan, "
    }
    if (isODP) {
      reason = reason + "Other Destination Preferred, "
    }
    if (isSCBM) {
      reason = reason + "Schedule Change, "
    }
    if (isCBEPD) {
      reason = reason + "Change of Event Place, "
    }
    if (isDF) {
      reason = reason + "Death in Family, "
    }
    if (isGC) {
      reason = reason + "Group Cancelled, "
    }

    reason = reason + this.state.reason

    this.props.cancelHotelBooking({
      "booking_id": booking_id,
      "reason": reason
    })

  }

  renderCancellationModal() {
    if (this.state.isCancellable && this.state.cancellationModal)
      return (
        <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.cancellationModal}
          onRequestClose={() => { this.setState({ cancellationModal: false }) }}>
          <TouchableHighlight
            underlayColor={"rgba(0,0,0,0.5)"}
            activeOpacity={0.9}
            onPress={() => this.setState({ cancellationModal: false })} style={{ backgroundColor: "rgba(0,0,0,0.5)", justifyContent: "center", flex: 1, alignItems: "center" }}>

            <TouchableHighlight onPress={() => { }}
              underlayColor={colors.colorWhite}
              activeOpacity={1}
              style={{ width: "90%", backgroundColor: colors.colorWhite, borderRadius: 15, }}>
              <View>
                <View style={{ flexDirection: 'row', alignItems: 'center', height: 40, width: '100%', paddingTop: 10, justifyContent: 'center' }}>
                  {/* <View style={{ width: '20%', alignItems: 'center', justifyContent: 'center' }}>
                    <TouchableOpacity onPress={() => this.setState({ cancellationModal: false })}>
                      <Image tintColor={colors.colorBlue} style={{ height: 15, width: 15 }} source={Images.ic_image_back} resizeMode={'contain'} />
                    </TouchableOpacity>
                  </View> */}
                  <View style={{ width: '100%', justifyContent: "center", alignItems: 'center' }}>
                    <Text
                      style={{ fontFamily: Fonts.bold, color: colors.colorBlue, fontSize: 14, }}
                    >Reason For Cancellation</Text>
                  </View>
                </View>
                <View style={{ padding: 15 }}>

                  <TouchableOpacity onPress={() => this.setState({ isCOP: !this.state.isCOP, password: '', phone: '', })} style={{ alignItems: 'center', flexDirection: 'row', alignSelf: 'baseline', width: '100%', marginBottom: 10 }}>
                    <Text style={{ fontFamily: Fonts.medium, color: colors.colorBlack62, fontSize: 12, marginLeft: 8 }}>Change Of Plan</Text>
                    <View style={{ alignItems: 'flex-end', flex: 1 }}>
                      <View style={{ width: 17, height: 17, backgroundColor: this.state.isCOP ? colors.colorBlue : 'white', borderRadius: 5, borderColor: this.state.isCOP ? colors.colorBlue : colors.colorBlack62, alignItems: 'center', justifyContent: 'center', borderWidth: 1 }}>
                        {this.state.isCOP && <Image resizeMode={'contain'} style={{ width: 17, height: 17, zIndex: 2 }} source={Images.ic_image_checked} />}
                      </View>
                    </View>
                  </TouchableOpacity>

                  <TouchableOpacity onPress={() => this.setState({ isODP: !this.state.isODP, password: '', phone: '', })} style={{ alignItems: 'center', flexDirection: 'row', alignSelf: 'baseline', width: '100%', marginBottom: 10 }}>
                    <Text style={{ fontFamily: Fonts.medium, color: colors.colorBlack62, fontSize: 12, marginLeft: 8 }}>Other Destination Preferred</Text>
                    <View style={{ alignItems: 'flex-end', flex: 1 }}>
                      <View style={{ width: 17, height: 17, backgroundColor: this.state.isODP ? colors.colorBlue : 'white', borderRadius: 5, borderColor: this.state.isODP ? colors.colorBlue : colors.colorBlack62, alignItems: 'center', justifyContent: 'center', borderWidth: 1 }}>
                        {this.state.isODP && <Image resizeMode={'contain'} style={{ width: 17, height: 17, zIndex: 2 }} source={Images.ic_image_checked} />}
                      </View>
                    </View>
                  </TouchableOpacity>

                  <TouchableOpacity onPress={() => this.setState({ isSCBM: !this.state.isSCBM, password: '', phone: '', })} style={{ alignItems: 'center', flexDirection: 'row', alignSelf: 'baseline', width: '100%', marginBottom: 10 }}>
                    <Text style={{ fontFamily: Fonts.medium, color: colors.colorBlack62, fontSize: 12, marginLeft: 8 }}>Schedule Change</Text>
                    <View style={{ alignItems: 'flex-end', flex: 1 }}>
                      <View style={{ width: 17, height: 17, backgroundColor: this.state.isSCBM ? colors.colorBlue : 'white', borderRadius: 5, borderColor: this.state.isSCBM ? colors.colorBlue : colors.colorBlack62, alignItems: 'center', justifyContent: 'center', borderWidth: 1 }}>
                        {this.state.isSCBM && <Image resizeMode={'contain'} style={{ width: 17, height: 17, zIndex: 2 }} source={Images.ic_image_checked} />}
                      </View>
                    </View>
                  </TouchableOpacity>

                  <TouchableOpacity onPress={() => this.setState({ isCBEPD: !this.state.isCBEPD, password: '', phone: '', })} style={{ alignItems: 'center', flexDirection: 'row', alignSelf: 'baseline', width: '100%', marginBottom: 10 }}>
                    <Text style={{ fontFamily: Fonts.medium, color: colors.colorBlack62, fontSize: 12, marginLeft: 8 }}>Change of Event Place</Text>
                    <View style={{ alignItems: 'flex-end', flex: 1 }}>
                      <View style={{ width: 17, height: 17, backgroundColor: this.state.isCBEPD ? colors.colorBlue : 'white', borderRadius: 5, borderColor: this.state.isCBEPD ? colors.colorBlue : colors.colorBlack62, alignItems: 'center', justifyContent: 'center', borderWidth: 1 }}>
                        {this.state.isCBEPD && <Image resizeMode={'contain'} style={{ width: 17, height: 17, zIndex: 2 }} source={Images.ic_image_checked} />}
                      </View>
                    </View>
                  </TouchableOpacity>

                  <TouchableOpacity onPress={() => this.setState({ isDF: !this.state.isDF, password: '', phone: '', })} style={{ alignItems: 'center', flexDirection: 'row', alignSelf: 'baseline', width: '100%', marginBottom: 10 }}>
                    <Text style={{ fontFamily: Fonts.medium, color: colors.colorBlack62, fontSize: 12, marginLeft: 8 }}>Death in Family</Text>
                    <View style={{ alignItems: 'flex-end', flex: 1 }}>
                      <View style={{ width: 17, height: 17, backgroundColor: this.state.isDF ? colors.colorBlue : 'white', borderRadius: 5, borderColor: this.state.isDF ? colors.colorBlue : colors.colorBlack62, alignItems: 'center', justifyContent: 'center', borderWidth: 1 }}>
                        {this.state.isDF && <Image resizeMode={'contain'} style={{ width: 17, height: 17, zIndex: 2 }} source={Images.ic_image_checked} />}
                      </View>
                    </View>
                  </TouchableOpacity>

                  <TouchableOpacity onPress={() => this.setState({ isGC: !this.state.isGC, password: '', phone: '', })} style={{ alignItems: 'center', flexDirection: 'row', alignSelf: 'baseline', width: '100%', marginBottom: 10 }}>
                    <Text style={{ fontFamily: Fonts.medium, color: colors.colorBlack62, fontSize: 12, marginLeft: 8 }}>Group Cancelled</Text>
                    <View style={{ alignItems: 'flex-end', flex: 1 }}>
                      <View style={{ width: 17, height: 17, backgroundColor: this.state.isGC ? colors.colorBlue : 'white', borderRadius: 5, borderColor: this.state.isGC ? colors.colorBlue : colors.colorBlack62, alignItems: 'center', justifyContent: 'center', borderWidth: 1 }}>
                        {this.state.isGC && <Image resizeMode={'contain'} style={{ width: 17, height: 17, zIndex: 2 }} source={Images.ic_image_checked} />}
                      </View>
                    </View>
                  </TouchableOpacity>
                </View>
                <Dash style={{ width: '100%', height: 0.5, }} dashColor={colors.colorGrey} dashLength={2} dashThickness={1} />
                <View style={{ padding: 15 }}>
                  <Text style={{ fontFamily: Fonts.medium, color: colors.colorBlack62, fontSize: 12, marginLeft: 8, marginBottom: 5 }}>Specify Other Reasons</Text>
                  <View style={styles.textAreaContainer} >
                    <TextInput
                      style={styles.textArea}
                      underlineColorAndroid="transparent"
                      placeholder="Type Reason"
                      placeholderTextColor="grey"
                      numberOfLines={5}
                      value={this.state.reason}
                      onChangeText={(text) => this.setState({ reason: text })}
                      multiline={true}
                    />
                  </View>
                </View>
                <View style={{ alignItems: 'center', justifyContent: 'center', marginBottom: 10 }}>
                  <TouchableOpacity
                    onPress={() => this.cancelBooking()}
                    style={{ alignItems: 'center', justifyContent: 'center', paddingHorizontal: 10, paddingVertical: 8, backgroundColor: colors.colorBlue, borderRadius: 5 }}>
                    <Text style={{ fontFamily: Fonts.medium, color: colors.colorWhite, fontSize: 12 }}>{"Cancel Booking"}</Text>
                  </TouchableOpacity>
                </View>

              </View>
            </TouchableHighlight>

          </TouchableHighlight>
          <MyLoader />
        </Modal>

      )
  }


  render() {
    if (!this.state.BookingDetails) return null
    return (
      <View style={styles.container}>
        <SafeAreaView backgroundColor={colors.colorBlue} />
        <CommonHeader title={"Booking Details"} />
        {this.renderCancellationModal()}
        <ScrollView >
          <CardView
            style={{
              backgroundColor: colors.colorWhite,
              marginHorizontal: 10,
              marginVertical: 5
            }}
            cardElevation={2}
            cardMaxElevation={3}
            cornerRadius={8}>
            <View style={{ borderRadius: 8, overflow: 'hidden' }}>
              {this._renderHotelItem()}
              <Dash style={{ width: '100%', height: 0.5, }} dashColor={colors.colorGrey} dashLength={2} dashThickness={1} />
              {/* {this._renderBookingItem()} */}
              {this._renderCheckInItem()}
              <Dash style={{ width: '100%', height: 0.5, }} dashColor={colors.colorGrey} dashLength={2} dashThickness={1} />

              {this._renderRoomItem()}
              <Dash style={{ width: '100%', height: 0.5, }} dashColor={colors.colorGrey} dashLength={2} dashThickness={1} />

              {this.renderGuestDetail()}
              <Dash style={{ width: '100%', height: 0.5, }} dashColor={colors.colorGrey} dashLength={2} dashThickness={1} />
              {this._renderBookingId()}
              <Dash style={{ width: '100%', height: 0.5, }} dashColor={colors.colorGrey} dashLength={2} dashThickness={1} />
              {this._renderEmailDetails()}
              <Dash style={{ width: '100%', height: 0.5, }} dashColor={colors.colorGrey} dashLength={2} dashThickness={1} />
              {this._renderPhoneDetails()}
              <Dash style={{ width: '100%', height: 0.5, }} dashColor={colors.colorGrey} dashLength={2} dashThickness={1} />
              {this._renderPriceDetail()}
              <Dash style={{ width: '100%', height: 0.5, }} dashColor={colors.colorGrey} dashLength={2} dashThickness={1} />
              {this._renderDownloadTicket()}
              {this.state.BookingDetails.cancellation && this.state.BookingDetails.cancellation.status && <><Dash style={{ width: '100%', height: 0.5, }} dashColor={colors.colorGrey} dashLength={2} dashThickness={1} />
                {this._renderCancellationStatus()}</>}
            </View>
          </CardView>
          {/* {this.renderFareDetails()} */}

          {this.cancellationPolicyItem()}

          {this.renderCancelBookingButton()}
        </ScrollView>
        {/* {this._renderConfirmButton()} */}
        <SafeAreaView backgroundColor={colors.colorBlue} />
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getHotelBookingDetailsAction: (data) => dispatch(getHotelBookingDetailsAction(data)),
    cancelHotelBooking: (data) => dispatch(cancelHotelBookingAction(data))
  };
};

export default connect(null, mapDispatchToProps)(HotelBookingDetails)