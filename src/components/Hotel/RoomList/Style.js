import { Dimensions, StyleSheet } from "react-native";
import { colors } from "../../../Utils/Colors";

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white"
    //backgroundColor: "rgba(244,248,254,1)",
  },
  innerContainer: {
    flex: 1, padding: 10, backgroundColor: '#f2f2f2'
  },

  buttonContainer: {
    flex: 1,
    flexDirection: 'row',

    marginTop: 20
  },
  listStyle: {
    paddingBottom: 15,
    paddingTop: 15,
    paddingLeft: 10,
    paddingRight: 10,
    marginTop: 20,
    flex: 1,
    elevation: 10,
    borderRadius: 8,
    shadowColor: "black",
    shadowOpacity: 0.3,
    shadowOffset: { width: 1, height: 2 },
    // shadowRadius:5,
    backgroundColor: 'white'
    // justifyContent: 'center',
    // alignItems: 'center'
  },
  contentContainer: {
    flex: 7,
    // justifyContent: 'center',
    // alignItems: 'flex-start'
  },
  imageContainer: {
    width: '35%',
    justifyContent: 'flex-start',
    alignItems: 'center'
  },

  listcontent: {
    flexDirection: 'row',
    shadowColor: "black",
  },
  button: {

    padding: 10,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: colors.colorBlue,
    borderRadius: 3,
    shadowOpacity: 0.3,
    shadowOffset: { width: 1, height: 2 },
    elevation: 4,
  }, styleHotelListView: {
    // backgroundColor: "white",
    borderRadius: 10,
    paddingBottom: 80,
    marginBottom: 10,
    marginLeft: 3,
    marginRight: 3
  },
  containerPopup: {
    backgroundColor: "rgba(0,0,0,0.5)",
    height: Dimensions.get('window').height - 60,
    justifyContent: "center",
    alignItems: "center"
  },
  stylePopUpView: {
    width: "80%",
    backgroundColor: "white",
    borderRadius: 15,
    padding: 20
  },

})