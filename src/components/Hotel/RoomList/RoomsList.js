import React, { Component } from "react";
import { BackHandler, Dimensions, FlatList, Image, Modal, SafeAreaView, ScrollView, StatusBar, Text, TouchableOpacity, View } from 'react-native';
import { connect } from "react-redux";
import { saveSelectedRoomData } from "../../../Redux/Actions";
import { colors } from "../../../Utils/Colors";
import Fonts from "../../../Utils/Fonts";
import Images from '../../../Utils/Images';
import BottomStrip from "../../custom/BottomStrip";
import CommonHeader from '../../custom/CommonHeader';
import NavigationServices from './../../../Utils/NavigationServices';
import { styles } from "./Style";
import { imageBaseUrl } from "../../../Utils/APIManager/APIConstants";


let SOURCES = null
class RoomsList extends Component {
  constructor(props) {
    super(props)
    this.state = {
      hotelDetails: [],
      roomsData: null,
      loading: false,
      popUp: false,
      CancellationPolicy: "",
      combination: null,
      totalPrice: 0,
      localAgencyPrice: 0,
      currency: '',
      currentSelectedRoomCombination: null,
      selectedRoomsArray: null,
    }
    this._renderHotelRoomListItem = this._renderHotelRoomListItem.bind(this);
  }

  //--------------- Component Life Cycle Methods -----------

  UNSAFE_componentWillMount() {

    // let roomData = this.props.hotelDetails.rooms.roomlist
    const { hotelData } = this.props
    if(!hotelData || !hotelData.rooms || !hotelData.rooms.roomlist) return
    let roomData = hotelData.rooms.roomlist;
    console.log("hotelData" + JSON.stringify(hotelData));
    if (roomData && roomData.length > 0) {
      let newRoomData = roomData.map(v => ({ ...v, isAllowed: true, isChecked: false }))
      let hotelDetails = hotelData.hotel;
      let combination = hotelData.rooms.combination
      this.setState({
        roomsData: newRoomData,
        hotelDetails: hotelDetails,
        combination: combination
      })
    }
    //console.log("room list");
    // console.log(JSON.stringify(hotelData));
    // console.log(JSON.stringify(roomData));
    // console.log( "Room List hotel search param " + JSON.stringify(this.props.hotelSearchParam ))
  }

  componentDidMount() {
    let { navigation } = this.props;
    let traceId = navigation.getParam('traceId', null)
    let source = navigation.getParam('source', null)
    SOURCES = {
      source: source,
      sourceDetail: {
        trace: traceId
      }
    }
    this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
      NavigationServices.goBack();
      let a = this.props.navigation.getParam('showMap', null)
      if (a != null) a()
      return true;
    });
  }

  componentWillUnmount() {
    this.backHandler.remove();
  }

  changeInSelectRooms(e) {
    let numberOfRooms = 0;
    if (this.props.hotelSearchParams && this.props.hotelSearchParams.numberOfRooms) {
      numberOfRooms = this.props.hotelSearchParams.numberOfRooms
    }
    let currency = ""
    let { roomsData, combination, currentSelectedRoomCombination } = this.state
    let roomCombination = combination.RoomCombination

    let currentRoom;
    let checkedCount = 0;
    let selectedRoomsArray = [];
    let totalPrice = 0;
    let localAgencyPrice = 0;

    for (let i = 0; i < roomsData.length; i++) {
      if (roomsData[i].RoomIndex == e) {
        currentRoom = roomsData[i];
      }
      if (roomsData[i].isChecked) {
        checkedCount++;
        localAgencyPrice = localAgencyPrice + Math.ceil(roomsData[i].Price.AgencyFare)
        totalPrice = totalPrice + roomsData[i].Price.AgencyFare
        currency = roomsData[i].Price.CurrencyCode
        selectedRoomsArray.push(roomsData[i]);
      }

    }

    // console.log('checked count', checkedCount)
    // console.log('selectedRoomsArray count', selectedRoomsArray)

    if (checkedCount == numberOfRooms) {
      for (let i = 0; i < roomsData.length; i++) {
        roomsData[i].isAllowed = roomsData[i].isChecked
      }
    }

    else if (checkedCount > 0) {

      console.log('test.................', combination.InfoSource)
      if (combination.InfoSource == 'OpenCombination') {
        console.log('open combination ............................................')

        if (currentRoom.isChecked) {

          for (let i = 0; i < roomCombination.length; i++) {
            const element = roomCombination[i];

            if (element.RoomIndex.includes(e)) {
              currentSelectedRoomCombination = roomCombination[i].RoomIndex;
              break;
            }

          }

          console.log("SELECTED_COMB:" + JSON.stringify(currentSelectedRoomCombination))

          for (let i = 0; i < roomsData.length; i++) {
            if (currentSelectedRoomCombination.includes(roomsData[i].RoomIndex)) {
              if (roomsData[i].RoomIndex == e) {
                roomsData[i].isAllowed = true
              } else {
                roomsData[i].isAllowed = false
              }

            }
            // else {
            // roomsData[i].isAllowed = true
            // }
          }

        } else {
          for (let i = 0; i < roomCombination.length; i++) {
            const element = roomCombination[i];

            if (element.RoomIndex.includes(e)) {
              currentSelectedRoomCombination = roomCombination[i].RoomIndex;
              break;
            }

          }

          for (let i = 0; i < roomsData.length; i++) {
            if (currentSelectedRoomCombination.includes(roomsData[i].RoomIndex)) {
              roomsData[i].isAllowed = true
            }
            // else {
            // roomsData[i].isAllowed = false
            // }
          }
        }



      } else {

        console.log('closed combination ............................................')
        for (let i = 0; i < roomCombination.length; i++) {
          const element = roomCombination[i];

          if (element.RoomIndex.includes(e)) {
            currentSelectedRoomCombination = roomCombination[i].RoomIndex;
            break;
          }

        }

        console.log('current room', currentRoom);

        for (let i = 0; i < roomsData.length; i++) {
          if (currentSelectedRoomCombination.includes(roomsData[i].RoomIndex)) {
            roomsData[i].isAllowed = true
          } else {
            roomsData[i].isAllowed = false
          }
        }
      }


    } else {

      for (let i = 0; i < roomsData.length; i++) {
        roomsData[i].isAllowed = true
      }
    }

    combination.RoomCombination = roomCombination
    // console.log("roomsData", JSON.stringify(roomsData))
    this.setState({
      roomsData: roomsData,
      combination: combination,
      currentSelectedRoomCombination: currentSelectedRoomCombination,
      selectedRoomsArray: selectedRoomsArray,
      totalPrice: Math.ceil(totalPrice),
      localAgencyPrice: localAgencyPrice,
      currency: currency
    })

  }

  roomPoPup() {
    let popUpView = this.state.popUpView
    return (
      <Modal
        style={styles.containerPopup}
        transparent={true}
        animationType={'slide'}
        visible={this.state.popUp}>
        <View style={styles.containerPopup}>
          <View style={styles.stylePopUpView}>
            {popUpView}
            <TouchableOpacity
              underlayColor={'transparent'}
              onPress={() => this.setState({
                popUp: false
              })}>
              <Text style={{ color: colors.colorBlue, fontWeight: '700', fontSize: 16, alignSelf: 'flex-end', marginTop: 10 }}>
                Dismiss
                    </Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    )
  }
  roomSelectionTriggered() {

  }

  _renderHotelRoomListItem(item, index) {
    // console.log(JSON.stringify(item))
    let name = "";
    let price = "";
    let currency = "";
    if (item.RoomTypeName) {
      name = item.RoomTypeName;

    }
    if (item.Price && item.Price.AgencyFare) {
      price = Math.ceil(item.Price.AgencyFare)
    }
    if (item.Price && item.Price.CurrencyCode) {
      currency = item.Price.CurrencyCode;
    }
    return (

      <View opacity={item.isAllowed ? 1 : 0.5} style={{ width: '100%', alignItems: 'flex-start', flexDirection: 'row', padding: 10, backgroundColor: 'white', marginBottom: 1, }}>

        <TouchableOpacity disabled={!item.isAllowed} onPress={() => {
          let newArray = [...this.state.roomsData];
          newArray[index].isChecked = !newArray[index].isChecked;
          this.setState({
            roomsData: newArray
          }, () => this.changeInSelectRooms(item.RoomIndex))
        }} style={{ alignItems: 'center', paddingVertical: 4 }}>
          <Image resizeMode={'contain'} style={{ width: 20, height: 20 }} source={item.isChecked ? Images.ic_image_checked : Images.ic_image_unchecked} />
        </TouchableOpacity>


        <TouchableOpacity disabled={!item.isAllowed} onPress={() => {
          let newArray = [...this.state.roomsData];
          newArray[index].isChecked = !newArray[index].isChecked;
          this.setState({
            roomsData: newArray
          }, () => this.changeInSelectRooms(item.RoomIndex))
        }} style={{ paddingHorizontal: 10, justifyContent: 'center', paddingLeft: 12 }}>

          <Text style={{ fontFamily: Fonts.medium, lineHeight: 20, color: colors.colorBlack21, fontSize: 12, flex: 1, flexWrap: 'wrap' }}>{name}</Text>
          <View style={{ flexDirection: 'row', width: '100%', paddingHorizontal: 10, alignItems: 'center', marginTop: 5 }}>
            <View style={{ flexDirection: 'row', width: '50%' }}>
              {/* {this.renderAmenities()} */}
              {this.renderAmenitiesFix(item.Amenities)}
            </View>

            <TouchableOpacity disabled={item.Amenities.length < 2}  onPress={() => { this.renderAmenities(item) }} style={{ width: '20%', alignItems: 'center' }}>
              {item.Amenities.length > 1 ? <Text style={{ fontFamily: Fonts.regular, color: colors.colorBlue, fontSize: 10 }}>{item.Amenities.length - 1} More</Text> : null}
            </TouchableOpacity>
            <View style={{ alignItems: 'flex-end', width: '30%' }}>
              <Text style={{ fontFamily: Fonts.semiBold, color: colors.colorBlack21, fontSize: 13 }}>{currency}{" "}{price}</Text>
            </View>
          </View>
        </TouchableOpacity>
      </View>

    )
  }

  renderAmenitiesFix(amenities) {
    //amenities = ["Room only rate", "Room only rate"]
    const length = amenities.length
    // console.log("RAmenities", JSON.stringify(amenities))
    if (length > 0) {
      const item = amenities[0]
      let name
      let icon
      if (item.name) { name = item.name } else { name = item }
      if (item.icon) { icon = { uri: imageBaseUrl+ item.icon } } else { icon = Images.ic_circular_check }
      return (
        <View style={{ flexDirection: 'row' }}>
          <View style={{ flexDirection: "row", alignItems: "center", flex: 1, }}>
            <Image
              tintColor={colors.colorBlue}
              style={{ tintColor: colors.colorBlue, height: 10, width: 10, }}
              source={icon} />
            <Text style={{ fontFamily: "Montserrat-Medium", fontSize: 10, margin: 5 }}>{name}</Text>
          </View>
          {/* {length>1?
            <View style={{ flexDirection: "row", alignItems: "center", flex: 1, }}>
            <Image
              style={{ height: 10, width: 10, }}
              source={Images.ic_circular_check} />
            <Text style={{ fontFamily: "Montserrat-Medium", fontSize: 10, margin: 5 }}>{amenities[1]}</Text>
          </View>
          :null} */}
        </View>
      )
    }

  }


  renderAmenities = (item) => {
    // console.warn(item.Amenities.length)
    // console.log(JSON.stringify(item.Amenities))

    let popUpView = <View>

      {item.Amenities.length > 0 ? <View><Text style={{ fontSize: 16, fontWeight: '600' }}>Amenities</Text>
        <FlatList
          data={item.Amenities}
          keyExtractor={(item,index)=> {return index}}
          style={{ marginBottom: 15, maxHeight: Dimensions.get('window').height - 250 < 200 ? Dimensions.get('window').height - 250 : 200 }}
          numColumns={2}
          renderItem={item => {
            // console.log("ITEM::",JSON.stringify(item))
            let name
            let icon
            if (item.item.name) { name = item.item.name } else { name = item.item }
            if (item.item.icon) { icon = { uri: imageBaseUrl+ item.item.icon } } else { icon = Images.ic_circular_check }
            return (<View style={{ flexDirection: "row", alignItems: "center", marginLeft: 7, marginRight: 7, flex: 1, marginTop: 5 }}>
              <Image
                style={{ height: 10, width: 10, }}
                source={icon} />
              <Text style={{ fontFamily: "Montserrat-Medium", fontSize: 10, margin: 5 }}>{name}</Text>
            </View>)
          }
          }
        /></View> : null}
      {/* <Text style={{ fontSize: 16, fontWeight: '600' }}>Cancellation Policy</Text>

      <Text style={{ marginTop: 15 }}>
        {item.CancellationPolicy ? item.CancellationPolicy.replace(/[^a-zA-Z0-9\%,\-.:^ ]/g, "").replace(/[\^]/g, ". ") : ' No cancellation policy available.'}
      </Text> */}
    </View>
    this.setState({
      popUpView: popUpView,
      popUp: true
    })

  }

  render() {
    let numberOfRooms = 0;
    if (this.props.hotelSearchParams && this.props.hotelSearchParams.numberOfRooms) {
      numberOfRooms = this.props.hotelSearchParams.numberOfRooms
    }
    if(!this.state.roomsData ||  this.state.roomsData.length<1) return <View/>
    return (
      <View style={styles.container}>
        <SafeAreaView style={{ backgroundColor: colors.colorBlue }}></SafeAreaView>
        <StatusBar barStyle="light-content" />
        <CommonHeader onBackPress={this.props.navigation.getParam('showMap', null)} title={"Select Room"}></CommonHeader>
        <View style={{ alignItems: 'center', backgroundColor: '#f2f2f2', padding: 10 }}>
          <Text style={{ fontFamily: Fonts.bold, color: colors.colorBlack21 }}>Choose {numberOfRooms} {numberOfRooms > 1 ? " Rooms" : " Room"}</Text>
        </View>
        <View style={styles.innerContainer}>

          <ScrollView style={{}}>
            {this.state.roomsData.map((item, index) => (
              this._renderHotelRoomListItem(item, index)
            ))}
            {this.roomPoPup()}
          </ScrollView>

        </View>
        {numberOfRooms > 0 && (this.state.selectedRoomsArray && this.state.selectedRoomsArray.length == numberOfRooms) ?
          <BottomStrip
            bottomStripStyle={{ position: 'relative' }}
            fireEvent={() => {
              const roomData = {
                selectedRoomsArray: this.state.selectedRoomsArray,
                sources: SOURCES,
                totalPrice: this.state.totalPrice,
                currency: this.state.currency
              }
              this.props.saveRoomData(roomData);
              NavigationServices.navigate('HotelReview');
            }} price={this.state.currency + " " + this.state.localAgencyPrice} /> : null}

        <SafeAreaView backgroundColor={colors.colorBlue} />
      </View>
    )
  }
}

const mapStateToProps = state => {
  //console.log("ReduxSD:", JSON.stringify(state))
  return {
    hotelData: state.HotelListreducer.selectedHotelItem,
    hotelSearchParams: state.HotelListreducer.hotelSearchParam
  }
}

const mapDispatchToProps = dispatch => {
  return {
    saveRoomData: (data) => dispatch(saveSelectedRoomData(data))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(RoomsList);