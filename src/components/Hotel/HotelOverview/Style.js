import { Dimensions, StyleSheet } from "react-native";
import { colors } from "../../../Utils/Colors";
import Fonts from "../../../Utils/Fonts";
const screenWidth = Dimensions.get("window").width;

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    //backgroundColor: "rgba(244,248,254,1)"
    backgroundColor: colors.gray
  },

  // selectRoomcontainer: {
  //   padding: 20
  // },
  seachHotelButton: {
    width: "100%",
    marginTop: 20,
    height: 45,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "rgba(43,128,206,1)",
    borderRadius: 8
  },
  selectRoomContainerText: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between"
  },
  styleImageSliderContainerView: {
    height: 175,
    width: "100%",
    backgroundColor: "rgba(244,248,254,1)",
  },
  styleHeaderSuper: {
    flex: 1,
    width: "100%",
  },
  styleHeaderTitle:{
    fontSize: 15, 
    padding: 12, 
    fontFamily: Fonts.medium, 
    color: colors.colorBlack 
  },
  styleTitelContainerView: {
    width: "100%",
    padding: 5,
  },
  styleTitleText: {
    fontSize: 18,
    flexWrap: 'wrap',
    color: "black",
    fontFamily: Fonts.medium
  },
  styleDistanceText: {
    fontSize: 11,
    textAlign: 'auto',
    fontFamily: Fonts.regular,
    paddingLeft:5,
    lineHeight: 18
  },
  styleSelectRoomView: {
    height: 32,
    justifyContent: "center",
    alignItems: "center",
    paddingHorizontal:12,
    backgroundColor: colors.colorBlue,
    borderRadius: 4,
    shadowColor: "black",
    shadowOpacity: 0.3,
    shadowOffset: { width: 1, height: 1 }
  },
  styleSectionTitleView: {
    height: 30,
    width: "100%",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center"
  },
  styleAnimitiesListItem: {
    height: 40,
    width: (screenWidth - 50) / 2,
    justifyContent: "center",
    borderWidth: 0.3,
    borderColor: "gray",
    // alignItems:'center',
    marginTop: 10,
    marginLeft: 5,
    paddingLeft: 10
    // backgroundColor:'red'
  },
  map: {
    ...StyleSheet.absoluteFillObject
  },
  styleMarkerViewUnselected: {
    height: 50,
    width: 50,
    justifyContent: "center",
    alignItems: "center"
  },
  styleMarkerUnselected: {
    height: 50,
    width: 50,
    borderRadius: 25,
    borderColor: "rgba(43,128,206,1)",
    borderWidth: 2
  },
  styleMarkerViewSelected: {
    height: "100%",

    flexDirection: "row",
    alignItems: "center",
    paddingLeft: 5,
    paddingRight: 10,
    backgroundColor: 'black',
    borderRadius: 30
  },
  styleMarkerSelectedTitleView: {
    marginLeft: 10,

    height: "100%",
    justifyContent: "center",

  }
});
