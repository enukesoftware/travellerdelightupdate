import React, { Component } from 'react';
import { ActivityIndicator, DeviceEventEmitter, Dimensions, FlatList, Image, SafeAreaView, ScrollView, Text, TouchableOpacity, View, Alert } from 'react-native';
import CardView from 'react-native-cardview';
import MapView, { Marker, PROVIDER_GOOGLE } from 'react-native-maps';
import { connect } from 'react-redux';
import { getHotelDetailsAction, selectedHotel } from '../../../Redux/Actions';
import { colors } from '../../../Utils/Colors';
import Fonts from '../../../Utils/Fonts';
import Images from '../../../Utils/Images';
import StringConstants from '../../../Utils/StringConstants';
import Carousel from '../../custom/Carousel';
import CommonHeader from '../../custom/CommonHeader';
import RatingBar from '../../custom/RatingBar';
import NavigationServices from './../../../Utils/NavigationServices';
import { styles } from './Style';
import { MyAlert } from '../../../Utils/Utility';
import  {imageBaseUrl} from '../../../Utils/APIManager/APIConstants';


const screen = Dimensions.get('window');

const ASPECT_RATIO = screen.width / screen.height;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

class HotelOverview extends Component {
  constructor(props) {
    super(props);

    this.mapViewRef = null;

    this.state = {
      position: 1,
      interval: null,
      hotelDetails: null,
      imagesArray: [],
      showFilter: false,
      loading: false,
      readMoreFacilities: false,
      readMoreAboutHotel: false,
      latitude: 0,
      longitude: 0,
      mapRegion: {
        latitude: 37.78825,
        longitude: -122.4324,
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA,
      },
      jsonDict: null,
      isMarkerSelected: false,
      isMap: false,
      isSlider: false

    };

    this._renderImageSliderView = this._renderImageSliderView.bind(this);
    this._renderHeaderView = this._renderHeaderView.bind(this);
    this._renderFacilitiesView = this._renderFacilitiesView.bind(this);
    this._renderAboutHotelView = this._renderAboutHotelView.bind(this);
    this._renderContactDetailsView = this._renderContactDetailsView.bind(this);
    this.updateHotelDetailsFromApi = this.updateHotelDetailsFromApi.bind(this);
  }

  //---------------Component LifeCycle Methods ---------------

  componentDidMount() {
    const { navigation } = this.props;
    const jsonDict = navigation.getParam('jsonDict', null);
    // console.log("jsonDict", JSON.stringify(jsonDict))
    //let jsonDict = { "code": "1297786", "id": "51", "source": "tbo", "traceId": "9d4d6269-38d6-4049-a1f9-f1a35c3e2e00", "price": 1581.37 }
    this.setState(
      {
        jsonDict: jsonDict,
        // interval: setInterval(() => {
        //   this.setState({
        //     position:
        //       this.state.position === this.state.imagesArray.length - 1
        //         ? 0
        //         : this.state.position + 1,
        //   });
        // }, 7000),
      },
      () => {
        DeviceEventEmitter.addListener(StringConstants.HOTEL_DETAILS_EVENT, this.updateHotelDetailsFromApi);
        //this.updateHotelDetailsFromApi(hotel)
        this.getHotelDetails();
      },
    );
  }

  // componentWillMount() {
  //   this.setState({
  //     interval: setInterval(() => {
  //       this.setState({
  //         position:
  //           this.state.position === this.state.imagesArray.length - 1
  //             ? 0
  //             : this.state.position + 1
  //       });
  //     }, 7000)
  //   });
  // }

  componentWillUnmount() {
    DeviceEventEmitter.removeListener(
      StringConstants.HOTEL_DETAILS_EVENT,
      this.updateHotelDetailsFromApi,
    );
    // clearInterval(this.state.interval);
  }

  updateHotelDetailsFromApi(response) {
    console.log("RESP:", JSON.stringify(response))
    if (response && response.hotel) {
      let imgArray = []
      let newImgArray = [];
      if (response.hotel.images) {
        imgArray = response.hotel.images;
        if (imgArray && imgArray.length > 0) {
          imgArray.forEach((item, index) => {
            if (index < 11)
              newImgArray.push({
                url: item,
              });
          });
        }
      }
      this.setState(
        {
          hotelDetails: response,
          imagesArray: newImgArray,
          loading: !this.state.loading,
          latitude: response.hotel.latitude && response.hotel.latitude != ''
            ? parseFloat(response.hotel.latitude)
            : 0,
          longitude: response.hotel.longitude && response.hotel.longitude != ''
            ? parseFloat(response.hotel.longitude)
            : 0,
          isMap: true,
          isSlider: true

        },
        () => {
          //alert(this.state.latitude)
        },
      );

      this.props.saveHotelDetail(response);
    }
    else if (response && response.code && response.code.error && response.code.error.ErrorMessage) {
      setTimeout(() => {
        MyAlert("Error", response.code.error.ErrorMessage, () => {
          NavigationServices.goBack();
        })
      }, 200);
    }
  }

  //-------------------- API Calling Methods --------------

  getHotelDetails() {
    this.props.getHotelDetailsAction(this.state.jsonDict);
  }

  //------------ Image SliderView ----------------

  _renderImageSliderView() {
    let lc = [1, 1, 1, 1, 1];
    let width = Dimensions.get('screen').width - 24;
    return (
      <View style={styles.styleImageSliderContainerView}>
        <Carousel
          delay={2000}
          bullets={true}
          style={{ width: width, height: '100%', elevation: 5 }}
          autoplay  >
          {this.state.imagesArray && this.state.imagesArray.length > 0 ? this.state.imagesArray.map((item, index) => this._renderImageItem(item, index, true))
            : lc.length > 0 ? lc.map((item, index) => this._renderImageItem(item, index, false)) : null}
        </Carousel>
      </View>
    );
  }

  _renderImageItem = (item, index, loaded) => {
    let width = Dimensions.get('screen').width - 24;
    if (loaded) {
      return (
        <View
          style={{
            width: width, height: '100%', alignItems: "center", justifyContent: 'center'
            , elevation: 3, overflow: 'hidden',
          }}>
          <Image source={{ uri: item.url }}
            style={{
              width: width,
              height: '100%',
              resizeMode: 'stretch',
            }} />
        </View>
      )
    } else {
      return (
        <View
          style={{
            width: width, height: 175, alignItems: "center", justifyContent: 'flex-start'
            , elevation: 3, overflow: 'hidden',
          }}>

          <View
            style={{
              width: width,
              height: 175,
              borderRadius: 5,
              backgroundColor: '#909090',
              alignItems: 'center',
              justifyContent: 'center',
              resizeMode: 'cover',

            }} >

            {<ActivityIndicator
              size={"large"}
              color={colors.colorBlue}
              animating={true} />}
          </View>
        </View>)
    }
  }


  //-------------- Render Header View ------------

  _renderHeaderView() {
    let dict = this.state.hotelDetails;
    return (
      <View style={[styles.styleHeaderSuper, { padding: 10 }]}>
        <View style={{ flexDirection: 'row', flex: 1, width: '100%' }}>
          <View style={styles.styleTitelContainerView}>
            <Text
              style={[styles.styleTitleText, {}]}
              adjustsFontSizeToFit={true}>
              {dict.hotel.name}
            </Text>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'flex-start',
                marginTop: 2,
              }}>
              <Text
                style={{
                  color: colors.colorBlack,
                  fontSize: 12,
                  fontFamily: Fonts.regular,
                }}>
                Starting From{' '}
              </Text>
              <Text
                style={{
                  color: colors.colorBlue,
                  fontSize: 12,
                  fontFamily: Fonts.semiBold,
                  letterSpacing: 1,
                }}>
                {this.state.jsonDict.currencycode} {this.state.jsonDict.price}
              </Text>
            </View>

            <View style={{ height: 20, width: 80, marginTop: 5 }}>
              <RatingBar starSize={14} count={dict.hotel.star} />
            </View>
          </View>

          {/* <View
            style={[styles.styleTitelContainerView, { alignItems: "flex-end", width: '40%' }]}
          >
            <View style={{ flexDirection: 'row', alignItems: 'center', flexWrap: 'wrap', justifyContent: "flex-end" }}>
              <Text style={{ color: colors.colorBlack, fontSize: 14, fontFamily: Fonts.medium }}>Starting from </Text>
              <Text style={{ color: colors.colorBlue, fontSize: 16, fontFamily: Fonts.semiBold }}>
                {this.state.jsonDict.currencycode} {this.state.jsonDict.price}
              </Text>
            </View>

          </View> */}
        </View>
        <Text style={styles.styleDistanceText} adjustsFontSizeToFit={true}>
          {dict.hotel.address}
        </Text>

        <View
          style={{
            height: 32,
            marginTop: 10,
            marginBottom: 5,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <TouchableOpacity
            style={styles.styleSelectRoomView}
            underlayColor="transparent"
            onPress={() => {
              if (this.state.hotelDetails.rooms && this.state.hotelDetails.rooms.roomlist) {
                //console.log(JSON.stringify(this.state.hotelDetails));
                this.setState({ isMap: false, isSlider: false }, () => {
                  NavigationServices.navigate('RoomsList', {
                    traceId: this.state.jsonDict.traceId,
                    source: this.state.jsonDict.source,
                    showMap: () => this.setState({ isMap: true, isSlider: true })
                  });
                })
              }else{
                alert("No Rooms Available")
              }
            }}>
            <Text
              style={{ color: 'white', fontFamily: Fonts.bold, fontSize: 12 }}>
              SELECT ROOM
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }

  //---------------- Render Facilities View --------------

  _renderFacilitiesView() {
    const facilities = this.state.hotelDetails.hotel.facilities;
    // console.log("Facilities:", JSON.stringify(facilities))
    //const facilities = ["24 Hour Front Desk", "Deepak", "Manoj", "Raj Kumar", "24 Hour Power Supply"];
    if (!facilities) return null;
    const length = facilities.length;
    if (length === 0) return null;
    let flatListHeight = 0;
    let showMore = false;
    if (length < 5) {
      flatListHeight = (length * 28) + 12
    }
    else {
      flatListHeight = 124;
      showMore = true
    }
    return (
      <CardView
        style={{
          //overflow: 'hidden',
          backgroundColor: colors.colorWhite,
          marginTop: 12,
          marginHorizontal: 12,
        }}
        cardElevation={2}
        cardMaxElevation={3}
        cornerRadius={10}>
        <View style={{ width: '100%', overflow: 'hidden', padding: 1 }}>
          <View
            style={[
              styles.styleHeaderSuper,
              { height: 'auto', flex: 1, flexWrap: 'wrap' },
            ]}>
            <View style={[styles.styleSectionTitleView]}>
              <Text style={styles.styleHeaderTitle}>Amenities</Text>
              <TouchableOpacity
                underlayColor="transparent"
                disabled={!showMore}
                onPress={() => {
                  this.setState({
                    readMoreFacilities: !this.state.readMoreFacilities,
                  });
                }}>

                <Text
                  style={{
                    padding: 12,
                    color: colors.colorBlue,
                    fontSize: 13,
                    fontFamily: Fonts.medium,
                  }}>
                  {showMore ? (this.state.readMoreFacilities ? 'Less' : 'More') : ''}
                </Text>
              </TouchableOpacity>
            </View>
            <FlatList
              // horizontal
              scrollEnabled={false}
              // horizontal = {true}

              style={{
                height: this.state.readMoreFacilities ? 'auto' : flatListHeight,
                paddingTop: 12,
              }}
              data={facilities}
              extraData={this.state.loading}
              renderItem={this._renderFacilitiesItem}
              keyExtractor={(item, index) => index.toString()}
            />
          </View>
        </View>
      </CardView>
    );
  }

  //----------Render List Item for Animities ---------

  _renderFacilitiesItem({ item, index }) {
    let name
    let icon
    if (item.name) { name = item.name } else { name = item }
    if (item.icon) { icon = { uri:  imageBaseUrl+item.icon } } else { icon = Images.ic_circular_check }
    return (
      <View style={{ flexDirection: 'row', margin: 5, marginLeft: 10, height: 18, alignItems: 'center' }}>
        <Image
          style={{ height: 15, width: 15, resizeMode: 'contain',tintColor:colors.colorBlue }}
          source={icon}
          tintColor={colors.colorBlue}
        />
        <Text numberOfLines={1} ellipsizeMode={'tail'}
          style={{
            fontFamily: Fonts.semiBold,
            marginLeft: 5,
            lineHeight: 18,
            flex: 1,
            color: '#626262',
            fontSize: 12,
          }}>
          {name}
        </Text>
      </View>
    );
  }

  //----------- Render About Hotel View -----------

  _renderAboutHotelView() {
    const regex = /(&nbsp;|<([^>]+)>)/gi;
    return (
      <CardView
        style={{
          //overflow: 'hidden',
          backgroundColor: colors.colorWhite,
          marginTop: 12,
          marginHorizontal: 12,
        }}
        cardElevation={2}
        cardMaxElevation={3}
        cornerRadius={10}>
        <View style={{ width: '100%', overflow: 'hidden', padding: 1 }}>
          <View
            style={[
              styles.styleHeaderSuper,
              {
                height:
                  this.state.readMoreAboutHotel ||
                    !this.state.hotelDetails.hotel.description
                    ? 'auto'
                    : 200,
                paddingBottom: 10,
              },
            ]}>
            {/* {this._renderTitleView("About Hotel", 1)} */}

            <View style={[styles.styleSectionTitleView, {}]}>
              <Text style={styles.styleHeaderTitle}>About the Place</Text>
              {this.state.hotelDetails.hotel.description && (
                <TouchableOpacity
                  underlayColor="transparent"
                  onPress={() => {
                    this.setState({
                      readMoreAboutHotel: !this.state.readMoreAboutHotel,
                    });
                  }}>
                  <Text
                    style={{
                      padding: 12,
                      color: colors.colorBlue,
                      fontSize: 13,
                      fontFamily: Fonts.medium,
                    }}>
                    {this.state.readMoreAboutHotel ? 'Less' : 'More'}
                  </Text>
                </TouchableOpacity>
              )}
            </View>

            <Text
              style={{
                padding: 12,
                fontSize: 12,
                color: '#626262',
                lineHeight: 20,
                textAlign: 'justify',
                fontFamily: Fonts.semiBold,
              }}
              numberOfLines={this.state.readMoreAboutHotel ? null : 7}
              ellipsizeMode={'tail'}>
              {this.state.hotelDetails.hotel.description
                ? this.state.hotelDetails.hotel.description.replace(regex, '')
                : 'No info Available'}
            </Text>
          </View>
        </View>
      </CardView>
    );
  }




  _renderContactDetailsView() {
    let dict = this.state.hotelDetails.hotel;
    return (
      <CardView
        style={{
          //overflow: 'hidden',
          backgroundColor: colors.colorWhite,
          marginTop: 12,
          marginBottom: 30,
          marginHorizontal: 12,
        }}
        cardElevation={2}
        cardMaxElevation={3}
        cornerRadius={10}>
        <View style={{ width: '100%', overflow: 'hidden', borderRadius: 10 }}>
          <View style={[styles.styleHeaderSuper, {}]}>
            {/* {this._renderTitleView("Contact Details", 2)} */}
            {/* <Text style={{ fontSize: 15, fontFamily: Fonts.semiBold, color: colors.colorBlack }}>Contact Details </Text>
            <Text style={{ fontSize: 12, color: "#626262", fontFamily: Fonts.medium, marginTop: 4 }}>
              Phone Number - {dict.phone}
            </Text> */}
            <Text style={styles.styleHeaderTitle}>Location On Map</Text>

            <View style={{ height: 240, width: '100%' }}>
              {this.state.isMap &&
                <MapView
                  ref={mapView => {
                    this.mapView = mapView;
                  }}
                  style={{
                    width: '100%',
                    height: 240,
                  }}
                  showsUserLocation={false}
                  provider={PROVIDER_GOOGLE}
                  region={{
                    latitude: this.state.latitude,
                    longitude: this.state.longitude,
                    latitudeDelta: 0.008,
                    longitudeDelta: 0.008,
                  }}>
                  <Marker
                    coordinate={{
                      latitude: this.state.latitude,
                      longitude: this.state.longitude,
                      latitudeDelta: 0.008,
                      longitudeDelta: 0.008,
                    }}
                    title={this.state.hotelDetails.hotel.name}
                  />
                </MapView>
              }
            </View>
          </View>
        </View>
      </CardView>
    );
  }

  //------------- renderMethod --------------
  render() {
    if (this.state.hotelDetails == null) {
      return (
        <View style={styles.container}>
          <SafeAreaView style={{ backgroundColor: colors.colorBlue }} />

          <CommonHeader title={'Review Hotel'} />

        </View>
      );
    }
    return (
      <View style={styles.container}>
        <SafeAreaView style={{ backgroundColor: colors.colorBlue }} />

        <CommonHeader title={'Review Hotel'}></CommonHeader>

        <ScrollView
          automaticallyAdjustContentInsets={true}
          showsVerticalScrollIndicator={false}
          style={{ flex: 1, backgroundColor: colors.gray }}>
          <View style={{ flex: 1 }}>


            <CardView
              style={{
                //overflow: 'hidden',
                backgroundColor: colors.colorWhite,
                marginTop: 12,
                marginHorizontal: 12,
              }}
              cardElevation={2}
              cardMaxElevation={3}
              cornerRadius={10}>
              <View
                style={{
                  borderTopLeftRadius: 10,
                  borderTopRightRadius: 10,
                  width: '100%',
                  overflow: 'hidden',
                  padding: 0,
                }}>

                {this.state.isSlider && this._renderImageSliderView()}

                {this._renderHeaderView()}

                <TouchableOpacity
                  style={{
                    borderRadius: 20,
                    position: 'absolute',
                    backgroundColor: 'white',
                    height: 35,
                    width: 35,
                    right: 10,
                    top: 10,
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}>
                  <Image
                    source={Images.ic_favorite_inactive}
                    style={{ width: 30, height: 30, resizeMode: 'contain' }}
                  />
                </TouchableOpacity>
              </View>
            </CardView>

            {this._renderFacilitiesView()}

            {this._renderAboutHotelView()}

            {this._renderContactDetailsView()}
          </View>
        </ScrollView>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
  };
};

const mapDispatchToProps = dispatch => {
  return {
    saveHotelDetail: data => dispatch(selectedHotel(data)),
    getHotelDetailsAction: data => dispatch(getHotelDetailsAction(data)),
  };
};

export default connect(null, mapDispatchToProps)(HotelOverview);
