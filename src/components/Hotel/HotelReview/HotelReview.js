import moment from 'moment';
import React, { Component } from 'react';
import { DeviceEventEmitter, Image, Modal, SafeAreaView, Text, TextInput, TouchableHighlight, TouchableOpacity, View } from 'react-native';
import CardView from 'react-native-cardview';
import Dash from 'react-native-dash';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { connect } from 'react-redux';
import { doLoginAction } from '../../../Redux/Actions';
import { colors } from '../../../Utils/Colors';
import Fonts from '../../../Utils/Fonts';
import Images from '../../../Utils/Images';
import NavigationServices from '../../../Utils/NavigationServices';
import StringConstants from '../../../Utils/StringConstants';
import { isEmailValid, roundOffTwo } from '../../../Utils/Utility';
import BookingBelt from '../../custom/BookingBelt';
import BottomStrip from '../../custom/BottomStrip';
import CommonHeader from '../../custom/CommonHeader';
import HotelFareDetails from '../HotelFareDetails';
import styles from './Style';

class HotelReview extends Component {
  constructor(props) {
    super(props);
    this.state = {
      popUpView: null,
      popUp: false,
      roomPriceObject: null,
      loginModal: false,
      isSigninTicked: false,
      phone: '',
      email: '',
      phoneN: '',
      emailN: '',
      password: '',
    };
    this.updateLoginDetail = this.updateLoginDetail.bind(this);
  }

  UNSAFE_componentWillMount() {
    this.calculateFareDetails();
  }

  componentDidMount() {
    DeviceEventEmitter.addListener(StringConstants.IS_LOGINEVENT, this.updateLoginDetail)
  }

  componentWillUnmount() {
    DeviceEventEmitter.removeListener(StringConstants.IS_LOGINEVENT, this.updateLoginDetail)
  }

  calculateFareDetails() {
    const selectedRoomsArray = this.props.selectedRoomData.selectedRoomsArray;
    let roomPrices = [];
    let totalRoomPrice = 0;
    let totalTax = 0;
    let totalOtherCharges = 0;
    let totalAgencyFare = 0;
    selectedRoomsArray.forEach((element, index) => {
      totalRoomPrice = roundOffTwo(totalRoomPrice + element.Price.RoomPrice)
      totalTax = roundOffTwo(totalTax + element.Price.Tax)
      totalAgencyFare = roundOffTwo(totalAgencyFare + element.Price.AgencyFare)
      totalOtherCharges = roundOffTwo(totalOtherCharges + element.Price.OthrCharges)
      roomPrices.push({
        CurrencyCode: element.Price.CurrencyCode,
        RoomPrice: element.Price.RoomPrice,
        Tax: element.Price.Tax,
        OtherCharges: element.Price.OthrCharges,
        AgencyFare: element.Price.AgencyFare,
      });
    });
    if (roomPrices.length > 0)
      this.setState(
        {
          roomPriceObject: {
            totalRoomPrice: totalRoomPrice,
            totalTax: totalTax,
            totalOtherCharges: totalOtherCharges,
            totalAgencyFare: totalAgencyFare,
            roomPrices: roomPrices,
            currency: this.props.selectedRoomData.currency,
            totalPrice: this.props.selectedRoomData.totalPrice
          },
        },
        () => console.log(JSON.stringify(this.state.roomPriceObject)),
      );
  }

  renderHotelDetails() {
    const hotelSearchParam = this.props.hotelSearchParam;
    const selectedHotelItem = this.props.selectedHotelItem;

    const hotelName = selectedHotelItem.hotel.name;
    const address = selectedHotelItem.hotel.address;
    let guestCount =
      hotelSearchParam.numberOfRooms +
      (hotelSearchParam.numberOfRooms > 1 ? ' Rooms' : ' Room') +
      ' | ' +
      hotelSearchParam.numberOfGuest +
      (hotelSearchParam.numberOfGuest > 1 ? ' Guests' : ' Guest');

    return (
      <CardView
        style={{ backgroundColor: colors.colorWhite, margin: 10, }}
        cardElevation={2}
        cardMaxElevation={3}
        cornerRadius={5}>
        <View style={{ borderRadius: 5, overflow: 'hidden' }}>
          <View style={{ padding: 15, justifyContent: 'center' }}>
            <Text style={styles.hotelName}>{hotelName}</Text>
            <Text style={styles.guestCount}>{guestCount}</Text>
            <Text style={styles.guestCount}>{address}</Text>
          </View>

          <View style={styles.separator} />

          <View style={{ width: '100%', flexDirection: 'row', padding: 15 }}>
            <View style={{ width: '30%' }}>
              <Text style={styles.checkIn}>Check In</Text>
              <Text style={styles.estimate}>
                {moment(hotelSearchParam.checkIn).format('DD MMM, YYYY')}
              </Text>
            </View>
            <View style={styles.dotView}>
              <View style={styles.leftDot} />
              <View style={styles.rightDot} />
              {/* <Image source={Images.imgbluedot} style={{ width: "10%", resizeMode: "contain" }}></Image> */}
              <Dash
                style={styles.dashStyle}
                dashColor={colors.colorBlue}
                dashLength={2}
                dashThickness={1}
              />
              <View
                style={{
                  alignItems: 'center', justifyContent: 'center', backgroundColor: 'white',
                }}>
                <Image
                  source={Images.ic_hotel_icon}
                  tintColor={colors.colorBlue}
                  style={{ width: 20, height: 20, resizeMode: 'contain' }}
                />
              </View>
            </View>
            <View style={{ width: '30%', alignItems: 'flex-end' }}>
              <Text style={styles.checkIn}>Check Out</Text>
              <Text style={styles.estimate}>
                {moment(hotelSearchParam.checkOut).format('DD MMM, YYYY')}
              </Text>
            </View>
          </View>
        </View>
      </CardView>
    );
  }

  renderRoomsDetails() {
    return (
      <CardView
        style={{
          backgroundColor: colors.colorWhite,
          margin: 10,
        }}
        cardElevation={2}
        cardMaxElevation={3}
        cornerRadius={5}>
        <View style={{ borderRadius: 5, overflow: 'hidden' }}>
          <View
            style={{ padding: 10, paddingHorizontal: 15, justifyContent: 'center', }}>
            <Text
              style={{ fontFamily: Fonts.medium, fontSize: 16, color: colors.colorBlack21, }}>
              Room Details
            </Text>
          </View>

          <View style={styles.separator} />

          {this.renderRoomType()}
        </View>
      </CardView>
    );
  }

  renderRoomType() {
    let selectedRoomsArray = this.props.selectedRoomData.selectedRoomsArray;
    // selectedRoomsArray.push({
    //   RoomTypeName: '1 Person In 6-Bed Dormitory - Female Only',
    //   Inclusion: ['Free Meal', 'Meal'],
    // });
    return (
      <View style={{ flex: 1, padding: 15 }}>
        {selectedRoomsArray.map((item, index) => {
          return (
            <View style={{ flex: 1 }}>
              <View style={{ flexDirection: 'row', paddingBottom: 5, flex: 1 }}>
                <Text style={styles.roomType}>{index + 1}. Room Type : </Text>
                <Text style={styles.roomTypeName}>{item.RoomTypeName}</Text>
              </View>
              {item.Inclusion && item.Inclusion.length > 0 && (
                <View style={{ flexDirection: 'row' }}>
                  <Text style={styles.roomType}>{'   '} Inclusion : </Text>
                  {this.renderInclusion(item)}
                </View>
              )}
              <View style={{ flexDirection: 'row', margin: 5 }}>
                <TouchableOpacity onPress={() => NavigationServices.goBack()}>
                  <Text style={{ margin: 5, fontFamily: Fonts.semiBold, fontSize: 12, color: colors.colorBlue, }}>
                    {'Change Room'}
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={() => this.onPressCancellationPolicy(item)}>
                  <Text style={{ margin: 5, fontFamily: Fonts.semiBold, fontSize: 12, color: colors.colorGold, }}>
                    {'Booking & CancellationPolicy'}
                  </Text>
                </TouchableOpacity>
              </View>

              {index + 1 != selectedRoomsArray.length && (
                <View style={[styles.separator, { margin: 10 }]} />
              )}
            </View>
          );
        })}
      </View>
    );
  }
  renderInclusion(room) {
    return (
      <View style={{ flex: 1 }}>
        {room.Inclusion.map((item) => {
          return (<Text style={styles.roomTypeName}>{item}</Text>)
        })}
      </View>
    );
  }

  onPressCancellationPolicy(item) {
    let popUpView = (
      <View>
        <Text style={{ fontSize: 16, fontWeight: '600' }}>
          Cancellation Policy
        </Text>

        <Text style={{ marginTop: 15 }}>
          {item.CancellationPolicy
            ? item.CancellationPolicy.replace(
              /[^a-zA-Z0-9\%,\-.:^ ]/g, '',
            ).replace(/[\^]/g, '. ')
            : ' No cancellation policy available.'}
        </Text>
      </View>
    );
    this.setState({
      popUpView: popUpView,
      popUp: true,
    });
  }

  roomPoPup() {
    let popUpView = this.state.popUpView;
    return (
      <Modal
        style={styles.containerPopup}
        transparent={true}
        animationType={'slide'}
        visible={this.state.popUp}>
        <View style={styles.containerPopup}>
          <View style={styles.stylePopUpView}>
            {popUpView}
            <TouchableOpacity
              onPress={() => this.setState({ popUp: false, })}>
              <Text style={{ color: colors.colorBlue, fontWeight: '700', fontSize: 16, alignSelf: 'flex-end', marginTop: 10, }}>  Dismiss </Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    );
  }

  continue = () => {
    // console.log("USERDATA:", JSON.stringify(this.props.userData))
    if (this.props.isLogin && this.props.userData) {
      let phone = "NA"
      let email = "NA"
      let userId = null
      if (this.props.userData.address && this.props.userData.address.phone) phone = this.props.userData.address.phone
      if (this.props.userData.email) email = this.props.userData.email
      if (this.props.userData.id) userId = this.props.userData.id
      NavigationServices.navigate('GuestDetails', {
        roomPriceObject: this.state.roomPriceObject,
        phone: phone,
        email: email,
        userId: userId
      });
    }
    else {
      this.setState({
        loginModal: true
      })
    }
  }

  guestLogin() {
    const { phone, email, password, isSigninTicked } = this.state
    if (!email) {
      alert("Please Enter Email")
      return
    }
    if (!isEmailValid(email)) {
      alert('Please Enter valid Email')
      return
    }
    if (isSigninTicked) {
      if (!password) {
        alert("Please Enter Password")
        return
      }
      this.props.doLoginAction({
        email: email,
        password: password,
        loginFrom: "FlightDetails"
      }, this.props)
    } else {
      if (phone.trim().length == 0) {
        alert('Please Enter your Phone Number')
        return
      }
      if (phone.trim().length < 10) {
        alert('Please Enter valid Phone Number')
        return
      }

      this.setState({
        loginModal: false,
        email: '',
        phone: '',
        password: ''
      }, () => {
        NavigationServices.navigate('GuestDetails', {
          roomPriceObject: this.state.roomPriceObject,
          phone: this.state.phoneN,
          email: this.state.emailN
        })
      })


    }
  }

  updateLoginDetail(data) {
    if (data === true && this.props.userData) {
      this.setState({
        loginModal: false,
        password: '',
        email: ''
      }, () => {
        let phone = "NA"
        let email = "NA"
        let userId = null
        if (this.props.userData.address && this.props.userData.address.phone) phone = this.props.userData.address.phone
        if (this.props.userData.email) email = this.props.userData.email
        if (this.props.userData.id) userId = this.props.userData.id

        setTimeout(() => {
          NavigationServices.navigate('GuestDetails', {
            roomPriceObject: this.state.roomPriceObject,
            phone: phone,
            email: email,
            userId: userId
          })
        }, 300);
      })
    }
  }

  renderLoginModal() {
    return (
      <Modal
        animationType="slide"
        transparent={true}
        visible={this.state.loginModal}
        onRequestClose={() => { this.setState({ loginModal: false }) }}>
        <TouchableHighlight
          activeOpacity={0.9}
          underlayColor={"rgba(0,0,0,0.5)"}
          onPress={() => this.setState({ loginModal: false })} style={{ backgroundColor: "rgba(0,0,0,0.5)", justifyContent: "center", flex: 1, alignItems: "center" }}>

          <TouchableHighlight onPress={() => { }}
            underlayColor={colors.colorWhite}
            activeOpacity={1}
            style={{ width: "80%", backgroundColor: colors.colorWhite, borderRadius: 15, padding: 15, }}>
            <View>
              <View style={{ flexDirection: 'row', alignItems: 'center', height: 30, width: '100%' }}>
                <View style={{ width: '20%', alignItems: 'center', justifyContent: 'center' }}>
                  <TouchableOpacity onPress={() => this.setState({ loginModal: false })}>
                    <Image tintColor={colors.colorBlue} style={{ height: 15, width: 15 }} source={Images.ic_image_back} resizeMode={'contain'} />
                  </TouchableOpacity>
                </View>
                <View style={{ width: '60%', justifyContent: "center", alignItems: 'center' }}>
                  <Text
                    style={{ fontFamily: Fonts.bold, color: colors.colorBlue, fontSize: 18, }}
                  >LOGIN  </Text>
                </View>
              </View>
              <View style={{ padding: 10 }}>
                <TextInput
                  style={{ height: 34, margin: 4, fontSize: 12, padding: 4, fontFamily: Fonts.regular, borderBottomColor: colors.lightgrey, borderBottomWidth: 1 }}
                  value={this.state.email}
                  onChangeText={text => this.setState({ email: text, emailN: text })}
                  placeholder={'Email'}
                  autoCapitalize={"none"}
                  keyboardType={'email-address'}
                  autoCorrect={false}
                  placeholderTextColor={colors.colorBlack21}
                />

                {!this.state.isSigninTicked &&
                  <TextInput
                    style={{ height: 34, margin: 4, fontSize: 12, padding: 4, fontFamily: Fonts.regular, borderBottomColor: colors.lightgrey, borderBottomWidth: 1 }}
                    value={this.state.phone}
                    onChangeText={text => this.setState({ phone: text, phoneN: text })}
                    placeholder={'Phone'}
                    autoCapitalize={"none"}
                    keyboardType={'phone-pad'}
                    autoCorrect={false}
                    placeholderTextColor={colors.colorBlack21}
                    maxLength={10}
                  />}
                {this.state.isSigninTicked &&
                  <TextInput
                    style={{ height: 34, fontSize: 12, margin: 4, padding: 4, fontFamily: Fonts.regular, borderBottomColor: colors.lightgrey, borderBottomWidth: 1 }}
                    value={this.state.password}
                    onChangeText={text => this.setState({ password: text })}
                    placeholder={"Password"}
                    autoCapitalize={"none"}
                    autoCorrect={false}
                    password={true}
                    textContentType={'password'}
                    // keyboardType={'default'}
                    secureTextEntry={true}
                    placeholderTextColor={colors.colorBlack21}
                  />}

                <View style={{ paddingVertical: 10, paddingHorizontal: 8, marginBottom: 1, }}>
                  <TouchableOpacity onPress={() => this.setState({ isSigninTicked: !this.state.isSigninTicked, password: '', phone: '', })} style={{ alignItems: 'center', flexDirection: 'row', alignSelf: 'baseline' }}>
                    <View style={{ width: 17, height: 17, backgroundColor: this.state.isSigninTicked ? colors.colorBlue : 'white', borderRadius: 5, borderColor: this.state.isSigninTicked ? colors.colorBlue : colors.colorBlack62, alignItems: 'center', justifyContent: 'center', borderWidth: 1 }}>
                      {this.state.isSigninTicked && <Image resizeMode={'contain'} style={{ width: 14, height: 14, zIndex: 2 }} source={Images.ic_image_checked} />}
                    </View>
                    <Text style={{ fontFamily: Fonts.medium, color: colors.colorBlack62, fontSize: 12, marginLeft: 8 }}>Existing User? Sign In</Text>
                  </TouchableOpacity>
                </View>

                <View style={{ alignItems: 'center', justifyContent: 'center', paddingTop: 14 }}>
                  <TouchableOpacity
                    onPress={() => this.guestLogin()}
                    style={{ alignItems: 'center', justifyContent: 'center', paddingHorizontal: 10, paddingVertical: 8, backgroundColor: colors.colorBlue, borderRadius: 5 }}>
                    <Text style={{ fontFamily: Fonts.bold, color: colors.colorWhite, fontSize: 14 }}>{this.state.isSigninTicked ? "Sign In" : "BOOK AS GUEST"}</Text>
                  </TouchableOpacity>
                </View>

              </View>
            </View>

          </TouchableHighlight>

        </TouchableHighlight>

      </Modal>

    )
  }


  render() {
    return (
      <View style={styles.container}>
        <SafeAreaView backgroundColor={colors.colorBlue} />
        <CommonHeader title={'Review Hotel'} />
        <View style={styles.container}>
          <KeyboardAwareScrollView
            //contentContainerStyle={styles.container}
            contentInsetAdjustmentBehavior={'automatic'}>
            <BookingBelt type={1} isHotel={true} style={{ paddingBottom: 5 }} />
            <View>
              {this.renderHotelDetails()}
              {this.renderRoomsDetails()}
              <HotelFareDetails
                data={this.state.roomPriceObject}
                currency={this.props.selectedRoomData.currency}
                totalPrice={this.props.selectedRoomData.totalPrice}
              />
            </View>
            {this.roomPoPup()}
          </KeyboardAwareScrollView>
          {
            <BottomStrip
              fireEvent={() => this.continue()}
              price={this.props.selectedRoomData.currency +
                ' ' +
                Math.ceil(this.props.selectedRoomData.totalPrice)
              }
              bottomStripStyle={{ position: 'relative' }}
            />
          }
          {this.renderLoginModal()}

          <SafeAreaView backgroundColor={colors.colorBlue} />
        </View>
      </View>
    );
  }
}


const mapStateToProps = state => {
  // console.log('Review Redux:', JSON.stringify(state.HotelListreducer));
  return {
    hotelSearchParam: state.HotelListreducer.hotelSearchParam,
    selectedRoomData: state.HotelListreducer.selectedRoomData,
    selectedHotelItem: state.HotelListreducer.selectedHotelItem,
    isLogin: state.isLoginReducer,
    userData: state.userDataReducer,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    doLoginAction: (data) => dispatch(doLoginAction(data)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(HotelReview);
