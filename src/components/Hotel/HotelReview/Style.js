import { StyleSheet } from 'react-native';
import { colors } from '../../../Utils/Colors';
import Fonts from '../../../Utils/Fonts';

export default styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  estimate: {
    fontFamily: Fonts.semiBold,
    color: colors.colorBlack21,
    fontSize: 14,
  },
  checkIn: {
    fontFamily: Fonts.regular,
    fontSize: 12,
    lineHeight: 17,
    color: colors.colorBlack21,
  },
  hotelName: {
    fontFamily: Fonts.semiBold,
    fontSize: 17,
    letterSpacing: 0.8,
    color: colors.colorBlack21,
  },
  guestCount: {
    fontFamily: Fonts.regular,
    fontSize: 11,
    textAlign: 'auto',
    letterSpacing: 0.5,
    color: colors.colorBlack21,
    lineHeight: 20,
  },
  separator: {
    width: '100%',
    height: 1,
    backgroundColor: colors.lightgrey,
  },
  dotView: {
    width: '40%',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  leftDot: {
    backgroundColor: colors.colorBlue,
    borderRadius: 5,
    position: 'absolute',
    left: -1,
    height: 10,
    width: 10,
  },
  rightDot: {
    backgroundColor: colors.colorBlue,
    borderRadius: 5,
    position: 'absolute',
    right: -1,
    height: 10,
    width: 10,
  },
  dashStyle: {
    width: '100%',
    height: 0.5,
    position: 'absolute',
    left: 0,
  },
  roomType: {
    fontFamily: Fonts.semiBold,
    letterSpacing: 0.5,
    color: colors.colorBlack21,
    fontSize: 12,
  },
  roomTypeName: {
    fontFamily: Fonts.semiBold,
    letterSpacing: 0.5,
    color: colors.colorBlack62,
    fontSize: 12,
    flexWrap: 'wrap',
    flex: 1,
  },
  containerPopup: {
    backgroundColor: 'rgba(0,0,0,0.5)',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  stylePopUpView: {
    width: '80%',
    backgroundColor: 'white',
    borderRadius: 15,
    padding: 20,
  },
});
