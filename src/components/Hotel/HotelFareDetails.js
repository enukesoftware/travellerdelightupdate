import React, { Component } from 'react';
import { Dimensions, Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import CardView from 'react-native-cardview';
import { colors } from '../../Utils/Colors';
import Fonts from '../../Utils/Fonts';
import Images from '../../Utils/Images';
const screenWidth = Dimensions.get('screen').width;
const screenHeight = Dimensions.get('screen').height;

export default class HotelFareDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isFareHidden: false,
    };
  }

  renderFareDetails = () => {
    const { isFareHidden } = this.state;
    const roomPriceObject = this.props.data;
    const {
      totalRoomPrice,
      totalTax,
      totalAgencyFare,
      totalOtherCharges,
      roomPrices,
      currency,
      totalPrice
    } = roomPriceObject;
    return (
      <CardView
        style={{
          backgroundColor: colors.colorWhite,
          margin: 10,
        }}
        cardElevation={2}
        cardMaxElevation={3}
        cornerRadius={5}>
        <View style={{ borderRadius: 5, overflow: 'hidden' }}>
          <TouchableOpacity
            onPress={() => this.setState({ isFareHidden: !isFareHidden })}
            style={{ padding: 10, alignItems: 'center', flexDirection: 'row' }}>
            <Text
              style={{
                fontFamily: Fonts.medium,
                fontSize: 16,
                color: colors.colorBlack21,
              }}>
              Fare Details
            </Text>
            <View
              style={{
                justifyContent: 'center',
                flex: 1,
                alignItems: 'flex-end',
                paddingEnd: 10,
              }}>
              <Image
                style={{ width: 15, height: 15, resizeMode: 'contain' }}
                source={isFareHidden ? Images.ic_closed : Images.ic_opened}
              />
            </View>
          </TouchableOpacity>
          {!isFareHidden && (
            <>
              <View
                style={{
                  height: 1,
                  backgroundColor: colors.lightgrey,
                  width: '100%',
                }}
              />
              <View style={{ paddingHorizontal: 10 }}>
                <View style={{ paddingVertical: 10 }}>
                  <View style={styles.fullView}>
                    <View
                      style={{
                        justifyContent: 'center',
                        alignItems: 'flex-start',
                        width: '60%',
                      }}>
                      <Text
                        style={{
                          fontFamily: Fonts.medium,
                          color: colors.colorBlack21,
                          fontSize: 15,
                          paddingHorizontal: 2,
                        }}>
                        Hotel Charges
                      </Text>
                    </View>
                    <View
                      style={{
                        justifyContent: 'center',
                        alignItems: 'flex-end',
                        width: '40%',
                      }}>
                      <Text
                        style={{
                          fontFamily: Fonts.medium,
                          color: colors.colorBlack21,
                          fontSize: 15,
                          paddingHorizontal: 2,
                        }}>
                        {currency +
                          ' ' +
                          (totalRoomPrice < 100
                            ? totalRoomPrice.toFixed(2)
                            : totalRoomPrice)}
                      </Text>
                    </View>
                  </View>
                  {this.renderChildViews(1)}
                </View>
                <View
                  style={{
                    height: 1,
                    backgroundColor: colors.lightgrey,
                    width: '100%',
                  }}
                />
                <View style={{ paddingVertical: 10 }}>
                  <View style={styles.fullView}>
                    <View
                      style={{
                        justifyContent: 'center',
                        alignItems: 'flex-start',
                        width: '60%',
                      }}>
                      <Text
                        style={{
                          fontFamily: Fonts.medium,
                          color: colors.colorBlack21,
                          fontSize: 15,
                          paddingHorizontal: 2,
                        }}>
                        Hotel Taxes
                      </Text>
                    </View>
                    <View
                      style={{
                        justifyContent: 'center',
                        alignItems: 'flex-end',
                        width: '40%',
                      }}>
                      <Text
                        style={{
                          fontFamily: Fonts.medium,
                          color: colors.colorBlack21,
                          fontSize: 15,
                          paddingHorizontal: 2,
                        }}>
                        {currency +
                          ' ' +
                          (totalTax < 100 ? totalTax.toFixed(2) : totalTax)}
                      </Text>
                    </View>
                  </View>
                  {this.renderChildViews(2)}
                </View>

                <View
                  style={{
                    height: 1,
                    backgroundColor: colors.lightgrey,
                    width: '100%',
                  }}
                />

                <View style={{ paddingVertical: 10 }}>
                  <View style={styles.fullView}>
                    <View
                      style={{
                        justifyContent: 'center',
                        alignItems: 'flex-start',
                        width: '60%',
                      }}>
                      <Text
                        style={{
                          fontFamily: Fonts.medium,
                          color: colors.colorBlack21,
                          fontSize: 15,
                          paddingHorizontal: 2,
                        }}>
                        Other Charges
                      </Text>
                    </View>
                    <View
                      style={{
                        justifyContent: 'center',
                        alignItems: 'flex-end',
                        width: '40%',
                      }}>
                      <Text
                        style={{
                          fontFamily: Fonts.medium,
                          color: colors.colorBlack21,
                          fontSize: 15,
                          paddingHorizontal: 2,
                        }}>
                        {currency +
                          ' ' +
                          (totalOtherCharges < 100
                            ? totalOtherCharges.toFixed(2)
                            : totalOtherCharges)}
                      </Text>
                    </View>
                  </View>
                </View>

                <View
                  style={{
                    height: 1,
                    backgroundColor: colors.lightgrey,
                    width: '100%',
                  }}
                />

                <View style={{ paddingVertical: 10 }}>
                  <View style={styles.fullView}>
                    <View
                      style={{
                        justifyContent: 'center',
                        alignItems: 'flex-start',
                        width: '60%',
                      }}>
                      <Text
                        style={{
                          fontFamily: Fonts.medium,
                          color: colors.colorBlack21,
                          fontSize: 15,
                          paddingHorizontal: 2,
                        }}>
                        Total
                      </Text>
                    </View>
                    <View
                      style={{
                        justifyContent: 'center',
                        alignItems: 'flex-end',
                        width: '40%',
                      }}>
                      <Text
                        style={{
                          fontFamily: Fonts.medium,
                          color: colors.colorBlack21,
                          fontSize: 15,
                          paddingHorizontal: 2,
                        }}>
                        {currency +
                          ' ' +
                          (totalAgencyFare < 100
                            ? totalAgencyFare.toFixed(2)
                            : totalAgencyFare.toFixed(2))}
                      </Text>
                    </View>
                  </View>
                </View>

                <View
                  style={{
                    height: 1,
                    backgroundColor: colors.lightgrey,
                    width: '100%',
                  }}
                />

                <View style={{ paddingVertical: 10 }}>
                  <View style={styles.fullView}>
                    <View
                      style={{
                        justifyContent: 'center',
                        alignItems: 'flex-start',
                        width: '60%',
                      }}>
                      <Text
                        style={{
                          fontFamily: Fonts.medium,
                          color: colors.colorBlack21,
                          fontSize: 15,
                          paddingHorizontal: 2,
                        }}>
                        Payable Price
                      </Text>
                    </View>
                    <View
                      style={{
                        justifyContent: 'center',
                        alignItems: 'flex-end',
                        width: '40%',
                      }}>
                      <Text
                        style={{
                          fontFamily: Fonts.medium,
                          color: colors.colorBlack21,
                          fontSize: 15,
                          paddingHorizontal: 2,
                        }}>
                        {currency +
                          ' ' +
                          Math.ceil(
                            totalPrice
                              ? totalPrice
                              : totalAgencyFare,
                          )}
                      </Text>
                    </View>
                  </View>
                </View>
              </View>
            </>
          )}
        </View>
      </CardView>
    );
  };

  renderChildViews(t) {
    const roomPrices = this.props.data.roomPrices;
    return roomPrices.map((item, index) => {
      return (
        <>
          <View style={[styles.fullView, { paddingTop: 8, paddingLeft: 10 }]}>
            <View
              style={{
                justifyContent: 'center',
                alignItems: 'flex-start',
                width: '60%',
              }}>
              <Text
                style={{
                  fontFamily: Fonts.medium,
                  color: 'grey',
                  fontSize: 14,
                  paddingHorizontal: 2,
                }}>
                {'Room ' + (index + 1) + ' ' + (t == 2 ? 'Tax' : '')}
              </Text>
            </View>

            <View
              style={{
                justifyContent: 'center',
                alignItems: 'flex-end',
                width: '40%',
              }}>
              <Text
                style={{
                  fontFamily: Fonts.medium,
                  color: 'grey',
                  fontSize: 14,
                  paddingHorizontal: 2,
                }}>
                {item.CurrencyCode + ' ' + (t == 1 ? item.RoomPrice : item.Tax)}
              </Text>
            </View>
          </View>
        </>
      );
    });
  }

  render() {
    if (this.props.data) return this.renderFareDetails();
    else return null
  }
}

const styles = StyleSheet.create({
  fullView: {
    flexDirection: 'row',
    alignItems: 'center',
    width: '100%',
    paddingHorizontal: 8,
  },
});
