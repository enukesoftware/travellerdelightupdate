import React, { Component } from "react";
import { Dimensions, FlatList, Image, ScrollView, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import Dash from 'react-native-dash';
import { Dropdown } from 'react-native-material-dropdown';
import { SafeAreaView } from "react-navigation";
import { colors } from "../../../Utils/Colors";
import { commonstyle } from "../../../Utils/Commonstyle";
import Fonts from "../../../Utils/Fonts";
import Images from "../../../Utils/Images";
import Counter from "../../custom/Counter";


const screenWidth = Dimensions.get('screen').width;
const screenHeight = Dimensions.get('screen').height;

export default class GuestCountModel extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      roomCount: this.props.roomCount,
      GuestCount: this.props.guestCount,
      ageArray: []
    }
    this.roomData = JSON.parse(JSON.stringify(this.props.roomData));
    this._renderRoomView = this._renderRoomView.bind(this)

  }

  UNSAFE_componentWillMount() {
    let ageArray = []
    for (let i = 1; i < 12; i++) {
      ageArray.push({ id: i, value: i })
    }
    this.setState({ ageArray })
  }

  removeItem = (index) => {

    console.log("deleting item " + index)

    this.roomData.splice(index, 1);
    // var arr = [...this.state.roomData];
    // arr.splice(index, 1);
    // this.setState({
    //     roomData: arr
    // }, () => {  })
    this.totalRoomAndGuest()

  }


  incrementCount = (item, TYPE) => {


    console.log("increase item count")
    console.log(JSON.stringify(item))
    console.log(TYPE)
    if (TYPE === "ADULT") {
      if (item.adult < 4) {


        item.adult = item.adult + 1
        // this.setState({
        //     loading:!this.state.loading
        // })
        this.totalRoomAndGuest()
      } else {
        alert("Max 4 allowed")
      }
    } else if (TYPE === "CHILD") {
      if (item.children < 2) {

        item.children = item.children + 1
        if (!item.childrenage) {
          item.childrenage = []
          item.childrenage.push("10");
        } else {
          item.childrenage.push("10");
        }
        //item.childrenage.push("10");
        // childrenAgeItem.push("10");
        // item.childrenage =  childrenAgeItem;
        // this.setState({
        //     loading:!this.state.loading
        // })
        this.totalRoomAndGuest();
      } else {
        alert("Max 2 allowed")
      }
    }
  }

  changeAge(item, value,index) {
    //alert(value)
    item.childrenage[index] = value
    this.totalRoomAndGuest();

  }


  totalRoomAndGuest() {
    console.log("executing total room")
    let arr = this.roomData;
    console.log(JSON.stringify(arr));
    let totalGuest = 0;
    arr.forEach(element => {
      totalGuest = totalGuest + element.children + element.adult;
    });
    this.setState({
      roomCount: arr.length,
      GuestCount: totalGuest,
      loading: !this.state.loading
    })
    console.log("guest count" + this.state.GuestCount);
    console.log("room count" + this.state.roomCount);
  }

  decrementCount = (item, TYPE) => {

    console.log("decrease item count")
    console.log(JSON.stringify(item))
    console.log(TYPE)

    if (TYPE === "ADULT") {
      if (item.adult > 1) {

        item.adult = item.adult - 1;
        this.totalRoomAndGuest();
      }
    } else if (TYPE === "CHILD") {
      if (item.children > 0) {
        if (item.childrenage) {
          item.childrenage.splice(-1, 1)
        }
        // this.setState({
        //     GuestCount:this.state.GuestCount-1
        // })
        item.children = item.children - 1

        this.totalRoomAndGuest();
      }
    }
  }

  componentDidMount() {

  }

  _renderRoomView({ item, index }) {

    return (
      <View style={{ marginVertical: 2 }}>
        <View style={{ padding: 12, paddingHorizontal: 15, backgroundColor: colors.gray, flexDirection: "row", justifyContent: "space-between" }}>
          <Text style={commonstyle.blackText21}>  Room {index + 1}   </Text>
          {this.roomData.length != 1 && <TouchableOpacity
            style={{ width: 30 }}
            underlayColor="transparent"
            onPress={() => {
              this.removeItem(index);
            }}>
            <Image
              style={{ height: 18, width: 18 }}
              source={Images.ic_image_dustbin} />
          </TouchableOpacity>}
        </View>
        {console.log("index is " + index)}
        <View style={{ paddingBottom: 15 }}>
          {/* Adult View */}
          <View style={{ flexDirection: 'row', width: '100%', paddingHorizontal: 20, paddingTop: 15 }}>
            <View style={{ width: '60%', flexDirection: 'row', alignItems: 'center' }}>
              <Text style={{ fontFamily: Fonts.bold, color: colors.colorBlack21 }}>Adults</Text>
              <Text style={[{ fontFamily: Fonts.regular, color: colors.colorBlack21 }, { fontSize: 11, marginLeft: 5 }]}>(Above 12 years)</Text>
            </View>
            <View style={{ width: '40%', flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end' }}>
              <Counter
                onPressAdd={() => this.incrementCount(item, "ADULT")}
                onPressRemove={() => this.decrementCount(item, "ADULT")}
                count={item.adult} />
            </View>
          </View>

          {/* Child View */}

          <View style={{ flexDirection: 'row', width: '100%', paddingHorizontal: 20, paddingTop: 15 }}>
            <View style={{ width: '60%', flexDirection: 'row', alignItems: 'center' }}>
              <Text style={{ fontFamily: Fonts.bold, color: colors.colorBlack21 }}>Children</Text>
              <Text style={[{ fontFamily: Fonts.regular, color: colors.colorBlack21 }, { fontSize: 11, marginLeft: 5 }]}>(Below 12 years)</Text>
            </View>
            <View style={{ width: '40%', flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end' }}>
              <Counter
                onPressAdd={() => this.incrementCount(item, "CHILD")}
                onPressRemove={() => this.decrementCount(item, "CHILD")}
                count={item.children} />
            </View>
          </View>

          {item.children > 0 && <View style={{ flexDirection: 'row', width: '100%', paddingHorizontal: 20, paddingTop: 15 }}>
            <View style={{ width: '60%', flexDirection: 'row', alignItems: 'center' }}>
              <Text style={{ fontFamily: Fonts.bold, color: colors.colorBlack21 }}>Age Of Child 1</Text>
              <Text style={[{ fontFamily: Fonts.regular, color: colors.colorBlack21 }, { fontSize: 11, marginLeft: 5 }]}></Text>
            </View>
            <View style={{ width: '40%', flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end' }}>
              <View>
                <View style={{
                  borderColor: 'grey',
                  borderWidth: 1,
                  borderRadius: 5,
                  height: 34,
                  width:80,
                  flexDirection: 'row',
                  overflow: 'hidden'
                }}>
                  <Dropdown placeholder={"Country"}
                    placeholderTextColor={"grey"}
                    data={this.state.ageArray}
                    fontSize={12}
                    fontFamily={Fonts.regular}
                    value={item.childrenage[0]}
                    containerStyle={{
                      paddingRight: 10,
                      paddingLeft: 10,
                      width: "100%",
                    }}
                    inputContainerStyle={{
                      borderBottomColor: 'transparent',
                    }}
                    pickerStyle={{
                      width: "90%",
                      marginLeft: 10,
                      marginTop: 40,
                    }}
                    itemTextStyle={{ color: "grey", fontSize: 12, height: 34, fontFamily: Fonts.regular }}
                    itemColor={"grey"}
                    dropdownOffset={{ top: 8, left: 0 }}
                    onChangeText={(value, index) => {
                      this.changeAge(item, value,0)
                    }}
                  />
                </View>
              </View>
            </View>
          </View>}

          {item.children > 1 && <View style={{ flexDirection: 'row', width: '100%', paddingHorizontal: 20, paddingTop: 15 }}>
          <View style={{ width: '60%', flexDirection: 'row', alignItems: 'center' }}>
              <Text style={{ fontFamily: Fonts.bold, color: colors.colorBlack21 }}>Age Of Child 2</Text>
              <Text style={[{ fontFamily: Fonts.regular, color: colors.colorBlack21 }, { fontSize: 11, marginLeft: 5 }]}></Text>
            </View>
            <View style={{ width: '40%', flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end' }}>
              <View>
                <View style={{
                  borderColor: 'grey',
                  borderWidth: 1,
                  borderRadius: 5,
                  height: 34,
                  width:80,
                  flexDirection: 'row',
                  overflow: 'hidden'
                }}>
                  <Dropdown placeholder={"Country"}
                    placeholderTextColor={"grey"}
                    data={this.state.ageArray}
                    fontSize={12}
                    fontFamily={Fonts.regular}
                    value={item.childrenage[1]}
                    containerStyle={{
                      paddingRight: 10,
                      paddingLeft: 10,
                      width: "100%",
                    }}
                    inputContainerStyle={{
                      borderBottomColor: 'transparent',
                    }}
                    pickerStyle={{
                      width: "90%",
                      marginLeft: 10,
                      marginTop: 40,
                    }}
                    itemTextStyle={{ color: "grey", fontSize: 12, height: 34, fontFamily: Fonts.regular }}
                    itemColor={"grey"}
                    dropdownOffset={{ top: 8, left: 0 }}
                    onChangeText={(value, index) => {
                      this.changeAge(item, value,1)
                    }}
                  />
                </View>
              </View>
            </View>
          </View>}
        </View>
      </View>
    )
  }


  render() {

    const roomDict = {
      adult: 1,
      children: 0,
      childrenage: []
    }


    return (
      <View style={{ flex: 1 }}>
        <SafeAreaView backgroundColor={colors.colorBlue} />
        <View style={styles.header}>
          <TouchableOpacity style={styles.headerBtn} onPress={() => {
            this.roomData = JSON.parse(JSON.stringify(this.props.roomData));
            this.setState({
              loading: !this.state.loading,
              roomCount: this.props.roomCount,
              GuestCount: this.props.guestCount,
            }, () => { this.props.onModalClose(this.roomData, this.state.roomCount, this.state.GuestCount); })
          }}>
            <Image source={Images.ic_image_close} style={{ width: 20, height: 20, resizeMode: "contain" }}></Image>
          </TouchableOpacity>
          <View style={styles.headerTitle}>
            <Text style={styles.titleText}>  {this.state.roomCount}  {this.state.roomCount > 1 ? "Rooms" : "Room"}  {this.state.GuestCount} {this.state.GuestCount > 1 ? "Guests" : "Guest"}</Text>
          </View>
          <TouchableOpacity style={styles.headerBtn} onPress={() => {
            this.props.onModalClose(this.roomData, this.state.roomCount, this.state.GuestCount);
          }}>
            <Text style={{
              fontSize: 14,
              fontFamily: Fonts.medium,
              color: colors.colorBlue
            }}>Done</Text>
          </TouchableOpacity>
        </View>
        <ScrollView style={styles.container} >
          {/* {this._renderRoomView()} */}
          <View style={{}}>
            <View>
              <FlatList
                data={this.roomData}
                extraData={this.state.loading}
                renderItem={(item, index) => this._renderRoomView(item, index)}
              />
            </View>


            <View style={{ paddingVertical: 20, alignItems: 'center', justifyContent: 'center' }}>
              <Dash
                style={{ position: 'absolute', left: 0, right: 0 }}
                dashGap={4}
                dashLength={5}
                dashThickness={0.8}
                dashColor={colors.colorGrey} />
              <View style={{ backgroundColor: 'white', }}>
                <TouchableOpacity onPress={() => {
                  this.roomData.push(roomDict);
                  this.totalRoomAndGuest()
                  this.setState({ loading: !this.state.loading })
                }} style={{ flexDirection: 'row', padding: 5, alignItems: 'center' }}>
                  <View style={{ width: 25, height: 25, borderRadius: 15, alignItems: 'center', justifyContent: 'center', backgroundColor: colors.colorBlue, overflow: 'hidden' }}>
                    <Image style={{ width: 25, height: 25 }} source={Images.ic_image_minus} />
                  </View>
                  <Text style={{ marginLeft: 10, fontFamily: Fonts.semiBold, color: colors.colorBlue, fontSize: 14 }}>ADD ROOM</Text>
                </TouchableOpacity>
              </View>


            </View>

          </View>
        </ScrollView>
      </View>
    )
  }
}
const styles = StyleSheet.create({

  container: {
    flex: 1,
    backgroundColor: "white"
  },
  itemContainer: {
    marginTop: 5,
    marginBottom: 5,
    marginRight: 10,
    marginLeft: 10,
    backgroundColor: 'white',
    shadowColor: "gray",
    shadowOpacity: 0.5,
    shadowOffset: { width: 1, height: 1 },
    shadowRadius: 5,
    borderRadius: 10,
    elevation: 4
  },
  addButtonStyle: {
    height: 40,
    paddingLeft: 10,
    paddingRight: 10,
    backgroundColor: colors.colorBlue,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'flex-end',
    margin: 10,
    elevation: 4,
    borderRadius: 7
  },
  header: {
    height: 50,
    width: "100%",
    backgroundColor: colors.gray,
    flexDirection: "row",
    alignItems: "center"
  },
  headerBtn: {
    width: 50,
    height: "100%",
    alignItems: "center",
    justifyContent: "center",
    // backgroundColor: "red"
  },
  headerTitle: {
    width: screenWidth - 110,
    height: 50,
    justifyContent: "center",
    alignItems: "center"
  },
  titleText: {
    fontSize: 14,
    fontFamily: Fonts.medium,
    color: colors.colorBlack
  },
})
