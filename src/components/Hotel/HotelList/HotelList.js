import moment from 'moment';
import React, { Component } from 'react';
import { DeviceEventEmitter, Dimensions, FlatList, Image, Modal, SafeAreaView, StatusBar, Text, TextInput, TouchableOpacity, View } from 'react-native';
import { connect } from 'react-redux';
import RangeSlider from 'rn-range-slider';
import { getHotelListAction, setLoadingAction } from '../../../Redux/Actions';
import { colors } from '../../../Utils/Colors';
import { commonstyle } from '../../../Utils/Commonstyle';
import Fonts from '../../../Utils/Fonts';
import Images from '../../../Utils/Images';
import NavigationServices from '../../../Utils/NavigationServices';
import StringConstants from '../../../Utils/StringConstants';
import CommonHeader from '../../custom/CommonHeader';
import RatingBar from '../../custom/RatingBar';
import HotelFilters from '../HotelFilters';
import HotelItemElement from './HotelItemElement';
import styles from './Style';

const screenHeight = Dimensions.get('window').height;
const screenWidth = Dimensions.get('window').height;
const screen = Dimensions.get('window');


const ASPECT_RATIO = screen.width / screen.height;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

class HotelList extends Component {
  constructor(props) {
    super(props);
    this.mapViewRef = null;
    this.state = {
      // selectedIndex: 0,
      hotelsListData: null,
      sortingStatus: 0,
      searchList: [],
      isSearchActive: false,
      loading: false,
      selectedDict: null,
      searchStr: '',
      is1starSelected: false,
      is2starSelected: false,
      is3starSelected: false,
      is4starSelected: false,
      is5starSelected: false,
      isFilterActive: false,
      filterData: [],
      showFilter: false,
      selectedMinPrice: null,
      selectedMaxPrice: null,
      minPrice: null,
      maxPrice: null,
      source: {
        source: '',
        traceId: '',
      },
      currencycode: null,
      // hotelSearchParam:[],
      // mapRegion: {
      //   latitude: 37.78825,
      //   longitude: -122.4324,
      //   latitudeDelta: LATITUDE_DELTA,
      //   longitudeDelta: LONGITUDE_DELTA
      // }
    };
    this.minimumselPrice = 0;
    this.maximumselPrice = 0;
    this.getHotelsList = this.getHotelsList.bind(this);
    this.hotelsList = this.hotelsList.bind(this);
    this.updateHotelListFromApi = this.updateHotelListFromApi.bind(this);
  }

  // --------------- component Life cycle methods ------------

  componentDidMount() {
    DeviceEventEmitter.addListener(
      StringConstants.HOTEL_LIST_EVENT,
      this.updateHotelListFromApi,
    );
    this.getHotelsList();
    // this.updateHotelListFromApi(HotelListData)
  }

  componentWillUnmount() {
    DeviceEventEmitter.removeListener(
      StringConstants.HOTEL_LIST_EVENT,
      this.updateHotelListFromApi,
    );
  }

  updateHotelListFromApi(res) {
    let min, max;
    if (res && res.results) {
      res.results.forEach((item, index) => {
        if (index == 0) {
          min = item.priceDetail.agencyFare;
          max = item.priceDetail.agencyFare;
        } else {
          if (item.priceDetail.agencyFare < min) {
            min = item.priceDetail.agencyFare;
          }
          if (item.priceDetail.agencyFare > max) {
            max = item.priceDetail.agencyFare;
          }
        }
      });
    }

    //  console.log("1121"+JSON.stringify(res.results))
    //  console.log("1121"+JSON.stringify(sort1))
    //  console.log("1121"+JSON.stringify(sort2))

    this.setState({
      hotelsListData: res.results,
      source: res.source,
      minPrice: min,
      maxPrice: max,
      selectedMinPrice: min,
      selectedMaxPrice: max,
      currencycode: res.results[0].priceDetail.CurrencyCode,
    });
  }

  //--------------- API Calling Methods ----------------
  getHotelsList() {
    let dict = this.props.hotelSearchParams;

    if (dict != null && dict.roomData != null) {
      console.log('get hotel list room data ' + dict.roomData);

      // this.setState({
      //   hotelSearchParam: dict
      // });

      let param = {
        keyword: dict.name + ', ' + dict.country,
        type_id: dict.id,
        type: 'city',
        check_in: this.getDate(dict.checkIn),
        check_out: this.getDate(dict.checkOut),
        room: dict.roomData,
        nationality: dict.nationality,
      };

      this.props.getHotelListAction(param);
    } else {
      console.log('wrong props receive ' + JSON.stringify(dict));
    }
  }

  onFilterSelection = data => {
    this.setState({ showFilter: false }, () => {
      this.props.setLoadingAction(true);
    });
    if (data) {
      this.setState({
        is1starSelected: data.is1starSelected,
        is2starSelected: data.is2starSelected,
        is3starSelected: data.is3starSelected,
        is4starSelected: data.is4starSelected,
        is5starSelected: data.is5starSelected,
        selectedMinPrice: data.selectedMinPrice,
        selectedMaxPrice: data.selectedMaxPrice,
        sortingStatus: data.sortingStatus,
      });

      const {
        is1starSelected,
        is2starSelected,
        is3starSelected,
        is4starSelected,
        is5starSelected,
        selectedMinPrice,
        selectedMaxPrice,
        minPrice,
        maxPrice,
        sortingStatus,
      } = data;


      if (
        is1starSelected ||
        is2starSelected ||
        is3starSelected ||
        is4starSelected ||
        is5starSelected ||
        selectedMinPrice ||
        selectedMaxPrice ||
        sortingStatus != 0
      ) {
        this.setState({ isFilterActive: true });
        // filter data Item
        console.log("filteredData:", JSON.stringify(data))

        let data = this.state.hotelsListData;
        if (
          is1starSelected ||
          is2starSelected ||
          is3starSelected ||
          is4starSelected ||
          is5starSelected
        ) {
          data = this.state.hotelsListData.filter((item, index) => {
            if (is1starSelected) {
              if (item.star == 1) {
                return true;
              }
            }

            if (is2starSelected) {
              if (item.star == 2) {
                return true;
              }
            }

            if (is3starSelected) {
              // console.log("3 STAR ITEM1:"+JSON.stringify(item))
              if (item.star == 3) {
                // console.log("3 STAR ITEM2:"+JSON.stringify(item))
                return true;
              }
            }

            if (is4starSelected) {
              if (item.star == 4) {
                return true;
              }
            }

            if (is5starSelected) {
              if (item.star == 5) {
                return true;
              }
            }
          });
        }

        data = data.filter((item, index) => {
          // console.log("PRICE_ITEM1:"+JSON.stringify(item))
          if (
            item.priceDetail.agencyFare >= selectedMinPrice &&
            item.priceDetail.agencyFare <= selectedMaxPrice
          ) {
            // console.log("PRICE_ITEM2:"+JSON.stringify(item))
            return true;
          }
        });

        switch (sortingStatus) {
          case 0:
            data.sort(function (a, b) {
              return a.priceDetail.agencyFare - b.priceDetail.agencyFare;
            });
            break;
          case 1:
            data.sort(function (a, b) {
              return a.priceDetail.agencyFare - b.priceDetail.agencyFare;
            });
            break;
          case 2:
            data.sort(function (a, b) {
              return b.priceDetail.agencyFare - a.priceDetail.agencyFare;
            });
            break;
          case 3:
            data.sort(function (a, b) {
              return b.star - a.star;
            });
            break;
          case 4:
            data.sort(function (a, b) {
              return b.star - a.star;
            });
            break;
        }

        this.setState(
          {
            filterData: data,
            loading: !this.state.loading,
          },
          () => this.props.setLoadingAction(false),
        );
        console.log(data);
      }
    }
    this.setState({ loading: !this.state.loading }, () => {
      this.props.setLoadingAction(false);
    });
  };

  getDate(date) {
    //return dateFormat(date, "yyyy-mm-dd");
    return moment(date).format('YYYY-MM-DD');
  }
  getDate2(date) {
    //return dateFormat(date, "yyyy-mm-dd");
    return moment(date).format('DD MMM');
  }

  hotelsList() {
    let flatListData = this.state.isSearchActive ? this.state.searchList : this.state.isFilterActive ? this.state.filterData : this.state.hotelsListData

    if(flatListData && flatListData.length==0){
      return (
        <View style={{ flex: 1, marginTop: 0,alignItems:'center',justifyContent:'center' }}>
          {/* {this.searchComponent()} */}
          <Text style={{fontSize:16,fontFamily:Fonts.bold,color:colors.colorBlue}}>No Hotel Available</Text>
        </View>
      );
    }
    return (
      <View style={{ flex: 1, marginTop: 0 }}>
        {/* {this.searchComponent()} */}
        <FlatList
          // maxToRenderPerBatch={10}
          initialNumToRender={20}
          windowSize={11}
          removeClippedSubviews={true}
          showsVerticalScrollIndicator={false}
          showsHorizontalScrollIndicator={false}
          style={{ flex: 1 }}
          data={flatListData}
          renderItem={({ item }) => (
            <HotelItemElement
              hotel={item}
              fullDetail={false}
              onPress={() => {
                this.openHotel(item);
              }}
            />
          )}
          keyExtractor={item => item.index}
          extraData={this.state.loading}
        />
      </View>
    );
  }
  _renderItem = ({ item }) => (
    <TouchableOpacity
      activeOpacity={0.6}
      onPress={() => {
        // alert(JSON.stringify(item))

        // let dict = {
        //   id: item.code,
        //   index: item.index,
        //   source: this.state.source.source,
        //   traceId: this.state.source.traceId,
        //   currencycode: item.priceDetail.currencyCode,
        //   price: item.priceDetail.agencyFare
        // }

        let dict = {
          code: item.code,
          id: item.index.toString(),
          source: this.state.source.source,
          traceId: this.state.source.traceId,
          currencycode: item.priceDetail.CurrencyCode,
          price: Math.ceil(item.priceDetail.agencyFare),
          rooms: this.props.hotelSearchParams.numberOfRooms,
        };

        console.log('DICT', JSON.stringify(dict));

        NavigationServices.navigate('HotelOverview', { jsonDict: dict });
      }}>
      <View>
        <Image
          source={{ uri: item.photo }}
          style={styles.imageStyle}
          resizeMode="cover"
        />
        <View style={styles.imageOverlayContent}>
          <View style={[styles.imageInnerContent, { width: '70%' }]}>
            <Text style={styles.HotelTitleText}>{item.name}</Text>

            <View style={{ height: '100%' }}>
              <View style={styles.RatinBarStyle}>
                <RatingBar count={item.star} starsize={12} />
              </View>
              <Text
                numberOfLines={3}
                ellipsizeMode={'tail'}
                style={styles.addressStyle}>
                {item.address}
              </Text>
            </View>
          </View>
          <View style={styles.priceBtnContainer}>
            <View style={{ flexDirection: 'row', justifyContent: 'flex-end' }}>
              <Text
                style={{
                  fontFamily: Fonts.semiBold,
                  color: colors.colorBlue,
                  fontSize: 18,
                }}>
                {hotel.priceDetail && hotel.priceDetail.CurrencyCode == 'INR'
                  ? '₹ '
                  : null}
              </Text>
              <Text
                style={{
                  fontFamily: Fonts.semiBold,
                  color: colors.black,
                  fontSize: 18,
                }}>
                {hotel.priceDetail && hotel.priceDetail.agencyFare
                  ? hotel.priceDetail.agencyFare
                  : null}
              </Text>
            </View>
            {/* <Text style={styles.hotelPricedesc} adjustsFontSizeToFit={true}>
                {item.hotel.description}
              </Text> */}
          </View>
        </View>
      </View>
    </TouchableOpacity>
  );

  searchHotel() {
    if (String(this.state.searchStr).trim().length == 0) {
      return;
    }

    let data;
    if (this.state.isFilterActive) {
      data = this.state.filterData.filter(dict => {
        let name = String(dict.name);
        return name
          .toLocaleLowerCase()
          .startsWith(this.state.searchStr.toLocaleLowerCase());
      });
    } else {
      data = this.state.hotelsListData.filter(dict => {
        let name = String(dict.name);
        return name
          .toLocaleLowerCase()
          .startsWith(this.state.searchStr.toLocaleLowerCase());
      });
    }

    this.setState({
      searchList: data,
      // searchStr: str,
      isSearchActive: true,
      loading: !this.state.loading,
    });
  }

  // onChangeText(str) {
  //   if (String(str).trim().length == 0) {
  //     this.setState({
  //       searchStr: str
  //     });
  //     return;
  //   }

  //   let data;
  //   if(this.state.isFilterActive){
  //     data = this.state.filterData.filter(dict => {
  //       let name = String(dict.hotel.name);
  //       return name.toLocaleLowerCase().startsWith(str.toLocaleLowerCase());
  //     });
  //   }else{
  //     data = this.state.hotelsListData.filter(dict => {
  //       let name = String(dict.hotel.name);
  //       return name.toLocaleLowerCase().startsWith(str.toLocaleLowerCase());
  //     });
  //   }

  //   this.setState({
  //     searchList: data,
  //     searchStr: str,
  //     isSearchActive: true,
  //     loading: !this.state.loading
  //   });
  // }

  searchComponent() {
    return (
      <View style={styles.hotelSearchContainer}>
        <View style={styles.hotelSearch}>
          <View
            style={{
              width: '20%',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Image
              style={{ height: 30, width: 30 }}
              source={Images.ic_search}
              resizeMode={'contain'}
            />
          </View>

          <TextInput
            placeholder={'Hotel Name'}
            placeholderTextColor={'rgb(112,128,144)'}
            autoCorrect={false}
            style={{ fontSize: 16 }}
            value={this.state.searchStr}
            onChangeText={text => this.setState({ searchStr: text })}
            onEndEditing={() => {
              if (this.state.searchStr.trim().length == 0) {
                this.setState({
                  isSearchActive: false,
                  searchList: [],
                  loading: !this.state.loading,
                });
              }
            }}
          />
        </View>

        {/* <View style={{ width: "15%" }}> */}
        <TouchableOpacity
          style={styles.goButton}
          underlayColor="transparent"
          onPress={() => {
            this.searchHotel();
            //  Actions.CityHotel()
          }}>
          <Text
            style={{
              color: 'white',
              fontSize: 16,
              fontFamily: 'Montserrat-SemiBold',
            }}>
            Go
          </Text>
        </TouchableOpacity>
        {/* </View> */}
      </View>
    );
  }

  // searchComponentzForMap() {
  //   return (
  //     <View
  //       style={[
  //         styles.hotelSearchContainer,
  //         {
  //           backgroundColor: "transparent",
  //           position: "absolute",
  //           width: "100%",
  //           marginTop: 20
  //         }
  //       ]}
  //     >
  //       <View style={[styles.hotelSearch, { marginLeft: 10 }]}>
  //         <View
  //           style={{
  //             width: "20%",
  //             justifyContent: "center",
  //             alignItems: "center"
  //           }}
  //         >
  //           <Image
  //             style={{ height: 30, width: 30 }}
  //             source={Images.ic_search}
  //             resizeMode={"contain"}
  //           />
  //         </View>
  //         <View style={{ width: "80%" }}>
  //           <TextInput
  //             placeholder={"Hotel Name"}
  //             placeholderTextColor={"rgb(112,128,144)"}
  //             autoCorrect={false}
  //             style={{ fontSize: 16 }}
  //             value={this.state.searchStr}
  //             onChangeText={text => this.setState({searchStr:text})}
  //             onEndEditing={() => {
  //               if (this.state.searchStr.trim().length == 0) {
  //                 this.setState({
  //                   isSearchActive: false,
  //                   searchList: [],
  //                   loading: !this.state.loading
  //                 });
  //               }
  //             }}
  //           />
  //         </View>
  //       </View>

  //       <View style={{ width: "15%" }}>
  //         <TouchableOpacity
  //           style={styles.goButton}
  //           underlayColor="transparent"
  //           onPress={() => {
  //             this.searchHotel()
  //             //  Actions.CityHotel()
  //           }}
  //         >
  //           <Text style={{ color: "white", fontSize: 16, fontWeight: "500" }}>
  //             Go
  //           </Text>
  //         </TouchableOpacity>
  //       </View>
  //     </View>
  //   );
  // }

  _renderNavigationBar = () => {
    // const {destination ,origin,return_date,departure_date} = this.props.flightSearchParams;
    return (
      <View style={styles.styleNavBar}>
        <View style={styles.styleNavBarSubView}>
          <TouchableOpacity
            style={styles.styleLeftBtn}
            underlayColor="transparent"
            onPress={() => {
              NavigationServices.goBack();
            }}>
            <Image
              style={{ height: 25, width: 25 }}
              resizeMode="contain"
              source={Images.ic_image_back}
            />
          </TouchableOpacity>
          

          <View style={[styles.styleNavBarRightView]}>
            <TouchableOpacity
              style={{
                width: 30,
                marginRight: 10,
              }}
              underlayColor="transparent"
              onPress={() => {
                this.setState({
                  showFilter: true,
                });
              }}>
              <Image
                style={{ height: 20, width: 20, marginLeft: 5 }}
                source={Images.ic_image_filter}
                resizeMode="contain"
              />
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  };
  openHotel(item) {
    let dict = {
      code: item.code,
      id: item.index.toString(),
      source: this.state.source.source.toUpperCase(),
      traceId: this.state.source.traceId,
      currencycode: item.priceDetail.CurrencyCode,
      price: Math.ceil(item.priceDetail.agencyFare),
      rooms: this.props.hotelSearchParams.numberOfRooms,
    };
    // alert(JSON.stringify(dict))
    console.log('DICT', JSON.stringify(dict));

    NavigationServices.navigate('HotelOverview', { jsonDict: dict });
  }
  renderMiddle() {
    let param = this.props.hotelSearchParams;
    if (param)
      // alert(JSON.stringify(param))
      return (
        <View style={styles.headerContent}>
          <View style={styles.headerContentRow}>
            <Text style={styles.headerStnName}>{param.name}</Text>
          </View>
          <View style={styles.headerContentRow}>
            <Text style={styles.headerContentRowText}>
              {this.getDate2(Date.parse(param.checkIn)) +
                '-' +
                this.getDate2(Date.parse(param.checkOut))}
            </Text>
            <View style={styles.headerRowDevider}></View>
            <Text style={styles.headerContentRowText}>
              {param.numberOfRooms +
                (param.numberOfRooms > 1 ? ' Rooms' : ' Room')}
            </Text>
            <View style={styles.headerRowDevider}></View>
            <Text style={styles.headerContentRowText}>
              {param.numberOfGuest +
                (param.numberOfGuest > 1 ? ' Guests' : ' Guest')}{' '}
            </Text>
          </View>
        </View>
      );
  }
  renderFilterModelOld() {
    return (
      <Modal
        animationType="slide"
        transparent={false}
        visible={this.state.showFilter}>
        <View
          style={[commonstyle.itemBackground, { borderRadius: 2, marginTop: 5 }]}>
          <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
            <Text style={[commonstyle.blackText, { fontSize: 15 }]}>Sorting</Text>
            <TouchableOpacity
              underlayColor={'transparent'}
              onPress={() => {
                this.setState({
                  sortingStatus: 0,
                });
              }}>
              <Text
                style={{
                  fontFamily: 'Montserrat-SemiBold',
                  fontSize: 15,
                  color: colors.colorBlue,
                }}>
                {' '}
                Reset{' '}
              </Text>
            </TouchableOpacity>
          </View>
          <View
            style={{
              marginTop: 25,
              flexDirection: 'row',
              justifyContent: 'space-evenly',
              marginBottom: 10,
            }}>
            <TouchableOpacity
              underlayColor={'transparent'}
              onPress={() => {
                this.setState({
                  sortingStatus: 1,
                });
              }}>
              <View style={{ alignItems: 'center' }}>
                <View
                  style={{
                    padding: 5,
                    width: 50,
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}>
                  <Image
                    style={{
                      width: 18,
                      height: 18,
                      tintColor:
                        this.state.sortingStatus == 1
                          ? colors.colorBlue
                          : '#626262',
                    }}
                    source={Images.icon_low}
                    resizeMode={'contain'}
                  />
                </View>
                <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                  <Text
                    style={{
                      fontFamily: Fonts.regular,
                      textAlign: 'center',
                      fontSize: 10,
                      color:
                        this.state.sortingStatus == 1
                          ? colors.colorBlue
                          : '#626262',
                    }}>
                    {'Price\nLow To High'}
                  </Text>
                </View>
              </View>
            </TouchableOpacity>
            <View style={{ width: 1, height: 40, backgroundColor: 'grey' }} />

            <TouchableOpacity
              underlayColor={'transparent'}
              onPress={() => {
                this.setState({
                  sortingStatus: 2,
                });
              }}>
              <View style={{ alignItems: 'center' }}>
                <View
                  style={{
                    padding: 5,
                    width: 50,
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}>
                  <Image
                    style={{
                      width: 18,
                      height: 18,
                      tintColor:
                        this.state.sortingStatus == 2
                          ? colors.colorBlue
                          : '#626262',
                    }}
                    source={Images.icon_high}
                    resizeMode={'contain'}
                  />
                </View>
                <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                  <Text
                    style={{
                      fontFamily: Fonts.regular,
                      textAlign: 'center',
                      fontSize: 10,
                      color:
                        this.state.sortingStatus == 2
                          ? colors.colorBlue
                          : '#626262',
                    }}>
                    {'Price\nHigh To Low'}
                  </Text>
                </View>
              </View>
            </TouchableOpacity>
            <View style={{ width: 1, height: 40, backgroundColor: 'grey' }} />

            <TouchableOpacity
              underlayColor={'transparent'}
              onPress={() => {
                this.setState({
                  sortingStatus: 3,
                });
              }}>
              <View style={{ alignItems: 'center' }}>
                <View
                  style={{
                    padding: 5,
                    width: 50,
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}>
                  <Image
                    style={{
                      width: 18,
                      height: 18,
                      tintColor:
                        this.state.sortingStatus == 3
                          ? colors.colorBlue
                          : '#626262',
                    }}
                    source={Images.icon_rating}
                    resizeMode={'contain'}
                  />
                </View>
                <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                  <Text
                    style={{
                      fontFamily: Fonts.regular,
                      textAlign: 'center',
                      fontSize: 10,
                      color:
                        this.state.sortingStatus == 3
                          ? colors.colorBlue
                          : '#626262',
                    }}>
                    {'User\nRating'}
                  </Text>
                </View>
              </View>
            </TouchableOpacity>
            <View style={{ width: 1, height: 40, backgroundColor: 'grey' }} />

            <TouchableOpacity
              underlayColor={'transparent'}
              onPress={() => {
                this.setState({
                  sortingStatus: 4,
                });
              }}>
              <View style={{ alignItems: 'center' }}>
                <View
                  style={{
                    padding: 5,
                    width: 50,
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}>
                  <Image
                    style={{
                      width: 18,
                      height: 18,
                      tintColor:
                        this.state.sortingStatus == 4
                          ? colors.colorBlue
                          : '#626262',
                    }}
                    source={Images.star}
                    resizeMode={'contain'}
                  />
                </View>
                <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                  <Text
                    style={{
                      fontFamily: Fonts.regular,
                      textAlign: 'center',
                      fontSize: 10,
                      color:
                        this.state.sortingStatus == 4
                          ? colors.colorBlue
                          : '#626262',
                    }}>
                    {'Hotel\nPopularity'}
                  </Text>
                </View>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    );
  }
  renderFilterModel() {
    if (this.state.maxPrice && this.state.minPrice)
      return (
        <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.showFilter}>
          <HotelFilters
            selectedFilters={{
              selectedMinPrice: this.state.selectedMinPrice,
              selectedMaxPrice: this.state.selectedMaxPrice,
              minPrice: this.state.minPrice,
              maxPrice: this.state.maxPrice,
              currencycode: this.state.currencycode,
              sortingStatus: this.state.sortingStatus,
              is1starSelected: this.state.is1starSelected,
              is2starSelected: this.state.is2starSelected,
              is3starSelected: this.state.is3starSelected,
              is4starSelected: this.state.is4starSelected,
              is5starSelected: this.state.is5starSelected,
            }}
            onFilterSelection={this.onFilterSelection}
          />
        </Modal>
      );
  }

  renderRangeSlider() {
    const {
      minPrice,
      maxPrice,
      selectedMaxPrice,
      selectedMinPrice,
      currencycode,
    } = this.state;
    let min = 0;
    let max = 10000;
    let initialLowValue = 0;
    let initialHighValue = 10000;
    let radioValue = 10;

    // if (minPrice) {
    //   min = parseInt(minPrice)
    //   initialLowValue = parseInt(minPrice)
    // }
    if (maxPrice) {
      max = parseInt(maxPrice);
      initialHighValue = parseInt(maxPrice);
    }
    if (selectedMinPrice) {
      initialLowValue = parseInt(selectedMinPrice);
    }
    if (selectedMaxPrice) {
      initialHighValue = parseInt(selectedMaxPrice);
    }

    if (selectedMinPrice != null && selectedMaxPrice != null) {
      if (selectedMinPrice < 1000 && selectedMaxPrice == 1000) {
        radioValue = 1;
      }
      if (selectedMinPrice == 1000 && selectedMaxPrice == 2000) {
        radioValue = 2;
      }
      if (selectedMinPrice == 2000 && selectedMaxPrice == 3000) {
        radioValue = 3;
      }
      if (selectedMinPrice == 3000 && selectedMaxPrice == 4000) {
        radioValue = 4;
      }
      if (selectedMinPrice == 4000 && selectedMaxPrice > 4000) {
        radioValue = 5;
      }
    }
    // console.log("selectedMinPrice" + selectedMinPrice)
    // console.log("selectedMaxPrice" + selectedMaxPrice)
    // console.log("RadioValue" + radioValue)
    //alert(initialHighValue)
    if (maxPrice)
      return (
        <View style={{ alignItems: 'center', marginBottom: 10 }}>
          <View
            style={{ width: '100%', paddingHorizontal: 15, paddingVertical: 10 }}>
            <TouchableOpacity
              style={{ flexDirection: 'row', alignItems: 'center' }}
              onPress={() => {
                this.changePriceRangeRadio(1);
              }}>
              <View
                style={{
                  width: 20,
                  height: 20,
                  borderRadius: 10,
                  alignItems: 'center',
                  justifyContent: 'center',
                  borderColor: colors.colorBlue,
                  borderWidth: 1,
                }}>
                <View
                  style={{
                    width: 10,
                    height: 10,
                    borderRadius: 5,
                    backgroundColor:
                      radioValue == 1 ? colors.colorBlue : 'white',
                  }}
                />
              </View>
              <View style={{ paddingHorizontal: 15, alignItems: 'flex-start' }}>
                <Text
                  style={{
                    fontFamily: Fonts.medium,
                    color: colors.colorBlack21,
                    fontSize: 13,
                  }}>
                  Less Than {currencycode && currencycode + ' '}1000
                </Text>
              </View>
            </TouchableOpacity>
          </View>

          <View
            style={{ width: '100%', paddingHorizontal: 15, paddingVertical: 10 }}>
            <TouchableOpacity
              style={{ flexDirection: 'row', alignItems: 'center' }}
              onPress={() => {
                this.changePriceRangeRadio(2);
              }}>
              <View
                style={{
                  width: 20,
                  height: 20,
                  borderRadius: 10,
                  alignItems: 'center',
                  justifyContent: 'center',
                  borderColor: colors.colorBlue,
                  borderWidth: 1,
                }}>
                <View
                  style={{
                    width: 10,
                    height: 10,
                    borderRadius: 5,
                    backgroundColor:
                      radioValue == 2 ? colors.colorBlue : 'white',
                  }}
                />
              </View>
              <View style={{ paddingHorizontal: 15, alignItems: 'flex-start' }}>
                <Text
                  style={{
                    fontFamily: Fonts.medium,
                    color: colors.colorBlack21,
                    fontSize: 13,
                  }}>
                  {currencycode && currencycode + ' '}1000 -{' '}
                  {currencycode && currencycode + ' '}2000
                </Text>
              </View>
            </TouchableOpacity>
          </View>

          <View
            style={{ width: '100%', paddingHorizontal: 15, paddingVertical: 10 }}>
            <TouchableOpacity
              style={{ flexDirection: 'row', alignItems: 'center' }}
              onPress={() => {
                this.changePriceRangeRadio(3);
              }}>
              <View
                style={{
                  width: 20,
                  height: 20,
                  borderRadius: 10,
                  alignItems: 'center',
                  justifyContent: 'center',
                  borderColor: colors.colorBlue,
                  borderWidth: 1,
                }}>
                <View
                  style={{
                    width: 10,
                    height: 10,
                    borderRadius: 5,
                    backgroundColor:
                      radioValue == 3 ? colors.colorBlue : 'white',
                  }}
                />
              </View>
              <View style={{ paddingHorizontal: 15, alignItems: 'flex-start' }}>
                <Text
                  style={{
                    fontFamily: Fonts.medium,
                    color: colors.colorBlack21,
                    fontSize: 13,
                  }}>
                  {currencycode && currencycode + ' '}2000 -{' '}
                  {currencycode && currencycode + ' '}3000
                </Text>
              </View>
            </TouchableOpacity>
          </View>

          <View
            style={{ width: '100%', paddingHorizontal: 15, paddingVertical: 10 }}>
            <TouchableOpacity
              style={{ flexDirection: 'row', alignItems: 'center' }}
              onPress={() => {
                this.changePriceRangeRadio(4);
              }}>
              <View
                style={{
                  width: 20,
                  height: 20,
                  borderRadius: 10,
                  alignItems: 'center',
                  justifyContent: 'center',
                  borderColor: colors.colorBlue,
                  borderWidth: 1,
                }}>
                <View
                  style={{
                    width: 10,
                    height: 10,
                    borderRadius: 5,
                    backgroundColor:
                      radioValue == 4 ? colors.colorBlue : 'white',
                  }}
                />
              </View>
              <View style={{ paddingHorizontal: 15, alignItems: 'flex-start' }}>
                <Text
                  style={{
                    fontFamily: Fonts.medium,
                    color: colors.colorBlack21,
                    fontSize: 13,
                  }}>
                  {currencycode && currencycode + ' '}3000 -{' '}
                  {currencycode && currencycode + ' '}4000
                </Text>
              </View>
            </TouchableOpacity>
          </View>

          <View
            style={{ width: '100%', paddingHorizontal: 15, paddingVertical: 10 }}>
            <TouchableOpacity
              style={{ flexDirection: 'row', alignItems: 'center' }}
              onPress={() => {
                this.changePriceRangeRadio(5);
              }}>
              <View
                style={{
                  width: 20,
                  height: 20,
                  borderRadius: 10,
                  alignItems: 'center',
                  justifyContent: 'center',
                  borderColor: colors.colorBlue,
                  borderWidth: 1,
                }}>
                <View
                  style={{
                    width: 10,
                    height: 10,
                    borderRadius: 5,
                    backgroundColor:
                      radioValue == 5 ? colors.colorBlue : 'white',
                  }}
                />
              </View>
              <View style={{ paddingHorizontal: 15, alignItems: 'flex-start' }}>
                <Text
                  style={{
                    fontFamily: Fonts.medium,
                    color: colors.colorBlack21,
                    fontSize: 13,
                  }}>
                  {currencycode && currencycode + ' '}4000 and above
                </Text>
              </View>
            </TouchableOpacity>
          </View>

          <View style={{ width: '100%', padding: 15, flexDirection: 'row' }}>
            <View
              style={{
                width: '50%',
                paddingHorizontal: 15,
                alignItems: 'flex-start',
              }}>
              <Text
                style={{
                  fontFamily: Fonts.medium,
                  color: colors.colorBlack21,
                  fontSize: 13,
                }}>
                MIN: {currencycode && currencycode}{' '}
                {selectedMinPrice ? selectedMinPrice : initialLowValue}
              </Text>
            </View>
            <View
              style={{
                width: '50%',
                paddingHorizontal: 15,
                alignItems: 'flex-end',
              }}>
              <Text
                style={{
                  fontFamily: Fonts.medium,
                  color: colors.colorBlack21,
                  fontSize: 13,
                }}>
                MAX: {currencycode && currencycode}{' '}
                {selectedMaxPrice ? selectedMaxPrice : initialHighValue}
              </Text>
            </View>
          </View>
          <RangeSlider
            ref={component => (this._rangeSlider = component)}
            style={{ width: Dimensions.get('window').width - 40, height: 60 }}
            gravity={'center'}
            min={min}
            //style={{ backgroundColor: 'red' }}
            max={max}
            step={1}
            textFormat={(currencycode && currencycode) + ' %d'}
            thumbRadius={8}
            textSize={14}
            thumbBorderWidth={0}
            thumbColor={'#989898'}
            labelBackgroundColor={colors.colorBlue}
            labelBorderColor={colors.colorBlue}
            initialLowValue={initialLowValue}
            initialHighValue={initialHighValue}
            selectionColor={colors.colorBlue}
            blankColor={colors.colorGrey}
            lineWidth={4}
            labelFontSize={8}
            labelBorderRadius={10}
            onValueChanged={(low, high, fromUser) => {
              //alert(JSON.stringify(fromUser))
              this.selectedMinPrice = low;
              this.selectedMaxPrice = high;
              // this.setState({ selectedMinPrice: low, selectedMaxPrice: high })
            }}
            onTouchEnd={() => {
              this.setState({
                selectedMinPrice: this.selectedMinPrice,
                selectedMaxPrice: this.selectedMaxPrice,
              });
            }}
          />
        </View>
      );
  }

  changePriceRangeRadio(radioValue) {
    switch (radioValue) {
      case 1:
        this.setState({ selectedMinPrice: 0, selectedMaxPrice: 1000 }, () => {
          this._rangeSlider.setLowValue(0);
          this._rangeSlider.setHighValue(1000);
        });
        break;
      case 2:
        this.setState({ selectedMinPrice: 1000, selectedMaxPrice: 2000 }, () => {
          this._rangeSlider.setLowValue(1000);
          this._rangeSlider.setHighValue(2000);
        });
        break;
      case 3:
        this.setState({ selectedMinPrice: 2000, selectedMaxPrice: 3000 }, () => {
          this._rangeSlider.setLowValue(2000);
          this._rangeSlider.setHighValue(3000);
        });
        break;
      case 4:
        this.setState({ selectedMinPrice: 3000, selectedMaxPrice: 4000 }, () => {
          this._rangeSlider.setLowValue(3000);
          this._rangeSlider.setHighValue(4000);
        });
        break;
      case 5:
        this.setState(
          { selectedMinPrice: 4000, selectedMaxPrice: this.state.maxPrice },
          () => {
            this._rangeSlider.setLowValue(4000);
            this._rangeSlider.setHighValue(this.state.maxPrice);
          },
        );
        break;
    }
  }

  renderSortingFilter() {
    return (
      <View style={{ width: '100%', paddingHorizontal: 15, paddingVertical: 10 }}>
        <View style={{ flexDirection: 'row', justifyContent: 'space-evenly' }}>
          <TouchableOpacity
            underlayColor={'transparent'}
            onPress={() => {
              this.setState({
                sortingStatus: 1,
              });
            }}>
            <View style={{ alignItems: 'center' }}>
              <View
                style={{
                  padding: 5,
                  width: 50,
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                <Image
                  style={{
                    width: 18,
                    height: 18,
                    tintColor:
                      this.state.sortingStatus == 1
                        ? colors.colorBlue
                        : '#626262',
                  }}
                  source={Images.icon_low}
                  resizeMode={'contain'}
                />
              </View>
              <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                <Text
                  style={{
                    fontFamily: Fonts.regular,
                    textAlign: 'center',
                    fontSize: 10,
                    color:
                      this.state.sortingStatus == 1
                        ? colors.colorBlue
                        : '#626262',
                  }}>
                  {'Price\nLow To High'}
                </Text>
              </View>
            </View>
          </TouchableOpacity>
          <View style={{ width: 1, height: 40, backgroundColor: 'grey' }} />

          <TouchableOpacity
            underlayColor={'transparent'}
            onPress={() => {
              this.setState({
                sortingStatus: 2,
              });
            }}>
            <View style={{ alignItems: 'center' }}>
              <View
                style={{
                  padding: 5,
                  width: 50,
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                <Image
                  style={{
                    width: 18,
                    height: 18,
                    tintColor:
                      this.state.sortingStatus == 2
                        ? colors.colorBlue
                        : '#626262',
                  }}
                  source={Images.icon_high}
                  resizeMode={'contain'}
                />
              </View>
              <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                <Text
                  style={{
                    fontFamily: Fonts.regular,
                    textAlign: 'center',
                    fontSize: 10,
                    color:
                      this.state.sortingStatus == 2
                        ? colors.colorBlue
                        : '#626262',
                  }}>
                  {'Price\nHigh To Low'}
                </Text>
              </View>
            </View>
          </TouchableOpacity>
          <View style={{ width: 1, height: 40, backgroundColor: 'grey' }} />

          <TouchableOpacity
            underlayColor={'transparent'}
            onPress={() => {
              this.setState({
                sortingStatus: 3,
              });
            }}>
            <View style={{ alignItems: 'center' }}>
              <View
                style={{
                  padding: 5,
                  width: 50,
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                <Image
                  style={{
                    width: 18,
                    height: 18,
                    tintColor:
                      this.state.sortingStatus == 3
                        ? colors.colorBlue
                        : '#626262',
                  }}
                  source={Images.icon_rating}
                  resizeMode={'contain'}
                />
              </View>
              <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                <Text
                  style={{
                    fontFamily: Fonts.regular,
                    textAlign: 'center',
                    fontSize: 10,
                    color:
                      this.state.sortingStatus == 3
                        ? colors.colorBlue
                        : '#626262',
                  }}>
                  {'User\nRating'}
                </Text>
              </View>
            </View>
          </TouchableOpacity>
          <View style={{ width: 1, height: 40, backgroundColor: 'grey' }} />

          <TouchableOpacity
            underlayColor={'transparent'}
            onPress={() => {
              this.setState({
                sortingStatus: 4,
              });
            }}>
            <View style={{ alignItems: 'center' }}>
              <View
                style={{
                  padding: 5,
                  width: 50,
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                <Image
                  style={{
                    width: 18,
                    height: 18,
                    tintColor:
                      this.state.sortingStatus == 4
                        ? colors.colorBlue
                        : '#626262',
                  }}
                  source={Images.star}
                  resizeMode={'contain'}
                />
              </View>
              <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                <Text
                  style={{
                    fontFamily: Fonts.regular,
                    textAlign: 'center',
                    fontSize: 10,
                    color:
                      this.state.sortingStatus == 4
                        ? colors.colorBlue
                        : '#626262',
                  }}>
                  {'Hotel\nPopularity'}
                </Text>
              </View>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    );
  }

  render() {
    return (
      <View style={styles.container}>
        <SafeAreaView backgroundColor={colors.colorBlue}></SafeAreaView>
        <StatusBar backgroundColor={colors.colorBlue} />

        <CommonHeader
          titleView={() => this.renderMiddle()}
          fireEvent={() => {
            this.setState({
              showFilter: true,
            });
          }}
          rightImg={Images.ic_image_filter}
        />
        

        <View
          style={{
            flex: 1,
            backgroundColor: colors.gray,
          }}>
          <View style={{ padding: 10, paddingHorizontal: 12, flex: 1 }}>
            <Text
              style={{
                fontSize: 16,
                color: colors.colorBlack,
                fontFamily: Fonts.medium,
                marginBottom: 8,
              }}>
              Recommended For You
            </Text>
            {this.hotelsList()}
          </View>
        </View>
        {this.renderFilterModel()}

      </View>
    );
  }
}

const mapDisatchToProps = dispatch => {
  return {
    getHotelListAction: data => dispatch(getHotelListAction(data)),
    setLoadingAction: data => dispatch(setLoadingAction(data)),
  };
};

const mapStateToProps = state => {
  // console.log("redux state is ", JSON.stringify(state))
  return {
    hotelSearchParams: state.HotelListreducer.hotelSearchParam,
    hotelList: state.HotelListreducer.HotellistData,
  };
};

export default connect(mapStateToProps, mapDisatchToProps)(HotelList);
