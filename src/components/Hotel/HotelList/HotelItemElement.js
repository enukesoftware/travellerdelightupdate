import React, { Component } from 'react';
import { Image, Text, TouchableOpacity, View } from 'react-native';
import CardView from 'react-native-cardview';
import { colors } from '../../../Utils/Colors';
import Fonts from "../../../Utils/Fonts";
import Images from '../../../Utils/Images';
import { filterAmenities } from '../../../Utils/Utility';
import ImageLoad from '../../custom/ImageLoad';


class HotelItemElement extends Component {
  constructor(props) {
    super(props)
    this.state = {
      amenities: null,
      hotel: {

      }

    }
  }

  componentDidMount() {
    let newAmenities = this.props.hotel.amenities && this.props.hotel.amenities.length > 0 ? filterAmenities(this.props.hotel.amenities) : null
    // alert(JSON.stringify(newAmenities))
    this.setState({
      amenities: newAmenities
    })


  }
  render() {
    let { hotel, onPress } = this.props;
    return (
      <CardView style={{
        //overflow: 'hidden',
        backgroundColor: colors.colorWhite,
        marginBottom: 16,
        marginHorizontal: 2
      }}
        cardElevation={2}
        cardMaxElevation={3}
        cornerRadius={10}>
        <TouchableOpacity onPress={onPress} style={{ borderTopLeftRadius: 10, borderTopRightRadius: 10, overflow: "hidden" }}>
          <ImageLoad
            placeholderSource={Images.img_placeholder}
            source={{ uri: hotel.photo }}
            loadingStyle={{ size: 'large', color: colors.colorBlue }}
            style={{ width: '100%', height: 170 }} />

          {/* <ImageSliderz
            height={170}
            width={'100%'}
            containerStyle={{}}
            dataSource={this.props.hotel.images}
            indicatorSelectedColor="white"
            indicatorSize={7}
            position={this.state.position}
            arrowSize={0}
            //onPositionChanged={position => this.setState({ position })}
            titleViewBackgroundColor='rgba(0,0,0,0.5)'
            imageBorderRadius={10}
            indicatorAlignment='flex-end'
          /> */}
          <TouchableOpacity style={{ borderRadius: 20, position: 'absolute', backgroundColor: 'white', height: 35, width: 35, right: 10, top: 10, alignItems: 'center', justifyContent: 'center' }}>
            <Image source={Images.ic_favorite_inactive} style={{ width: 30, height: 30, resizeMode: 'contain' }} />
          </TouchableOpacity>
          <View onPress={onPress} style={{ flexDirection: 'row', width: '100%', paddingHorizontal: 10, paddingVertical: 12 }}>
            <View style={{ width: '70%' }}>
              <Text style={{ fontFamily: Fonts.medium, color: colors.black, fontSize: 16 }}>{this.props.hotel.name ? this.props.hotel.name : null}</Text>
              <Text numberOfLines={2} ellipsizeMode="tail" style={{ fontFamily: Fonts.semiBold, color: "#626262", fontSize: 10, paddingTop: 4, paddingBottom: 4 ,lineHeight: 18}}>{hotel.address ? hotel.address : null}</Text>
            </View>
            <View style={{ flex: 1, alignItems: 'flex-end' }}>
              <View style={{ flexDirection: 'row', justifyContent: 'flex-end' }}>
                <Text style={{ fontFamily: Fonts.semiBold, color: colors.colorBlue, fontSize: 18 }}>{hotel.priceDetail && hotel.priceDetail.CurrencyCode == "INR" ? "₹ " : hotel.priceDetail.CurrencyCode}</Text>
                <Text style={{ fontFamily: Fonts.semiBold, color: colors.black, fontSize: 18, letterSpacing:1.5 }}>{hotel.priceDetail && hotel.priceDetail.agencyFare ? Math.ceil(hotel.priceDetail.agencyFare) : null}</Text>
              </View>
              <View style={{ flexDirection: 'row', justifyContent: 'flex-end', paddingTop: 3 }}>
                <Text style={{ fontFamily: Fonts.semiBold, color: "grey", fontSize: 10 }}>{hotel.priceDetail ? "Per Night" : null}</Text>
              </View>
              <View style={{ flexDirection: 'row', marginTop: 8, alignItems: 'center', justifyContent: 'center' }}>
                <Image source={Images.star} style={{ width: 12, height: 12, resizeMode: 'contain' }} />
                <Text style={{ fontFamily: Fonts.semiBold, color: '#4d4d4d', fontSize: 13, paddingLeft: 4 }}>{hotel.star ? hotel.star : null}</Text>
              </View>
            </View>
          </View>
        </TouchableOpacity>
      </CardView>
    )
  }
}

export default HotelItemElement
