import { Dimensions, StyleSheet } from 'react-native';
import { colors } from '../../../Utils/Colors';

const screenSize = Dimensions.get('window');

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f5f5f5',
    height: screenSize.height,
  },
  styleAdultOrChildViewContainer: {
    width: '100%',
    padding: 15,
    backgroundColor: 'white',
    marginBottom: 5,
  },

  styleTitleViewContainer: {
    height: 30,
    width: '100%',
    justifyContent: 'center',
    marginLeft: 5,
  },

  styleBaggageDropView: {
    height: '100%',
    width: '100%',
    borderRadius: 5,
    borderColor: 'gray',
    borderWidth: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingLeft: 5,
    paddingRight: 5,
  },

  addButtonStyle: {
    height: 40,
    width: 80,
    backgroundColor: colors.colorBlue,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'flex-end',
    marginRight: 20,
    borderRadius: 7,
  },
  continueButtonStyle: {
    height: 50,
    width: '95%',
    backgroundColor: colors.colorBlue,
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 7,
    bottom: 15,
  },
  stylePopUpView: {
    width: '80%',
    backgroundColor: 'white',
    borderRadius: 15,
  },
  styleTitleView: {
    margin: 15,
    height: 20,
    justifyContent: 'center',
  },
  styleBottomBtnsView: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    margin: 15,
    height: 35,
    paddingRight: 10,
  },
  styleCancelOrDoneBtnView: {
    height: '100%',
    width: 60,
    justifyContent: 'center',
    alignItems: 'center',
  },
  containerPopup: {
    backgroundColor: 'rgba(0,0,0,0.5)',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  contactInput: {  flex: 1, fontSize: 13, padding: 4, fontFamily: Fonts.regular, },
  contactInputView: { height: 40, flexDirection: 'row', alignItems: 'flex-end',padding: 4,borderBottomColor: 'grey', borderBottomWidth: 1,marginHorizontal:5 }
});
