import moment from 'moment';
import React, { Component } from 'react';
import { Modal, SafeAreaView, SectionList, Text, TextInput, TouchableOpacity, View } from 'react-native';
import { CalendarList } from 'react-native-calendars';
import CardView from 'react-native-cardview';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { Dropdown } from 'react-native-material-dropdown';
import { connect } from 'react-redux';
import Titles from '../../../json/Titles';
import { checkRoomBlock } from '../../../Redux/Actions';
import { colors } from '../../../Utils/Colors';
import Fonts from '../../../Utils/Fonts';
import BookingBelt from '../../custom/BookingBelt';
import BottomStrip from '../../custom/BottomStrip';
import CommonHeader from '../../custom/CommonHeader';
import HotelFareDetails from '../HotelFareDetails';
import DateTimePicker from "react-native-modal-datetime-picker";
import { styles } from './Styles';
import { toUpperEveryWord } from '../../../Utils/Utility';


const guestType = {
  Adult: 'Adult',
  child: 'Child',
};

let sectionItemIndex = -1;
let listItemIndex = -1;
class GuestDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      adultTitleActionSheet: null,
      childTitleActionSheet: null,
      sectionData: [],
      email: '',
      phoneNumber: '',
      // panCard: "EBQPS3333T",
      panCard: '',
      CalendarModal: false,
      countryCode: { "id": 105, "value": "+91" },
      passportExpDateSection: null,
      roomPriceObject: null,
      userId: null,
      selectedIndexForDob: -1,
      isDateTimePickerExpiry: false,
      currentDate: null
    };

    this._renderAdultView = this._renderAdultView.bind(this);
    this._renderChildView = this._renderChildView.bind(this);
    this.dateSelectedHandler = this.dateSelectedHandler.bind(this);

    this._renderAdultOrChildView = this._renderAdultOrChildView.bind(this);
    this.adultTitlesArr = ['Mr.', 'Mrs.', 'Ms.', 'Cancel'];
    this.childTitlesArr = ['Master', 'Miss', 'Cancel'];
    //         // this._rendercheckInItem = this._rendercheckInItem.bind(this);
    //         // this._handleDatePicked = this._handleDatePicked.bind(this);
  }

  componentDidMount() {
    let roomPriceObject = this.props.navigation.getParam('roomPriceObject', null);
    let phone = this.props.navigation.getParam('phone', null);
    let email = this.props.navigation.getParam('email', null);
    let userId = this.props.navigation.getParam('userId', null);


    let roomData = this.props.hotelSearchParams.roomData;
    console.log('RoomData:=>' + JSON.stringify(roomData));
    if (roomData) {
      const guestArray = [];
      if (roomData != null) {
        let adultCount;
        let childCount;
        let childrenAge;
        let index = 0;
        roomData.forEach(element => {
          const roomGuestArray = [];
          adultCount = element.adult;
          childCount = element.children;
          childrenAge = element.childrenage

          console.log(
            'index ' +
            index +
            '   ' +
            'ChildCount ' +
            childCount +
            'AdultCount ' +
            adultCount,
          );
          while (adultCount) {
            roomGuestArray.push({
              type: guestType.Adult, title: '', fName: '', mName: '', lName: '',
            });
            adultCount--;
          }
          while (childCount) {
            //console.log("childrenAge[childCount-1]",childrenAge[childCount-1])
            roomGuestArray.push({
              type: guestType.child, title: '', fName: '', mName: '', lName: '', age: JSON.stringify(childrenAge[childCount - 1]),
            });
            childCount--;
          }
          console.log('LOOP ROOMGUEST ARRAY' + JSON.stringify(roomGuestArray));
          const room = index + 1;
          const dict = {
            data: roomGuestArray,
            key: 'Room ' + room,
            index: index,
          };
          guestArray.push(dict);
          //   guestArray.push(roomGuestArray)
          console.log('LOOP GUEST ARRAY' + JSON.stringify(guestArray));
          index++;
          // roomCount = roomCount+1
        });
        this.setState({
          sectionData: [...guestArray],
          roomPriceObject: roomPriceObject,
          phoneNumber: phone,
          email: email,
          userId: userId
        });
        //this.sectionData = ;

        console.log('guest array ' + JSON.stringify(guestArray));
      } else {
        console.log('guest count null');
      }
    } else {
      console.log(
        'no props data' +
        JSON.stringify(this.props.hotelSearchParams) +
        '   ' +
        JSON.stringify(this.props.roomDetails),
      );
    }

    // this.calculatePassengers(dict.adult, dict.child, dict.infant);
  }

  ApiHotelBooking() {
    console.log('calling Hotel Booking Api');
  }


  _renderCalenderPopUp() {
    let selectedDate =
      this.state.selectedIndex == 0
        ? this.state.checkIndate
        : this.state.checkOutDate;
    let markingDates = {};

    markingDates[selectedDate] = { selected: true };

    return (
      <View>
        <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.CalendarModal}
          onRequestClose={() => { }}>
          <View style={styles.containerPopup}>
            <View style={styles.stylePopUpView}>
              <View style={styles.styleTitleView}>
                <Text style={{ color: 'black', fontSize: 18, fontWeight: '600' }}>
                  Calender
                </Text>
              </View>
              <View style={{ height: 300, margin: 20 }}>
                <CalendarList
                  calendarWidth={'100%'}
                  pastScrollRange={0}
                  minDate={
                    this.state.selectedIndex === 0
                      ? new Date()
                      : this.state.checkIndate
                  }
                  onDayPress={this.dateSelectedHandler}
                  markedDates={markingDates}
                  // markingType='period'
                  theme={{
                    backgroundColor: 'white', calendarBackground: 'white', todayTextColor: colors.colorBlue, selectedDayBackgroundColor: colors.colorBlue,
                  }}
                />
              </View>

              {/* <View style={styles.styleBottomBtnsView}>

                <TouchableOpacity
                  style={styles.styleCancelOrDoneBtnView}
                  underlayColor="transparent"
                  onPress={() => {
                    this.setState({
                      CalendarModal: false
                    })
                  }}
                >
                  <Text style={{ color: "gray", fontSize: 14 }}>Close</Text>
                </TouchableOpacity>
              </View> */}
            </View>
          </View>
        </Modal>
      </View>
    );
  }

  validateEntryFields() {
    let arr = this.state.sectionData;
    console.log(JSON.stringify(arr))
    let isDetailsValid = true;
    let passportArray = []

    arr.some((sectionItem) => {
      const dataArray = sectionItem.data;
      console.log("dataArray" + JSON.stringify(dataArray))

      dataArray.some((item, index) => {
        console.log(JSON.stringify(index + " j " + JSON.stringify(item)))

        if (item.title.trim().length == 0) {
          alert('Please Select Title.');
          isDetailsValid = false;
          return true;
        }
        if (String(item.fName).trim().length == 0) {
          alert('Please Enter First Name.');
          isDetailsValid = false;
          return true;
        }

        if (String(item.lName).trim().length == 0) {
          alert('Please Enter Last Name.');
          isDetailsValid = false;
          return true;
        }

        if (item.type == guestType.child) {
          if (String(item.age).trim().length == 0) {
            alert("Please Enter the child's age");
            isDetailsValid = false;
            return true;
          }
          if (item.age <= 0) {
            alert("Child's age must be greater than 0");
            isDetailsValid = false;
            return true;
          }
          if (item.age >= 12) {
            alert("Child's age must be below 12");
            isDetailsValid = false;
            return true;
          }
        }

        if (
          this.props.hotelSearchParams &&
          this.props.hotelSearchParams.nationality &&
          this.props.hotelSearchParams.nationality != 'IN' &&
          (!item.passportNo || item.passportNo.length == 0)
        ) {
          alert('Please enter passport number');
          isDetailsValid = false;
          return true;
        }

        if (this.props.hotelSearchParams && this.props.hotelSearchParams.nationality && this.props.hotelSearchParams.nationality != 'IN') {
          let filter = passportArray.filter(i => { return i == item.passportNo })
          console.log("filter", JSON.stringify(filter))
          console.log("passportArray", JSON.stringify(passportArray))
          if (filter.length == 0) {
            passportArray.push(item.passportNo)
            console.log("passportArray AFTER PUSHED", JSON.stringify(passportArray))
          }
          else {
            alert("Passport numbers can't be same for different Guests");
            isDetailsValid = false;
            return true
          }
        }

        if (
          this.props.hotelSearchParams &&
          this.props.hotelSearchParams.nationality &&
          this.props.hotelSearchParams.nationality != 'IN' &&
          (!item.passportExpDate || item.passportExpDate.length == 0)
        ) {
          alert('Please enter passport expiry date');
          isDetailsValid = false;
          return true;
        }


        isDetailsValid = true;
      });
      if (!isDetailsValid) {
        return true;
      }
    });

    if (isDetailsValid) {
      if (this.state.phoneNumber.length != 10) {
        alert('You must update a valid phone number on your profile screen to continue');
        isDetailsValid = false;
        return false;
      }
      // if (!this.isEmailValid(this.state.email)) {
      //   alert('Invalid Email');
      //   isDetailsValid = false;
      //   return false;
      // }
      if (
        this.props.hotelSearchParams &&
        this.props.hotelSearchParams.nationality &&
        this.props.hotelSearchParams.nationality == 'IN' &&
        (!this.state.panCard || this.state.panCard.length == 0)
      ) {
        alert('Please Enter PAN Card Number');
        isDetailsValid = false;
        return false;
      }
    }

    if (isDetailsValid) {
      // let a = [{ "data": [{ "type": "Adult", "title": "Mr.", "fName": "as", "mName": "safa", "lName": "sfa" }], "key": "Room 1", "index": 0 }]

      // NavigationServices.navigate('ReviewBooking', {
      //   guestDetails: this.state.sectionData,
      //   email: this.state.email,
      //   phone: this.state.phoneNumber,
      //   panCard: this.state.panCard,
      // });

      this.continueToPayment()

      console.log();
    }
  }

  calculatePassengers(numOfAdult, numOfChild) {
    let arr = [];

    if (numOfAdult > 0) {
      for (i = 0; i < numOfAdult; i++) {
        arr.push(this.state.adultDict);
      }
    }

    if (numOfChild > 0) {
      for (i = 0; i < numOfChild; i++) {
        arr.push(this.state.childDict);
      }
    }

    console.log('Passenger Details' + JSON.stringify(arr));
    this.setState({
      passengersArray: arr,
      loading: !this.state.loading,
    });
  }

  isEmailValid(text) {
    console.log(text);
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (reg.test(text) === false) {
      console.log('Email is Not Correct');
      return false;
    } else {
      console.log('Email is Correct');
      return true;
    }
  }

  onChangeText(itemIndex, text, key, section, item) {
    if (section != null && section.index != null) {
      console.log(
        'section ' + JSON.stringify(section) + ' index',
        itemIndex,
        'item data is ' + JSON.stringify(item),
        'text is ' + text,
        'key is ' + key,
      );
      const data = [...this.state.sectionData];
      if (key === 'title') {
        data[section.index].data[itemIndex].title = text;
      } else if (key === 'fName') {
        data[section.index].data[itemIndex].fName = text // toUpperEveryWord(text);
      } else if (key === 'mName') {
        data[section.index].data[itemIndex].mName = text //toUpperEveryWord(text);
      } else if (key === 'lName') {
        data[section.index].data[itemIndex].lName = text //toUpperEveryWord(text);
      } else if (key === 'age') {
        data[section.index].data[itemIndex].age = text;
      } else if (key == 'passportNo') {
        data[section.index].data[itemIndex]['passportNo'] = text;
      }

      this.setState({
        loading: !this.state.loading,
      });

      this.setState({
        sectionData: [...data],
      });
    } else if (key && key == 'PAN') {
      this.setState({
        panCard: text,
      });
    }
  }

  _renderContactDetailsView() {
    const { email, phoneNumber } = this.state;
    let hotelParam = this.props.hotelSearchParams;
    return (
      <CardView style={{
        backgroundColor: colors.colorWhite,
        margin: 10,
      }}
        cardElevation={2}
        cardMaxElevation={3}
        cornerRadius={5}>
        <View style={{ borderRadius: 5, overflow: 'hidden', padding: 10, }}>
          <View style={{ alignItems: 'center', flexDirection: 'row', }}>
            <Text style={{ fontFamily: Fonts.medium, fontSize: 16, color: colors.colorBlack21, }}>
              Contact Detail
            </Text>
          </View>
          <View style={{ flexDirection: 'row' }}>
            {/* <View style={{ width: '40%' }}>
              <View style={styles.contactInputView}>
                <Dropdown placeholder={"Country Code"}
                  placeholderTextColor={"grey"}
                  data={countriesCodes}
                  fontSize={13}
                  fontFamily={Fonts.regular}
                  value={this.state.countryCode.value}
                  containerStyle={{
                    paddingRight: 4,
                    paddingLeft: 1,
                    width: "100%",
                  }}
                  inputContainerStyle={{
                    borderBottomColor: 'grey', borderBottomWidth: 1, height: 34,
                  }}
                  pickerStyle={{
                    width: "90%",
                    marginLeft: 10,
                    marginTop: 40,
                  }}
                  itemTextStyle={{ color: "grey", fontSize: 13, height: 34, fontFamily: Fonts.regular }}
                  itemColor={"grey"}
                  dropdownOffset={{ top: 10, left: 0 }}
                  onChangeText={(value, index) => {
                    this.setState({ countryCode: countriesCodes[index] });
                  }}
                /></View>
            </View>*/}
            <View style={{ width: '100%' }}>
              <View style={styles.contactInputView}>
                <Text
                  style={[styles.contactInput, {}]}
                  value={phoneNumber}
                  onChangeText={text =>
                    this.setState({
                      phoneNumber: text,
                    })
                  }
                  maxLength={10}
                  numberOfLines={1}
                  keyboardType={'phone-pad'}
                  placeholder={'Phone'}
                  autoCapitalize={'none'}
                  autoCorrect={false}
                  placeholderTextColor={'grey'}
                > {phoneNumber}</Text>
              </View></View>
          </View>

          <View
            style={styles.contactInputView}>
            <Text style={styles.contactInput}
              value={email}
              numberOfLines={1}
              onChangeText={text => this.setState({ email: text, })}
              placeholder={'Email'}
              keyboardType={'email-address'}
              autoCapitalize={'none'}
              autoCorrect={false}
              placeholderTextColor={'grey'}
            >{email}</Text>
          </View>
          {hotelParam &&
            hotelParam.nationality &&
            hotelParam.nationality == 'IN' ? (
              <View style={styles.contactInputView}>
                <TextInput
                  numberOfLines={1}
                  style={styles.contactInput}
                  value={this.state.panCard}
                  onChangeText={text => this.onChangeText(0, text.replace(/[^A-Za-z0-9]/g, ''), 'PAN', 0, 0)}
                  placeholder={'PAN Card'}
                  autoCapitalize={'characters'}
                  autoCorrect={false}
                  placeholderTextColor={'grey'}
                />
              </View>) : null}
        </View>
      </CardView>
    );
  }

  _renderChildView(item, index, section) {
    console.log('sectionList item ' + JSON.stringify(item));
    console.log('section is ' + JSON.stringify(section));
    return (
      <View style={styles.styleAdultOrChildViewContainer}>
        <View style={{ width: '100%', flexDirection: 'row' }}>
          <View style={{ width: '50%', }}>
            <Dropdown
              placeholder={'Title'}
              placeholderTextColor={'grey'}
              data={Titles.child}
              fontSize={13}
              fontFamily={Fonts.regular}
              value={item.title}
              containerStyle={{
                width: '100%',
              }}
              inputContainerStyle={{
                borderBottomColor: 'grey', borderBottomWidth: 1, padding: 4, height: 34, margin: 4,
              }}
              pickerStyle={{
                width: '50%', marginLeft: 10, marginTop: 40,
              }}
              itemTextStyle={{
                color: 'grey', fontSize: 13, height: 34, fontFamily: Fonts.regular,
              }}
              itemColor={'grey'}
              dropdownOffset={{ top: 10, left: 0 }}
              onChangeText={text =>
                this.onChangeText(index, text, 'title', section, item)
              }
            />
          </View>
          <View style={{ width: '50%' }}>
            <View
              style={{
                height: 34, margin: 4, padding: 4, fontFamily: Fonts.regular, borderBottomColor: 'grey', borderBottomWidth: 1,
              }}>
              <Text
                style={{
                  height: 34, fontSize: 13, fontFamily: Fonts.regular,    // borderBottomColor: 'grey',    // borderBottomWidth: 1
                }}>
                {item.type}
              </Text>
            </View>
          </View>
        </View>

        <View style={{ width: '100%', flexDirection: 'row' }}>
          <View style={{ width: '50%' }}>
            <TextInput
              numberOfLines={1}
              style={{
                height: 34, margin: 4, fontSize: 13, padding: 4, fontFamily: Fonts.regular, borderBottomColor: 'grey', borderBottomWidth: 1,
              }}
              value={item.fName}
              onChangeText={text =>
                this.onChangeText(index, text, 'fName', section, item)
              }
              placeholder={'First Name'}
              autoCapitalize={'none'}
              autoCorrect={false}
              placeholderTextColor={'grey'}
            />
          </View>
          <View style={{ width: '50%' }}>
            <TextInput
              style={{
                height: 34, margin: 4, fontSize: 13, padding: 4, fontFamily: Fonts.regular, borderBottomColor: 'grey', borderBottomWidth: 1,
              }}
              value={item.lName}
              onChangeText={text =>
                this.onChangeText(index, text, 'lName', section, item)
              }
              placeholder={'Last Name'}
              autoCapitalize={'none'}
              autoCorrect={false}
              placeholderTextColor={'grey'}
            />
          </View>
        </View>

        {this.props.hotelSearchParams.nationality &&
          this.props.hotelSearchParams.nationality != 'IN' ? (
            <View style={{ width: '100%', flexDirection: 'row' }}>
              <View style={{ width: '50%', padding: 5 }}>
                <TextInput
                  numberOfLines={1}
                  style={{
                    height: 34, margin: 4, fontSize: 13, padding: 4, fontFamily: Fonts.regular, borderBottomColor: 'grey', borderBottomWidth: 1,
                  }}
                  value={item.passportNo ? item.passportNo : ''}
                  onChangeText={text =>
                    this.onChangeText(index, text.replace(/[^A-Za-z0-9]/g, ''), 'passportNo', section, item)
                  }
                  placeholder={'Passport Number'}
                  autoCapitalize={'characters'}
                  autoCorrect={false}
                  placeholderTextColor={'grey'}
                />
              </View>
              <TouchableOpacity
                activeOpacity={0.9}
                style={{ width: '50%', padding: 5 }}
                // onPress={() => {

                //   this.setState({
                //     CalendarModal: true, passportExpDateSection: {
                //       section: section, itemIndex: index, item: item,
                //     },
                //   });
                // }}
                onPress={() => {
                  this.setState({
                    passportExpDateSection: {
                      section: section, itemIndex: index, item: item,
                    },
                    isDateTimePickerExpiry: true,
                    currentDate: (item.passport_expiry && item.passport_expiry != '') ? moment(item.passport_expiry).toDate() : null
                  })
                }}>
                <Text
                  style={{
                    height: 34, margin: 4, fontSize: 13, padding: 4, fontFamily: Fonts.regular, marginBottom: 0, paddingBottom: 0, color: item.passportExpDateToShow ? null : 'grey'
                  }}>
                  {item.passportExpDateToShow
                    ? item.passportExpDateToShow
                    : 'Expiry Date'}
                </Text>
                <View
                  style={{
                    height: 1, width: '100%', backgroundColor: 'gray',
                  }}></View>
              </TouchableOpacity>
            </View>
          ) : null}

        {/* <View style={{ width: '100%', flexDirection: 'row' }}>
          <View style={{ width: '50%', padding: 5 }}>
            <TextInput
              numberOfLines={1}
              style={{
                height: 34, margin: 4, fontSize: 13, padding: 4, fontFamily: Fonts.regular, borderBottomColor: 'grey', borderBottomWidth: 1,
              }}
              value={item.age}
              onChangeText={text =>
                this.onChangeText(index, text, 'age', section, item)
              }
              placeholder={'Age (below 12)'}
              autoCapitalize={'none'}
              keyboardType={'number-pad'}
              autoCorrect={false}
              placeholderTextColor={'grey'}
            />
          </View>
        </View> */}
      </View>
    );
  }
  renderPassportExpiryModal = () => {
    return (
      <DateTimePicker
        isVisible={this.state.isDateTimePickerExpiry}
        onConfirm={this._handleDatePickedExpiry}
        minimumDate={new Date()}
        date={this.state.currentDate || new Date()}
        onCancel={this._hideDateTimePicker}
      />
    )
  }

  _handleDatePickedExpiry = date => {
    console.log("A date has been picked: ", date);

    let { section, itemIndex } = this.state.passportExpDateSection;

    if (section != null && section.index != null) {
      const data = [...this.state.sectionData];
      data[section.index].data[itemIndex]['passportExpDate'] =
        moment(date).format("YYYY-MM-DD") + 'T00:00:00';;
      data[section.index].data[itemIndex]['passportExpDateToShow'] =
        moment(date).format("YYYY-MM-DD");
      this.setState({
        sectionData: [...data],
      });
      this._hideDateTimePicker();

    }
  };

  dateSelectedHandler = date => {
    console.log('EXP_DATE:' + JSON.stringify(date.dateString));

    if (this.state.passportExpDateSection) {
      let { section, itemIndex } = this.state.passportExpDateSection;

      if (section != null && section.index != null) {
        const data = [...this.state.sectionData];
        data[section.index].data[itemIndex]['passportExpDate'] =
          date.dateString + 'T18:30:00.000Z';
        data[section.index].data[itemIndex]['passportExpDateToShow'] =
          date.dateString;

        this.setState({
          sectionData: [...data],
          CalendarModal: false,
        });
      }
    }
  };


  _hideDateTimePicker = () => this.setState({ isDateTimePickerExpiry: false });


  _renderAdultView(item, index, section) {
    console.log(
      '_renderAdultView' +
      JSON.stringify(item) +
      ' ' +
      index +
      ' ' +
      ' ' +
      JSON.stringify(section),
    );
    return (
      <View style={styles.styleAdultOrChildViewContainer}>
        <View style={{ width: '100%', flexDirection: 'row' }}>
          <View style={{ width: '50%', }}>
            <Dropdown
              placeholder={'Title'}
              placeholderTextColor={'grey'}
              data={Titles.adult}
              fontSize={13}
              fontFamily={Fonts.regular}
              value={item.title}
              containerStyle={{
                width: '100%',
              }}
              inputContainerStyle={{
                borderBottomColor: 'grey', borderBottomWidth: 1, padding: 4, height: 34, margin: 4,
              }}
              pickerStyle={{
                width: '50%', marginLeft: 10, marginTop: 40,
              }}
              itemTextStyle={{
                color: 'grey', fontSize: 13, height: 34, fontFamily: Fonts.regular,
              }}
              itemColor={'grey'}
              dropdownOffset={{ top: 10, left: 0 }}
              onChangeText={text =>
                this.onChangeText(index, text, 'title', section, item)
              }
            />
          </View>
          <View style={{ width: '50%', }}>
            <View
              style={{
                height: 34, margin: 4, padding: 4, fontFamily: Fonts.regular, borderBottomColor: 'grey', borderBottomWidth: 1,
              }}>
              <Text
                style={{
                  height: 34, fontSize: 13, fontFamily: Fonts.regular,    // borderBottomColor: 'grey',    // borderBottomWidth: 1
                }}>
                {item.type}
              </Text>
            </View>
          </View>
        </View>

        <View style={{ width: '100%', flexDirection: 'row' }}>
          <View style={{ width: '50%' }}>
            <TextInput
              numberOfLines={1}
              style={{
                height: 34, margin: 4, fontSize: 13, padding: 4, fontFamily: Fonts.regular, borderBottomColor: 'grey', borderBottomWidth: 1,
              }}
              value={item.fName}
              onChangeText={text =>
                this.onChangeText(index, text, 'fName', section, item)
              }
              placeholder={'First Name'}
              autoCapitalize={'none'}
              autoCorrect={false}
              placeholderTextColor={'grey'}
            />
          </View>
          <View style={{ width: '50%' }}>
            <TextInput
              numberOfLines={1}
              style={{
                height: 34, margin: 4, fontSize: 13, padding: 4, fontFamily: Fonts.regular, borderBottomColor: 'grey', borderBottomWidth: 1,
              }}
              value={item.lName}
              onChangeText={text =>
                this.onChangeText(index, text, 'lName', section, item)
              }
              placeholder={'Last Name'}
              autoCapitalize={'none'}
              autoCorrect={false}
              placeholderTextColor={'grey'}
            />
          </View>
        </View>

        {this.props.hotelSearchParams.nationality &&
          this.props.hotelSearchParams.nationality != 'IN' ? (
            <View style={{ width: '100%', flexDirection: 'row' }}>
              <View style={{ width: '50%', padding: 5 }}>
                <TextInput
                  numberOfLines={1}
                  style={{
                    height: 34, margin: 4, fontSize: 13, padding: 4, fontFamily: Fonts.regular, borderBottomColor: 'grey', borderBottomWidth: 1,
                  }}
                  value={item.passportNo ? item.passportNo : ''}
                  onChangeText={text =>
                    this.onChangeText(index, text, 'passportNo', section, item)
                  }
                  placeholder={'Passport Number'}
                  autoCapitalize={'characters'}
                  autoCorrect={false}
                  placeholderTextColor={'grey'}
                />
              </View>
              <TouchableOpacity
                activeOpacity={0.9}
                style={{ width: '50%', padding: 5 }}
                // onPress={() => {
                //   this.setState({
                //     CalendarModal: true, passportExpDateSection: {
                //       section: section, itemIndex: index, item: item,
                //     },
                //   });
                // }}
                onPress={() => {
                  this.setState({
                    passportExpDateSection: {
                      section: section, itemIndex: index, item: item,
                    },
                    isDateTimePickerExpiry: true,
                    currentDate: (item.passportExpDate && item.passportExpDate != '') ? moment(item.passportExpDate).toDate() : null
                  })
                }}

              >
                <Text
                  style={{
                    height: 34, margin: 4, fontSize: 13, padding: 4, fontFamily: Fonts.regular, marginBottom: 0, paddingBottom: 0, color: item.passportExpDateToShow ? null : 'grey'
                  }}>
                  {item.passportExpDateToShow
                    ? item.passportExpDateToShow
                    : 'Expiry Date'}
                </Text>
                <View
                  style={{
                    height: 1, width: '100%', backgroundColor: 'gray',
                  }}></View>
              </TouchableOpacity>
            </View>
          ) : null}
      </View>
    );
  }

  _renderAdultOrChildView({ item, index, section }) {
    console.log('sectionLisf item ' + JSON.stringify(item));
    return (
      <View style={{}}>
        {item.type === 'Adult'
          ? this._renderAdultView(item, index, section)
          : this._renderChildView(item, index, section)}
      </View>
    );
  }

  renderGuestDetailView() {
    return (

      <CardView style={{
        backgroundColor: colors.colorWhite,
        margin: 10,
      }}
        cardElevation={2}
        cardMaxElevation={3}
        cornerRadius={5}>
        <View style={{ borderRadius: 5, overflow: 'hidden', }}>
          <View style={{ alignItems: 'center', flexDirection: 'row', padding: 10, }}>
            <Text style={{ fontFamily: Fonts.medium, fontSize: 16, color: colors.colorBlack21, }}>
              Guest Information
            </Text>
          </View>
          <SectionList
            renderItem={this._renderAdultOrChildView}
            renderSectionHeader={({ section: { key } }) => (
              <View style={{ width: '100%', backgroundColor: '#f6f6f6', padding: 10 }}>
                <Text style={{ fontFamily: Fonts.medium, fontSize: 13, color: colors.colorBlack62 }}>{key}</Text>
              </View>

            )}
            sections={this.state.sectionData}
            keyExtractor={(item, index) => item + index}
          />
        </View>
      </CardView>


    )
  }

  continueToPayment() {
    // console.log("Props:", JSON.stringify(this.props))
    // console.log("State:", JSON.stringify(this.state))
    const { phone, email, panCard } = this.state
    const guestDetails = this.state.sectionData
    const { hotel, rooms } = this.props.savedHotelDetails
    const selectedRoomData = this.props.savedHotelRoom
    const selectedRoomsArray = selectedRoomData.selectedRoomsArray
    const sources = selectedRoomData.sources
    const finalPrice = Math.ceil(selectedRoomData.totalPrice);
    const currency = selectedRoomData.currency

    let GuestDetailsArray = []
    guestDetails.forEach((item, index) => {
      let newDataArray = []
      item.data.forEach((d, i) => {
        newDataArray.push({
          title: d.title,
          first_name: d.fName,
          middle_name: d.mName,
          last_name: d.lName,
          type: d.type,
          passportNo: d.passportNo ? d.passportNo : "",
          passportExpDate: d.passportExpDate ? d.passportExpDate : "",
        })
      })
      GuestDetailsArray.push(newDataArray)
    })


    const hotelObject = {
      id: hotel.id,
      name: hotel.name,
      index: hotel.index,
      address: hotel.address,
      city: hotel.city,
      country: hotel.country,
      roomCombination: rooms.combination.RoomCombination,
      selectedRoom: selectedRoomsArray,
    }




    const hotelBookingObject = {
      user_id: this.state.userId,
      guestDetail: GuestDetailsArray,
      check_in: moment(Date.parse(this.props.hotelSearchParams.checkIn)).format("YYYY-MM-DD"),
      check_out: moment(Date.parse(this.props.hotelSearchParams.checkOut)).format("YYYY-MM-DD"),
      hotel: hotelObject,
      description: hotel.description,
      room: selectedRoomsArray,
      source: sources.source,
      sourceDetail: sources.sourceDetail,
      ResultIndex: hotel.index,
      hotelRooms: rooms.roomlist,
      email: this.state.email,
      mobile: this.state.phoneNumber,
      orderID: "",
      captureID: "",
      PAN: panCard,
      nationality: this.props.hotelSearchParams.nationality
    }
    console.log("hotelBookingObject:", JSON.stringify(hotelBookingObject))
    this.props.checkRoomBlock({ data: hotelBookingObject, isFlight: false, price: finalPrice, currency: currency })

  }


  render() {
    let hotelParam = this.props.hotelSearchParams;
    return (
      <View style={styles.container}>
        {this._renderCalenderPopUp()}
        {this.renderPassportExpiryModal()}
        <SafeAreaView style={{ backgroundColor: colors.colorBlue }} />
        <CommonHeader title="Guest Details" />
        <BookingBelt type={2} isHotel={true} style={{ paddingBottom: 5 }} />

        <KeyboardAwareScrollView style={{}}>

          {this.renderGuestDetailView()}

          {this._renderContactDetailsView()}
          <HotelFareDetails data={this.state.roomPriceObject} />


        </KeyboardAwareScrollView>

        {this.state.roomPriceObject && <BottomStrip fireEvent={() => {
          this.validateEntryFields()
          //this.bookTicket()
        }
        } price={this.state.roomPriceObject.currency + " " + Math.ceil(this.state.roomPriceObject.totalPrice)} bottomStripStyle={{ position: 'relative' }} />}

        <SafeAreaView backgroundColor={colors.colorBlue} />
      </View>
    );
  }
}
const mapStateToProps = state => {
  console.log(
    // 'HOTEL_PARAM:' , JSON.stringify(state.HotelListreducer.hotelSearchParam),
  );
  return {
    hotelSearchParams: state.HotelListreducer.hotelSearchParam,
    savedHotelDetails: state.HotelListreducer.selectedHotelItem,
    savedHotelRoom: state.HotelListreducer.selectedRoomData
  };
};

const mapDispatchToProps = dispatch => {
  return {
    checkRoomBlock: (data) => dispatch(checkRoomBlock(data)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(GuestDetails);
