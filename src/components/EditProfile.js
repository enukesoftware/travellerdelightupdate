import React, { Component } from "react";
import { DeviceEventEmitter, Dimensions, Image, Platform, SafeAreaView, StatusBar, StyleSheet, Text, TouchableHighlight, TouchableOpacity, View } from "react-native";
import CardView from "react-native-cardview";
import { TextInput } from "react-native-gesture-handler";
import ImagePicker from 'react-native-image-picker';
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { connect } from "react-redux";
import { updateUserProfileAction } from "../Redux/Actions";
import { imageBaseUrl } from '../Utils/APIManager/APIConstants';
import { colors } from "../Utils/Colors";
import Fonts from "../Utils/Fonts";
import Images from "../Utils/Images";
import CommomHeader from './custom/CommonHeader';


const screenWidth = Dimensions.get('screen').width;
const screenHeight = Dimensions.get('screen').height;

const options = {
  title: 'Select Avatar',
  customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
  storageOptions: {
    skipBackup: true,
    path: 'images',
  },
};

class EditProfile extends Component {
  constructor(props) {
    super(props);
    this.ActionSheet = null;
    this.state = {
      isDateTimePickerVisible: false,
      titlesArr: ["Mr.", "Mrs.", "Ms.", "Cancel"],
      userData: null,
      selectedUserImgPath: null,
      isImageSelected: false,
    };

    this._renderPersonalDetailView = this._renderPersonalDetailView.bind(this);
    this._renderContactDetailsView = this._renderContactDetailsView.bind(this);
    this._renderProfilePictureView = this._renderProfilePictureView.bind(this);
  }

  //MARK:-------------- Component Life cycle method -----------

  componentDidMount() {
    const user = this.props.userData
    let userImage;


    newUserData = Object.assign({}, user)
    console.log("USER_DATA:" + JSON.stringify(newUserData))


    if (newUserData.image && newUserData.image.path) {
      userImage = imageBaseUrl + newUserData.image.path
      console.log("IMAGE_PATH:" + JSON.stringify(newUserData))
    }
    this.setState({
      userData: newUserData,
      selectedUserImgPath: { uri: userImage }
    });
    DeviceEventEmitter.addListener("citySelected", dict => {
      let data = this.state.userData;
      let address = data.address;
      address["city"] = dict.name;
      address.country["gta_code"] = dict.country_code;
      address.country["id"] = dict.country_id;
      data["address"] = address;
      this.setState({
        userData: data
      });
      //Actions.refresh();
    });

  }


  //MARK:----------------- Update User Profile ------------------

  APIUpdateUserProfile() {

    let userDict = this.state.userData

    // let address = {
    //   "phone": userDict.address.phone,
    //   "address1": userDict.address.address1,
    //   "address2": userDict.address.address2,
    //   "city": userDict.address.city,
    //   "country_id": userDict.address.country.id,
    //   "zip": userDict.address.zip
    // }

    let param = new FormData();

    if (this.state.isImageSelected) {
      param.append("image", this.state.selectedUserImgPath);
    }
    param.append("id", userDict.id);
    param.append("first_name", userDict.first_name);
    param.append("middle_name", userDict.middle_name ? userDict.middle_name : "");
    param.append("last_name", userDict.last_name);
    param.append("address[phone]", userDict.address && userDict.address.phone ? userDict.address.phone : "");
    param.append("address[address1]", userDict.address && userDict.address.address1 ? userDict.address.address1 : "");
    param.append("address[address2]", userDict.address && userDict.address.address2 ? userDict.address.address2 : "");
    param.append("address[city]", userDict.address && userDict.address.city ? userDict.address.city : "");
    param.append("address[country_id]", userDict.address && userDict.address.country && userDict.address.country.id ? userDict.address.country.id : "");
    param.append("address[zip]", userDict.address && userDict.address.zip ? userDict.address.zip : "");

    this.props.updateUserProfileAction(param, this.props)


  }

  validateFiels() {
    let userDict = this.state.userData


    if (String(userDict.first_name).trim().length == 0) {
      alert("Please Enter First Name.");
      return;
    }


    if (String(userDict.last_name).trim().length == 0) {
      alert("Please Enter Last Name.");
      return;
    }

    if (!userDict.address || !userDict.address.phone || userDict.address.phone.trim().length == 0) {
      alert("Please enter phone number.");
      return;
    }


    if (!userDict.address || !userDict.address.phone || userDict.address.phone.trim().length < 10) {
      alert("Please enter valid phone number");
      return;
    }


    if (userDict.address) {

      if (userDict.address.address1 && userDict.address.address1.trim().length == 0) {
        alert("Please Enter Address line 1.");
        return;
      }

      if (userDict.address.address2 && userDict.address.address2.trim().length == 0) {
        alert("Please Enter Address line 2.");
        return;
      }

      if (userDict.address.city && userDict.address.city.trim().length == 0) {
        alert("Please Enter City.");
        return;
      }

      if (userDict.address.zip && userDict.address.zip.trim().length == 0) {
        alert("Please Enter Postal Code.");
        return;
      }
    }



    console.log("AFTER_VALID")


    this.APIUpdateUserProfile()
  }

  showImagePicker() {
    // return
    // this.setState({ loading: true });
    const options = {
      rotation: 360,
      allowsEditing: true,
      noData: true,
      mediaType: "photo",
      maxWidth: 300,
      maxHeight: 300,
      storageOptions: {
        skipBackup: true
      }
    };
    ImagePicker.showImagePicker(options, response => {
      console.log("Response = ", response);
      // this.setState({ loading: false });
      if (response.didCancel) {
        //   console.log(JSON.stringify(source));
        console.warn("User cancelled image picker");
      } else if (response.error) {
        //  console.log(JSON.stringify(source));
        console.warn("ImagePicker Error: ", response.error);
      } else if (response.customButton) {
        //  console.log('User tapped custom button: ', response.customButton);
      } else {

        const source = { uri: response.uri };

        let fileName = "profile_picture.jpg"
        // if (response && response.fileName) {
        //   let extension = response.fileName.substr(response.fileName.lastIndexOf('.'));
        //   if (extension && extension != "") {
        //     fileName = this.state.userData.id + "_" + response.fileName
        //   }

        // }

        if (response && response.uri) {
          let extension = response.uri.substr(response.uri.lastIndexOf('.'));
          if (extension && extension != "") {
            // fileName = this.state.userData.id + "_" + response.fileName
            fileName = 'profile_picture_' + this.state.userData.id + extension
          }

        }

        console.log("RES:" + JSON.stringify(response))
        console.log("FILE:" + JSON.stringify(fileName))


        const myImg = {
          uri: response.uri,
          type: "image/jpeg",
          name: fileName
        };

        this.setState({
          selectedUserImgPath: myImg,
          isImageSelected: true,
        });
      }
    });
  }

  //MARK:----------------- Open ImagePicker Controller -----------------
  showImagePicker2() {
    // ImagePicker.showImagePicker(null, (response) => {
    //   console.log('Response = ', response);

    //   if (response.didCancel) {
    //     console.log('User cancelled image picker');
    //   } else if (response.error) {
    //     console.log('ImagePicker Error: ', response.error);
    //   } else if (response.customButton) {
    //     console.log('User tapped custom button: ', response.customButton);
    //   } else {
    //     const source = { uri: response.uri };

    //     // You can also display the image using data:
    //     // const source = { uri: 'data:image/jpeg;base64,' + response.data };

    //     this.setState({
    //       selectedUserImgPath: source.uri,
    //     });
    //   }
    // });
  }


  //MARK:-------------- Date Picker Methods ----------

  //   _showDateTimePicker = (index) => );

  _hideDateTimePicker = index =>
    this.setState({ isDateTimePickerVisible: false });

  _handleDatePicked = date => {
    console.log("A date has been picked: ", date.dateString);

    this._hideDateTimePicker();
  };

  //MARK:--------------- Action Sheet Show Method --------------
  showActionSheet(index) {
    this.ActionSheet.show();
  }

  //MARK:-------------- Render Profile Picture View   --------------
  _renderProfilePictureView() {
    const data = this.state.userData;
    return (
      <View
        style={{
          height: 150,
          width: "100%",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <View style={{ height: 100, width: 100, borderRadius: 50 }}>
          <Image
            style={{ height: '100%', width: '100%', borderRadius: 50, }}
            source={(this.state.selectedUserImgPath && this.state.selectedUserImgPath.uri) ? { uri: this.state.selectedUserImgPath.uri } : Images.ic_user_profile}
            resizeMode="cover"
          />
          <TouchableHighlight
            style={{
              height: 30,
              width: 30,
              position: "absolute",
              alignSelf: "flex-end",
              bottom: 0,
            }}
            activeOpacity={0.9}
            underlayColor='transparent'
            onPress={() => {
              this.showImagePicker()
            }}
          >
            <Image
              style={{ height: 25, width: 25 }}
              source={Images.ic_image_camera}
              resizeMode="contain"
            />
          </TouchableHighlight>
        </View>
      </View>
    );
  }

  //MARK:-  ------------ On TextChange Method ---------------

  onTextChange(value, key, isAddress) {
    let data = this.state.userData;
    if (isAddress) {
      let address = data.address!=null?data.address:{phone:''};
      address[key] = value;
      data.address = address;
      console.log(JSON.stringify(address))
    } else {
      data[key] = value;
    }
    this.setState({
      userData: data
    },()=>console.log(JSON.stringify(this.state.userData)));
  }

  //MARK:---------------- Render title method -----------------
  _renderTitleView(title) {
    return (
      <View style={styles.styleTitleViewContainer}>
        <Text style={{ fontSize: 16, fontWeight: "bold", color: "black" }}>
          {title}
        </Text>
      </View>
    );
  }

  //MARK:-------------- Render Personal Details View------------------
  _renderPersonalDetailView() {
    const userDict = this.state.userData;
    return (
      <View>
        <CardView style={styles.card}
          cardElevation={2}
          cardMaxElevation={3}
          cornerRadius={10}>
          <View style={{ width: '100%', borderRadius: 10, overflow: 'hidden' }}>
            <View style={styles.cardheader}>
              <Image source={Images.img_primary_contact} style={{ width: 30, height: 30, resizeMode: "contain" }}></Image>
              <Text style={styles.cardheaderText}>Personal Details</Text>
            </View>
            <View style={[styles.cardCOntent, { padding: 10 }]}>
              <TextInput
                style={{
                  height: 34,
                  margin: 4,
                  fontSize: 13,
                  padding: 4,
                  fontFamily: Fonts.regular,
                  borderBottomColor: 'grey',
                  borderBottomWidth: 1
                }}
                value={userDict.first_name}
                onChangeText={text => this.onTextChange(text, "first_name", false)}
                placeholder={'First Name'}
                autoCapitalize={"none"}
                autoCorrect={false}
                placeholderTextColor={'grey'}
              />

              <TextInput
                style={{
                  height: 34,
                  margin: 4,
                  fontSize: 13,
                  padding: 4,
                  fontFamily: Fonts.regular,
                  borderBottomColor: 'grey',
                  borderBottomWidth: 1
                }}
                value={userDict.last_name}
                onChangeText={text => this.onTextChange(text, "last_name", false)}
                placeholder={'Last Name'}
                autoCapitalize={"none"}
                autoCorrect={false}
                placeholderTextColor={'grey'}
              />


            </View>
          </View>
        </CardView>
      </View>
    );
  }

  //MARK:-------------- Render Address Details View ---------------

  citySelectedHandler = cityDict => {
    console.log("cityDictD", cityDict.name)
    this.setState(prevState => ({
      userData: {
        ...prevState.userData,           // copy all other key-value pairs of userData object
        address: {                     // specific object of address object
          ...prevState.userData.address,   // copy all address key-value pairs
          city: cityDict.name          // update value of specific key
        }
      }
    }))
  }

  


  //MARK: ----------------- Render Contact Details View --------------
  _renderContactDetailsView() {
    const userData = this.state.userData;
    return (
        <CardView style={styles.card}
          cardElevation={2}
          cardMaxElevation={3}
          cornerRadius={10}>
          <View style={{ width: '100%', borderRadius: 10, overflow: 'hidden' }}>
            <View style={styles.cardheader}>
              <Image source={Images.img_primary_contact} style={{ width: 30, height: 30, resizeMode: "contain" }}></Image>
              <Text style={styles.cardheaderText}>Contact Details</Text>
            </View>
            <View style={[styles.cardCOntent, { padding: 10 }]}>

              <Text
                style={{
                  height: 34,
                  margin: 4,
                  fontSize: 13,
                  padding: 4,
                  fontFamily: Fonts.regular,
                  borderBottomColor: 'grey',
                  borderBottomWidth: 1
                }}
              >{userData.email}</Text>

              <TextInput
                style={{
                  height: 34,
                  margin: 4,
                  fontSize: 13,
                  padding: 4,
                  fontFamily: Fonts.regular,
                  borderBottomColor: 'grey',
                  borderBottomWidth: 1
                }}
                value={this.state.userData.address ? this.state.userData.address.phone ? this.state.userData.address.phone : "" : ""}
                onChangeText={text => this.onTextChange(text, "phone", true)}
                placeholder={'Phone Number'}
                keyboardType={'phone-pad'}
                maxLength={10}
                textContentType={'telephoneNumber'}
                autoCapitalize={"none"}
                autoCorrect={false}
                placeholderTextColor={'grey'}
              />

            </View>
          </View>
        </CardView>
    );
  }

  //MARK:---------------- Render Save Button -------------
  _renderSaveBtn2() {
    return (
      <View style={{ padding: 20 }}>
        <TouchableHighlight
          style={[
            styles.styleAdultOrChildViewContainer,
            {
              backgroundColor: colors.colorBlue,
              alignItems: "center",
              marginBottom: 15
            }
          ]}
          activeOpacity={0.9}
          underlayColor="transparent"
          onPress={() => {
            this.validateFiels()
          }}
        >
          <Text style={{ fontSize: 18, color: "white", fontWeight: "bold" }}>
            SAVE
          </Text>
        </TouchableHighlight>
      </View>
    );
  }


  //MARK:---------------- Render Save Button -------------
  _renderSaveBtn() {
    return (
      <View >
        <TouchableOpacity
          style={styles.styleSaveButton}
          underlayColor="transparent"
          activeOpacity={0.9}
          onPress={() => {
            this.validateFiels()
          }}
        >
          <Text style={{ fontSize: 16, color: "white", fontFamily: Fonts.semiBold }}>
            SAVE
          </Text>
        </TouchableOpacity>
      </View>
    );
  }

  _renderBackgroundImage() {
    return (
      <Image source={Images.img_home_bg}
        style={{
          height: screenHeight / 2.5,
          width: screenWidth,
          resizeMode: "stretch"
        }}>
      </Image>
    )
  }

  //MARK:---------------- Render Method -------------------------
  render() {
    if (!this.state.userData) {
      return (
        <View style={styles.container}>
          {Platform.OS == 'Android'
            ? < SafeAreaView backgroundColor={colors.colorBlue} ></SafeAreaView>
            : null
          }

          {this._renderBackgroundImage()}
          <View style={styles.mainContainer}>
            <StatusBar barStyle="light-content" backgroundColor={colors.colorBlue} />
            <CommomHeader title={"Edit Profile"} isTransparent={true} />
          </View>
        </View>
      );
    }
    return (
      <View style={styles.container}>
        {/* <SafeAreaView backgroundColor={colors.colorBlue}/> */}
        {this._renderBackgroundImage()}
        <View style={styles.mainContainer}>
         <SafeAreaView backgroundColor={colors.colorBlue}/>
        <CommomHeader title={"Edit Profile"} isTransparent={true} />

          <KeyboardAwareScrollView showsVerticalScrollIndicator={false} style={{width:'100%'}}>
            <View style={{ paddingBottom: 20 }}>
              {this._renderProfilePictureView()}
              {this._renderPersonalDetailView()}
              {this._renderContactDetailsView()}
            </View>
          </KeyboardAwareScrollView>
          <View style={{ backgroundColor: colors.colorBlue, width: '100%' }}>
            {this._renderSaveBtn()}
          </View>
        </View>
        <SafeAreaView />
      </View>
     
    );
  }
}

const mapStateToProps = (state) => {
  return {
    userData: state.userDataReducer
  }
}

const mapDispatchToProps = dispatch => {
  return {
    updateUserProfileAction: (data, props) => dispatch(updateUserProfileAction(data, props))
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(EditProfile);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // backgroundColor: "rgba(244,248,254,1)"
  },
  mainContainer: {
    position: "absolute",
    top: 0,
    width: '100%',
    height: '100%',
    flex: 1,
    left: 0,
    alignItems: "center",
    // backgroundColor: "red"
  },
  card: {
    backgroundColor: colors.colorWhite,
   // backgroundColor:'red',
   marginHorizontal:10,
    marginTop: 15,
  },
  cardheaderText: {
    color: colors.colorBlack,
    fontFamily: Fonts.medium,
    fontSize: 13, marginLeft: 12
  },
  cardheader: {
    height: 40, width: "100%",
    flexDirection: "row",
    alignItems: "center",
    backgroundColor: colors.gray,
    padding: 10
  },
  styleSaveButton: {
    width: "100%",
    height: 40,
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "center",
    backgroundColor: colors.colorBlue,
  },
  styleTitleViewContainer: {
    height: 30,
    width: "100%",
    justifyContent: "center",
    marginLeft: 5
  },
  styleDobAndNationalityContainer: {
    flexDirection: "row",
    height: 55,
    width: "100%",
    justifyContent: "space-between"
  },
  styleGenderContainerView: {
    flexDirection: "row",
    height: 40,
    alignItems: "center",
    padding: 5
  },
  styleGenderView: {
    marginLeft: 15,
    flexDirection: "row",
    alignItems: "center"
  },
  styleGenderMarkBtn: {
    height: 30,
    width: 30,
    justifyContent: "center",
    alignItems: "center"
  }
});
