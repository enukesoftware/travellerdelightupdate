import AsyncStorage from '@react-native-community/async-storage';
import React, { Component } from 'react';
import { Image, ImageBackground, Text, TouchableOpacity, View, StatusBar } from 'react-native';
import CardView from 'react-native-cardview';
import Swiper from 'react-native-swiper';
import { connect } from "react-redux";
import { getAirportListAction, getNationalityAction } from '../../Redux/Actions';
import { colors } from '../../Utils/Colors';
import CommonHeader from '../custom/CommonHeader'
import Fonts from '../../Utils/Fonts';
import Images from '../../Utils/Images';
import NavigationServices from '../../Utils/NavigationServices';
import { TouchableHighlight } from 'react-native-gesture-handler';



class Help extends Component {
  constructor() {
    super()
    this.state = {
      showDone: false,
      helpImages: [
        Images.ic_help_1,
        Images.ic_help_2,
        Images.ic_help_3,
        Images.ic_help_4,
        Images.ic_help_5,
        Images.ic_help_6,
        Images.ic_help_7,
        Images.ic_help_8,
        Images.ic_help_9,
        Images.ic_help_10,
        Images.ic_help_11,
      ],
      currentIndex:0
    }
  }

  componentDidMount() {
    // if (!this.props.airPortListData || this.props.airPortListData.length == 0) {
    //   this.props.getAirportListAction()
    // }
    // if (!this.props.nationalityListData || this.props.nationalityListData.length == 0) {
    //   this.props.getNationalityAction()
    // }
  }

  ChangeImage() {
    if(this.state.currentIndex<this.state.helpImages.length-1)
    this.setState({currentIndex: this.state.currentIndex+1})
    else{NavigationServices.goBack()}

  }



  Step() {
    return (
      <View style={{ flex: 1,width:'100%',height:'100%',position:'absolute', justifyContent: 'flex-start'}}>
        <TouchableHighlight activeOpacity={1} underlayColor={null} onPress={() => this.ChangeImage()}>
          <Image style={{ width: '100%', height: '100%', resizeMode: 'stretch' }}
            // imageStyle={{ width:'100%',height:'100%',resizeMode:'stretch' }}
            source={this.state.helpImages[this.state.currentIndex]} >
            {/* <Image resizeMode='contain' source={Images.ic_image_logo} style={{ width: '50%' }} /> */}
          </Image>
        </TouchableHighlight>
      </View>
    )
  }


  render() {
    return (
      <View style={{ flex: 1, backgroundColor:  '#00000e6' }}>
        <StatusBar backgroundColor={'#00000e6'} />
        {/* <CommonHeader isTransparent={true} title={''} /> */}
          {this.Step()}
        <CommonHeader isTransparent={true} title={''} />


          {/* <Swiper
            ref={pager => this.viewPager = pager}
            onIndexChanged={(index) => { this._onItemChanged(index) }}
            loop={false}
            scrollEnabled={false}
            // autoplay={true}
            showsPagination={false}
            autoplayTimeout={2.5}>

            {this.Step(Images.ic_help_1)}
            {this.Step(Images.ic_help_2)}

          </Swiper> */}


      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    airPortListData: state.airportListDataReducer,
    nationalityListData: state.nationalityDataReducer,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getAirportListAction: () => dispatch(getAirportListAction()),
    getNationalityAction: () => dispatch(getNationalityAction())
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Help)
