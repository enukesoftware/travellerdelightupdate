import moment from "moment";
import React, { Component } from 'react';
import { DeviceEventEmitter, Dimensions, FlatList, Image, StyleSheet, Text, TouchableHighlight, TouchableOpacity, View } from 'react-native';
import CardView from 'react-native-cardview';
import { SafeAreaView } from 'react-navigation';
import { connect } from "react-redux";
import { doLogoutAction, flightBookedListAction, getHotelTicketListAction, setLoadingAction } from '../Redux/Actions';
import { colors } from "../Utils/Colors";
import Fonts from '../Utils/Fonts';
import Images from '../Utils/Images';
import StringConstants from '../Utils/StringConstants';
import NavigationServices from './../Utils/NavigationServices';
import CommonHeader from './custom/CommonHeader';


const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;

const Page = "Booking List Page"

class BookingList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      hotelFlightSelectedIndex: 0,

      flightListData: [],
      selectedIndex: 0,
      hotelListData: [],

      hotelCompletedBookingData: [],
      hotelUpcomingBookingData: [],
      hotelCancelledBookingData: [],

      flightCompletedBookingData: [],
      flightUpcomingBookingData: [],
      flightCancelledBookingData: [],
      spinner: false,
      flightLoaded:false,
      hotelLoaded:false
    }


    // this.loginEventHandler = this.loginEventHandler.bind(this);
    this.logoutEventHandler = this.logoutEventHandler.bind(this);
    this.loginEventHandler = this.loginEventHandler.bind(this);
    this.renderHotelList = this.renderHotelList.bind(this);
    this.filterHoteldata = this.filterHoteldata.bind(this);
    this.sessionExpire = this.sessionExpire.bind(this);
    this.filterFlightData = this.filterFlightData.bind(this);
    this.closeLoaderFromFlight = this.closeLoaderFromFlight.bind(this);
    this.closeLoaderFromHotel = this.closeLoaderFromHotel.bind(this);
  }

  componentDidMount() {
    console.log("componenet did mount")

    if (this.props.isLogin) {
      this.getHotelBookedList(true)
      this.getFlightBookedList(true)
    }
    DeviceEventEmitter.addListener(StringConstants.FLIGHT_BOOKED_LIST_EVENT_FLAG, this.closeLoaderFromFlight)
    DeviceEventEmitter.addListener(StringConstants.HOTEL_BOOKED_LIST_EVENT_FLAG, this.closeLoaderFromHotel)
    DeviceEventEmitter.addListener(StringConstants.IS_LOGINEVENT, this.loginEventHandler)
    DeviceEventEmitter.addListener(StringConstants.IS_LOGOUTEVENT, this.logoutEventHandler)
    DeviceEventEmitter.addListener(StringConstants.SESSION_EXPIRE_EVENT, this.sessionExpire)
    DeviceEventEmitter.addListener(StringConstants.HOTEL_BOOKING_LIST_EVENT, this.filterHoteldata)
    DeviceEventEmitter.addListener(StringConstants.FLIGHT_BOOKED_LIST_EVENT, this.filterFlightData)
  }

  componentWillUnmount() {

    DeviceEventEmitter.removeListener(StringConstants.FLIGHT_BOOKED_LIST_EVENT_FLAG, this.closeLoaderFromFlight)
    DeviceEventEmitter.removeListener(StringConstants.HOTEL_BOOKED_LIST_EVENT_FLAG, this.closeLoaderFromHotel)
    DeviceEventEmitter.removeListener(StringConstants.IS_LOGINEVENT, this.loginEventHandler)
    DeviceEventEmitter.removeListener(StringConstants.IS_LOGOUTEVENT, this.logoutEventHandler)
    DeviceEventEmitter.removeListener(StringConstants.SESSION_EXPIRE_EVENT, this.sessionExpire)
    DeviceEventEmitter.removeListener(StringConstants.HOTEL_BOOKING_LIST_EVENT, this.filterHoteldata)
    DeviceEventEmitter.removeListener(StringConstants.FLIGHT_BOOKED_LIST_EVENT, this.filterFlightData)
  }

  closeLoaderFromHotel(){
    this.setState({hotelLoaded:true},()=>{
      if(this.state.hotelLoaded && this.state.flightLoaded){
        this.props.setLoadingAction(false)
      }
    })
  }

  closeLoaderFromFlight(){
    this.setState({flightLoaded:true},()=>{
      if(this.state.hotelLoaded && this.state.flightLoaded){
        this.props.setLoadingAction(false)
      }
    })
  }

  filterHoteldata(data) {

    var upcomingBookings = [];
    var todayDate = new Date();
    var cancelledBookings = [];

    todayDate = moment(todayDate).format("YYYY-MM-DD")

    var completedBooking = data && data.length > 0 && data.filter(item => {
      itemDate = moment(item.check_in).format("YYYY-MM-DD")
      // return itemdate < todayDate
      if (item.status == 2 || item.cancellation) {
        cancelledBookings.push(item)
      }
      else
        if (itemDate < todayDate) {
          return item;
        }
        else {
          upcomingBookings.push(item);
        }
    })


    this.setState({
      hotelUpcomingBookingData: upcomingBookings,
      hotelCompletedBookingData: completedBooking,
      hotelCancelledBookingData: cancelledBookings
    })
    console.log("past bookings " + JSON.stringify(completedBooking));
    console.log("upcoming bookings" + JSON.stringify(upcomingBookings));
  }

  sessionExpire() {
    this.props.doLogoutAction()
  }

  filterFlightData(data) {
    console.log("FLIGHT_RES:" + JSON.stringify(data))
    var upcomingBookings = [];
    var todayDate = new Date();
    var cancelledData = [];

    todayDate = moment(todayDate).format("YYYY-MM-DD")

    var completedBooking = data && data.length > 0 && data.filter(item => {
      if (item.flights && item.flights.length > 0) {
        let itemdate = moment(item.flights[0].depart_at).format("YYYY-MM-DD")
        // return itemdate < todayDate
        if (item.cancelled == 1) {
          cancelledData.push(item)
        }
        else if (itemdate < todayDate) {
          return item;
        }
        else {
          upcomingBookings.push(item);
        }
      }
    })



    this.setState({
      flightUpcomingBookingData: upcomingBookings,
      flightCompletedBookingData: completedBooking,
      flightCancelledBookingData: cancelledData
    })
  }

  logoutEventHandler(islogout) {
    console.log(Page + " in hander logout event handler ")
    if (islogout) {
      console.log(Page + "logging out")
      this.setState({
        isLogin: false
      })
      // Actions.refresh()
    } else {
      setTimeout(() => {
        alert("Unable to logout user")
      }, 100)
    }
  }

  loginEventHandler(isLogin) {
    console.log("in hander login event handler ")
    if (isLogin) {
      this.setState({
        isLogin: true
      })
      this.getHotelBookedList(false)
      this.getFlightBookedList(false)
    } else {
      this.setState({
        isLogin: false
      })
    }
  }



  getFlightBookedList(data) {
    this.props.flightBookedListAction(data)
  }

  getHotelBookedList(data) {
    console.log("calling hotel booking list  api")
    this.props.getHotelTicketListAction(data)
  }

  noBookingItem() {
    let text = ""
    if (this.props.isLogin) {
      text = "You do not have any booking with us"
    } else {
      text = "Please login to see your trips"
    }
    return (
      <View style={{ justifyContent: 'center', alignItems: 'center', flex: 1 }}>
        <Image
          style={{ height: 100, width: 105, margin: 10 }}
          source={Images.ic_image_nervous} />
        <Text>
          {text}
        </Text>
      </View>
    )
  }

  _renderHotelListItem = ({ item, index }) => {
    return (
      <CardView
        style={{
          backgroundColor: colors.colorWhite,
          marginHorizontal: 10,
          marginVertical: 5
        }}
        cardElevation={2}
        cardMaxElevation={3}
        cornerRadius={8}>
        <TouchableOpacity onPress={() => NavigationServices.navigate("HotelBookingDetails", { id: item.id })} style={{ width: '100%', borderRadius: 8, overflow: 'hidden', flexDirection: 'row', }}>
          <View style={{ width: 60, alignItems: 'center', justifyContent: 'flex-start', padding: 8 }}>
            <View style={{ width: 30, height: 30, backgroundColor: '#f5f5f5', borderRadius: 15, overflow: 'hidden', alignItems: 'center', justifyContent: 'center' }}>
              <Image source={Images.ic_image_bed} tintColor={colors.colorBlue} style={{ height: 20, width: 20, resizeMode: 'center', tintColor: colors.colorBlue }} />
            </View>
          </View>
          <View style={{ flex: 1, padding: 8 }}>
            <View style={{ flexDirection: 'row', width: '100%', alignItems: 'center', paddingBottom: 8 }}>
              <Text style={{ fontSize: 12, fontFamily: Fonts.semiBold, color: colors.colorBlack21 }}>{item.hotel_name}</Text>
            </View>

            <View style={{ flexDirection: 'row', width: '100%', alignItems: 'center', paddingBottom: 8 }}>
              <View style={{ width: '50%', flexDirection: 'row', alignItems: 'center' }}>
                <Image tintColor={colors.colorBlack21} style={{ width: 10, height: 10, marginRight: 5 }} resizeMode={'contain'} source={Images.ic_calander} />

                <Text style={{ fontSize: 9, fontFamily: Fonts.medium, color: colors.colorBlack21 }}>{"Check In: "}</Text>
                <Text style={{ fontSize: 9, fontFamily: Fonts.regular, color: colors.colorBlack21 }}>{item.check_in ? moment(item.check_in).format("DD MMM, YYYY") : "2 December, 2019"}</Text>
              </View>
              <View style={{ width: '50%', justifyContent: 'center', flexDirection: 'row', alignItems: 'center' }}>
                <Text style={{ fontSize: 9, fontFamily: Fonts.medium, color: colors.colorBlack21 }}>{" Check Out: "}</Text>
                <Text style={{ fontSize: 9, fontFamily: Fonts.regular, color: colors.colorBlack21 }}>{item.check_out ? moment(item.check_out).format("DD MMM, YYYY") : "2 December, 2019"}</Text>
              </View>
            </View>

            <View style={{ flexDirection: 'row', width: '100%', alignItems: 'center', paddingBottom: 8 }}>
              <Image tintColor={colors.colorBlack21} style={{ width: 10, height: 10, marginRight: 5 }} resizeMode={'contain'} source={Images.ic_user_black} />
              <Text numberOfLines={1} ellipsizeMode={'tail'} style={{ fontSize: 9, fontFamily: Fonts.regular, color: colors.colorBlack21 }}>{item.guests[0].first_name + " " + item.guests[0].last_name + (item.guests.length > 1 ? (", +" + (item.guests.length - 1) + " More") : "")}</Text>
            </View>

            <View style={{ flexDirection: 'row', width: '100%', alignItems: 'center', paddingBottom: 8 }}>
              <Text numberOfLines={1} ellipsizeMode={'tail'} style={{ fontSize: 9, fontFamily: Fonts.regular, color: colors.colorBlack21 }}>{"Hotel in " + item.hotel_city}</Text>
              <Text numberOfLines={1} ellipsizeMode={'tail'} style={{ fontSize: 9, fontFamily: Fonts.regular, color: colors.colorBlack21 }}>{" | Reviwed"}</Text>
            </View>
          </View>
        </TouchableOpacity>
      </CardView>)
  }

  renderHotelList() {
    let hotelData = [];
    if (this.state.selectedIndex == 0) {
      hotelData = this.state.hotelUpcomingBookingData
    } else if (this.state.selectedIndex == 1) {
      hotelData = this.state.hotelCompletedBookingData
    }
    else {
      hotelData = this.state.hotelCancelledBookingData
    }

    return (
      <View style={{ margin: 10, marginTop: 20, flex: 1 }}>
        <FlatList
          style={{ flex: 1 }}
          showsVerticalScrollIndicator={false}
          data={hotelData}
          renderItem={this._renderHotelListItem}
        />
      </View>
    )
  }

  renderHotelFlightTab() {
    return (
      <View style={{ width: '100%', alignItems: 'center' }}>
        <View style={styles.tabs}>
          <TouchableOpacity activeOpacity={0.9} style={[styles.tabButton,
          { backgroundColor: this.state.hotelFlightSelectedIndex == 0 ? colors.colorGold : colors.colorWhite, }
          ]}
            onPress={() =>
              this.setState({
                hotelFlightSelectedIndex: 0
              })
            }>
            <Text style={[styles.tabText, {
              color: this.state.hotelFlightSelectedIndex == 0 ? colors.colorWhite : colors.colorBlack
            }]}>Flight</Text>
          </TouchableOpacity>
          <TouchableOpacity activeOpacity={0.9} style={[styles.tabButton, {
            backgroundColor: this.state.hotelFlightSelectedIndex == 1 ? colors.colorGold : colors.colorWhite,
          }]} onPress={() =>
            this.setState({
              hotelFlightSelectedIndex: 1
            })
          }>
            <Text style={[styles.tabText, {
              color: this.state.hotelFlightSelectedIndex == 1 ? colors.colorWhite : colors.colorBlack
            }]}>Hotel</Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }

  renderHotelFlightTab2() {
    return (
      <View style={styles.hotelflighttabcontainer}>
        <View style={styles.hotelflighttabstyle}>
          <TouchableHighlight
            underlayColor="transparent"
            activeOpacity={0.9}
            onPress={() => {
              // this.getFlightBookedList()
              this.setState({
                hotelFlightSelectedIndex: 0
              });
            }}
            style={
              this.state.hotelFlightSelectedIndex == 0
                ? styles.hotelflightselectedTab
                : styles.hotelflightnotSelectedTab
            }
          >
            <Text
              style={
                this.state.hotelFlightSelectedIndex == 0
                  ? styles.selectedTabText
                  : styles.notSelectedTabText
              }
            >
              Flight
              </Text>
          </TouchableHighlight>
          <TouchableHighlight
            activeOpacity={0.9}
            underlayColor="transparent"
            onPress={() => {
              this.setState({
                hotelFlightSelectedIndex: 1
              });
            }}
            style={
              this.state.hotelFlightSelectedIndex == 1
                ? styles.hotelflightselectedTab
                : styles.hotelflightnotSelectedTab
            }
          >
            <Text
              style={
                this.state.hotelFlightSelectedIndex == 1
                  ? styles.selectedTabText
                  : styles.notSelectedTabText
              }
            >
              Hotel
              </Text>
          </TouchableHighlight>
        </View>
      </View>
    )
  }

  upcomingCompletetab() {
    return (
      <View style={styles.upcomingTabContainer}>
        <TouchableOpacity
          activeOpacity={0.9}
          style={[this.state.selectedIndex == 0 ? styles.selectedTab : styles.unSelectedTab, { borderBottomLeftRadius: 20, borderTopLeftRadius: 20 }]}
          underlayColor={'transparent'}
          onPress={() => this.setState({ selectedIndex: 0 })}
        >
          <Text style={this.state.selectedIndex == 0 ? styles.selectedTabText : styles.unSelectedTabText}>
            Upcoming
        </Text>
        </TouchableOpacity>
        <TouchableOpacity
          activeOpacity={0.9}
          style={[this.state.selectedIndex == 1 ? styles.selectedTab : styles.unSelectedTab, { borderLeftWidth: 1, borderLeftColor: 'grey', borderRightColor: 'grey', borderRightWidth: 1 }]}
          underlayColor={'transparent'}
          onPress={() => this.setState({ selectedIndex: 1 })}
        >
          <Text style={this.state.selectedIndex == 1 ? styles.selectedTabText : styles.unSelectedTabText}>
            Completed
        </Text>
        </TouchableOpacity>
        <TouchableOpacity
          activeOpacity={0.9}
          style={[this.state.selectedIndex == 2 ? styles.selectedTab : styles.unSelectedTab, { borderBottomRightRadius: 20, borderTopRightRadius: 20 }]}
          underlayColor={'transparent'}
          onPress={() => this.setState({ selectedIndex: 2 })}
        >
          <Text style={this.state.selectedIndex == 2 ? styles.selectedTabText : styles.unSelectedTabText}>
            Cancelled
        </Text>
        </TouchableOpacity>
      </View>
    )
  }

  renderFlightList() {

    let flightData = [];
    if (this.state.selectedIndex == 0) {
      flightData = this.state.flightUpcomingBookingData
    } else if (this.state.selectedIndex == 1) {
      flightData = this.state.flightCompletedBookingData
    }
    else {
      hotelData = this.state.hotelCancelledBookingData
    }
    // console.log("FlightListData",JSON.stringify(flightData))
    return (
      <View style={{ margin: 10, marginTop: 20, flex: 1, }}>
        <FlatList
          style={{ flex: 1, }}
          showsVerticalScrollIndicator={false}
          data={flightData}
          renderItem={(item) => this.renderFlightListItem(item)}
        />
      </View>
    )
  }

  showBookingItems() {
    return (
      <View style={{ flex: 1 }}>
        {this.renderHotelFlightTab()}
        {this.upcomingCompletetab()}
        {(this.state.hotelFlightSelectedIndex == 1 ? this.renderHotelList() : this.renderFlightList())}
      </View>
    )
  }

  flightListItemRow(text1, text2) {
    return (
      <View style={{ flexDirection: "row", justifyContent: "space-between", marginTop: 10 }}>
        <Text>
          {text1}
        </Text>
        <Text style={{ color: colors.colorBlue }}>
          {text2}
        </Text>
      </View>
    )
  }
  renderFlightListItem({ item }) {
    console.log("FlightBookItem:", item)
    let origin = item.flights[0].origin_city.city_name
    let destination = item.flights[item.flights.length - 1].destination_city.city_name
    let RoundTrip = false

    if (item.flights[0].trip_indicator != 0) {
      let flights = item.flights.filter((item) => {
        if (RoundTrip === false && item.trip_indicator != 1) {
          RoundTrip = true
        }
        return item.trip_indicator == 1
      })
      destination = flights[flights.length - 1].destination_city.city_name
    }

    return (
      <CardView
        style={{
          backgroundColor: colors.colorWhite,
          marginHorizontal: 10,
          marginVertical: 5
        }}
        cardElevation={2}
        cardMaxElevation={3}
        cornerRadius={8}>
        <TouchableOpacity onPress={() => NavigationServices.navigate("BookingDetails", { id: item.id })} style={{ width: '100%', borderRadius: 8, overflow: 'hidden', flexDirection: 'row', padding: 5 }}>
          <View style={{ width: 60, alignItems: 'center', justifyContent: 'flex-start', padding: 8 }}>
            <View style={{ width: 30, height: 30, backgroundColor: '#f5f5f5', borderRadius: 15, overflow: 'hidden', alignItems: 'center', justifyContent: 'center' }}>
              <Image source={Images.ic_image_planeblue} style={{ height: 20, width: 20, resizeMode: 'center' }} />
            </View>
          </View>
          <View style={{ flex: 1, padding: 8 }}>
            <View style={{ flexDirection: 'row', width: '100%', alignItems: 'center', paddingBottom: 8 }}>
              <Text style={{ fontSize: 12, fontFamily: Fonts.semiBold, color: colors.colorBlack21 }}>{origin}</Text>
              <Image tintColor={colors.colorBlack21} style={{ width: 11, height: 11, marginHorizontal: 5, tintColor: colors.colorBlack21 }} resizeMode={'contain'} source={Images.ic_right_arrow_big} />
              <Text style={{ fontSize: 12, fontFamily: Fonts.semiBold, color: colors.colorBlack21 }}>{destination}</Text>
            </View>

            <View style={{ flexDirection: 'row', width: '100%', alignItems: 'center', paddingBottom: 8 }}>
              <Image tintColor={colors.colorBlack21} style={{ width: 10, height: 10, marginRight: 5 }} resizeMode={'contain'} source={Images.ic_calander} />
              <Text style={{ fontSize: 9, fontFamily: Fonts.regular, color: colors.colorBlack21, letterSpacing: 0.7 }}>{item.doj ? moment(item.doj).format("DD MMMM, YYYY") : "2 December, 2019"}</Text>
              <Image tintColor={colors.colorBlack21} style={{ width: 10, height: 10, marginRight: 5, marginLeft: 10 }} resizeMode={'contain'} source={Images.ic_user_black} />
              <Text numberOfLines={1} ellipsizeMode={'tail'} style={{ fontSize: 9, fontFamily: Fonts.regular, color: colors.colorBlack21 }}>{item.passengers[0].first_name + " " + item.passengers[0].last_name + (item.passengers.length > 1 ? (", +" + (item.passengers.length - 1) + " More") : "")}</Text>
            </View>

            <View style={{ flexDirection: 'row', width: '100%', alignItems: 'center', paddingBottom: 8 }}>
              <Text numberOfLines={1} ellipsizeMode={'tail'} style={{ fontSize: 9, fontFamily: Fonts.regular, color: colors.colorBlack21, letterSpacing: 0.7 }}>{RoundTrip?"Round Trip":"One Way"}</Text>
              {/* <Text numberOfLines={1} ellipsizeMode={'tail'} style={{ fontSize: 9, fontFamily: Fonts.regular, color: colors.colorBlack21, letterSpacing: 0.7 }}>{" | Economy"}</Text> */}
              <Text numberOfLines={1} ellipsizeMode={'tail'} style={{ fontSize: 9, fontFamily: Fonts.regular, color: colors.colorBlack21, letterSpacing: 0.7 }}>{" | "}{item.flights[0].airline_name}</Text>
            </View>
          </View>
        </TouchableOpacity>
      </CardView>)

  }

  _renderBackgroundImage() {
    return (
      <Image source={Images.img_home_bg}
        style={{
          height: screenHeight / 2.5,
          width: screenWidth,
          resizeMode: "stretch"
        }}>
      </Image>
    )
  }

  render() {
    return (
      <View style={styles.container}>
        {this._renderBackgroundImage()}
        <View style={styles.mainContainer}>
          <SafeAreaView backgroundColor={'transparent'} />
          <CommonHeader title={"My Trips"} noBack={true} isTransparent={true} />
          <View style={{ flex: 1, position: 'relative' }}>
            {(this.props.isLogin ? this.showBookingItems() : this.noBookingItem())}
          </View>
        </View>
      </View>
    )
  }

}

const mapStateToProps = (state) => {
  return {
    isLogin: state.isLoginReducer,
    userData: state.userDataReducer
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getHotelTicketListAction: (data) => dispatch(getHotelTicketListAction(data)),
    flightBookedListAction: () => dispatch(flightBookedListAction()),
    doLogoutAction: () => dispatch(doLogoutAction()),
    setLoadingAction: (data) => dispatch(setLoadingAction(data))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(BookingList)

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  mainContainer: {
    position: "absolute",
    top: 0,
    width: '100%',
    height: '100%',
    flex: 1,
    left: 0,
    // alignItems: "center",
    // backgroundColor: "red"
  },
  tabs: {
    height: 40,
    width: screenWidth - 24,
    borderRadius: 10,
    flexDirection: "row",
    overflow: "hidden",
    borderWidth: .5,
    borderColor: "lightgrey",
    marginTop: 20,
  },
  tabButton: {
    height: "100%",
    width: (screenWidth - 24) / 2,
    alignItems: "center",
    justifyContent: "center"
  },
  tabText: {
    color: colors.colorBlack,
    fontSize: 14,
    fontFamily: Fonts.medium
  },
  upcomingTabContainer: {
    width: 300,
    marginTop: 30,
    height: 40,
    alignSelf: 'center',
    flexDirection: 'row',
    borderWidth: .5,
    borderColor: 'grey',
    borderRadius: 20,
    shadowColor: "gray",
    shadowOpacity: 0.3,
    shadowOffset: { width: 1, height: 1 },
    shadowRadius: 5,
    elevation: 4


  },
  selectedTab: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colors.colorBlue,

  },
  unSelectedTab: {
    flex: 1,
    backgroundColor: colors.colorWhite,
    justifyContent: 'center',
    alignItems: 'center',
  },
  selectedTabText: {
    fontFamily: Fonts.medium,
    color: 'white',
    fontSize: 13,
  },
  unSelectedTabText: {
    color: 'grey',
    fontFamily: Fonts.medium,
    fontSize: 13,
  },
  hotelflighttabcontainer: {
    shadowColor: "gray",
    shadowOpacity: 0.2,
    elevation: 4,
    shadowOffset: { width: 0, height: 5 }
  },
  hotelflighttabstyle: {
    height: 50,
    backgroundColor: "white",
    borderRadius: 25,
    margin: 30,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    shadowColor: "gray",
    shadowOpacity: 0.4,
    elevation: 6,
    shadowOffset: { width: 0, height: 3 }
  },
  hotelflightnotSelectedTab: {
    backgroundColor: "white",
    height: 50,
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 25
  },
  hotelflightselectedTab: {
    height: 50,
    flex: 1,
    backgroundColor: colors.colorBlue,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 25,
    shadowOpacity: 0.4,
    shadowOffset: { width: 0, height: 1 },
    shadowRadius: 1
  },
  selectedTabText: {
    color: "white",
    fontWeight: "bold",
    fontSize: 14
  },
  notSelectedTabText: {
    color: colors.colorBlue,
    fontSize: 14,
    fontWeight: "bold"
  },
  hotellistbtn: {
    marginLeft: 10,
    backgroundColor: colors.colorBlue,
    alignItems: "center",
    justifyContent: "center",
    padding: 10,
    borderRadius: 4
  },
  hotelitemlist: {
    margin: 10,
    backgroundColor: "white",
    borderRadius: 5,
    shadowColor: "gray",
    shadowOpacity: 0.5,
    shadowOffset: { width: 1, height: 1 },
    shadowRadius: 5,
    elevation: 6
  },
  flightlistitem: {
    margin: 10,
    backgroundColor: "white",
    borderRadius: 5,
    shadowColor: "gray",
    shadowOpacity: 0.5,
    shadowOffset: { width: 1, height: 1 },
    shadowRadius: 5,
    elevation: 6
  },
});
