
import React, { Component } from "react";
import { DeviceEventEmitter, Dimensions, Image, ImageBackground, Platform, ScrollView, StyleSheet, Text, TouchableHighlight, TouchableOpacity, View } from "react-native";
import CardView from 'react-native-cardview';
import Dash from 'react-native-dash';
import { SafeAreaView } from "react-navigation";
import { connect } from "react-redux";
import { getAirportListAction, getNationalityAction, getPageSettingAction, isLoginAction, setLoadingAction, userDataAction, userTokenAction } from '../Redux/Actions';
import { imageBaseUrl } from "../Utils/APIManager/APIConstants";
import { colors } from "../Utils/Colors";
import Fonts from "../Utils/Fonts";
import Images from "../Utils/Images";
import StringConstants from '../Utils/StringConstants';
import NavigationServices from './../Utils/NavigationServices';
import CommonHeader from './custom/CommonHeader';


const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      spinner: false,
      banners: [{
        image: Images.ic_image_plane1,
        title: "Up to 25% instant discount on international flights",
        validity: "Valid Till 25 December"
      }, {
        image: Images.ic_image_plane2,
        title: "Up to 15% instant discount on international flights",
        validity: "Valid Till 31 January"
      }, {
        image: Images.ic_image_plane1,
        title: "Up to 25% instant discount on international flights",
        validity: "Valid Till 25 December"
      }],
      hotel_lower_section_title: null,
      hotel_upper_section_title: null,
      flight_routes_title: null,
      top_destination: null,
      top_intl_destination: null,
      top_route: null,
      routeIcons: [
        Images.ic_top_route_1,
        Images.ic_top_route_2,
        Images.ic_top_route_3,
        Images.ic_top_route_4,
      ]
    }
    this.updatePageSetting = this.updatePageSetting.bind(this)
  }
  //-----------------Components Life cycle Methods ---------------

  componentDidMount() {
    // console.log("home component did mount");
    // if (!this.props.airPortListData)
    //   this.props.getAirportListAction();
    if (!this.props.airPortListData || this.props.airPortListData.length == 0) {
      this.props.getAirportListAction()
    }
    if (!this.props.nationalityListData || this.props.nationalityListData.length == 0) {
      this.props.getNationalityAction()
    }
    this.props.getPageSettingAction()
    DeviceEventEmitter.addListener(StringConstants.PAGE_SETTING_EVENT, this.updatePageSetting)

    //this.updatePageSetting(pageSetting)
  }

  componentWillUnmount() {
    DeviceEventEmitter.removeListener(StringConstants.PAGE_SETTING_EVENT, this.updatePageSetting)
  }

  updatePageSetting(data) {
    if (!data) { console.log("PAGE_SETTING: ", "NOT FOUND ANY DATA"); return; }
    console.log("PAGE_SETTING: ", JSON.stringify(data))
    let hotel_lower_section_title = "Top International Destination"
    let hotel_upper_section_title = "Top Destinations"
    let flight_routes_title = "Top Flight Routes"
    let top_destination = null
    let top_intl_destination = null
    let top_route = null

    if (data.page_title) {
      if (data.page_title.flight_routes_title) {
        flight_routes_title = data.page_title.flight_routes_title
      }
      if (data.page_title.hotel_upper_section_title) {
        hotel_upper_section_title = data.page_title.hotel_upper_section_title
      }
      if (data.page_title.hotel_lower_section_title) {
        hotel_lower_section_title = data.page_title.hotel_lower_section_title
      }
    }
    if (data.top_destination && data.top_destination.length > 0) {
      top_destination = data.top_destination
    }
    if (data.top_intl_destination && data.top_intl_destination.length > 0) {
      top_intl_destination = data.top_intl_destination
    }
    if (data.top_route && data.top_route.length > 0) {
      top_route = data.top_route
    }

    this.setState({
      hotel_lower_section_title,
      hotel_upper_section_title,
      flight_routes_title,
      top_destination,
      top_intl_destination,
      top_route
    }, () => {
      // console.log("New State",JSON.stringify(this.state))
    })
  }

  renderTopDestinationView(title, array) {
    if (array && array.length > 0)
      return (
        <View style={{
          // backgroundColor: '#ffffff60',
          marginVertical: 15,
        }}>
          <View style={{ width: "100%", flexDirection: "row", justifyContent: "space-between", alignItems: "center", paddingHorizontal: 15 }}>
            <Text style={{ color: colors.colorBlack21, fontSize: 14, fontFamily: Fonts.semiBold, alignSelf: 'baseline' }}>{title}</Text>
            {/* <TouchableOpacity style={{ marginRight: 15 }} onPress={() => { }}>
              <Text style={{ color: colors.colorBlack21, fontSize: 12, fontFamily: Fonts.semiBold }}>VIEW ALL</Text>
            </TouchableOpacity> */}
          </View>
          <View style={{ width: '100%', marginTop: 10, paddingHorizontal: 15 }}>
            <ScrollView horizontal={true}
              showsHorizontalScrollIndicator={false}>
              {
                array.map((destination, index) => (
                  <CardView
                    cardElevation={2}
                    cardMaxElevation={3}
                    cornerRadius={5}
                    style={{ backgroundColor: colors.colorWhite, marginHorizontal: 15, marginLeft: 6, marginRight: 6, }}>

                    <TouchableHighlight
                      activeOpacity={0.7}
                      //underlayColor={'none'}
                      onPress={() => {
                        const cityDict = { "id": destination.city_id, "country": destination.country_name, "name": destination.city_name, "country_code": destination.country_name, "country_id": "" }
                        NavigationServices.navigate("HotelSearch", { cityDict: cityDict })
                      }} key={index} style={{ borderRadius: 5, overflow: "hidden" }}>
                      <ImageBackground resizeMode={'cover'} style={{
                        // width: 259,
                        // height: 189,
                        // width: 150,
                        // height: 189,
                        width: (screenWidth / 2) - 45,
                        height: 200,
                        backgroundColor: 'white',
                        overflow: "hidden"
                      }} source={{ uri: imageBaseUrl + destination.city_image }}>
                        <View style={{
                          flex: 1,
                          backgroundColor: Platform.OS == "ios" ? 'rgba(0, 0, 0, .3)' : 'rgba(0, 0, 0, .3)',
                          justifyContent: "flex-end"
                        }}>
                          <Text style={{
                            fontFamily: Fonts.semiBold,
                            fontSize: 12,
                            color: colors.colorWhite,
                            margin: 10
                          }}>{destination.city_name}</Text>
                        </View>
                      </ImageBackground>
                    </TouchableHighlight>
                  </CardView>
                ))
              }
            </ScrollView>
          </View></View>
      )
  }

  renderTopRoutes() {
    let { flight_routes_title, top_route, routeIcons } = this.state;
    if (top_route && top_route.length > 0) {
      return (
        <View style={{ backgroundColor: '#ffffff60', marginVertical: 5 }}>
          <View style={{ width: "100%", flexDirection: "row", justifyContent: "space-between", alignItems: "center", paddingHorizontal: 15 }}>
            <Text style={{ color: colors.colorBlack21, fontSize: 14, fontFamily: Fonts.semiBold, alignSelf: 'baseline' }}>{flight_routes_title}</Text>
            {/* <TouchableOpacity style={{ marginRight: 15 }} onPress={() => { }}>
              <Text style={{ color: colors.colorBlack21, fontSize: 12, fontFamily: Fonts.semiBold }}>VIEW ALL</Text>
            </TouchableOpacity> */}
          </View>
          <View style={{ width: '100%', marginTop: 10, paddingLeft: 6, paddingRight: 6 }}>
            <CardView
              cardElevation={2}
              cardMaxElevation={3}
              cornerRadius={5}
              style={{ backgroundColor: colors.colorWhite, marginHorizontal: 15, }}>
              <View style={{ borderRadius: 5, overflow: "hidden", width: '100%' }}>
                <ScrollView>
                  {top_route.map((item, index) => {
                    if (index < 4)
                      return (<>
                        <TouchableOpacity onPress={() => {
                          const data = {
                            sourceDict: {
                              "airport_name": "No Origin Name",
                              "country_code": item.origin_country_code,
                              "country_name": item.origin_country_name,
                              "city_name": item.origin_city_name,
                              "airport_code": item.origin_city_code,
                              "top_city": 1,
                              "isSource": true
                            },
                            destinationDict: {
                              "airport_name": "No Destination Name",
                              "country_code": item.destination_country_code,
                              "country_name": item.destination_country_name,
                              "city_name": item.destination_city_name,
                              "airport_code": item.destination_city_code,
                              "top_city": 1,
                              "isSource": false
                            }
                          }
                          NavigationServices.navigate("FlightSearch", { data: data })
                        }} style={{ width: '100%', padding: 10, margin: 2, flexDirection: 'row', alignItems: 'center' }}>
                          <Image source={routeIcons[index]} resizeMode={'contain'} style={{ height: 30, width: 30, }} />
                          <Text style={{ fontFamily: Fonts.semiBold, fontSize: 13, color: '#606060', paddingHorizontal: 10 }}>
                            {item.origin_city_name} to {item.destination_city_name} ({item.origin_city_code}-{item.destination_city_code})
                          </Text>
                        </TouchableOpacity>
                        {index < 3 && <Dash dashGap={4} dashLength={3} dashThickness={0.5} dashColor={'grey'} />}
                      </>
                      )
                  })
                  }
                </ScrollView>
              </View>
            </CardView>
          </View>
        </View>)
    }

  }



  render() {
    // console.log("home component did mount");

    let { banners, destinations, hotel_upper_section_title, hotel_lower_section_title, flight_routes_title, top_destination, top_intl_destination, top_route } = this.state;
    return (
      <View style={styles.container}>
        {/* <StatusBar barStyle="light-content" backgroundColor={colors.colorBlue} /> */}
        <Image source={Images.img_home_bg} style={{ height: screenHeight / 2.5, width: screenWidth, resizeMode: "stretch" }} />
        <View style={styles.mainContainer}>
          <SafeAreaView />
          <CommonHeader title={"Traveller Delight"} noBack={true} isTransparent={true} fireEvent={() => { }} rightImg={Images.ic_image_notifications} />
          <View style={{ width: '100%', flex: 1 }}>
            <ScrollView showsVerticalScrollIndicator={false} style={{ flex: 1, width: '100%', paddingBottom: 25 }}>
              <View style={styles.mainButtonContainer}>
                <TouchableOpacity style={styles.mainButton} onPress={() => {
                  NavigationServices.navigate('FlightSearch');
                }}>
                  <Image
                    source={Images.ic_flight_search}
                    style={{ height: 35, width: 35, resizeMode: "contain" }}></Image>
                  <Text style={styles.mainButtontext}>Flight Search</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.mainButton} onPress={() => {
                  NavigationServices.navigate('HotelSearch');
                }}>
                  <Image
                    source={Images.ic_hotel_search}
                    style={{ height: 35, width: 35, resizeMode: "contain" }}></Image>
                  <Text style={styles.mainButtontext}>Hotel Search</Text>
                </TouchableOpacity>
              </View>

              {/* <View style={styles.bannerContainer}>
                <ScrollView horizontal={true}
                  showsHorizontalScrollIndicator={false}>
                  {
                    banners.map((banner, index) => (
                      <View key={index} style={{
                        marginLeft: 6,
                        marginRight: 6,
                      }}>
                        <ImageBackground style={styles.bannerImage} source={banner.image}>
                          <View style={styles.innerFrame}>
                            <Text style={styles.bannerTitle}>{banner.title}</Text>
                            <Text style={styles.bannerValidity}>{banner.validity}</Text>
                          </View>
                        </ImageBackground>
                        <View style={styles.bannerButton} >
                          <TouchableOpacity style={{
                            flex: 1, width: 130,
                            justifyContent: "center", alignItems: "center"
                          }} onPress={() => { }}>
                            <Text style={styles.bannerButtonText}>VIEW DETAILS</Text>
                          </TouchableOpacity>
                        </View>
                      </View>

                    ))
                  }
                </ScrollView>
              </View> */}

              {this.renderTopDestinationView(hotel_upper_section_title, top_destination)}
              {this.renderTopRoutes()}
              {this.renderTopDestinationView(hotel_lower_section_title, top_intl_destination)}




            </ScrollView>
          </View>
        </View>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    userData: state.userDataReducer,
    isLogin: state.isLoginReducer,
    userToken: state.userTokenReducer,
    airPortListData: state.airportListDataReducer,
    nationalityListData: state.nationalityDataReducer,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getAirportListAction: () => dispatch(getAirportListAction()),
    setLoadingAction: (data) => dispatch(setLoadingAction(data)),
    userDataAction: (data) => dispatch(userDataAction(data)),
    isLoginAction: (data) => dispatch(isLoginAction(data)),
    userTokenAction: (data) => dispatch(userTokenAction(data)),
    getNationalityAction: () => dispatch(getNationalityAction()),
    getPageSettingAction: () => dispatch(getPageSettingAction())
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Home)

const styles = StyleSheet.create({
  bannerButtonText: {
    fontSize: 14,
    fontWeight: "500",
    color: colors.colorWhite
  },
  bannerButton: {
    backgroundColor: colors.colorBlue,
    height: 40, width: 130,
    justifyContent: "center", alignItems: "center",
    position: "absolute",
    bottom: 10,
    left: 14,
    zIndex: 2,
    borderRadius: 8,
    overflow: "hidden"
  },
  bannerValidity: {
    fontWeight: "400",
    fontSize: 14,
    color: colors.colorWhite,
    lineHeight: 25,
    marginTop: 5
  },
  bannerTitle: {
    fontWeight: "700",
    fontSize: 16,
    color: colors.colorWhite,
    lineHeight: 25,
  },
  bannerImage: {
    height: 130,
    width: screenWidth - 70,
    borderRadius: 10,
    backgroundColor: "red",
    overflow: 'hidden',
    resizeMode: "cover",
    zIndex: 0
  },
  bannerContainer: {
    height: 160,
    marginTop: 40,
    paddingLeft: 6,
    paddingRight: 7
  },
  innerFrame: {
    position: "absolute",
    top: 0,
    left: 0,
    zIndex: 1,
    height: "100%",
    width: "100%",
    paddingLeft: 14,
    paddingTop: 14,
    backgroundColor: Platform.OS == "ios" ? 'rgba(0, 0, 0, .7)' : 'rgba(0, 0, 0, .5)',
  },
  mainButtonContainer: {
    width: "100%",
    paddingLeft: 12, paddingRight: 12,
    flexDirection: "row",
    marginTop: 25,
    justifyContent: "space-between"
  },
  mainButtontext: {
    paddingLeft: 15,
    color: colors.colorBlack,
    fontSize: 14,
    fontWeight: "500"
  },
  mainButton: {
    height: 60,
    width: (screenWidth / 2) - 20,
    backgroundColor: colors.colorWhite,
    borderRadius: 8,
    borderWidth: .5,
    borderColor: "lightgrey",
    flexDirection: "row",
    alignItems: "center",
    paddingLeft: 12
  },
  header: {
    height: 45,
    width: "100%",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
  },
  mainContainer: {
    position: "absolute",
    top: 0,
    width: screenWidth,
    //height: screenHeight,
    //  flex: 1,
    height: '100%',
    // left: 0,
  },
  headerButton: {
    height: "100%",
    width: 50,
    // backgroundColor: "blue",
    justifyContent: "center",
    alignItems: "center"
  },
  headerTitle: {
    color: colors.colorWhite,
    fontSize: 16,
    fontWeight: "600"
  },
  container: {
    flex: 1,
    //backgroundColor: "rgba(244,248,254,1)"
    backgroundColor: "#f5f5f5",
  },
  styleUserView: {
    height: 100,
    width: "100%",
    flexDirection: "row"
  },
  styleUserNameView: {
    width: "70%",
    height: "100%",
    justifyContent: "center"
  },
  styleUserImageView: {
    width: "30%",
    height: "100%",
    justifyContent: "center",
    alignItems: "center"
  },
  styleUserPointsandWalletView: {
    height: 60,
    width: "100%",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center"
  },
  stylewinnerView: {
    height: 50,
    width: "48%",
    alignItems: "center",
    flexDirection: "row",
    backgroundColor: "white",
    shadowColor: "black",
    shadowOpacity: 0.3,
    shadowOffset: { width: 5, height: 2 },
    elevation: 10
  },
  styleListSuperView: {
    height: ((screenWidth - 40) / 4) + 10,
    width: (screenWidth - 40) / 4,
    justifyContent: 'center',
    alignItems: 'center'
  },
  styleListItem: {
    height: 50,
    width: 50,
    borderRadius: 5,
    justifyContent: 'center',
    alignItems: 'center',
    shadowColor: "black",
    shadowOpacity: 0.3,
    shadowOffset: { width: 5, height: 2 },
    elevation: 10
  }
});
