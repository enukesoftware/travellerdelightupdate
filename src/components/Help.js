import React, { Component } from 'react';
import { Text, StyleSheet, View } from 'react-native';
import CommonHeader from './custom/CommonHeader';
import CardView from 'react-native-cardview';
import { colors } from '../Utils/Colors';
import Fonts from '../Utils/Fonts';
import { TouchableOpacity } from 'react-native-gesture-handler';
import NavigationServices from '../Utils/NavigationServices';


export default class Help extends Component {
  constructor(props) {
    super(props);

  }

  render() {
    return (
      <View style={styles.container}>
        <CommonHeader noBack={true} title={"Help"} />
        <CardView
          style={{
            backgroundColor: colors.colorWhite,
            marginHorizontal: 10,
            marginVertical: 10
          }}
          cardElevation={2}
          cardMaxElevation={3}
          cornerRadius={8}>
          <TouchableOpacity onPress={() => { NavigationServices.navigate("BookingsHelp") }} style={{ width: '100%', borderRadius: 8, padding: 15, overflow: 'hidden', justifyContent: 'center' }}>
            <Text style={{ FontFamily: Fonts.medium }}>1. Booking Help</Text>
          </TouchableOpacity>
        </CardView>
      </View>
    )
  }

}

const styles = StyleSheet.create({
  container: {
    flex:1,
    backgroundColor: '#f5f5f5',
  },

});
