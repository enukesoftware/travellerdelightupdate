
import React, { Component } from "react";
import { SafeAreaView, Dimensions, StyleSheet, View, ToastAndroid, Alert, Platform } from "react-native";

import { connect } from "react-redux";
import StringConstants from '../../Utils/StringConstants';
import NavigationServices from '../../Utils/NavigationServices';
import CommonHeader from '../custom/CommonHeader';
import { colors } from "../../Utils/Colors";
// import Pdf from 'react-native-pdf';
import Images from "../../Utils/Images";
// import RNFetchBlob from 'rn-fetch-blob'
import moment from "moment";
import { request, PERMISSIONS, RESULTS, openSettings } from 'react-native-permissions';
import APIConstants from "../../Utils/APIManager/APIConstants";
import WebView from "react-native-webview";





class ViewTicket extends Component {
  constructor(props) {
    super(props);
    this.state = {
      bookingId: null,
      isHotel: null,
      source: null,
      isDownload: false
    }
  }
  componentDidMount() {
    let source = null
    let { navigation } = this.props;
    let bookingId = navigation.getParam('bookingId', null);
    let isHotel = navigation.getParam('isHotel', false);
    if (isHotel) { source = { uri: APIConstants.downloadHotelTicket + bookingId } }
    else { source = { uri: APIConstants.downloadFlightTicket + bookingId } }
    this.setState({ bookingId: bookingId, isHotel: isHotel, source: source })
  }

  componentWillUnmount() {
  }

  checkPermissions() {
    request(Platform.OS === 'android' ? PERMISSIONS.ANDROID.WRITE_EXTERNAL_STORAGE : PERMISSIONS.IOS.PHOTO_LIBRARY)
      .then(result => {
        switch (result) {
          case RESULTS.UNAVAILABLE:
            console.log(
              'This feature is not available (on this device / in this context)',
            );
            break;
          case RESULTS.DENIED:
            console.log(
              'The permission has not been requested / is denied but requestable',
              Alert.alert(
                "Permission Required",
                "TravellerDelight needs storage permission to download ticket.\n Please open setting and grant storage permission",
                [
                  { text: 'Dismiss' }, { text: "Grant", onPress: () => this.checkPermissions() }
                ]
              )
            );
            break;
          case RESULTS.GRANTED:
            console.log('The permission is granted');
            this.downloadFile()
            break;
          case RESULTS.BLOCKED:
            console.log('The permission is denied and not requestable anymore');
            Alert.alert(
              "Permission Required",
              "TravellerDelight needs storage permission to download ticket.\n Please open setting and grant storage permission",
              [
                { text: 'Dismiss' }, { text: "Open Setting", onPress: () => openSettings().catch(e => { console.log(e) }) }
              ]
            )


            break;
        }
      })
      .catch(error => {
        // …
        console.log(error);

      });
  }

  downloadFile() {
    this.setState({
      isDownload: true
    })


    return
    // let dirs = RNFetchBlob.fs.dirs
    // let bookingId = this.state.bookingId ? this.state.bookingId : 639
    // RNFetchBlob
    //   .config({
    //     // response data will be saved to this path if it has access right.
    //     path: dirs.DownloadDir + '/TravellerDelight/' + (this.state.isHotel ? 'hotel-ticket' : 'flight-ticket') + bookingId + '-' + (moment(new Date()).format("YYYYMMDD")) + '.pdf'
    //   })
    //   .fetch('GET', this.state.source.uri, {
    //     //some headers ..
    //   })
    //   .then((res) => {
    //     // the path should be dirs.DocumentDir + 'path-to-file.anything'
    //     console.log('The file saved to ', res.path())
    //     ToastAndroid.show("Ticket Downloaded in " + res.path(), ToastAndroid.LONG)
    //   }).catch(error => {
    //     console.log(error)
    //   })
  }

  handleNavigationChange = (navState, f) => {
    if (f) {
      console.log("Downloading"+JSON.stringify(navState))
      this.setState({ isDownload: false })
    }
    else
      console.log(JSON.stringify(navState))
  }

  handleWebViewRequest = request => {
    console.log("Request", JSON.stringify(request))
    const { url } = request;
    if (!url.includes("embedded=true")) return false;
    else return true
  }



  render() {
    if (!this.state.source) return <View></View>
    return (
      <View style={styles.container}>
        <SafeAreaView backgroundColor={colors.colorBlue} />
        <CommonHeader title={this.state.isHotel ? "Hotel Ticket" : "Flight Ticket"} rightImg={Platform.OS=='android'? Images.ic_download:null} fireEvent={() => {
          this.checkPermissions()
          //this.downloadFile()
        }} />
        {/* <Pdf
          source={this.state.source}
          onLoadComplete={(numberOfPages, filePath) => {
            console.log(`number of pages: ${numberOfPages}`);
          }}
          onPageChanged={(page, numberOfPages) => {
            console.log(`current page: ${page}`);
          }}
          onError={(error) => {
            console.log(error);
          }}
          onPressLink={(uri) => {
            console.log(`Link presse: ${uri}`)
          }}
          style={styles.pdf} /> */}
        <WebView style={{ flex: 1 }} source={{ uri: `https://drive.google.com/viewerng/viewer?embedded=true&url=${this.state.source.uri + '/1'}` }}
          startInLoadingState={true}
          onNavigationStateChange={(navState) => this.handleNavigationChange(navState)}
          onShouldStartLoadWithRequest={(r) => this.handleWebViewRequest(r)}
          ref={c => { this.WebView = c }}
        />
        <View style={{ width: 0, height: 0 }}>
          {this.state.isDownload ?
            <WebView source={{ uri: this.state.source.uri + "/" + (this.state.isHotel ? 'hotel-ticket' : 'flight-ticket') + "-" + this.state.bookingId + '-' + (moment(new Date()).format("YYYYMMDD")) + '.pdf' }}
              onNavigationStateChange={(navState) => this.handleNavigationChange(navState, true)}

            /> : null}
        </View>



      </View>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    userData: state.userDataReducer,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    userDataAction: (data) => dispatch(userDataAction(data)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ViewTicket)

const styles = StyleSheet.create({

  container: {
    flex: 1,
    backgroundColor: "#f5f5f5",
  },
  pdf: {
    flex: 1,
    width: Dimensions.get('window').width,
  }
});
