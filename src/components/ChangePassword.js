
import React, { Component } from "react";
import { DeviceEventEmitter, TextInput, Image, SafeAreaView, ImageBackground, Platform, ScrollView, StyleSheet, Text, TouchableHighlight, TouchableOpacity, View } from "react-native";
import CardView from 'react-native-cardview';
import Dash from 'react-native-dash';
import { connect } from "react-redux";
import { changePasswordAction } from '../Redux/Actions';
import { imageBaseUrl } from "../Utils/APIManager/APIConstants";
import { colors } from "../Utils/Colors";
import Fonts from "../Utils/Fonts";
import Images from "../Utils/Images";
import StringConstants from '../Utils/StringConstants';
import NavigationServices from './../Utils/NavigationServices';
import CommonHeader from './custom/CommonHeader';
import { MyAlert } from "../Utils/Utility";



class ChangePassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      oldPassword: '',
      newPassword: '',
      confirmPassword: ''
    }
    this.changePasswordResponse = this.changePasswordResponse.bind(this)
  }
  componentDidMount() {
    DeviceEventEmitter.addListener(StringConstants.CHANGE_PASSWORD_EVENT, this.changePasswordResponse)
  }

  componentWillUnmount() {
    DeviceEventEmitter.removeListener(StringConstants.CHANGE_PASSWORD_EVENT, this.changePasswordResponse)
  }

  changePasswordResponse(data) {

  }

  validateAndSubmit = () => {
    const { oldPassword, newPassword, confirmPassword } = this.state

    if (oldPassword == '') {
      MyAlert("Authentication Error", "Please Enter Old Password")
      return
    }

    if (oldPassword.length < 6) {
      MyAlert("Authentication Error", "Please Enter A Valid 6 Digit Old Password")
      return
    }

    if (newPassword == '') {
      MyAlert("Authentication Error", "Please Enter New Password")
      return
    }

    if (newPassword.length < 6) {
      MyAlert("Authentication Error", "Please Enter A Valid 6 Digit New Password")
      return
    }

    if (confirmPassword == '') {
      MyAlert("Authentication Error", "Please Enter Confirm Password")
      return
    }

    if (confirmPassword.length < 6) {
      MyAlert("Authentication Error", "Please Enter A Valid 6 Digit Confirm Password")
      return
    }

    if (confirmPassword != newPassword) {
      MyAlert("Authentication Error", "Password and Confirm Password does not match")
      return
    }
    this.props.changePasswordAction({
      "old_password": oldPassword,
      "password": newPassword,
      "password_confirmation": confirmPassword
    })
  }


  render() {
    const { oldPassword, newPassword, confirmPassword } = this.state
    return (
      <View style={styles.container}>
        <SafeAreaView backgroundColor={colors.colorBlue} />
        <CommonHeader title={"Change Password"} />

        <CardView style={{
          //overflow: 'hidden',
          margin: 20,
          backgroundColor: colors.colorWhite,
        }}
          cardElevation={2}
          cardMaxElevation={3}
          cornerRadius={10}>
          <View style={{ width: '100%', overflow: 'hidden', borderRadius: 10, alignItems: 'center', paddingHorizontal: 15, paddingTop: 15 }}>
            <TextInput
              style={{
                width: '100%',
                height: 34,
                margin: 4,
                fontSize: 13,
                padding: 4,
                fontFamily: Fonts.regular,
                borderBottomColor: 'grey',
                borderBottomWidth: 1
              }}
              value={oldPassword}
              onChangeText={text => this.setState({ oldPassword: text })}
              placeholder={"Old Password"}
              autoCapitalize={"none"}
              autoCorrect={false}
              secureTextEntry
              placeholderTextColor={'grey'}
            />
          </View>
          <View style={{ width: '100%', overflow: 'hidden', borderRadius: 10, alignItems: 'center', paddingHorizontal: 15, paddingTop: 15 }}>
            <TextInput
              style={{
                width: '100%',
                height: 34,
                margin: 4,
                fontSize: 13,
                padding: 4,
                fontFamily: Fonts.regular,
                borderBottomColor: 'grey',
                borderBottomWidth: 1
              }}
              value={newPassword}
              onChangeText={text => this.setState({ newPassword: text })}
              placeholder={"New Password"}
              autoCapitalize={"none"}
              autoCorrect={false}
              secureTextEntry
              placeholderTextColor={'grey'}
            />
          </View>
          <View style={{ width: '100%', overflow: 'hidden', borderRadius: 10, alignItems: 'center', padding: 15 }}>
            <TextInput
              style={{
                width: '100%',
                height: 34,
                margin: 4,
                fontSize: 13,
                padding: 4,
                fontFamily: Fonts.regular,
                borderBottomColor: 'grey',
                borderBottomWidth: 1
              }}
              value={confirmPassword}
              onChangeText={text => this.setState({ confirmPassword: text })}
              placeholder={"Confirm Password"}
              autoCapitalize={"none"}
              autoCorrect={false}
              secureTextEntry
              placeholderTextColor={'grey'}
            />

            <TouchableOpacity onPress={() => this.validateAndSubmit()} style={{ backgroundColor: colors.colorBlue, paddingHorizontal: 20, paddingVertical: 10, marginTop: 15, alignItems: 'center', justifyContent: 'center', borderRadius: 8, overflow: 'hidden' }}>
              <Text style={{ color: colors.colorWhite, fontFamily: Fonts.medium, fontSize: 14 }}>Change Password</Text>
            </TouchableOpacity>
          </View>
        </CardView>

      </View>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    userData: state.userDataReducer,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    changePasswordAction: (data) => dispatch(changePasswordAction(data)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ChangePassword)

const styles = StyleSheet.create({

  container: {
    flex: 1,
    //backgroundColor: "rgba(244,248,254,1)"
    backgroundColor: "#f5f5f5",
  }
});
