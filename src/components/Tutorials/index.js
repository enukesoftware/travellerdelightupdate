import AsyncStorage from '@react-native-community/async-storage';
import React, { Component } from 'react';
import { Image, ImageBackground, Text, TouchableOpacity, View } from 'react-native';
import CardView from 'react-native-cardview';
import Swiper from 'react-native-swiper';
import { connect } from "react-redux";
import { getAirportListAction, getNationalityAction } from '../../Redux/Actions';
import { colors } from '../../Utils/Colors';
import Fonts from '../../Utils/Fonts';
import Images from '../../Utils/Images';
import NavigationServices from '../../Utils/NavigationServices';



class Tutorials extends Component {
  constructor() {
    super()
    this.state = {
      showDone: false,
    }
  }

  componentDidMount() {
    // if (!this.props.airPortListData || this.props.airPortListData.length == 0) {
    //   this.props.getAirportListAction()
    // }
    // if (!this.props.nationalityListData || this.props.nationalityListData.length == 0) {
    //   this.props.getNationalityAction()
    // }
  }

  _onItemChanged(index) {
    if (index == 2) {
      this.setState({ showDone: true })
      // this.props.isTutorialAction(true)
      AsyncStorage.setItem("isTutorial", JSON.stringify(true))


      // setTimeout(() => {
      //   NavigationServices.navigate('DashboardStack', { NavigateFrom: "Tutorials" })
      // }, 1000);
    } else {
      this.setState({ showDone: false })
    }
  }

  Step1() {
    return (
      <View style={{ flex: 1, justifyContent: 'flex-start' }}>
        <ImageBackground style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}
          imageStyle={{ top: 0, position: 'absolute' }}
          source={Images.ic_spl_1} >
          <Image resizeMode='contain' source={Images.ic_image_logo} style={{ width: '50%' }} />
        </ImageBackground>
      </View>
    )
  }
  Step2(data) {
    let heading, desc, img, showDone;
    switch (data) {
      case 'F':
        heading = 'Book A Flight'
        desc = "Are you ready to take the best flights to your favorite destination? Traveller Delight welcomes you to explore new destinations with great deals on flights. Just leave all the turbulence to us and enjoy the booking journey."
        img = Images.ic_spl_2
        showDone = false
        break;
      case 'H':
        heading = 'Book A Hotel'
        desc = "You are on your way to booking the most exciting stays with Traveller Delight! Browse inspiring accommodations to your heart’s content or just discover the perfect space to spice up your travel experience."
        img = Images.ic_spl_3
        showDone = true
        break;
    }
    return (
      <View style={{ flex: 1, justifyContent: 'flex-start' }}>
        <ImageBackground style={{ flex: 1 }}
          imageStyle={{ top: 0, position: 'absolute' }}
          source={img} >
          <View style={{ marginTop: 55, padding: 30, flex: 1 }} >
            <Text style={{ fontSize: 24, fontFamily:Fonts.bold, color: 'white', margin: 5 }}> {heading}</Text>
            <Text style={{ fontSize: 12, fontFamily:Fonts.medium, color: 'white', textAlign: 'justify', lineHeight: 20, margin: 5, marginTop: 10 }}> {desc}</Text>
          </View>
          {showDone && <>
            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }} >
              <CardView style={{
                backgroundColor: 'white'
              }}
                cardElevation={4}
                cardMaxElevation={4}
                cornerRadius={25}>

                <TouchableOpacity
                  onPress={() => { NavigationServices.navigate('DashboardStack', { NavigateFrom: "Tutorials" }) }}
                  style={{ borderRadius: 25, overflow: 'hidden', padding: 15, paddingHorizontal: 20, width: '100%' }}>
                  <Text style={{ fontFamily: Fonts.semiBold, color: colors.colorBlue }}>Proceed</Text>
                </TouchableOpacity></CardView>
            </View>
            <View style={{ flex: 1, }} >

            </View>
          </>}
          {/* {showDone
          ? <TouchableOpacity style={{alignSelf:'flex-end', margin:20}}>
            <Text style={{color:colors.colorWhite,fontFamily:Fonts.medium,fontSize:18}}>Done ></Text>
          </TouchableOpacity>
          :null} */}

        </ImageBackground>
      </View>
    )

  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        <View style={{ flex: 9 }}>
          <Swiper
            // ref={pager => this.pager = pager}
            onIndexChanged={(index) => { this._onItemChanged(index) }}
            loop={false}
            autoplay={true}
            autoplayTimeout={2.5}
          >

            {this.Step1()}
            {this.Step2('F')}
            {this.Step2('H')}

          </Swiper>
          

        </View>
        <View style={{ flex: 1 }} />
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    airPortListData: state.airportListDataReducer,
    nationalityListData: state.nationalityDataReducer,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getAirportListAction: () => dispatch(getAirportListAction()),
    getNationalityAction: () => dispatch(getNationalityAction())
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Tutorials)
