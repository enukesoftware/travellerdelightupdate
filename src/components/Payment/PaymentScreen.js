import React, { Component } from "react";
import { Dimensions, Image, StyleSheet, Text, TextInput, TouchableOpacity, View,Alert } from "react-native";
import CardView from "react-native-cardview";
import { SafeAreaView } from "react-navigation";
import { connect } from "react-redux";
import { colors } from "../../Utils/Colors";
import Fonts from "../../Utils/Fonts";
import Images from "../../Utils/Images";
import NavigationServices from '../../Utils/NavigationServices';
import BookingBelt from "../custom/BookingBelt";
import BottomStrip from "../custom/BottomStrip";
import Header from "../custom/CommonHeader";
import { Environment } from "../../Utils/APIManager/APIConstants";


const screenWidth = Dimensions.get("window").width;

class PaymentScreen extends Component {

  constructor(props) {
    super(props)
    this.state = {
      paymentType: 0,
      cardNo: "",
      cardName: "",
      expiryDate: new Date(),
      cvv: "",
      price: null,
      currency: null,
      bookingObj: null,
    }
  }

  componentDidMount = () => {
    let { navigation } = this.props;
    let data = navigation.getParam('data', null);
    let isFlight = navigation.getParam('isFlight', null);
    if (isFlight) {
      this.setState({
        price: data.final_booking_amount, isFlight, currency: data.price.currency, bookingObj: data
      }, () => {
        // console.clear();
        console.log("Flight data has been set:==" + JSON.stringify(data));
      });
    }
    else {
      let price = navigation.getParam('price', null);
      let currency = navigation.getParam('currency', null);
      this.setState({
        price: price, isFlight, currency: currency, bookingObj: data
      }, () => console.log("Hotel data has been set"));
    }

    this.setState({
      // price: 200, isFlight, currency: "INR", bookingObj: {}

    })
  }

  checkout = () => {
    if(this.state.paymentType!=0) return;
    if(Environment==='PlayStore'){
    setTimeout(() => {
      Alert.alert(
        "Warning",
        "This is a demo app so payment can not be processed",
        [{
          text: "Ok", onPress: () => {
            // if(this.state.isFlight)
            // NavigationServices.navigate("FlightList")
            // else
            // NavigationServices.navigate("HotelList")
            // DeviceEventEmitter.emit(StringConstants.SESSION_EXPIRE_EVENT) 
          }
        }],
        { cancelable: false }
      )
    }, 100);
    return
  }
    let { bookingObj, currency, price, isFlight } = this.state
    NavigationServices.navigate("PaypalWebView", { bookingObj: bookingObj, currency: currency, price: price, isFlight: isFlight });
  }

  render() {
    let { paymentType, cardNo, data, price, currency, cvv, expiryDate, isFlight } = this.state;
    if (!price) return null

    return (
      <SafeAreaView style={{ flex: 1, backgroundColor: colors.colorBlue }}>
        <Header title="Payment" />
        <View style={{
          flex: 1,
          backgroundColor: colors.backgroundColor
        }}>

          <BookingBelt type={3} isHotel={!isFlight} style={{ paddingBottom: 5 }} />

          {/* <View style={styles.header}>
            <TouchableOpacity style={styles.headerBackButton} onPress={() => {
              // alert("Hello");
              NavigationServices.goBack();
            }}>
              <Image source={Images.ic_image_back} style={{ width: 15, height: 15, resizeMode: "contain" }}></Image>
              <Text style={styles.backBtnText}> Back</Text>
            </TouchableOpacity>
            <View style={styles.title}>
              <Text style={styles.titleText}>Payment</Text>
            </View>
          </View> */}
          <View style={{
            flex: 1, alignItems: "center",
            padding: 12,
          }}>
            <CardView style={styles.card}
              cardElevation={2}
              cardMaxElevation={3}
              cornerRadius={10}>
              <View style={styles.cardheader}>
                <Image source={Images.ic_image_payment} style={{ width: 25, height: 25, resizeMode: "contain" }}></Image>
                <Text style={styles.cardheaderText}>Select Payment Mode</Text>
              </View>
              <View style={{ padding: 12, }}>

                <View style={styles.cardCOntent}>
                  <View style={styles.radioButton} >
                    <TouchableOpacity style={{ flexDirection: "row", alignItems: "center", }} onPress={() => this.setState({ paymentType: 0 })}>
                      <Image source={paymentType == 0 ? Images.ic_radio_checked : Images.ic_radio_unchecked} style={styles.radioImg}></Image>
                      <Text style={styles.radioText}>Paypal</Text>
                    </TouchableOpacity>
                  </View>
                </View>

                <View style={styles.cardCOntent}>
                  <View style={styles.radioButton} >
                    <TouchableOpacity style={{ flexDirection: "row", alignItems: "center", }} onPress={() => this.setState({ paymentType: 1 })}>
                      <Image source={paymentType == 1 ? Images.ic_radio_checked : Images.ic_radio_unchecked} style={styles.radioImg}></Image>
                      <Text style={styles.radioText}>Debit Card</Text>
                    </TouchableOpacity>
                  </View>
                  <View style={styles.radioButton} >
                    <TouchableOpacity style={{ flexDirection: "row", alignItems: "center", }} onPress={() => this.setState({ paymentType: 2 })}>
                      <Image source={paymentType == 2 ? Images.ic_radio_checked : Images.ic_radio_unchecked} style={styles.radioImg}></Image>
                      <Text style={styles.radioText}>Credit Card</Text></TouchableOpacity>
                  </View>
                </View>
              </View>
            </CardView>

            {!this.state.paymentType == 0 ? <CardView style={styles.card}
              cardElevation={2}
              cardMaxElevation={3}
              cornerRadius={10}>
              <View style={styles.cardheader}>
                <Image source={Images.ic_card_blue} style={{ width: 25, height: 25, resizeMode: "contain" }}></Image>
                <Text style={styles.cardheaderText}>Card Information</Text>
              </View>
              <View style={[styles.cardCOntent, { flexDirection: "column" }]}>
                <TextInput style={styles.input}
                  placeholder="Card Number"
                  placeholderTextColor={colors.colorBlack}
                  keyboardType="number-pad"
                  value={cardNo}
                  onChangeText={(text) => this.setState({ cardNo: text })}></TextInput>
                <TextInput style={styles.input}
                  placeholder="Name on Card"
                  placeholderTextColor={colors.colorBlack}
                  keyboardType="default"
                  value={cardNo}
                  onChangeText={(text) => this.setState({ cardNo: text })}></TextInput>
                <View style={{
                  width: screenWidth - 25,
                  height: 50,
                  flexDirection: "row",
                  justifyContent: "space-between",
                  alignItems: "center"
                }}>
                  <View style={{
                    width: "50%",
                    height: "100%", paddingLeft: 12,
                    paddingRight: 12
                  }}>
                    <TextInput style={styles.input}
                      placeholder="Expiry Date"
                      placeholderTextColor={colors.colorBlack}
                      keyboardType="default"
                      value={expiryDate}
                      onChangeText={(text) => this.setState({ expiryDate: text })}></TextInput>
                    <Image source={Images.ic_image_calender2} style={{
                      width: 15, height: 15, position: "absolute", right: 20, bottom: 25
                    }}></Image>
                  </View>
                  <View style={{
                    width: "50%",
                    height: "100%", paddingLeft: 12,
                    paddingRight: 12
                  }}>
                    <TextInput style={styles.input}
                      placeholder="Enter CVV"
                      placeholderTextColor={colors.colorBlack}
                      keyboardType="default"
                      value={cvv}
                      onChangeText={(text) => this.setState({ cvv: text })}></TextInput>
                    <Image source={Images.ic_card_black} style={{
                      width: 15, height: 15, position: "absolute", right: 20, bottom: 25
                    }}></Image>
                  </View>
                </View>
              </View>
            </CardView> : null}
          </View>
          {price ?
            <BottomStrip price={currency + " " + price} fireEvent={this.checkout}></BottomStrip> : null}
        </View>
      </SafeAreaView>
    )
  }
}

const mapStateToProps = (state) => {
}

const mapDispatchToProps = (dispatch) => {
  return null;
};

const styles = StyleSheet.create({
  input: {
    width: "100%",
    height: 35,
    paddingLeft: 10,
    justifyContent: "center",
    borderBottomColor: colors.colorBlack,
    borderBottomWidth: .2,
    marginBottom: 7
  },
  cardCOntent: {
    width: screenWidth - 25,
    backgroundColor: colors.colorWhite,
    flexDirection: "row",
    alignItems: "center",
  },
  radioText: {
    fontSize: 14, color: colors.colorBlack,
    fontFamily: Fonts.medium, marginLeft: 10
  },
  radioButton: {
    width: (screenWidth - 25) / 2.5,
    flexDirection: "row",
    height: 50, alignItems: "center",
    // backgroundColor: "red"
  },
  radioImg: { width: 15, height: 15, resizeMode: "contain" },
  card: {
    overflow: 'hidden',
    backgroundColor: colors.colorWhite,
    marginVertical: 10,
    marginHorizontal: 10,
    width: "100%",
    borderColor: colors.lightgrey,
    borderWidth: 1
  },
  cardheaderText: {
    color: colors.colorBlack,
    fontFamily: Fonts.semiBold,
    fontSize: 14, marginLeft: 12
  },
  cardheader: {
    height: 40, width: "100%",
    flexDirection: "row",
    alignItems: "center",
    backgroundColor: "rgb(245,245,245)",
    padding: 10
  },
  titleText: {
    fontSize: 16, color: colors.colorWhite,
    fontFamily: Fonts.bold,
    letterSpacing: .8
  },
  title: {
    width: screenWidth - 140,
    height: "100%",
    justifyContent: "center",
    alignItems: "center"
  },
  header: {
    height: 45,
    width: screenWidth,
    backgroundColor: colors.colorBlue,
    alignItems: "center",
    flexDirection: "row"
  },
  backBtnText: {
    color: colors.colorWhite,
    fontFamily: Fonts.semiBold,
    fontSize: 14
  },
  headerBackButton: {
    width: 70,
    // backgroundColor: "blue",
    height: "100%",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
  },
})

export default PaymentScreen