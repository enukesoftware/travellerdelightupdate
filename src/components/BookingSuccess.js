import React from "react";
import { Dimensions, ImageBackground, ScrollView, StyleSheet, Text, View ,TouchableOpacity} from "react-native";
import { connect } from 'react-redux';
import { colors } from "../Utils/Colors";
import Fonts from "../Utils/Fonts";
import Images from "../Utils/Images";
import NavigationServices from '../Utils/NavigationServices';
const screenWidth = Dimensions.get("window").width;
const screenHeight = Dimensions.get("window").height;

class BookingSuccess extends React.Component {
  state = {
    data: null,
    isHotel: null
  };

  componentDidMount = () => {
    let { navigation } = this.props;
    let data = navigation.getParam('data', null);
    let isHotel = navigation.getParam('isHotel', null);

    this.setState({ data, isHotel }, () => {
      console.log("data is :--" + JSON.stringify(this.state.data));
    });
  }

  render() {
    let { data, isHotel } = this.state;
    if (!data) return null
    return (
      <View style={{
        flex: 1,
        backgroundColor: colors.backgroundColor,
        // padding: 14,
      }}>
        <ScrollView showsVerticalScrollIndicator={false}>
          <ImageBackground source={Images.ic_booking_success} style={{
            width: screenWidth, alignItems: "center",
            height: screenHeight / 2.5, justifyContent: "center"
          }} resizeMode="stretch">
            {/* <Image source={require("../../Assets/images/booked-checked.png")}
              style={{ width: "70%", height: "50%", resizeMode: "contain" }}></Image> */}
          </ImageBackground>
          <View style={{
            flex: 1,
            padding: 20
          }}>
            {isHotel ?
              <View style={styles.cardView}>
                <View style={styles.row}>
                  <Text style={styles.key}>Booking ID</Text>
                  <Text style={styles.value}>{data.bookingId}</Text>
                </View>
                <View style={styles.row}>
                  <Text style={styles.key}>Check In Date</Text>
                  <Text style={styles.value}>{data.checkInDate}</Text>
                </View>
                <View style={styles.row}>
                  <Text style={styles.key}>Check Out Date</Text>
                  <Text style={styles.value}>{data.checkOutDate}</Text>
                </View>
                <View style={styles.row}>
                  <Text style={styles.key}>Transaction ID</Text>
                  <Text style={styles.value}>{data.transactionId}</Text>
                </View>
              </View>
              :
              <View style={styles.cardView}>
                <View style={styles.row}>
                  <Text style={styles.key}>Booking ID</Text>
                  <Text style={styles.value}>{data.bookingId}</Text>
                </View>
                <View style={styles.row}>
                  <Text style={styles.key}>Departure Date</Text>
                  <Text style={styles.value}>{this.props.flightSearchParams.departure_date}</Text>
                </View>
                {this.props.flightSearchParams.return_date ?
                  <View style={styles.row}>
                    <Text style={styles.key}>Return Date</Text>
                    <Text style={styles.value}>{this.props.flightSearchParams.return_date}</Text>
                  </View> : null
                }
                <View style={styles.row}>
                  <Text style={styles.key}>Transaction ID</Text>
                  <Text style={styles.value}>{data.transactionId}</Text>
                </View>
              </View>
            }

            <View style={{
              flexDirection: "row",
              width: "100%", paddingTop: 30,
              justifyContent: "space-between",
            }}>
              <TouchableOpacity onPress={() => {
                NavigationServices.navigate("DashboardStack")
                NavigationServices.navigate("ViewTicket", { bookingId: data.bookingId,isHotel:isHotel })
              }} style={styles.button}>
                <Text style={styles.btnText}>VIEW TICKET</Text>
              </TouchableOpacity>
              <TouchableOpacity style={[styles.button, { backgroundColor: colors.colorRed }]}
                onPress={() => NavigationServices.navigate("DashboardStack")}>
                <Text style={styles.btnText}>BACK HOME</Text>
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  btnText: {
    fontFamily: Fonts.bold,
    color: colors.colorWhite,
    fontSize: 16, letterSpacing: .7
  },
  button: {
    width: (screenWidth / 2) - 30,
    height: 45,
    backgroundColor: colors.colorBlue,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 10
  },
  cardView: {
    width: "100%",
    borderColor: colors.lightBlue,
    borderWidth: 1,
    backgroundColor: colors.colorWhite,
  },
  value: {
    fontSize: 14,
    color: colors.colorBlack,
    fontFamily: Fonts.semiBold,
    letterSpacing: .7
  },
  key: {
    fontSize: 14,
    color: colors.colorBlack,
    fontFamily: Fonts.regular,
    letterSpacing: .7
  },
  row: {
    height: 45, width: "100%",
    borderBottomColor: colors.lightBlue,
    borderBottomWidth: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center", paddingLeft: 14,
    paddingRight: 14
  },
})

const mapStateToProps = (state) => {
  //console.log("myState", JSON.stringify(state))
  return {
    flightSearchParams: state.FlightsReducer.flightSearchParams

  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    bookFlightAction: (data) => dispatch(bookFlightAction(data)),
  };
};

export default connect(mapStateToProps, {})(BookingSuccess)