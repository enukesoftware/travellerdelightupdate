import React, { Component } from 'react';
import { Dimensions, Image, StyleSheet, Text, View } from 'react-native';
import { colors } from '../../Utils/Colors';
import Fonts from '../../Utils/Fonts';
import Images from '../../Utils/Images';

const screenWidth = Dimensions.get('window').width;
export default class BookingBelt extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    const {type, style, isHotel} = this.props;
    return (
      <View style={[style, {marginHorizontal: 0, paddingHorizontal: 0}]}>
        <View style={styles.reviewBelt}>
          <View style={styles.circleViewEnabled}>
            <Image
              style={styles.image}
              source={
                isHotel ? Images.ic_review_hotel : Images.ic_review_flight
              }
            />
          </View>
          {type > 1 ? (
            <>
              <View style={styles.styleLineEnabled} />
              <View style={styles.circleViewEnabled}>
                <Image style={styles.image} source={Images.ic_traveller} />
              </View>
            </>
          ) : (
            <>
              <View style={styles.styleLineDisabled} />
              <View style={styles.circleViewDisabled}>
                <Image style={styles.image} source={Images.ic_traveller} />
              </View>
            </>
          )}

          {type > 2 ? (
            <>
              <View style={styles.styleLineEnabled} />
              <View style={styles.circleViewEnabled}>
                <Image style={styles.image} source={Images.ic_payment} />
              </View>
            </>
          ) : (
            <>
              <View style={styles.styleLineDisabled} />
              <View style={styles.circleViewDisabled}>
                <Image style={styles.image} source={Images.ic_payment} />
              </View>
            </>
          )}
        </View>
        <View style={styles.viewText}>
          <View style={{alignItems: 'flex-start',width:'30%'}}>
            <Text style={styles.textItem}>REVIEW</Text>
          </View>
          <View style={{alignItems: 'center',width:'40%'}}>
            <Text style={styles.textItem}>
              {isHotel ? 'GUESTS' : 'TRAVELLERS'}
            </Text>
          </View>
          <View style={{alignItems: 'flex-end',width:'30%'}}>
            <Text style={styles.textItem}>PAYMENT</Text>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  reviewBelt: {
    width: screenWidth,
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 25,
    paddingTop: 10,
    paddingBottom: 5,
  },
  circleViewEnabled: {
    width: 45,
    height: 45,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 25,
    backgroundColor: colors.colorBlue,
  },
  circleViewDisabled: {
    width: 45,
    height: 45,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 25,
    backgroundColor: 'grey',
  },
  styleLineEnabled: {
    width: (screenWidth - 185) / 2,
    height: 2,
    backgroundColor: colors.colorBlue,
  },
  styleLineDisabled: {
    width: (screenWidth - 185) / 2,
    height: 2,
    backgroundColor: 'grey',
  },
  itemView: {
    width: 45,
    alignItems: 'center',
  },
  viewText: {
    paddingHorizontal: 25,
    flexDirection: 'row',
    width:'100%'
    //justifyContent: 'space-between',
  },
  textItem: {
    color: colors.colorBlack,
    fontFamily: Fonts.medium,
    fontSize: 12,
    textAlign:'center',
    justifyContent: 'center',
  },
  image: {
    resizeMode: 'center',
    width: 22,
    height: 22,
  },
});
