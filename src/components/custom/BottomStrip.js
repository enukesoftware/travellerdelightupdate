import React, { Component } from "react";
import { Dimensions, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import { colors } from "../../Utils/Colors";
import Fonts from "../../Utils/Fonts";
const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;

export default class BottomStrip extends Component {
  render() {
    let { fireEvent, price, bottomStripStyle, middleText } = this.props;
    return (
      <View style={[styles.bottomStrip, bottomStripStyle]}>
        <View style={styles.priceView}>
          {/* <Image source={Images.imgrupeewhite} style={styles.priceImg}></Image> */}
          <Text style={styles.priceText}>{price ? price : "0"}</Text>
        </View>
        {middleText &&
          <View style={styles.priceView}>
            {/* <Image source={Images.imgrupeewhite} style={styles.priceImg}></Image> */}
            <Text style={[styles.priceText, { fontSize: 13 }]}>{middleText}</Text>
          </View>}
        <TouchableOpacity style={styles.bottomBtn} onPress={() => fireEvent()}>
          <Text style={styles.bottomBtnTxt}>CONTINUE</Text>
        </TouchableOpacity>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  bottomBtnTxt: {
    fontFamily: Fonts.semiBold,
    color: colors.colorBlue,
    fontSize: 12, letterSpacing: .5
  },
  bottomBtn: {
    backgroundColor: colors.colorWhite,
    height: 30,
    width: 110,
    borderRadius: 8,
    justifyContent: "center",
    alignItems: "center"
  },
  priceText: {
    flexDirection: "row",
    color: colors.colorWhite,
    fontFamily: Fonts.bold,
    fontSize: 18,
    lineHeight: 30
  },
  priceImg: {
    width: 20, height: 20, resizeMode: "contain"
  },
  priceView: {
    //width: "50%",
    height: "100%",
    flexDirection: "row",
    alignItems: "center"
  },
  bottomStrip: {
    height: 50,
    width: "100%",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    paddingRight: 14,
    paddingLeft: 14,
    backgroundColor: colors.colorBlue,
    position: "absolute",
    bottom: 0
  },
})