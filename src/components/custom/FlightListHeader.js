import moment from 'moment';
import React from "react";
import { Dimensions, Image, StyleSheet, Text, View } from "react-native";
import { colors } from "../../Utils/Colors";
import Fonts from "../../Utils/Fonts";
import Images from "../../Utils/Images";
import { getCabinClass } from "../../Utils/Utility";
import CommonHeader from "./CommonHeader";
const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;

export default class FlightListHeader extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
    }
  }
  renderTitleView() {
    let { data } = this.props;
    if (!data) return null

    return (
      <View style={styles.headerContent}>
        <View style={[styles.headerContentRow, { alignItems: 'center' }]}>
          <Text style={styles.headerStnName}>{data.origin}</Text>
          <Image source={data.tripType == 1 ? Images.ic_image_plane : Images.ic_dual_way} style={data.tripType == 1 ? styles.headerContentIcon : styles.headerContentIcon2}></Image>
          <Text style={styles.headerStnName}>{data.destination}</Text>
        </View>
        <View style={styles.headerContentRow}>
          {
            data.tripType == 1 ?
              <Text style={styles.headerContentRowText}>{this.getDate(data.departure_date)}</Text> :
              <Text style={styles.headerContentRowText}>{this.getDate(data.departure_date)} - {this.getDate(data.return_date)}</Text>
          }
          <View style={styles.headerRowDevider}></View>
          <Text style={styles.headerContentRowText}>{getCabinClass(data.travel_class,"code","shortName")}</Text>
          <View style={styles.headerRowDevider}></View>
          <Text style={styles.headerContentRowText}>{data.ADT + data.CHD + data.INF} Traveller</Text>
        </View>
      </View>
    )
  }
  getDate(date) {
    //return dateFormat(date, "ddd, mmm dS");
    return moment(date).format("DD MMM");
  }



  render() {
    let { fireEvent, rightImg,noBack } = this.props;
    return (
      <CommonHeader noBack={noBack?noBack:false} titleView={() => this.renderTitleView()} rightImg={rightImg} fireEvent={fireEvent} />
    )
  }
}

const styles = StyleSheet.create({


  headerRowDevider: {
    height: 12, width: 1.5,
    backgroundColor: colors.colorWhite,
    marginLeft: 7,
    marginRight: 7,
    marginTop: 5
  },
  headerContentRowText: {
    color: colors.colorWhite,
    fontSize: 12,
    fontFamily: Fonts.medium,
    lineHeight: 20
  },
  headerContentIcon: {
    width: 20, height: 20,
    resizeMode: "contain",
    marginLeft: 10,
    marginRight: 10
  },
  headerContentIcon2: {
    width: 11, height: 11,
    resizeMode: "contain",
    marginLeft: 10,
    marginRight: 10
  },
  headerStnName: {
    color: colors.colorWhite,
    fontSize: 14,
    fontFamily: Fonts.semiBold
  },
  headerContentRow: { width: "100%", flexDirection: "row", justifyContent: "center" },
  headerContent: {
    height: "100%",
    width: '100%',
    padding: 8,
    justifyContent: "center"
  }
})