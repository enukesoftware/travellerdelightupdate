import React from "react";
import { Dimensions, ImageBackground, FlatList, ScrollView, StyleSheet, Text, View, TouchableOpacity, SafeAreaView } from "react-native";
import { connect } from 'react-redux';
import { colors } from "../Utils/Colors";
import Fonts from "../Utils/Fonts";
import Images from "../Utils/Images";
import { imageBaseUrl } from "../Utils/APIManager/APIConstants";
import NavigationServices from '../Utils/NavigationServices';
import CommonHeader from "./custom/CommonHeader";
import CardView from "react-native-cardview";
const screenWidth = Dimensions.get("window").width;
const screenHeight = Dimensions.get("window").height;

export default class TopDestinationsAndFlights extends React.Component {
    state = {
        data: null,
        isHotel: null
    };

    UNSAFE_componentWillMount = () => {
        let { navigation } = this.props;
        let data = navigation.getParam('data', null);
        let key = navigation.getParam('key', null);

        key = 1

        data = [
            {
                "id":2,
                "agency_id":6,
                "city_id":"8257",
                "city_name":"Bangkok",
                "city_image":"top_destinations/bangkok.jpg",
                "city_description":"[\"Capital of Thailand\",\"Delicious street food\",\"Grand Palace and Wat Pho\",\"Skyscrapers\",\" nightclubs\",\" cabaret shows\",\"\"]",
                "country_name":"Thailand",
                "country_code":null,
                "section":"BOTTOM",
                "starting_price":2300,
                "start_date":"2020-01-28 13:35:32",
                "end_date":"2020-09-30 23:59:59"
                },
                {
                "id":13,
                "agency_id":6,
                "city_id":"16198",
                "city_name":"Singapore",
                "city_image":"top_destinations/singapore.jpg",
                "city_description":"[\"Perfect blend of culture and history\",\"top tourist destinations\",\"Garden hosts the highest diversity\",\"Diverse cultures and cuisines.\"]",
                "country_name":"Singapore",
                "country_code":null,
                "section":"BOTTOM",
                "starting_price":2500,
                "start_date":"2020-01-28 13:28:47",
                "end_date":"2020-09-30 23:59:59"
                },
                {
                "id":14,
                "agency_id":6,
                "city_id":"13599",
                "city_name":"Switzerland",
                "city_image":"top_destinations/switzerland.jpg",
                "city_description":"[\"The Matterhorn\",\"Jungfraujoch: The Top of Europe\",\"Lake Geneva\",\"Chateau de Chillon\",\" Montreux\"]",
                "country_name":"Switzerland",
                "country_code":null,
                "section":"BOTTOM",
                "starting_price":2500,
                "start_date":"2020-01-28 13:28:47",
                "end_date":"2020-09-30 23:59:59"
                },
                {
                "id":15,
                "agency_id":6,
                "city_id":"25380",
                "city_name":"London",
                "city_image":"top_destinations/london.jpg",
                "city_description":"[\"The London Eye\",\"Buckingham Palace tour\",\"Tower of London\",\"Madame Tussauds \"]",
                "country_name":"United Kingdom - England",
                "country_code":null,
                "section":"BOTTOM",
                "starting_price":2500,
                "start_date":"2020-01-28 13:28:47",
                "end_date":"2020-09-30 23:59:59"
                }
        ]

        this.setState({ data, key }, () => {
            console.log("data is :--" + JSON.stringify(this.state.data));
        });
    }

    renderDestinationView() {
        const { data } = this.state;
        if (data)
            return (
                <FlatList
                    // maxToRenderPerBatch={10}
                    initialNumToRender={20}
                    windowSize={11}
                    removeClippedSubviews={true}
                    showsVerticalScrollIndicator={false}
                    showsHorizontalScrollIndicator={false}
                    style={{ flex: 1, width: '100%' }}
                    numColumns={2}
                    data={data}
                    renderItem={({ item, index }) => (
                        <View style={{ width: '50%', padding: 15, }}>

                            <CardView
                                cardElevation={3}
                                cardMaxElevation={3}
                                cornerRadius={8}
                                style={{ backgroundColor: colors.colorWhite, width: (screenWidth / 2) - 45,  }}>



                                <View style={{ alignItems: 'center', justifyContent: 'center', overflow: 'hidden', borderRadius: 8 }}>
                                    <ImageBackground resizeMode={'cover'} style={{
                                        // width: 259,
                                        // height: 189,
                                        // width: 150,
                                        // height: 189,
                                        width: (screenWidth / 2) - 45,
                                        height: 200,
                                        backgroundColor: 'white',
                                        overflow: "hidden"
                                    }} source={{ uri: imageBaseUrl + item.city_image }}>
                                        <View style={{
                                            flex: 1,
                                            backgroundColor: Platform.OS == "ios" ? 'rgba(0, 0, 0, .3)' : 'rgba(0, 0, 0, .3)',
                                            justifyContent: "flex-end"
                                        }}>

                                        </View>
                                    </ImageBackground>
                                    <View style={{ width: '100%', backgroundColor: '#f5f5f5', padding: 5,paddingHorizontal:10, justifyContent: 'center' }}>
                                        <Text style={{ fontFamily: Fonts.semiBold, fontSize: 11, color: colors.colorBlack21, }}>{item.city_name}</Text>
                                        <Text style={{ fontFamily: Fonts.regular, fontSize: 11, color: colors.colorBlack62,marginTop:4 }}>Starting From <Text style={{ fontFamily: Fonts.semiBold, fontSize: 11, color: colors.colorGold, }}>{item.starting_price}</Text></Text>
                                    </View>
                                </View>
                            </CardView>
                        </View>
                    )}
                    keyExtractor={({ item, index }) => index}
                    extraData={this.state.loading}
                />
            )
    }

    renderTopRouteView() {

    }

    render() {
        const { key } = this.state;
        if (!key) { return <View /> }
        let title = ''
        if (key == 1) title = "Top Destinations"
        else if (key == 2) title = 'Top Flight Routes'
        else if (key == 3) title = 'Top International Destinations'
        return (
            <View style={{ flex: 1, backgroundColor: '#f5f5f5' }}>
                <SafeAreaView backgroundColor={colors.colorBlue} />
                <CommonHeader title={title} />

                {key == 2 ? this.renderTopRouteView() : this.renderDestinationView()}

            </View>
        )
    }
}