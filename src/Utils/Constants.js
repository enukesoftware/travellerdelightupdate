const Constants = {
    maxNumForFlightSearch:9,
    CabinClasses:[
        {
          key: 2,
          value: 'Economy Class',
          code:'Economy',
          shortValue:"Economy",
          shortName:"EC"
        },
        {
          key: 3,
          value: 'Premium Economy Class',
          code:'PREMIUM_ECONOMY',
          shortValue:"Premium Economy",
          shortName:"PEC"
        },
        {
          key: 4,
          value: 'Business Class',
          code:'Business',
          shortValue:"Business",
          shortName:"BC"
        },
        {
          key: 5,
          value: 'Premium Business Class',
          code:'Premium_Business',
          shortValue:"Premium Business",
          shortName:"PBC"
        },
        {
          key: 6,
          value: 'First Class',
          code:'First',
          shortValue:"First Class",
          shortName:"FC"
        }
      ]
}

export default Constants;