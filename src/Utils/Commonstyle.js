import { StyleSheet } from "react-native";
import { colors } from "./Colors";
import Fonts from "./Fonts";

export const commonstyle = StyleSheet.create({
  cardHeader: {
    width: "100%",
    height: 42,
    flexDirection: "row",
    justifyContent: "space-between",
    paddingLeft: 10,
    paddingRight: 10,
    alignItems: "center",
    backgroundColor: colors.gray
  },
  cardHeaderCol: {
    width: "50%",
    height: "100%",
    flexDirection: "row",
    alignItems: "center"
  },
  itemBackground: {
    backgroundColor: colors.colorWhite,
    margin: 12,
    marginBottom: 15,
    borderRadius: 10,
    padding: 15,
    elevation: 5,
    shadowColor: colors.colorBlack,
    shadowOpacity: 0.3,
    shadowOffset: { width: 2, height: 2 },
    shadowRadius: 5,
    // height: 200
    // overflow: "hidden"
  },
  itemTextHeading: {
    fontFamily: Fonts.medium,
    color: colors.headingTextColor,
  },
  blackText: {
    fontFamily: Fonts.medium,
    color: colors.colorBlack,
  },
  greyText: {
    fontFamily: Fonts.medium,
    color: colors.lightgrey,
  },
  blackText21: {
    fontFamily: Fonts.medium,
    color: colors.colorBlack21,
  },
  greyBold: {
    fontFamily: Fonts.bold,
    color: colors.lightgrey,
  },
  blackBold: {
    fontFamily: Fonts.bold,
    color: colors.colorBlack
  },
  blackSemiBold: {
    color: colors.colorBlack,
    fontFamily: Fonts.semiBold
  },
  greySemiBold: {
    color: colors.lightgrey,
    fontFamily: Fonts.semiBold
  }

});