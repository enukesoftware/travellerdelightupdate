export default Images = {
  ic_image_bed: require('../Assets/images/bed.png'),
  ic_user_profile: require('../Assets/images/userProfile.png'),
  ic_wallet: require('../Assets/images/wallet.png'),
  ic_search: require('../Assets/images/imgsearch.png'),
  ic_image_nervous: require('../Assets/images/nervous.png'),
  ic_image_plane: require('../Assets/images/plane.png'),
  ic_image_baggage: require('../Assets/images/baggage.png'),
  ic_forgot_password: require('../Assets/images/forgotpasswordimg.png'),
  ic_image_share: require('../Assets/images/share.png'),
  ic_paid_meal: require('../Assets/images/paidmeal.png'),
  ic_image_star: require('../Assets/images/star.png'),
  ic_question_mark: require('../Assets/images/questionmark.png'),
  ic_image_dustbin: require('../Assets/images/rubbishbin.png'),
  ic_image_camera: require('../Assets/images/Camera.png'),
  ic_sorting_inc: require('../Assets/images/sorting1.png'),
  ic_circular_check: require('../Assets/images/check3.png'),
  ic_sorting_dec: require('../Assets/images/sorting2.png'),
  ///new images
  ic_image_account: require('../Assets/images/Accounts.png'),
  ic_image_back: require('../Assets/images/Back-Button.png'),
  ic_image_calender: require('../Assets/images/Calender.png'),
  ic_image_close: require('../Assets/images/Close.png'),
  ic_down_arrow: require('../Assets/images/Down-Arrow.png'),
  ic_image_facebook: require('../Assets/images/Facebook.png'),
  ic_image_filter: require('../Assets/images/Filter.png'),
  ic_flight_info: require('../Assets/images/Flight-Info.png'),
  ic_flight_search: require('../Assets/images/Flight-Search.png'),
  ic_image_food: require('../Assets/images/Food.png'),
  ic_image_google: require('../Assets/images/google.png'),
  ic_help_active: require('../Assets/images/Help-Active.png'),
  ic_help_deactive: require('../Assets/images/Help-Deactive.png'),
  ic_home_active: require('../Assets/images/Home-active.png'),
  img_home_bg: require('../Assets/images/Home-bg.png'),
  ic_home_deactive: require('../Assets/images/Home-Deactive.png'),
  ic_hotel_search: require('../Assets/images/Hotel-Search.png'),
  ic_image_bg: require('../Assets/images/Login-bg.png'),
  ic_image_logo: require('../Assets/images/Logo.png'),
  ic_menu_button: require('../Assets/images/menu-button.png'),
  ic_image_minus: require('../Assets/images/Minus.png'),
  ic_image_plus: require('../Assets/images/Plus.png'),
  ic_strip_active: require('../Assets/images/My-Trips-Active.png'),
  ic_strip_deactive: require('../Assets/images/My-Trips-Deactive.png'),
  ic_image_notifications: require('../Assets/images/Notifications.png'),
  ic_image_planeblue: require('../Assets/images/Plane-Blue.png'),
  ic_image_planewhite: require('../Assets/images/Plane-White.png'),
  ic_plus_gold: require('../Assets/images/Plus-Icon.png'),
  ic_image_seat: require('../Assets/images/Seat.png'),
  ic_switch_flight: require('../Assets/images/Switch-Flight.png'),
  ic_switch_one: require('../Assets/images/switch1.png'),
  ic_switch_two: require('../Assets/images/switch2.png'),
  ic_image_user: require('../Assets/images/User.png'),
  ic_wallet_active: require('../Assets/images/Wallet-Active.png'),
  ic_wallet_deactive: require('../Assets/images/Wallet-Deactive.png'),
  ic_image_plane1: require('../Assets/images/plane-1.jpg'),
  ic_image_plane2: require('../Assets/images/plane-2.jpg'),
  ic_submit_button: require('../Assets/images/Submit-button.png'),
  ic_time1_active: require('../Assets/images/Time1-Active.jpg'),
  ic_time1_deactive: require('../Assets/images/Time1-Deactive.jpg'),
  ic_time2_active: require('../Assets/images/Time2-Active.jpg'),
  ic_time2_deactive: require('../Assets/images/Time2-Deactive.jpg'),
  ic_time3_active: require('../Assets/images/Time3-Active.jpg'),
  ic_time3_deactive: require('../Assets/images/Time3-Deactive.jpg'),
  ic_time4_active: require('../Assets/images/Time4-Active.jpg'),
  ic_time4_deactive: require('../Assets/images/Time4-Deactive.jpg'),
  ic_image_unchecked: require('../Assets/images/unchecked.png'),
  ic_image_checked: require('../Assets/images/checked.png'),
  ic_spl_1: require('../Assets/images/img_spl_1.jpg'),
  ic_spl_2: require('../Assets/images/img_spl_2.jpg'),
  ic_spl_3: require('../Assets/images/img_spl_3.jpg'),
  ic_air_conditioner_blue: require('../Assets/images/air_conditioner_blue.png'),
  ic_air_conditioner_black: require('../Assets/images/air_conditioner_black.png'),
  ic_favorite_active: require('../Assets/images/ic_favorite_active.png'),
  ic_favorite_inactive: require('../Assets/images/ic_favorite_inactive.png'),
  ic_breakfast_blue: require('../Assets/images/ic_breakfast_blue.png'),
  breakfast_black: require('../Assets/images/breakfast_black.png'),
  parking_blue: require('../Assets/images/parking_blue.png'),
  parking_black: require('../Assets/images/parking_black.png'),
  power_backup_blue: require('../Assets/images/power_backup_blue.png'),
  power_backup_black: require('../Assets/images/power_backup_black.png'),
  room_service_blue: require('../Assets/images/room_service_blue.png'),
  room_service_black: require('../Assets/images/room_service_black.png'),
  spa_blue: require('../Assets/images/spa_blue.png'),
  spa_black: require('../Assets/images/spa_black.png'),
  star: require('../Assets/images/star.png'),
  swimming_blue: require('../Assets/images/swimming_blue.png'),
  swimming_black: require('../Assets/images/swimming_black.png'),
  ic_wifi_blue: require('../Assets/images/ic_wifi_blue.png'),
  wifi_black: require('../Assets/images/wifi_black.png'),
  ic_image_payment: require('../Assets/images/payment.png'),
  ic_radio_checked: require('../Assets/images/checked-radio.png'),
  ic_radio_unchecked: require('../Assets/images/unchecked-radio.png'),
  ic_card_blue: require('../Assets/images/card-blue.png'),
  ic_image_calender2: require('../Assets/images/calender_2.png'),
  ic_card_black: require('../Assets/images/card-black.png'),
  ic_booking_success: require('../Assets/images/booked-checked.png'),
  ic_dual_way: require('../Assets/images/ic_dual_way.png'),
  ic_image_cancellation: require('../Assets/images/cancellation.png'),
  icon_holo_star: require('../Assets/images/icon_holo_star.png'),
  icon_high: require('../Assets/images/icon_high.png'),
  icon_low: require('../Assets/images/icon_low.png'),
  icon_rating: require('../Assets/images/icon_rating.png'),
  img_address_detail: require('../Assets/images/Address-Detail.png'),
  img_primary_contact: require('../Assets/images/Primary-Contact.png'),
  img_other_info: require('../Assets/images/Other-Information.png'),
  img_settings: require('../Assets/images/Settings.png'),
  img_edit: require('../Assets/images/Edit.png'),
  ic_right_arrow_big: require('../Assets/images/long-arrow-pointing-to-the-right.png'),
  ic_left_arrow_big: require('../Assets/images/leftarrowbig.png'),
  img_placeholder: require('../Assets/images/img_placeholder.png'),
  ic_cabin_bag: require('../Assets/images/ic_cabin_bag.png'),
  ic_check_in: require('../Assets/images/ic_check_in.png'),
  ic_review_flight: require('../Assets/images/ic_review_flight.png'),
  ic_meal: require('../Assets/images/ic_meal.png'),
  ic_payment: require('../Assets/images/ic_payment.png'),
  ic_seat: require('../Assets/images/ic_seat.png'),
  ic_traveller: require('../Assets/images/ic_traveller.png'),
  ic_calander: require('../Assets/images/ic_calander.png'),
  ic_closed: require('../Assets/images/ic_closed.png'),
  ic_opened: require('../Assets/images/ic_opened.png'),
  ic_review_hotel: require('../Assets/images/ic_review_hotel.png'),
  ic_hotel_icon: require('../Assets/images/ic_hotel_icon.png'),
  ic_top_route_1: require('../Assets/images/ic_top_route_1.png'),
  ic_top_route_2: require('../Assets/images/ic_top_route_2.png'),
  ic_top_route_3: require('../Assets/images/ic_top_route_3.png'),
  ic_top_route_4: require('../Assets/images/ic_top_route_4.png'),
  ic_top_route_5: require('../Assets/images/ic_top_route_5.png'),
  ic_user_black: require('../Assets/images/ic_user_black.png'),
  ic_download: require('../Assets/images/ic_download.png'),
  ic_lock: require('../Assets/images/ic_lock.png'),
  ic_help_1: require('../Assets/images/ic_help_1.jpg'),
  ic_help_2: require('../Assets/images/ic_help_2.jpg'),
  ic_help_3: require('../Assets/images/ic_help_3.jpg'),
  ic_help_4: require('../Assets/images/ic_help_4.jpg'),
  ic_help_5: require('../Assets/images/ic_help_5.jpg'),
  ic_help_6: require('../Assets/images/ic_help_6.jpg'),
  ic_help_7: require('../Assets/images/ic_help_7.jpg'),
  ic_help_8: require('../Assets/images/ic_help_8.jpg'),
  ic_help_9: require('../Assets/images/ic_help_9.jpg'),
  ic_help_10: require('../Assets/images/ic_help_10.jpg'),
  ic_help_11: require('../Assets/images/ic_help_11.jpg'),
};
