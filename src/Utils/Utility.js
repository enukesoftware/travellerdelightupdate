import moment from "moment";
import 'moment-precise-range-plugin';
import Constants from "./Constants";
import Images from "./Images";
import { Alert } from "react-native";

export function convertMinsIntoHandM(n) {
  var num = n;
  var hours = num / 60;
  var rhours = Math.floor(hours);
  var minutes = (hours - rhours) * 60;
  var rminutes = Math.round(minutes);
  return rhours + "h " + rminutes + "m";
}

export function MinutesToHours(n) {
  var num = parseFloat(n);
  var hours = num / 60;
  var rhours = Math.floor(hours);
  var minutes = (hours - rhours) * 60;
  var rminutes = Math.round(minutes);
  return rhours + "h " + rminutes + "m";
}

export function filterAmenities(amenities, active) {

  const allAmenities = [
    { "icon": active ? Images.ic_breakfast_blue : Images.breakfast_black, "amenity": "Breakfast" },
    { "icon": active ? Images.room_service_blue : Images.room_service_black, "amenity": "Room Service" },
    { "icon": active ? Images.ic_wifi_blue : Images.wifi_black, "amenity": "Free Wifi" },
    { "icon": active ? Images.spa_blue : Images.spa_black, "amenity": "Spa" },
    { "icon": active ? Images.parking_blue : Images.parking_black, "amenity": "Free Parking" },
    { "icon": active ? Images.ic_air_conditioner_blue : Images.ic_air_conditioner_black, "amenity": "Air Conditioner" },
    { "icon": active ? Images.power_backup_blue : Images.power_backup_black, "amenity": "Power Backup" },
    { "icon": active ? Images.swimming_blue : Images.swimming_black, "amenity": "Swimming Pool" },
  ]
  let newAmenities = [];
  for (var i = 0; i < amenities.length; i++) {
    let oldAmenity = amenities[i].toLowerCase();
    for (j = 0; j < allAmenities.length; j++) {
      if (allAmenities[j].amenity.toLowerCase().includes(oldAmenity)) {
        newAmenities.push(allAmenities[j])
        break;
      }
    }
  }
  // alert(JSON.stringify(newAmenities))
  return newAmenities;
}

export function roundOffTwo(num) {
  return Math.round(num * 100) / 100
}

export function isEmailValid(text) {
  console.log(text)
  // let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/
  let reg = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
  if (!reg.test(text)) {
    console.log('Email is Not Correct')
    return false
  } else {
    console.log('Email is Correct')
    return true
  }
}

/**
 * 
 * @param {* input input key} keyword 
 * @param {* type of input key (key,value,code,shortName)} keywordType 
 * @param {* return Type of Key (key,value,code,shortName)} returnTypeKey 
 */
export function getCabinClass(keyword, keywordType, returnTypeKey) {
  try {
    const cabinClasses = Constants.CabinClasses
    const cabinClass = cabinClasses.filter((item, index) => {
      return JSON.stringify(item[keywordType]) == JSON.stringify(keyword)
    })
    //console.log("getCabinClass",JSON.stringify(cabinClass))
    //console.log("getCabinClasses",JSON.stringify(Constants.CabinClasses))
    return cabinClass[0][returnTypeKey]
  }
  catch (e) {
    return ""
  }
}

export function getAge(date1, date2) {
  try {
    let age = moment.preciseDiff(date2 ? date2 : new Date(), date1, true);
    let newAge = (age.years + (age.months / 12) + (age.days / 365))
    //console.log(newAge)
    return newAge
  }
  catch (e) {
    console.log(e)
    // alert(e)
    return 0
  }
}
export function MyAlert(title, msg, okHandler) {
  return (
    Alert.alert(
      title,
      '' + msg + '',
      [
        { text: 'Dismiss', onPress: okHandler ? okHandler : null }
      ],{cancelable:false}
    )
  )

}

export function toUpperEveryWord(str) {
  try {
    return str
      .toLowerCase()
      .split(' ')
      .map((word) => {
        console.log("First capital letter: " + word[0]);
        console.log("remain letters: " + word.substr(1));
        return word[0].toUpperCase() + word.substr(1);
      })
      .join(' ');
  }
  catch (e) {
    console.log("toUpperEveryWord", JSON.stringify(e))
    return ""
  }
}
