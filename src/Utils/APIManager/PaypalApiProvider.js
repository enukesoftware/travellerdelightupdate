/* eslint-disable prettier/prettier */
import {Environment,paypalBaseUrl} from './APIConstants'

async function callApi(urlString, header, body, methodType) {
  console.log("----------- Api request is----------- ");
  console.log("url string " + urlString);
  console.log("header " + JSON.stringify(header));
  console.log("body " + JSON.stringify(body));
  console.log("methodType " + methodType)

  return fetch(urlString, {
    method: methodType,
    headers: header,
    body: methodType == "POST" ? JSON.stringify(body) : null
  })
    .then(response => {
      console.log(response)
      console.log("-----------Response is----------- ")
      console.log(response)
      if (response.status == 200 || response.status == 201) {
        return response.json()
      } else {
        throw new Error(" status code " + response.status)
      }
    })
    .then((responseJson) => {
      return responseJson
    })
    .catch((error) => {
      throw error
    })
}

async function callApiForToken(urlString, header, body, methodType) {
  console.log("----------- Api request is----------- ");
  console.log("url string " + urlString);
  console.log("header " + JSON.stringify(header));
  console.log("body " + JSON.stringify(body));
  console.log("methodType " + methodType)

  return fetch(urlString, {
    method: methodType,
    headers: header,
    // body: methodType == "POST" ? JSON.stringify(body) : null
    body: body,
  })
    .then(response => {
      console.log("-----------Response is----------- ")
      console.log(response)
      return response.json()
      // if (response.status == 200) {
      //     return response.json()
      // } else {
      //     throw new Error(" status code " + response.status)
      // }
    })
    .then((responseJson) => {
      return responseJson
    })
    .catch((error) => {
      throw error
    })
}

/**
 * Authorization token generated from client id and client secret
 * baseEncode(client_id:client_secret)
 * 
 * ----------------------- Live --------------------------
 * Client ID : AWIEj_v5v_fy305v5x3Wf0mf2KbgZPWwnFej3alLoijuTh7rzc9A1et5fN8E-G6dkYfnYtgSX6ST6GFk
 * Client Secret : EHMnkJZB2m_N-darev86qD0FKMDd35e4HEeA98-wiAz979asgP1VfEdMmAOLTKs8zcSSANZKE2khp9pA
 * Base Encoded AuthToken : QVdJRWpfdjV2X2Z5MzA1djV4M1dmMG1mMktiZ1pQV3duRmVqM2FsTG9panVUaDdyemM5QTFldDVmTjhFLUc2ZGtZZm5ZdGdTWDZTVDZHRms6RUhNbmtKWkIybV9OLWRhcmV2ODZxRDBGS01EZDM1ZTRIRWVBOTgtd2lBejk3OWFzZ1AxVmZFZE1tQU9MVEtzOHpjU1NBTlpLRTJraHA5cEE=
 * 
 * ---------------------- Sandbox -------------------------
 * Client ID : 
 * Client Secret : 
 * Base Encoded AuthToken = QVR1ekF4SVJ4eEMtWDBUampyVDVnZTIwTUlwVHU3eFR4bWlhOW1QUEc4NE1HMFFZWkxVOVpCU0NNdnBoX0NvMW5zR2d1SHdiQzVBTE91bEQ6RU5BUHUzQWEwOWR5Q29Rd2hzMDRzam1VM2VxdXNZUUxRM2ctM0ZCZ05NTUEtX3R6OHRrRDUxWTE3V210VzNXMEI5ZVZ6MURjWUFqamgyN3Q=
 */

async function fetchToken(urlString, body, methodType) {
  const liveToken = "Basic QVdJRWpfdjV2X2Z5MzA1djV4M1dmMG1mMktiZ1pQV3duRmVqM2FsTG9panVUaDdyemM5QTFldDVmTjhFLUc2ZGtZZm5ZdGdTWDZTVDZHRms6RUhNbmtKWkIybV9OLWRhcmV2ODZxRDBGS01EZDM1ZTRIRWVBOTgtd2lBejk3OWFzZ1AxVmZFZE1tQU9MVEtzOHpjU1NBTlpLRTJraHA5cEE="
  const stagingToken = 'Basic QVR1ekF4SVJ4eEMtWDBUampyVDVnZTIwTUlwVHU3eFR4bWlhOW1QUEc4NE1HMFFZWkxVOVpCU0NNdnBoX0NvMW5zR2d1SHdiQzVBTE91bEQ6RU5BUHUzQWEwOWR5Q29Rd2hzMDRzam1VM2VxdXNZUUxRM2ctM0ZCZ05NTUEtX3R6OHRrRDUxWTE3V210VzNXMEI5ZVZ6MURjWUFqamgyN3Q='
  let header = {
    "Content-Type": "application/x-www-form-urlencoded",
     "Authorization": Environment==='Production'?liveToken:stagingToken,
    // "Authorization": 'Basic =',
    // token: Constants.dailyToken,
    // agency_Id: 0
  }
  return callApiForToken(urlString, header, body, methodType)

}

async function fetchApprovalUrl(urlString, body, accessToken, methodType) {
  let header = {
    "Content-Type": "application/json",
    "Authorization": 'Bearer ' + accessToken,
    // token: Constants.dailyToken,
    // agency_Id: 0
  }
  return callApi(urlString, header, body, methodType)

}



export async function getPaypalTokenApi() {
  // let param = new FormData();
  // param.append("grant_type", 'client_credentials');
  // return fetchToken('https://api.sandbox.paypal.com/v1/oauth2/token', 'grant_type=client_credentials', "POST")
  return fetchToken(paypalBaseUrl+'/v1/oauth2/token', 'grant_type=client_credentials', "POST")
}

export async function getApprovalUrlApi(payload) {
  let param = payload.param
  let accessToken = payload.accessToken
  // let param = new FormData();
  // param.append("grant_type", 'client_credentials');
  // return fetchApprovalUrl('https://api.sandbox.paypal.com/v1/payments/payment', param, accessToken, "POST")
  return fetchApprovalUrl(paypalBaseUrl+'/v1/payments/payment', param, accessToken, "POST")
}

export async function getPaymentExecuteApi(payerId, paymentId, accessToken) {
  // let param = new FormData();
  // param.append("grant_type", 'client_credentials');
  // return fetchApprovalUrl('https://api.sandbox.paypal.com/v1/payments/payment/' + paymentId + '/execute', { payer_id: payerId }, accessToken, "POST")
  return fetchApprovalUrl(paypalBaseUrl+'/v1/payments/payment/' + paymentId + '/execute', { payer_id: payerId }, accessToken, "POST")
}