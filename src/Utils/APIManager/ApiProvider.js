import AsyncStorage from '@react-native-community/async-storage';
import moment from "moment";
import { dailyTokenAction } from './../../Redux/Actions';
import { store } from './../../Redux/Store/Store';
import APIConstants, { Environment } from "./APIConstants";
import axios from 'axios'
import { DeviceEventEmitter } from 'react-native';
import StringConstants from '../StringConstants'
import { MyAlert } from '../Utility';



async function callApi2(urlString, header, body, methodType, isMultipart) {
  console.log("----------- Api request is----------- ");
  console.log("url string " + urlString);
  console.log("header " + JSON.stringify(header));
  console.log("body " + JSON.stringify(body));
  console.log("methodType " + methodType)


  return fetch(urlString, {
    method: methodType,
    headers: header,
    body: isMultipart ? body : methodType == "POST" ? JSON.stringify(body) : null
  })
    .then((response) => {
      console.log("-----------Response is----------- ")
      console.log(response)
      console.log(JSON.stringify(response))
      // if (response.status == 200) {
      return response.json()
      // } else {
      //   throw new Error(" status code " + response.status)
      // }
    }
    )
    .then((response) => {
      return response
    })
    .catch((error) => {
      //throw error
      throw new Error("Result not found")
    })
}

async function callApi(urlString, header, body, methodType, isMultipart) {
  console.log("-----------AXIOS  Api request is----------- ");
  console.log("url string " + urlString);
  console.log("header " + JSON.stringify(header));
  console.log("body " + JSON.stringify(body));
  console.log("methodType " + methodType)

  return axios({
    method: methodType, //you can set what request you want to be
    url: urlString,
    data: isMultipart ? body : methodType == "POST" ? JSON.stringify(body) : null,
    headers: header
  }).then(res => {
    console.log("-----------AXIOS  Api Response is----------- ");
    console.log("url string " + urlString);
    console.log("header " + JSON.stringify(header));
    console.log("body " + JSON.stringify(body));
    console.log("methodType " + methodType)
    console.log(JSON.stringify(res.data));
    if (JSON.stringify(res.data).startsWith("<") || JSON.stringify(res.data).startsWith("\"<")) {
      DeviceEventEmitter.emit(StringConstants.STOP_LOADER_EVENT);
      setTimeout(() => {
        MyAlert("Error", "A webpage is returned instead of a response")
      }, 500);
      
    }
    return res.data
  }
    // , error => {
    //   console.log("-----------AXIOS  Api Error is----------- ")
    //   console.log(JSON.stringify(error))

    // }
  )
    .catch(e => {
      console.log("-----------AXIOS  Api catch is-----------")
      console.log(e)
      if (e.response && e.response.data) {
        console.log(JSON.stringify(e.response.data))
        if (JSON.stringify(e.response.data).startsWith("<") || JSON.stringify(e.response.data).startsWith("\"<")) {
          DeviceEventEmitter.emit(StringConstants.STOP_LOADER_EVENT);
          setTimeout(() => {
            MyAlert("Error", "A webpage is returned instead of a response")
          }, 500);
        }
        return e.response.data
      }
      else { throw new Error("Request Failed") }
    })
}

async function fetchApiData(urlString, body, methodType, isMultipart) {

  let userToken = store.getState().userTokenReducer;
  let dailyToken = store.getState().dailyTokenReducer;

  try {
    let token = dailyToken ? dailyToken : 'response'     //await getDailyToken();
    if (!dailyToken) { dailyTokenAction(token) }

    // saveToken(token)
    let header = {
      "Accept": "application/json",
      "Content-Type": "application/json",
      "Authorization": 'Bearer ' + userToken,
      "token": token,
      "source": "mobile",
      "origin": APIConstants.origin,
      "agency_id":APIConstants.agencyID
    }
    if (urlString.includes('node-api')) {
      console.log("HEADER_CHANGE")
      delete header['Authorization'];
    }
    
    if (urlString.includes('register')) {
      console.log("HEADER_CHANGE")
      delete header['Authorization'];
      delete header['token'];
    }
    if (isMultipart) {
      header['Content-Type'] = "multipart/form-data";
    }

    
    return callApi(urlString, header, body, methodType, isMultipart)
  } catch (error) {
    throw new Error(error)
  }
}

async function getDailyToken() {

  let header = {
    Accept: "application/json",
    "Content-Type": "application/json"
  };

  return fetch(
    APIConstants.getToken, {
    method: "GET",
    headers: header,
    body: null
  })
    .then((response) => response.json())
    .then(responseJson => {
      console.log("TOKEN:")
      let token = responseJson.token
      console.log("got Token " + token);
      let now = new Date();
      let dict = {
        date: moment(now).format("YY-MM-DD"),
        token: token
      };
      //Constants.dailyToken = token;
      store.dispatch(dailyTokenAction(token))
      // console.log("saving dictionary is  " + JSON.stringify(dict));
      //saveJsonString("flightToken", JSON.stringify(dict));
      return token
    })
    .catch((error) => {
      console.log("ERROR:" + JSON.stringify(error))
      throw new Error(error)
    })

}

export function getDataFromAsync(key, completion) {
  AsyncStorage.getItem(key, (error, result) => {
    if (error == null) {
      console.log("The result is " + result);
      completion(result);
    } else {
      completion(null);
    }
  });
}

export function removeItemfromAsync(key) {
  AsyncStorage.removeItem(key, error => {
    if (error) {
      console.log("error clearing " + key)
    } else {
      console.log(key + " item cleared")
    }
  }
  )
}

export function saveJsonString(key, jsonString) {
  AsyncStorage.setItem(key, jsonString, error => {
    if (error) {
      // alert((result == null) ? 'found nil' : 'done')
      console.log("error storing " + Key + error);
    } else {
      console.log(key + " stored successfully");
    }
  });
}

export function getErrorFromJson(error, isCatchSection) {
  if (isCatchSection) {
    return error.message
  }
  return error.code.description
}

export async function getFlightList(body) {

  return fetchApiData(APIConstants.getFlightList, body, "POST");

}

export async function loginUser(body) {
  console.log("----------Login User Api Call ------------------")
  return fetchApiData(APIConstants.login, body, "POST")
}

export async function logoutUser() {
  console.log("----------Logout User Api Call ------------------")
  return fetchApiData(APIConstants.logout, null, "GET")
}

// ---------------- Register User API ----------------
export async function registerUserApi(body) {
  console.log("----------Register User Api Call ------------------")
  return fetchApiData(APIConstants.register, body, "POST")
}

// ---------------- Forgot Password API ----------------
export async function forgotPasswordApi(body) {
  console.log("----------Forgot Password Api Call ------------------")
  return fetchApiData(APIConstants.forgotPassword, body, "POST")
}

// ---------------- Get Airport List API ----------------

export async function getAirPortList() {
  return fetchApiData(APIConstants.getAirports, null, "GET")
}

//MARK: -------------- GetHotelBookingList -----------
export async function getHotelBookedListApi() {
  return fetchApiData(APIConstants.hotelBookedList, null, "GET")

}

export async function getFlightRuleApi(param) {
  return fetchApiData(APIConstants.getFlightRule, param, "POST");
}

export async function confirmQuoteApi(param) {
  return fetchApiData(APIConstants.confirmQuote, param, "POST")
}

export async function getHotelListApi(param) {
  return fetchApiData(APIConstants.getHotelsList, param, "POST")
}

export async function getHotelDetailsApi(param) {
  return fetchApiData(APIConstants.getHotelDetail, param, "POST")

}

export function bookFlightApi(param) {
  return fetchApiData(APIConstants.bookFlight, param, "POST")
}

export function bookHotelApi(param) {
  return fetchApiData(APIConstants.bookHotel, param, "POST")
}

export function cancelHotelBookingApi(param) {
  return fetchApiData(APIConstants.cancelHotelBooking, param, "POST")
}

export function checkCancelStatusHotel(param) {
  return fetchApiData(APIConstants.checkCancelStatusHotel, param, "POST")
}

export function roomBlockApi(param) {
  return fetchApiData(APIConstants.blockRoom, param, "POST")
}

export function getCitiesApi(searchStr) {
  return fetchApiData(APIConstants.getCities + searchStr, null, "GET");
}


export function blockRoomApi(params) {
  return fetchApiData(APIConstants.blockRoom, params, "POST");
}

export function updateUserProfileApi(params) {
  return fetchApiData(APIConstants.updateProfile, params, "POST", true);
}

//MARK: -------------- Get list of booked flights -----------
export async function getFlightsBookedListApi() {
  return fetchApiData(APIConstants.flightBookedList, null, "GET");

}

export async function facebookLoginApi(body) {
  return fetchApiData(APIConstants.fbLogin, body, "POST")
}

export async function googleLoginApi(body) {
  return fetchApiData(APIConstants.googleLogin, body, "POST")
}

//MARK: -------------- Get Nationality-----------
export async function getNationalityApi() {
  return fetchApiData(APIConstants.nationality, null, "GET");
}

export async function getPageSettingApi() {
  return fetchApiData(APIConstants.pageSetting, null, "GET",);
}

// Initialize Payment Api

export async function capturePaymentApi(body) {
  return fetchApiData(APIConstants.capturePayment, body, "POST")
}

// Booked Hotel Detail Page Api

export async function getHotelBookingDetailApi(id) {
  return fetchApiData(APIConstants.hotelBookedList + "/" + id, null, "GET");
}

// Booked Flight Detail Page Api

export async function getFlightBookingDetailApi(id) {
  return fetchApiData(APIConstants.flightBookedList + "/" + id, null, "GET");
}

export async function changePasswordApi(body) {
  return fetchApiData(APIConstants.changePassword, body, "POST")
}

