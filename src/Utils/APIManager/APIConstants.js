
//export const baseUrl = 'http://travel.yiipro.com/api/';
//for development mode

// TravellerDelight Production 0.5 beta 13 March.apk
// TravellerDelight PlayStore 0.5 beta 13 March.apk
// TravellerDelight Staging 0.5 beta 13 March.apk

// export const baseUrl = 'http://travel.yiipro.com/node-api/';
// export const Environment = "Production"
export const Environment = "PlayStore"
// export const Environment = "Staging"

const serverUrl = "http://travellerdelight.com/node-api/v1/";
const baseUrl = Environment === 'Production' ? "https://www.spreadingtrips.com" : Environment === 'PlayStore' ? 'http://demo.travellerdelight.com' : "http://travel.yiipro.com"
const baseUrl2 = "https://www.spreadingtrips.com"

export const paypalBaseUrl = Environment === 'Production' ? "https://api.paypal.com" : "https://api.sandbox.paypal.com"
const nodeApi = "/node-api/v1/"
const api = "/api/"
export const imageBaseUrl = baseUrl + '/'
const APIConstants = {
  agencyID: Environment === 'Production' ? 6 : Environment === 'PlayStore' ? 20 : 8,
  origin: Environment === 'Production' ? baseUrl : Environment === 'PlayStore' ? baseUrl : "https://www.travellerdelight.com",


  // ------------   Flight Urls   ------------------

  // getFlightList: baseUrl2 + nodeApi,
  // getFlightRule: baseUrl2 + nodeApi + 'flight/rules',
  // getFlightsCalender: baseUrl2 + nodeApi + 'flight/calander',
  // bookFlight: baseUrl2 + nodeApi + 'flight/book',
  // confirmQuote: baseUrl2 + nodeApi + 'flight/quote',
  // flightBookingDetails: baseUrl2 + nodeApi + 'air-confirm/',



  // ------------   Hotel Urls   ------------------

  // getHotelsList: baseUrl2 + nodeApi + 'hotels',
  // blockRoom: baseUrl2 + nodeApi + 'hotels/room-block',
  // hotelBookingDetails: baseUrl2 + nodeApi + 'hotel-confirm/',
  // getHotelDetail: baseUrl2 + nodeApi + 'hotel/detail',
  // bookHotel: baseUrl2 + nodeApi + 'hotels/book',
  // cancelHotelBooking : baseUrl2 + nodeApi +'hotels/cancel',
  // checkCancelStatus : baseUrl2 + nodeApi +'hotels/cancel-status',


  // ------------   Other Urls   ------------------

  // getToken: baseUrl2 + nodeApi,
  // capturePayment : baseUrl2 + nodeApi +'/mobile/capture-payment',





  // ------------   Flight Urls   ------------------

  getFlightList: baseUrl + nodeApi,
  getFlightRule: baseUrl + nodeApi + 'flight/rules',
  getFlightsCalender: baseUrl + nodeApi + 'flight/calander',
  bookFlight: baseUrl + nodeApi + 'flight/book',
  confirmQuote: baseUrl + nodeApi + 'flight/quote',
  flightBookingDetails: baseUrl + nodeApi + 'air-confirm/',


  // ------------   Hotel Urls   ------------------

  getHotelsList: baseUrl + nodeApi + 'hotels',
  blockRoom: baseUrl + nodeApi + 'hotels/room-block',
  hotelBookingDetails: baseUrl + nodeApi + 'hotel-confirm/',
  getHotelDetail: baseUrl + nodeApi + 'hotel/detail',
  bookHotel: baseUrl + nodeApi + 'hotels/book',
  cancelHotelBooking: baseUrl + nodeApi + 'hotels/cancel',
  checkCancelStatusHotel: baseUrl + nodeApi + 'hotels/cancel-status',


  // ------------   Other Urls   ------------------

  getToken: baseUrl + nodeApi,
  capturePayment: baseUrl + nodeApi + 'mobile/capture-payment',



  getAirports: baseUrl + api + 'airports',
  getCities: baseUrl + api + 'hotel-suggestion?keyword=',
  login: baseUrl + api + 'login',
  changePassword: baseUrl + api + 'change-password',
  register: baseUrl + api + 'register',
  forgotPassword: baseUrl + api + 'forgotpassword',
  updateProfile: baseUrl + api + 'profile/update',
  logout: baseUrl + api + 'logout',
  hotelBookedList: baseUrl + api + 'hotel-ticket-list',
  flightBookedList: baseUrl + api + 'air-ticket-list',
  fbLogin: baseUrl + api + 'fbLogin',
  googleLogin: baseUrl + api + 'login/google',
  pageSetting: baseUrl + api + 'page-setting',
  nationality: baseUrl + '/tele-code',
  downloadFlightTicket: baseUrl + '/download-confirm/',
  downloadHotelTicket: baseUrl + '/download-hotel-confirm/'

}


export default APIConstants;

