import { GoogleSignin, statusCodes } from 'react-native-google-signin';
import { AccessToken, LoginManager } from 'react-native-fbsdk';
import { AsyncStorage } from 'react-native';
// import Constants from '../constants/constants';
// import firebase from 'react-native-firebase'

export const fbLogin = async () => {
  console.log("login in facebook");
  return LoginManager.logInWithPermissions(['public_profile', 'email', 'user_friends']).then(
    function (result) {
      console.log("RESULT:" + JSON.stringify(result))
      if (result.isCancelled) {
        alert('Login was cancelled');
      } else {
        return AccessToken.getCurrentAccessToken().then(
          (data) => {
            AsyncStorage.setItem('token', data.accessToken);
            return data;
          })
      }
    },
    function (error) {
      alert('Login failed with error: ' + error);
    }
  );
}

export const fbLogout = async () => {
  console.log("logout in facebook");
  try {
    LoginManager.logOut();
  }
  catch (e) {
    console.log("fbLogoutException", JSON.stringify(e))
  }
}

export const googleLogin = async () => {
  try {
    console.log("Login in google");
    // add any configuration settings here:
    await GoogleSignin.configure({
      // webClientId: "343397407136-k3njlv92teirrr5bvtamjcjfcrb2ggag.apps.googleusercontent.com",
      webClientId: '343397407136-suitfl0jn472dc9sg1nvdrk1b2uf73tj.apps.googleusercontent.com',
      offlineAccess: true,
    });
    console.log("configured");
    const isGoogleSignedIn = await GoogleSignin.isSignedIn();
    console.log("signed in google checked");
    if (isGoogleSignedIn) {
      try {
        console.log("signed in google going to log out");
        await GoogleSignin.revokeAccess();
        await GoogleSignin.signOut();
        console.log("google log out");
      } catch (error) {
        console.log("ERROR:" + JSON.stringify(error));
      }
    }

    await GoogleSignin.hasPlayServices();

    console.log("playservices checked");

    const data = await GoogleSignin.signIn();
    const token = await GoogleSignin.getTokens()

    console.log("google signed in");
    console.log("LOGIN_DATA" + JSON.stringify(data));
    console.log("TOKEN_DATA" + JSON.stringify(token));
    // AsyncStorage.setItem('token', data.idToken);

    return token;
    // create a new firebase credential with the token
    // const credential = firebase.auth.GoogleAuthProvider.credential(data.idToken, data.accessToken);
    // console.log("google got the credential");



    // login with credential  from firebase
    // const firebaseUserCredential = await firebase.auth().signInWithCredential(credential);

    // console.warn(JSON.stringify(firebaseUserCredential.user.toJSON()));
  } catch (error) {
    console.log("error in google" + JSON.stringify(error));
    if (error.code === statusCodes.SIGN_IN_CANCELLED) {
      // sign in was cancelled
      alert('cancelled');
    } else if (error.code === statusCodes.IN_PROGRESS) {
      // operation in progress already
      alert('in progress');
    } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
      alert('play services not available or outdated');
    } else {
      alert('Something went wrong', error.toString());
    }
  }
}

export const googleLogout = async () => {
  try {
    console.log("Logout in google");
    // add any configuration settings here:
    await GoogleSignin.configure({
      // webClientId: "343397407136-k3njlv92teirrr5bvtamjcjfcrb2ggag.apps.googleusercontent.com",
      webClientId: '343397407136-suitfl0jn472dc9sg1nvdrk1b2uf73tj.apps.googleusercontent.com',
      offlineAccess: true,
    });
    console.log("configured");
    const isGoogleSignedIn = await GoogleSignin.isSignedIn();
    console.log("signed in google checked");
    if (isGoogleSignedIn) {
      try {
        console.log("signed in google going to log out");
        await GoogleSignin.revokeAccess();
        await GoogleSignin.signOut();
        console.log("google log out");
      } catch (error) {
        console.log("ERROR:" + JSON.stringify(error));
      }
    }
    else return isGoogleSignedIn

    return await GoogleSignin.isSignedIn();

  } catch (error) {
    console.log("error in google" + JSON.stringify(error));
    if (error.code === statusCodes.SIGN_IN_CANCELLED) {
      // sign in was cancelled
      alert('cancelled');
    } else if (error.code === statusCodes.IN_PROGRESS) {
      // operation in progress already
      alert('in progress');
    } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
      alert('play services not available or outdated');
    } else {
      alert('Something went wrong', error.toString());
    }
  }
}